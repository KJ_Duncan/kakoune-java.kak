#### kakoune-java.kak


Java 18; operators, keywords, module system, throwables, constants, enums, static word completion, and import statement completers.  

JavaPolicy file syntax support.  

```text
provide-module -override java
---------------^^^^^^^^^
```

_'allow the module to replace an existing one with the same name.'_  
@see [Module commands](https://github.com/mawww/kakoune/blob/master/doc/pages/commands.asciidoc#module-commands)  
  
For the original [kakoune/rc/filetype/java.kak](https://github.com/mawww/kakoune/blob/master/rc/filetype/java.kak)  

```shell
# Built from and runs on kakoune commit:
# commit 021da117cf90bf25b65e3344fa8e43ab4262b714
# Date:   Sat Aug 13 18:53:27 2022 +0100

# kakoune build environment
export CXX="g++-12"
```


----


#### Install


```shell
$ mv rc/*.kak $HOME/.config/kak/autoload/
# or
$ mv rc/*.kak $XDG_CONFIG_HOME/kak/autoload/
```

[andreyorst-plug.kak](https://github.com/andreyorst/plug.kak)  
```
plug "KJ_Duncan/kakoune-java.kak" domain "bitbucket.org" config %{
  # to auto-load javaPolicy.kak
  require-module javaPolicy
}
```


----


#### Todo


- [ ] [kak-spec.kak](https://github.com/jbomanson/kak-spec.kak) unit tests.
- [x] java se 16 Enum completion candidates
- [x] error running hook InsertChar( )/java-insert: 1:1: 'java-insert-on-new-line': no such command, duplicates the `////` comment line if shares same name with `rc/filetype/java.kak`.
- [x] Kakoune `%opt{completers}` overflows on max number of fields. Post issue on [discuss kakoune](https://discuss.kakoune.com/).
- [x] rc/JavaPolicy into a user instantiated module to avoid conflicts with other `*.policy` files.
- [x] rc/javaPolicy.kak minimise calls in the `%sh{}` block.
- [x] import completer regex dot operator widens match `import<space>javalangcharacter<tab>` should expand to `import java.lang.Character;` instead `import java.lang.Character.UnicodeScript;`.
- [x] [Constant Field Values](https://docs.oracle.com/en/java/javase/15/docs/api/constant-values.html).
- [x] [Hierarchy For All Packages](https://docs.oracle.com/en/java/javase/15/docs/api/overview-tree.html) for import statements.


----


#### Usage Completers


```java
import<space>object<tab>
import java.lang.Object;

import<space>basicfileattributeview<tab>
import java.nio.file.attribute.BasicFileAttributeView;
```
  
A Java SE _'fully qualified import declaration'_ will append its _'public static final CONSTANT fields'_ and _'Enum Constants'_ onto kakoune's static word list to provide the user with completion candidates. Subsequent removal of the import statement and re-engaging insert mode clears the static word list off those constant fields.  
  
```java
import java.lang.Character;

BYTES
CURRENCY_SYMBOL
[...]

import java.nio.file.AccessMode;

EXECUTE
READ
WRITE

```
  
2021-04-25 SOLVED: Kakoune completion overflows on a max number of fields. Read the `rc/javaInheritanceHierarchy.kak` lessons learnt document notes for explanation.  
```
# overflow = javaClassHierarchy + javaInterfaceHierarchy

:echo %opt{completers}
:echo %sh{ printf %s "$kak_opt_java_class_hierarchy_completions" | awk '{print NF}' }
:echo %sh{ printf %s "$kak_opt_java_interface_hierarchy_completions" | awk '{print NF}' }
:echo %sh{ printf %s "$kak_opt_java_annotation_enum_hierarchy_completions" | awk '{print NF}' }
```


----


#### Usage JavaPolicy


Manual instantiation is required to load the `rc/javaPolicy.kak`. User activation is done via the kakoune editor command line:  

```
:require-module javaPolicy<ret>
```
  
This activates the module and avoids implicit conflicts with other `*.policy` files in the users vocabulary. As a hook is run on BufCreate load the _'javaPolicy'_ module prior to opening a kakoune buffer with the intended `*.policy` file. All subsequent buffers matched on the file-type will reflect the new _'JavaPolicy'_ state.  


----


#### Out-Of-The-Box


Lenormf's kakoune welcome mat for users.  
```text
# Allow cycling to the next/previous candidate with <tab> and <s-tab> when completing a word

hook global InsertCompletionShow .* %{
  try %{
    execute-keys -draft 'h<a-K>\h<ret>'
    map window insert <tab> <c-n>
    map window insert <s-tab> <c-p>
  }
}

hook global InsertCompletionHide .* %{
  unmap window insert <tab> <c-n>
  unmap window insert <s-tab> <c-p>
}

# if trailing whitespaces is an issue do
# not where whitespaces matter e.g. markdown
# <https://discuss.kakoune.com/t/kakoune-hook-negative-selector/1867/6>
hook global BufWritePre '.*\.java$' %{ try %{ execute-keys -draft '%s\h+$<ret>d' } }
```

-  Lenormand, F 2018, out-of-the-box, oob.kak, viewed 28 February 2021, https://github.com/lenormf/out-of-the-box/blob/master/oob.kak#L6
-  Lenormand, F 2020, kakoune-extra, viewed 01 March 2021, https://github.com/lenormf/kakoune-extra


----


#### Kak-Spec


_'kak-spec is a unit test framework for Kakoune scripts and plugins'_ [kak-spec](https://github.com/jbomanson/kak-spec.kak).  
```
$ cd kakoune-java.kak/spec/
$ kak-spec regex-javax-swing.kak-spec

# kak-spec

.....

## Summary

- Finished in 292.32 milliseconds
- 5 examples, 0 failures, 0 errors
```


----


#### Snippets


_'I have disabled these plugins as is currently appending regex to keywords, will look at reasons another time'_  
  
Click the links and follow the instructions:  

- [occivink/kakoune-snippets](https://github.com/occivink/kakoune-snippets)
- [andreyorst/kakoune-snippets-collection](https://github.com/andreyorst/kakoune-snippet-collection)


#### References


-  Bomanson, J 2021, kak-spec.kak, discuss.kakoune, viewed 25 February 2021, https://discuss.kakoune.com/t/kak-spec-unit-testing-for-kakoune-scripts/1585/5?u=duncan
-  Bomanson, J 2021, kak-spec.kak, github.com, viewed 25 February 2021, https://github.com/jbomanson/kak-spec.kak
-  Ftonneau 2020, Branching on a boolean option – without calling the shell, viewed 11 February 2021, https://discuss.kakoune.com/t/branching-on-a-boolean-option-without-calling-the-shell/929
-  IEEE and The Open Group 2018, shift - shift positional parameters, viewed 18 April 2021, https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_26
-  Kakoune 2020, _'5. Configuration & Autoloading'_, viewed 04 December 2020, https://github.com/mawww/kakoune#5-configuration--autoloading
-  Kakoune 2021, c-family.kak, viewed 29 January 2021, https://github.com/mawww/kakoune/blob/master/rc/filetype/c-family.kak
-  Kakoune 2021, clojure.kak, viewed 23 February 2021, https://github.com/mawww/kakoune/blob/master/rc/filetype/clojure.kak#L140
-  Melton, R & Orst, A 2020, _'plug.kak'_, viewed 07 December 2020, https://github.com/robertmeta/plug.kak
-  Oracle 2020, 3.9 Keywords, Chapter 3. Lexical Structure, Java Language Specification, Java SE 15, viewed 11 December 2020, https://docs.oracle.com/javase/specs/jls/se15/html/jls-3.html#jls-3.9
-  Oracle 2020, 6.7. Fully Qualified Names and Canonical Names, Chapter 6. Names, The Java Language Specification, Java SE 15 Edition, viewed 18 February, https://docs.oracle.com/javase/specs/jls/se15/html/jls-6.html#jls-6.7
-  Oracle 2020, FunctionalInterface, java.util.function, viewed 08 December 2020, https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/util/function/package-summary.html
-  Oracle 2020, Java Language Updates for Java SE 15, Java Language Updates, viewed 06 December 2020, https://docs.oracle.com/en/java/javase/15/language/java-language-changes.html
-  Oracle 2020, Java® Platform, Standard Edition & Java Development Kit, Version 15 API Specification, viewed 11 December 2020, https://docs.oracle.com/en/java/javase/15/docs/api/index.html
-  Oracle 2020, Permissions in the JDK, Security Developer's Guide, Java SE 15, viewed 02 December 2020, https://docs.oracle.com/en/java/javase/15/security/permissions-jdk1.html
-  Oracle 2020, The Java Language Specification, Java SE 15 Edition, viewed 11 December 2020, https://docs.oracle.com/javase/specs/jls/se15/html/index.html
-  Oracle 2021, Constant Field Values, Java SE 15, viewed 28 January 2021, https://docs.oracle.com/en/java/javase/15/docs/api/constant-values.html
-  Oracle 2021, Hierarchy For All Packages, Java SE 15, viewed 29 January 2021, https://docs.oracle.com/en/java/javase/15/docs/api/overview-tree.html
-  Oracle 2021, JSR 391: Java SE 16, JSR 391: Java SE 16 Specification, viewed 14 April 2021, https://cr.openjdk.java.net/~iris/se/16/latestSpec/apidiffs/overview-summary.html
-  Prion 2021, Unless this issue has been fixed, Discuss Kakoune, viewed 27 February 2021, https://discuss.kakoune.com/t/kak-opt-static-words-push-and-pop-operations-for-a-large-number-of-fields/1582/2
-  Screwtape 2020, Intro to Kakoune completers, Discuss Kakoune, viewed 29 January 2021, https://discuss.kakoune.com/t/intro-to-kakoune-completers/1198
-  Screwtape 2020, Intro to Kakoune completions, Screwtape's Notepad, viewed 29 January 2021, https://zork.net/~st/jottings/Intro_to_Kakoune_completions.html
-  Taupiqueur 2020, plug.kak, alexherbo2/plug.kak/, viewed 18 April 2021, https://github.com/alexherbo2/plug.kak/blob/master/rc/plug.kak#L108


----


#### Kakoune General Information


Work done? Have some fun. Share any improvements, ideas or thoughts with the community on [discuss.kakoune](https://discuss.kakoune.com/).  
  
Kakoune is an open source modal editor. The source code lives on github [mawww/kakoune](https://github.com/mawww/kakoune#-kakoune--).  
A discussion on the repository structure of _community plugins_ for Kakoune can be reviewed here: [Standardi\(s|z\)ation of plugin file-structure layout #2402](https://github.com/mawww/kakoune/issues/2402).  
Read the Kakoune wiki for information on install options via a [Plugin Manager](https://github.com/mawww/kakoune/wiki/Plugin-Managers).  
  
Thank you to the Kakoune community 230+ contributors for a great modal editor in the terminal. I use it daily for the keyboard shortcuts.  


----


That's it for the readme, anything else you may need to know just pick up a book and read it [_Polar Bookshelf_](https://getpolarized.io/). Thanks all. Bye.  
