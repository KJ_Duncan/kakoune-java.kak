# References:
# ----------
# Oracle 2021, Constant Field Values, Java SE 15, viewed 28 January 2021, https://docs.oracle.com/en/java/javase/15/docs/api/constant-values.html
# Oracle 2020, 6.7. Fully Qualified Names and Canonical Names, Chapter 6. Names, The Java Language Specification, Java SE 15 Edition, viewed 18 February, https://docs.oracle.com/javase/specs/jls/se15/html/jls-6.html#jls-6.7
#
# As at 18 Feb 2021,
#
# USAGE:
#       import java.lang.Character;
#
#       DIRECTIONALITY_PARAGRAPH_SEPARATOR
#
# A Java SE 'fully qualified import declaration' will append its 'public static
# final CONSTANT fields' onto kakoune's static word list to provide the user with
# completion candidates. Subsequent removal of the import statement and
# re-engaging insert mode clears the static word list off those constant fields.
#
# The name-space responsively updates on my daily workhorse a 2011 laptop.
#
# -------------------------------------------------------------------------------------------------- #
# state: nop  -> nay   -> nop
#        true -> false -> true
#
# java-constants-add:
#
#   if (regex pattern and not nay) add static words then set nop to nay
#   else continue
#
# java-constants-remove:
#
#   if (not nop and not regex pattern) remove static words then set nay to nop
#   else continue
#
# Helper functions for list size, addition (push) and (pop) subtraction confirmation.
#  :echo %sh{ printf %s "$kak_opt_static_words" | awk '{print NF}' }
#  :echo %sh{ printf %s "$kak_opt_org_xml_sax_helpers_namespacesupport" | awk '{print NF}' }
#
# -------------------------------------------------------------------------------------------------- #
# File type detection occurs in rc/java.kak via:
#   hook global BufCreate .*\.java %{ set-option buffer filetype java }
#
# Which initialises the below hook and subsequent
# require-module call to javaConstants.
#
hook global WinSetOption filetype=java %{
  require-module javaConstants
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str com_sun_java_accessibility_util_eventid_active "nop"
  declare-option str com_sun_jdi_classtype_active "nop"
  declare-option str com_sun_jdi_objectreference_active "nop"
  declare-option str com_sun_jdi_threadreference_active "nop"
  declare-option str com_sun_jdi_virtualmachine_active "nop"
  declare-option str com_sun_jdi_request_eventrequest_active "nop"
  declare-option str com_sun_jdi_request_steprequest_active "nop"
  declare-option str com_sun_management_garbagecollectionnotificationinfo_active "nop"
  declare-option str com_sun_security_auth_module_jndiloginmodule_active "nop"
  declare-option str com_sun_tools_jconsole_jconsolecontext_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_awt_adjustable_active "nop"
  declare-option str java_awt_alphacomposite_active "nop"
  declare-option str java_awt_awtevent_active "nop"
  declare-option str java_awt_basicstroke_active "nop"
  declare-option str java_awt_borderlayout_active "nop"
  declare-option str java_awt_component_active "nop"
  declare-option str java_awt_cursor_active "nop"
  declare-option str java_awt_displaymode_active "nop"
  declare-option str java_awt_event_active "nop"
  declare-option str java_awt_filedialog_active "nop"
  declare-option str java_awt_flowlayout_active "nop"
  declare-option str java_awt_font_active "nop"
  declare-option str java_awt_frame_active "nop"
  declare-option str java_awt_graphicsconfigtemplate_active "nop"
  declare-option str java_awt_graphicsdevice_active "nop"
  declare-option str java_awt_gridbagconstraints_active "nop"
  declare-option str java_awt_gridbaglayout_active "nop"
  declare-option str java_awt_image_active "nop"
  declare-option str java_awt_keyboardfocusmanager_active "nop"
  declare-option str java_awt_label_active "nop"
  declare-option str java_awt_mediatracker_active "nop"
  declare-option str java_awt_scrollbar_active "nop"
  declare-option str java_awt_scrollpane_active "nop"
  declare-option str java_awt_systemcolor_active "nop"
  declare-option str java_awt_textarea_active "nop"
  declare-option str java_awt_transparency_active "nop"
  declare-option str java_awt_color_colorspace_active "nop"
  declare-option str java_awt_color_icc_profile_active "nop"
  declare-option str java_awt_color_icc_profilergb_active "nop"
  declare-option str java_awt_datatransfer_dataflavor_active "nop"
  declare-option str java_awt_dnd_dndconstants_active "nop"
  declare-option str java_awt_dnd_dragsourcecontext_active "nop"
  declare-option str java_awt_event_actionevent_active "nop"
  declare-option str java_awt_event_adjustmentevent_active "nop"
  declare-option str java_awt_event_componentevent_active "nop"
  declare-option str java_awt_event_containerevent_active "nop"
  declare-option str java_awt_event_focusevent_active "nop"
  declare-option str java_awt_event_hierarchyevent_active "nop"
  declare-option str java_awt_event_inputevent_active "nop"
  declare-option str java_awt_event_inputmethodevent_active "nop"
  declare-option str java_awt_event_invocationevent_active "nop"
  declare-option str java_awt_event_itemevent_active "nop"
  declare-option str java_awt_event_keyevent_active "nop"
  declare-option str java_awt_event_mouseevent_active "nop"
  declare-option str java_awt_event_mousewheelevent_active "nop"
  declare-option str java_awt_event_paintevent_active "nop"
  declare-option str java_awt_event_textevent_active "nop"
  declare-option str java_awt_event_windowevent_active "nop"
  declare-option str java_awt_font_glyphjustificationinfo_active "nop"
  declare-option str java_awt_font_glyphmetrics_active "nop"
  declare-option str java_awt_font_glyphvector_active "nop"
  declare-option str java_awt_font_graphicattribute_active "nop"
  declare-option str java_awt_font_numericshaper_active "nop"
  declare-option str java_awt_font_opentype_active "nop"
  declare-option str java_awt_font_shapegraphicattribute_active "nop"
  declare-option str java_awt_geom_affinetransform_active "nop"
  declare-option str java_awt_geom_arc2d_active "nop"
  declare-option str java_awt_geom_path2d_active "nop"
  declare-option str java_awt_geom_pathiterator_active "nop"
  declare-option str java_awt_geom_rectangle2d_active "nop"
  declare-option str java_awt_im_inputmethodhighlight_active "nop"
  declare-option str java_awt_image_affinetransformop_active "nop"
  declare-option str java_awt_image_bufferedimage_active "nop"
  declare-option str java_awt_image_convolveop_active "nop"
  declare-option str java_awt_image_databuffer_active "nop"
  declare-option str java_awt_image_imageconsumer_active "nop"
  declare-option str java_awt_image_imageobserver_active "nop"
  declare-option str java_awt_image_volatileimage_active "nop"
  declare-option str java_awt_image_renderable_renderableimage_active "nop"
  declare-option str java_awt_print_pageable_active "nop"
  declare-option str java_awt_print_pageformat_active "nop"
  declare-option str java_awt_print_printable_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_beans_beaninfo_active "nop"
  declare-option str java_beans_designmode_active "nop"
  declare-option str java_beans_introspector_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_io_objectstreamconstants_active "nop"
  declare-option str java_io_pipedinputstream_active "nop"
  declare-option str java_io_streamtokenizer_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_lang_byte_active "nop"
  declare-option str java_lang_character_active "nop"
  declare-option str java_lang_double_active "nop"
  declare-option str java_lang_float_active "nop"
  declare-option str java_lang_integer_active "nop"
  declare-option str java_lang_long_active "nop"
  declare-option str java_lang_math_active "nop"
  declare-option str java_lang_short_active "nop"
  declare-option str java_lang_strictmath_active "nop"
  declare-option str java_lang_thread_active "nop"
  declare-option str java_lang_constant_constantdescs_active "nop"
  declare-option str java_lang_invoke_lambdametafactory_active "nop"
  declare-option str java_lang_invoke_methodhandleinfo_active "nop"
  declare-option str java_lang_invoke_methodhandles_lookup_active "nop"
  declare-option str java_lang_management_managementfactory_active "nop"
  declare-option str java_lang_management_memorynotificationinfo_active "nop"
  declare-option str java_lang_reflect_member_active "nop"
  declare-option str java_lang_reflect_modifier_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_math_bigdecimal_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_net_httpurlconnection_active "nop"
  declare-option str java_net_idn_active "nop"
  declare-option str java_net_socketoptions_active "nop"
  declare-option str java_net_http_websocket_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_nio_channels_selectionkey_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_rmi_activation_activationsystem_active "nop"
  declare-option str java_rmi_registry_registry_active "nop"
  declare-option str java_rmi_server_loaderhandler_active "nop"
  declare-option str java_rmi_server_logstream_active "nop"
  declare-option str java_rmi_server_objid_active "nop"
  declare-option str java_rmi_server_remoteref_active "nop"
  declare-option str java_rmi_server_serverref_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_security_key_active "nop"
  declare-option str java_security_privatekey_active "nop"
  declare-option str java_security_publickey_active "nop"
  declare-option str java_security_signature_active "nop"
  declare-option str java_security_interfaces_dsaprivatekey_active "nop"
  declare-option str java_security_interfaces_dsapublickey_active "nop"
  declare-option str java_security_interfaces_ecprivatekey_active "nop"
  declare-option str java_security_interfaces_ecpublickey_active "nop"
  declare-option str java_security_interfaces_rsamultiprimeprivatecrtkey_active "nop"
  declare-option str java_security_interfaces_rsaprivatecrtkey_active "nop"
  declare-option str java_security_interfaces_rsaprivatekey_active "nop"
  declare-option str java_security_interfaces_rsapublickey_active "nop"
  declare-option str java_security_spec_pssparameterspec_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_sql_connection_active "nop"
  declare-option str java_sql_databasemetadata_active "nop"
  declare-option str java_sql_parametermetadata_active "nop"
  declare-option str java_sql_resultset_active "nop"
  declare-option str java_sql_resultsetmetadata_active "nop"
  declare-option str java_sql_statement_active "nop"
  declare-option str java_sql_types_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_text_bidi_active "nop"
  declare-option str java_text_breakiterator_active "nop"
  declare-option str java_text_characteriterator_active "nop"
  declare-option str java_text_collationelementiterator_active "nop"
  declare-option str java_text_collator_active "nop"
  declare-option str java_text_dateformat_active "nop"
  declare-option str java_text_numberformat_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_time_year_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_util_calendar_active "nop"
  declare-option str java_util_formattableflags_active "nop"
  declare-option str java_util_gregoriancalendar_active "nop"
  declare-option str java_util_locale_active "nop"
  declare-option str java_util_locale_languagerange_active "nop"
  declare-option str java_util_resourcebundle_control_active "nop"
  declare-option str java_util_simpletimezone_active "nop"
  declare-option str java_util_spliterator_active "nop"
  declare-option str java_util_timezone_active "nop"
  declare-option str java_util_jar_jarentry_active "nop"
  declare-option str java_util_jar_jarfile_active "nop"
  declare-option str java_util_jar_jarinputstream_active "nop"
  declare-option str java_util_jar_jaroutputstream_active "nop"
  declare-option str java_util_logging_errormanager_active "nop"
  declare-option str java_util_logging_logger_active "nop"
  declare-option str java_util_logging_logmanager_active "nop"
  declare-option str java_util_prefs_preferences_active "nop"
  declare-option str java_util_regex_pattern_active "nop"
  declare-option str java_util_zip_deflater_active "nop"
  declare-option str java_util_zip_gzipinputstream_active "nop"
  declare-option str java_util_zip_zipentry_active "nop"
  declare-option str java_util_zip_zipfile_active "nop"
  declare-option str java_util_zip_zipinputstream_active "nop"
  declare-option str java_util_zip_zipoutputstream_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str javax_accessibility_accessiblecontext_active "nop"
  declare-option str javax_accessibility_accessibleextendedtext_active "nop"
  declare-option str javax_accessibility_accessiblerelation_active "nop"
  declare-option str javax_accessibility_accessibletablemodelchange_active "nop"
  declare-option str javax_accessibility_accessibletext_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str javax_crypto_cipher_active "nop"
  declare-option str javax_crypto_secretkey_active "nop"
  declare-option str javax_crypto_interfaces_dhprivatekey_active "nop"
  declare-option str javax_crypto_interfaces_dhpublickey_active "nop"
  declare-option str javax_crypto_interfaces_pbekey_active "nop"
  declare-option str javax_crypto_spec_desedekeyspec_active "nop"
  declare-option str javax_crypto_spec_deskeyspec_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str javax_imageio_imagewriteparam_active "nop"
  declare-option str javax_imageio_metadata_iiometadataformat_active "nop"
  declare-option str javax_imageio_metadata_iiometadataformatimpl_active "nop"
  declare-option str javax_imageio_plugins_tiff_baselinetifftagset_active "nop"
  declare-option str javax_imageio_plugins_tiff_exifgpstagset_active "nop"
  declare-option str javax_imageio_plugins_tiff_exifinteroperabilitytagset_active "nop"
  declare-option str javax_imageio_plugins_tiff_exifparenttifftagset_active "nop"
  declare-option str javax_imageio_plugins_tiff_exiftifftagset_active "nop"
  declare-option str javax_imageio_plugins_tiff_faxtifftagset_active "nop"
  declare-option str javax_imageio_plugins_tiff_geotifftagset_active "nop"
  declare-option str javax_imageio_plugins_tiff_tifftag_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str javax_management_attributechangenotification_active "nop"
  declare-option str javax_management_jmx_active "nop"
  declare-option str javax_management_mbeanoperationinfo_active "nop"
  declare-option str javax_management_mbeanservernotification_active "nop"
  declare-option str javax_management_query_active "nop"
  declare-option str javax_management_monitor_monitor_active "nop"
  declare-option str javax_management_monitor_monitornotification_active "nop"
  declare-option str javax_management_relation_relationnotification_active "nop"
  declare-option str javax_management_relation_roleinfo_active "nop"
  declare-option str javax_management_relation_rolestatus_active "nop"
  declare-option str javax_management_remote_jmxconnectionnotification_active "nop"
  declare-option str javax_management_remote_jmxconnector_active "nop"
  declare-option str javax_management_remote_jmxconnectorfactory_active "nop"
  declare-option str javax_management_remote_jmxconnectorserver_active "nop"
  declare-option str javax_management_remote_jmxconnectorserverfactory_active "nop"
  declare-option str javax_management_remote_rmi_rmiconnectorserver_active "nop"
  declare-option str javax_management_timer_timer_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str javax_naming_context_active "nop"
  declare-option str javax_naming_name_active "nop"
  declare-option str javax_naming_directory_attribute_active "nop"
  declare-option str javax_naming_directory_dircontext_active "nop"
  declare-option str javax_naming_directory_searchcontrols_active "nop"
  declare-option str javax_naming_event_eventcontext_active "nop"
  declare-option str javax_naming_event_namingevent_active "nop"
  declare-option str javax_naming_ldap_control_active "nop"
  declare-option str javax_naming_ldap_ldapcontext_active "nop"
  declare-option str javax_naming_ldap_managereferralcontrol_active "nop"
  declare-option str javax_naming_ldap_pagedresultscontrol_active "nop"
  declare-option str javax_naming_ldap_pagedresultsresponsecontrol_active "nop"
  declare-option str javax_naming_ldap_sortcontrol_active "nop"
  declare-option str javax_naming_ldap_sortresponsecontrol_active "nop"
  declare-option str javax_naming_ldap_starttlsrequest_active "nop"
  declare-option str javax_naming_ldap_starttlsresponse_active "nop"
  declare-option str javax_naming_spi_namingmanager_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str javax_net_ssl_standardconstants_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str javax_print_serviceuifactory_active "nop"
  declare-option str javax_print_uriexception_active "nop"
  declare-option str javax_print_attribute_resolutionsyntax_active "nop"
  declare-option str javax_print_attribute_size2dsyntax_active "nop"
  declare-option str javax_print_attribute_standard_mediaprintablearea_active "nop"
  declare-option str javax_print_event_printjobevent_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str javax_script_scriptcontext_active "nop"
  declare-option str javax_script_scriptengine_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str javax_security_auth_callback_confirmationcallback_active "nop"
  declare-option str javax_security_auth_callback_textoutputcallback_active "nop"
  declare-option str javax_security_auth_kerberos_kerberosprincipal_active "nop"
  declare-option str javax_security_auth_x500_x500principal_active "nop"
  declare-option str javax_security_sasl_sasl_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str javax_sound_midi_metamessage_active "nop"
  declare-option str javax_sound_midi_midifileformat_active "nop"
  declare-option str javax_sound_midi_sequence_active "nop"
  declare-option str javax_sound_midi_sequencer_active "nop"
  declare-option str javax_sound_midi_shortmessage_active "nop"
  declare-option str javax_sound_midi_sysexmessage_active "nop"
  declare-option str javax_sound_sampled_audiosystem_active "nop"
  declare-option str javax_sound_sampled_clip_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str javax_sql_rowset_baserowset_active "nop"
  declare-option str javax_sql_rowset_cachedrowset_active "nop"
  declare-option str javax_sql_rowset_joinrowset_active "nop"
  declare-option str javax_sql_rowset_webrowset_active "nop"
  declare-option str javax_sql_rowset_spi_syncfactory_active "nop"
  declare-option str javax_sql_rowset_spi_syncprovider_active "nop"
  declare-option str javax_sql_rowset_spi_syncresolver_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str javax_swing_abstractbutton_active "nop"
  declare-option str javax_swing_action_active "nop"
  declare-option str javax_swing_boxlayout_active "nop"
  declare-option str javax_swing_debuggraphics_active "nop"
  declare-option str javax_swing_defaultbuttonmodel_active "nop"
  declare-option str javax_swing_focusmanager_active "nop"
  declare-option str javax_swing_grouplayout_active "nop"
  declare-option str javax_swing_jcheckbox_active "nop"
  declare-option str javax_swing_jcolorchooser_active "nop"
  declare-option str javax_swing_jcomponent_active "nop"
  declare-option str javax_swing_jdesktoppane_active "nop"
  declare-option str javax_swing_jeditorpane_active "nop"
  declare-option str javax_swing_jfilechooser_active "nop"
  declare-option str javax_swing_jformattedtextfield_active "nop"
  declare-option str javax_swing_jinternalframe_active "nop"
  declare-option str javax_swing_jlayeredpane_active "nop"
  declare-option str javax_swing_jlist_active "nop"
  declare-option str javax_swing_joptionpane_active "nop"
  declare-option str javax_swing_jrootpane_active "nop"
  declare-option str javax_swing_jsplitpane_active "nop"
  declare-option str javax_swing_jtabbedpane_active "nop"
  declare-option str javax_swing_jtable_active "nop"
  declare-option str javax_swing_jtextfield_active "nop"
  declare-option str javax_swing_jtree_active "nop"
  declare-option str javax_swing_jviewport_active "nop"
  declare-option str javax_swing_listselectionmodel_active "nop"
  declare-option str javax_swing_scrollpaneconstants_active "nop"
  declare-option str javax_swing_spring_active "nop"
  declare-option str javax_swing_springlayout_active "nop"
  declare-option str javax_swing_swingconstants_active "nop"
  declare-option str javax_swing_transferhandler_active "nop"
  declare-option str javax_swing_windowconstants_active "nop"
  declare-option str javax_swing_border_bevelborder_active "nop"
  declare-option str javax_swing_border_etchedborder_active "nop"
  declare-option str javax_swing_border_titledborder_active "nop"
  declare-option str javax_swing_colorchooser_abstractcolorchooserpanel_active "nop"
  declare-option str javax_swing_event_ancestorevent_active "nop"
  declare-option str javax_swing_event_internalframeevent_active "nop"
  declare-option str javax_swing_event_listdataevent_active "nop"
  declare-option str javax_swing_event_tablemodelevent_active "nop"
  declare-option str javax_swing_plaf_basic_basiccombopopup_active "nop"
  declare-option str javax_swing_plaf_basic_basichtml_active "nop"
  declare-option str javax_swing_plaf_basic_basicinternalframeui_borderlistener_active "nop"
  declare-option str javax_swing_plaf_basic_basiclistui_active "nop"
  declare-option str javax_swing_plaf_basic_basicoptionpaneui_active "nop"
  declare-option str javax_swing_plaf_basic_basicscrollbarui_active "nop"
  declare-option str javax_swing_plaf_basic_basicsliderui_active "nop"
  declare-option str javax_swing_plaf_basic_basicsplitpanedivider_active "nop"
  declare-option str javax_swing_plaf_basic_basicsplitpaneui_active "nop"
  declare-option str javax_swing_plaf_metal_metaliconfactory_active "nop"
  declare-option str javax_swing_plaf_metal_metalscrollbarui_active "nop"
  declare-option str javax_swing_plaf_metal_metalsliderui_active "nop"
  declare-option str javax_swing_plaf_metal_metaltooltipui_active "nop"
  declare-option str javax_swing_plaf_nimbus_nimbusstyle_active "nop"
  declare-option str javax_swing_plaf_synth_synthconstants_active "nop"
  declare-option str javax_swing_table_tablecolumn_active "nop"
  declare-option str javax_swing_text_abstractdocument_active "nop"
  declare-option str javax_swing_text_abstractwriter_active "nop"
  declare-option str javax_swing_text_defaultcaret_active "nop"
  declare-option str javax_swing_text_defaulteditorkit_active "nop"
  declare-option str javax_swing_text_defaultstyleddocument_active "nop"
  declare-option str javax_swing_text_defaultstyleddocument_elementspec_active "nop"
  declare-option str javax_swing_text_document_active "nop"
  declare-option str javax_swing_text_jtextcomponent_active "nop"
  declare-option str javax_swing_text_plaindocument_active "nop"
  declare-option str javax_swing_text_styleconstants_active "nop"
  declare-option str javax_swing_text_stylecontext_active "nop"
  declare-option str javax_swing_text_tabstop_active "nop"
  declare-option str javax_swing_text_view_active "nop"
  declare-option str javax_swing_text_html_html_active "nop"
  declare-option str javax_swing_text_html_htmldocument_active "nop"
  declare-option str javax_swing_text_html_htmleditorkit_active "nop"
  declare-option str javax_swing_text_html_parser_dtd_active "nop"
  declare-option str javax_swing_text_html_parser_dtdconstants_active "nop"
  declare-option str javax_swing_tree_defaulttreeselectionmodel_active "nop"
  declare-option str javax_swing_tree_treeselectionmodel_active "nop"
  declare-option str javax_swing_undo_abstractundoableedit_active "nop"
  declare-option str javax_swing_undo_stateedit_active "nop"
  declare-option str javax_swing_undo_stateeditable_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str javax_tools_diagnostic_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str javax_transaction_xa_xaexception_active "nop"
  declare-option str javax_transaction_xa_xaresource_active "nop"
  declare-option str javax_transaction_xa_xid_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str javax_xml_xmlconstants_active "nop"
  declare-option str javax_xml_crypto_dsig_canonicalizationmethod_active "nop"
  declare-option str javax_xml_crypto_dsig_digestmethod_active "nop"
  declare-option str javax_xml_crypto_dsig_manifest_active "nop"
  declare-option str javax_xml_crypto_dsig_signaturemethod_active "nop"
  declare-option str javax_xml_crypto_dsig_signatureproperties_active "nop"
  declare-option str javax_xml_crypto_dsig_transform_active "nop"
  declare-option str javax_xml_crypto_dsig_xmlobject_active "nop"
  declare-option str javax_xml_crypto_dsig_xmlsignature_active "nop"
  declare-option str javax_xml_crypto_dsig_keyinfo_keyvalue_active "nop"
  declare-option str javax_xml_crypto_dsig_keyinfo_pgpdata_active "nop"
  declare-option str javax_xml_crypto_dsig_keyinfo_x509data_active "nop"
  declare-option str javax_xml_crypto_dsig_spec_excc14nparameterspec_active "nop"
  declare-option str javax_xml_datatype_datatypeconstants_active "nop"
  declare-option str javax_xml_datatype_datatypefactory_active "nop"
  declare-option str javax_xml_stream_xmlinputfactory_active "nop"
  declare-option str javax_xml_stream_xmloutputfactory_active "nop"
  declare-option str javax_xml_stream_xmlstreamconstants_active "nop"
  declare-option str javax_xml_transform_outputkeys_active "nop"
  declare-option str javax_xml_transform_result_active "nop"
  declare-option str javax_xml_transform_dom_domresult_active "nop"
  declare-option str javax_xml_transform_dom_domsource_active "nop"
  declare-option str javax_xml_transform_sax_saxresult_active "nop"
  declare-option str javax_xml_transform_sax_saxsource_active "nop"
  declare-option str javax_xml_transform_sax_saxtransformerfactory_active "nop"
  declare-option str javax_xml_transform_stax_staxresult_active "nop"
  declare-option str javax_xml_transform_stax_staxsource_active "nop"
  declare-option str javax_xml_transform_stream_streamresult_active "nop"
  declare-option str javax_xml_transform_stream_streamsource_active "nop"
  declare-option str javax_xml_xpath_xpathconstants_active "nop"
  declare-option str javax_xml_xpath_xpathfactory_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str jdk_dynalink_securelookupsupplier_active "nop"
  declare-option str jdk_dynalink_linker_guardingdynamiclinkerexporter_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str jdk_incubator_foreign_memorylayout_active "nop"
  declare-option str jdk_incubator_foreign_memorysegment_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str jdk_jfr_dataamount_active "nop"
  declare-option str jdk_jfr_enabled_active "nop"
  declare-option str jdk_jfr_period_active "nop"
  declare-option str jdk_jfr_stacktrace_active "nop"
  declare-option str jdk_jfr_threshold_active "nop"
  declare-option str jdk_jfr_timespan_active "nop"
  declare-option str jdk_jfr_timestamp_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str jdk_jshell_diag_active "nop"
  declare-option str jdk_jshell_execution_jdiexecutioncontrolprovider_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str jdk_management_jfr_flightrecordermxbean_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str org_ietf_jgss_gsscontext_active "nop"
  declare-option str org_ietf_jgss_gsscredential_active "nop"
  declare-option str org_ietf_jgss_gssexception_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str org_w3c_dom_domerror_active "nop"
  declare-option str org_w3c_dom_domexception_active "nop"
  declare-option str org_w3c_dom_node_active "nop"
  declare-option str org_w3c_dom_typeinfo_active "nop"
  declare-option str org_w3c_dom_userdatahandler_active "nop"
  declare-option str org_w3c_dom_bootstrap_domimplementationregistry_active "nop"
  declare-option str org_w3c_dom_css_cssprimitivevalue_active "nop"
  declare-option str org_w3c_dom_css_cssrule_active "nop"
  declare-option str org_w3c_dom_css_cssvalue_active "nop"
  declare-option str org_w3c_dom_events_event_active "nop"
  declare-option str org_w3c_dom_events_eventexception_active "nop"
  declare-option str org_w3c_dom_events_mutationevent_active "nop"
  declare-option str org_w3c_dom_ls_domimplementationls_active "nop"
  declare-option str org_w3c_dom_ls_lsexception_active "nop"
  declare-option str org_w3c_dom_ls_lsparser_active "nop"
  declare-option str org_w3c_dom_ls_lsparserfilter_active "nop"
  declare-option str org_w3c_dom_ranges_range_active "nop"
  declare-option str org_w3c_dom_ranges_rangeexception_active "nop"
  declare-option str org_w3c_dom_traversal_nodefilter_active "nop"
  declare-option str org_w3c_dom_xpath_xpathexception_active "nop"
  declare-option str org_w3c_dom_xpath_xpathnamespace_active "nop"
  declare-option str org_w3c_dom_xpath_xpathresult_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str org_xml_sax_helpers_namespacesupport_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  # ---------------------------------------- ENUMS ------------------------------------------------- #
  declare-option str com_sun_management_vmoption_origin_active "nop"
  declare-option str com_sun_nio_sctp_associationchangenotification_assocchangeevent_active "nop"
  declare-option str com_sun_nio_sctp_handlerresult_active "nop"
  declare-option str com_sun_nio_sctp_peeraddresschangenotification_addresschangeevent_active "nop"
  declare-option str com_sun_security_jgss_inquiretype_active "nop"
  declare-option str com_sun_source_doctree_attributetree_valuekind_active "nop"
  declare-option str com_sun_source_doctree_doctree_kind_active "nop"
  declare-option str com_sun_source_tree_casetree_casekind_active "nop"
  declare-option str com_sun_source_tree_lambdaexpressiontree_bodykind_active "nop"
  declare-option str com_sun_source_tree_memberreferencetree_referencemode_active "nop"
  declare-option str com_sun_source_tree_moduletree_modulekind_active "nop"
  declare-option str com_sun_source_tree_tree_kind_active "nop"
  declare-option str com_sun_source_util_taskevent_kind_active "nop"
  declare-option str com_sun_tools_jconsole_jconsolecontext_connectionstate_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_awt_component_baselineresizebehavior_active "nop"
  declare-option str java_awt_desktop_action_active "nop"
  declare-option str java_awt_desktop_quitstrategy_active "nop"
  declare-option str java_awt_desktop_usersessionevent_reason_active "nop"
  declare-option str java_awt_dialog_modalexclusiontype_active "nop"
  declare-option str java_awt_dialog_modalitytype_active "nop"
  declare-option str java_awt_event_focusevent_cause_active "nop"
  declare-option str java_awt_font_numericshaper_range_active "nop"
  declare-option str java_awt_graphicsdevice_windowtranslucency_active "nop"
  declare-option str java_awt_multiplegradientpaint_colorspacetype_active "nop"
  declare-option str java_awt_multiplegradientpaint_cyclemethod_active "nop"
  declare-option str java_awt_taskbar_feature_active "nop"
  declare-option str java_awt_taskbar_state_active "nop"
  declare-option str java_awt_trayicon_messagetype_active "nop"
  declare-option str java_awt_window_type_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_io_objectinputfilter_status_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_lang_annotation_elementtype_active "nop"
  declare-option str java_lang_annotation_retentionpolicy_active "nop"
  declare-option str java_lang_character_unicodescript_active "nop"
  declare-option str java_lang_constant_directmethodhandledesc_kind_active "nop"
  declare-option str java_lang_invoke_methodhandles_lookup_classoption_active "nop"
  declare-option str java_lang_invoke_varhandle_accessmode_active "nop"
  declare-option str java_lang_management_memorytype_active "nop"
  declare-option str java_lang_module_moduledescriptor_exports_modifier_active "nop"
  declare-option str java_lang_module_moduledescriptor_modifier_active "nop"
  declare-option str java_lang_module_moduledescriptor_opens_modifier_active "nop"
  declare-option str java_lang_module_moduledescriptor_requires_modifier_active "nop"
  declare-option str java_lang_processbuilder_redirect_type_active "nop"
  declare-option str java_lang_stackwalker_option_active "nop"
  declare-option str java_lang_system_logger_level_active "nop"
  declare-option str java_lang_thread_state_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_math_roundingmode_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_net_authenticator_requestortype_active "nop"
  declare-option str java_net_http_httpclient_redirect_active "nop"
  declare-option str java_net_http_httpclient_version_active "nop"
  declare-option str java_net_proxy_type_active "nop"
  declare-option str java_net_standardprotocolfamily_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_nio_file_accessmode_active "nop"
  declare-option str java_nio_file_attribute_aclentryflag_active "nop"
  declare-option str java_nio_file_attribute_aclentrypermission_active "nop"
  declare-option str java_nio_file_attribute_aclentrytype_active "nop"
  declare-option str java_nio_file_attribute_posixfilepermission_active "nop"
  declare-option str java_nio_file_filevisitoption_active "nop"
  declare-option str java_nio_file_filevisitresult_active "nop"
  declare-option str java_nio_file_linkoption_active "nop"
  declare-option str java_nio_file_standardcopyoption_active "nop"
  declare-option str java_nio_file_standardopenoption_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_security_cert_certpathvalidatorexception_basicreason_active "nop"
  declare-option str java_security_cert_crlreason_active "nop"
  declare-option str java_security_cert_pkixreason_active "nop"
  declare-option str java_security_cert_pkixrevocationchecker_option_active "nop"
  declare-option str java_security_cryptoprimitive_active "nop"
  declare-option str java_security_drbgparameters_capability_active "nop"
  declare-option str java_security_keyrep_type_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_sql_clientinfostatus_active "nop"
  declare-option str java_sql_jdbctype_active "nop"
  declare-option str java_sql_pseudocolumnusage_active "nop"
  declare-option str java_sql_rowidlifetime_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_text_normalizer_form_active "nop"
  declare-option str java_text_numberformat_style_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_time_chrono_hijrahera_active "nop"
  declare-option str java_time_chrono_isoera_active "nop"
  declare-option str java_time_chrono_minguoera_active "nop"
  declare-option str java_time_chrono_thaibuddhistera_active "nop"
  declare-option str java_time_dayofweek_active "nop"
  declare-option str java_time_format_formatstyle_active "nop"
  declare-option str java_time_format_resolverstyle_active "nop"
  declare-option str java_time_format_signstyle_active "nop"
  declare-option str java_time_format_textstyle_active "nop"
  declare-option str java_time_month_active "nop"
  declare-option str java_time_temporal_chronofield_active "nop"
  declare-option str java_time_temporal_chronounit_active "nop"
  declare-option str java_time_zone_zoneoffsettransitionrule_timedefinition_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str java_util_concurrent_timeunit_active "nop"
  declare-option str java_util_formatter_bigdecimallayoutform_active "nop"
  declare-option str java_util_locale_category_active "nop"
  declare-option str java_util_locale_filteringmode_active "nop"
  declare-option str java_util_locale_isocountrycode_active "nop"
  declare-option str java_util_stream_collector_characteristics_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str javax_lang_model_element_elementkind_active "nop"
  declare-option str javax_lang_model_element_modifier_active "nop"
  declare-option str javax_lang_model_element_moduleelement_directivekind_active "nop"
  declare-option str javax_lang_model_element_nestingkind_active "nop"
  declare-option str javax_lang_model_sourceversion_active "nop"
  declare-option str javax_lang_model_type_typekind_active "nop"
  declare-option str javax_lang_model_util_elements_origin_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str javax_net_ssl_sslengineresult_handshakestatus_active "nop"
  declare-option str javax_net_ssl_sslengineresult_status_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str javax_smartcardio_cardterminals_state_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str javax_swing_dropmode_active "nop"
  declare-option str javax_swing_event_rowsorterevent_type_active "nop"
  declare-option str javax_swing_grouplayout_alignment_active "nop"
  declare-option str javax_swing_jtable_printmode_active "nop"
  declare-option str javax_swing_layoutstyle_componentplacement_active "nop"
  declare-option str javax_swing_plaf_nimbus_abstractregionpainter_paintcontext_cachemode_active "nop"
  declare-option str javax_swing_rowfilter_comparisontype_active "nop"
  declare-option str javax_swing_sortorder_active "nop"
  declare-option str javax_swing_swingworker_statevalue_active "nop"
  declare-option str javax_swing_text_html_formsubmitevent_methodtype_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str javax_tools_diagnostic_kind_active "nop"
  declare-option str javax_tools_documentationtool_location_active "nop"
  declare-option str javax_tools_javafileobject_kind_active "nop"
  declare-option str javax_tools_standardlocation_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str javax_xml_catalog_catalogfeatures_feature_active "nop"
  declare-option str javax_xml_xpath_xpathevaluationresult_xpathresulttype_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str jdk_dynalink_linker_conversioncomparator_comparison_active "nop"
  declare-option str jdk_dynalink_standardnamespace_active "nop"
  declare-option str jdk_dynalink_standardoperation_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str jdk_incubator_foreign_clinker_typekind_active "nop"
  declare-option str jdk_incubator_vector_vectorshape_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str jdk_javadoc_doclet_docletenvironment_modulemode_active "nop"
  declare-option str jdk_javadoc_doclet_doclet_option_kind_active "nop"
  declare-option str jdk_javadoc_doclet_taglet_location_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str jdk_jfr_recordingstate_active "nop"
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  declare-option str jdk_jshell_snippet_kind_active "nop"
  declare-option str jdk_jshell_snippet_status_active "nop"
  declare-option str jdk_jshell_snippet_subkind_active "nop"
  declare-option str jdk_jshell_sourcecodeanalysis_completeness_active "nop"

  # -------------------------------------- PAGE BREAK ---------------------------------------------- #

  hook -group java-constant-field window InsertCompletionHide .* java-constants-add
  hook -group java-constant-field window InsertCompletionShow .* java-constants-remove
  
  hook -group java-enum-field window InsertCompletionHide .* java-enums-add
  hook -group java-enum-field window InsertCompletionShow .* java-enums-remove
 

  hook -once -always window WinSetOption filetype=.* %{
    remove-hooks window java-constant-field
    remove-hooks window java-enum-field
  }
}
# -------------------------------------------------------------------------------------------------- #
#
# kakoune appends to the static word list,
# no insertion filters are applied,
# duplicate values are uninhibited.
#
provide-module javaConstants %§
# -------------------------------------------------------------------------------------------------- #
# ------------------------------------- JAVA CONSTANTS ADD ----------------------------------------- #
define-command -hidden java-constants-add %{
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:com\.sun\.[a-z]+\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:com\.sun\.java\.accessibility\.util\.EventID)<ret>'
      evaluate-commands %opt{com_sun_java_accessibility_util_eventid_active}
      set-option -add window static_words %opt{com_sun_java_accessibility_util_eventid}
      set-option window com_sun_java_accessibility_util_eventid_active "nay"
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:com\.sun\.jdi\.[A-Za-z])<ret>'
      try %{
        execute-keys -draft '%s (?:com\.sun\.jdi\.ClassType)<ret>'
        evaluate-commands %opt{com_sun_jdi_classtype_active}
        set-option -add window static_words %opt{com_sun_jdi_classtype}
        set-option window com_sun_jdi_classtype_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:com\.sun\.jdi\.ObjectReference)<ret>'
        evaluate-commands %opt{com_sun_jdi_objectreference_active}
        set-option -add window static_words %opt{com_sun_jdi_objectreference}
        set-option window com_sun_jdi_objectreference_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:com\.sun\.jdi\.ThreadReference)<ret>'
        evaluate-commands %opt{com_sun_jdi_threadreference_active}
        set-option -add window static_words %opt{com_sun_jdi_threadreference}
        set-option window com_sun_jdi_threadreference_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:com\.sun\.jdi\.VirtualMachine)<ret>'
        evaluate-commands %opt{com_sun_jdi_virtualmachine_active}
        set-option -add window static_words %opt{com_sun_jdi_virtualmachine}
        set-option window com_sun_jdi_virtualmachine_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:com\.sun\.jdi\.request\.EventRequest)<ret>'
        evaluate-commands %opt{com_sun_jdi_request_eventrequest_active}
        set-option -add window static_words %opt{com_sun_jdi_request_eventrequest}
        set-option window com_sun_jdi_request_eventrequest_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:com\.sun\.jdi\.request\.StepRequest)<ret>'
        evaluate-commands %opt{com_sun_jdi_request_steprequest_active}
        set-option -add window static_words %opt{com_sun_jdi_request_steprequest}
        set-option window com_sun_jdi_request_steprequest_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:com\.sun\.management\.GarbageCollectionNotificationInfo)<ret>'
      evaluate-commands %opt{com_sun_management_garbagecollectionnotificationinfo_active}
      set-option -add window static_words %opt{com_sun_management_garbagecollectionnotificationinfo}
      set-option window com_sun_management_garbagecollectionnotificationinfo_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:com\.sun\.security\.auth\.module\.JndiLoginModule)<ret>'
      evaluate-commands %opt{com_sun_security_auth_module_jndiloginmodule_active}
      set-option -add window static_words %opt{com_sun_security_auth_module_jndiloginmodule}
      set-option window com_sun_security_auth_module_jndiloginmodule_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:com\.sun\.tools\.jconsole\.JConsoleContext)<ret>'
      evaluate-commands %opt{com_sun_tools_jconsole_jconsolecontext_active}
      set-option -add window static_words %opt{com_sun_tools_jconsole_jconsolecontext}
      set-option window com_sun_tools_jconsole_jconsolecontext_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.awt\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.awt\.[A-E])<ret>'
      try %{
        execute-keys -draft '%s (?:java\.awt\.Adjustable)<ret>'
        evaluate-commands %opt{java_awt_adjustable_active}
        set-option -add window static_words %opt{java_awt_adjustable}
        set-option window java_awt_adjustable_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.AlphaComposite)<ret>'
        evaluate-commands %opt{java_awt_alphacomposite_active}
        set-option -add window static_words %opt{java_awt_alphacomposite}
        set-option window java_awt_alphacomposite_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.AWTEvent)<ret>'
        evaluate-commands %opt{java_awt_awtevent_active}
        set-option -add window static_words %opt{java_awt_awtevent}
        set-option window java_awt_awtevent_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.BasicStroke)<ret>'
        evaluate-commands %opt{java_awt_basicstroke_active}
        set-option -add window static_words %opt{java_awt_basicstroke}
        set-option window java_awt_basicstroke_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.BorderLayout)<ret>'
        evaluate-commands %opt{java_awt_borderlayout_active}
        set-option -add window static_words %opt{java_awt_borderlayout}
        set-option window java_awt_borderlayout_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.Component)<ret>'
        evaluate-commands %opt{java_awt_component_active}
        set-option -add window static_words %opt{java_awt_component}
        set-option window java_awt_component_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.Cursor)<ret>'
        evaluate-commands %opt{java_awt_cursor_active}
        set-option -add window static_words %opt{java_awt_cursor}
        set-option window java_awt_cursor_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.DisplayMode)<ret>'
        evaluate-commands %opt{java_awt_displaymode_active}
        set-option -add window static_words %opt{java_awt_displaymode}
        set-option window java_awt_displaymode_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.Event)<ret>'
        evaluate-commands %opt{java_awt_event_active}
        set-option -add window static_words %opt{java_awt_event}
        set-option window java_awt_event_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:java\.awt\.[F-G])<ret>'
      try %{
        execute-keys -draft '%s (?:java\.awt\.FileDialog)<ret>'
        evaluate-commands %opt{java_awt_filedialog_active}
        set-option -add window static_words %opt{java_awt_filedialog}
        set-option window java_awt_filedialog_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.FlowLayout)<ret>'
        evaluate-commands %opt{java_awt_flowlayout_active}
        set-option -add window static_words %opt{java_awt_flowlayout}
        set-option window java_awt_flowlayout_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.Font)<ret>'
        evaluate-commands %opt{java_awt_font_active}
        set-option -add window static_words %opt{java_awt_font}
        set-option window java_awt_font_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.Frame)<ret>'
        evaluate-commands %opt{java_awt_frame_active}
        set-option -add window static_words %opt{java_awt_frame}
        set-option window java_awt_frame_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.GraphicsConfigTemplate)<ret>'
        evaluate-commands %opt{java_awt_graphicsconfigtemplate_active}
        set-option -add window static_words %opt{java_awt_graphicsconfigtemplate}
        set-option window java_awt_graphicsconfigtemplate_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.GraphicsDevice)<ret>'
        evaluate-commands %opt{java_awt_graphicsdevice_active}
        set-option -add window static_words %opt{java_awt_graphicsdevice}
        set-option window java_awt_graphicsdevice_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.GridBagConstraints)<ret>'
        evaluate-commands %opt{java_awt_gridbagconstraints_active}
        set-option -add window static_words %opt{java_awt_gridbagconstraints}
        set-option window java_awt_gridbagconstraints_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.GridBagLayout)<ret>'
        evaluate-commands %opt{java_awt_gridbaglayout_active}
        set-option -add window static_words %opt{java_awt_gridbaglayout}
        set-option window java_awt_gridbaglayout_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:java\.awt\.[H-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:java\.awt\.Image)<ret>'
        evaluate-commands %opt{java_awt_image_active}
        set-option -add window static_words %opt{java_awt_image}
        set-option window java_awt_image_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.KeyboardFocusManager)<ret>'
        evaluate-commands %opt{java_awt_keyboardfocusmanager_active}
        set-option -add window static_words %opt{java_awt_keyboardfocusmanager}
        set-option window java_awt_keyboardfocusmanager_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.Label)<ret>'
        evaluate-commands %opt{java_awt_label_active}
        set-option -add window static_words %opt{java_awt_label}
        set-option window java_awt_label_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.MediaTracker)<ret>'
        evaluate-commands %opt{java_awt_mediatracker_active}
        set-option -add window static_words %opt{java_awt_mediatracker}
        set-option window java_awt_mediatracker_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.Scrollbar)<ret>'
        evaluate-commands %opt{java_awt_scrollbar_active}
        set-option -add window static_words %opt{java_awt_scrollbar}
        set-option window java_awt_scrollbar_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.ScrollPane)<ret>'
        evaluate-commands %opt{java_awt_scrollpane_active}
        set-option -add window static_words %opt{java_awt_scrollpane}
        set-option window java_awt_scrollpane_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.SystemColor)<ret>'
        evaluate-commands %opt{java_awt_systemcolor_active}
        set-option -add window static_words %opt{java_awt_systemcolor}
        set-option window java_awt_systemcolor_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.TextArea)<ret>'
        evaluate-commands %opt{java_awt_textarea_active}
        set-option -add window static_words %opt{java_awt_textarea}
        set-option window java_awt_textarea_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.Transparency)<ret>'
        evaluate-commands %opt{java_awt_transparency_active}
        set-option -add window static_words %opt{java_awt_transparency}
        set-option window java_awt_transparency_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:java\.awt\.color\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:java\.awt\.color\.ColorSpace)<ret>'
        evaluate-commands %opt{java_awt_color_colorspace_active}
        set-option -add window static_words %opt{java_awt_color_colorspace}
        set-option window java_awt_color_colorspace_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.color\.ICC_Profile)<ret>'
        evaluate-commands %opt{java_awt_color_icc_profile_active}
        set-option -add window static_words %opt{java_awt_color_icc_profile}
        set-option window java_awt_color_icc_profile_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.color\.ICC_ProfileRGB)<ret>'
        evaluate-commands %opt{java_awt_color_icc_profilergb_active}
        set-option -add window static_words %opt{java_awt_color_icc_profilergb}
        set-option window java_awt_color_icc_profilergb_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:java\.awt\.datatransfer\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:java\.awt\.datatransfer\.DataFlavor)<ret>'
        evaluate-commands %opt{java_awt_datatransfer_dataflavor_active}
        set-option -add window static_words %opt{java_awt_datatransfer_dataflavor}
        set-option window java_awt_datatransfer_dataflavor_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:java\.awt\.dnd\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:java\.awt\.dnd\.DnDConstants)<ret>'
        evaluate-commands %opt{java_awt_dnd_dndconstants_active}
        set-option -add window static_words %opt{java_awt_dnd_dndconstants}
        set-option window java_awt_dnd_dndconstants_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.dnd\.DragSourceContext)<ret>'
        evaluate-commands %opt{java_awt_dnd_dragsourcecontext_active}
        set-option -add window static_words %opt{java_awt_dnd_dragsourcecontext}
        set-option window java_awt_dnd_dragsourcecontext_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:java\.awt\.event\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:java\.awt\.event\.ActionEvent)<ret>'
        evaluate-commands %opt{java_awt_event_actionevent_active}
        set-option -add window static_words %opt{java_awt_event_actionevent}
        set-option window java_awt_event_actionevent_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.event\.AdjustmentEvent)<ret>'
        evaluate-commands %opt{java_awt_event_adjustmentevent_active}
        set-option -add window static_words %opt{java_awt_event_adjustmentevent}
        set-option window java_awt_event_adjustmentevent_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.event\.ComponentEvent)<ret>'
        evaluate-commands %opt{java_awt_event_componentevent_active}
        set-option -add window static_words %opt{java_awt_event_componentevent}
        set-option window java_awt_event_componentevent_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.event\.ContainerEvent)<ret>'
        evaluate-commands %opt{java_awt_event_containerevent_active}
        set-option -add window static_words %opt{java_awt_event_containerevent}
        set-option window java_awt_event_containerevent_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.event\.FocusEvent)<ret>'
        evaluate-commands %opt{java_awt_event_focusevent_active}
        set-option -add window static_words %opt{java_awt_event_focusevent}
        set-option window java_awt_event_focusevent_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.event\.HierarchyEvent)<ret>'
        evaluate-commands %opt{java_awt_event_hierarchyevent_active}
        set-option -add window static_words %opt{java_awt_event_hierarchyevent}
        set-option window java_awt_event_hierarchyevent_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.event\.InputEvent)<ret>'
        evaluate-commands %opt{java_awt_event_inputevent_active}
        set-option -add window static_words %opt{java_awt_event_inputevent}
        set-option window java_awt_event_inputevent_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.event\.InputMethodEvent)<ret>'
        evaluate-commands %opt{java_awt_event_inputmethodevent_active}
        set-option -add window static_words %opt{java_awt_event_inputmethodevent}
        set-option window java_awt_event_inputmethodevent_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.event\.InvocationEvent)<ret>'
        evaluate-commands %opt{java_awt_event_invocationevent_active}
        set-option -add window static_words %opt{java_awt_event_invocationevent}
        set-option window java_awt_event_invocationevent_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.event\.ItemEvent)<ret>'
        evaluate-commands %opt{java_awt_event_itemevent_active}
        set-option -add window static_words %opt{java_awt_event_itemevent}
        set-option window java_awt_event_itemevent_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.event\.KeyEvent)<ret>'
        evaluate-commands %opt{java_awt_event_keyevent_active}
        set-option -add window static_words %opt{java_awt_event_keyevent}
        set-option window java_awt_event_keyevent_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.event\.MouseEvent)<ret>'
        evaluate-commands %opt{java_awt_event_mouseevent_active}
        set-option -add window static_words %opt{java_awt_event_mouseevent}
        set-option window java_awt_event_mouseevent_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.event\.MouseWheelEvent)<ret>'
        evaluate-commands %opt{java_awt_event_mousewheelevent_active}
        set-option -add window static_words %opt{java_awt_event_mousewheelevent}
        set-option window java_awt_event_mousewheelevent_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.event\.PaintEvent)<ret>'
        evaluate-commands %opt{java_awt_event_paintevent_active}
        set-option -add window static_words %opt{java_awt_event_paintevent}
        set-option window java_awt_event_paintevent_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.event\.TextEvent)<ret>'
        evaluate-commands %opt{java_awt_event_textevent_active}
        set-option -add window static_words %opt{java_awt_event_textevent}
        set-option window java_awt_event_textevent_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.event\.WindowEvent)<ret>'
        evaluate-commands %opt{java_awt_event_windowevent_active}
        set-option -add window static_words %opt{java_awt_event_windowevent}
        set-option window java_awt_event_windowevent_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:java\.awt\.font\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:java\.awt\.font\.GlyphJustificationInfo)<ret>'
        evaluate-commands %opt{java_awt_font_glyphjustificationinfo_active}
        set-option -add window static_words %opt{java_awt_font_glyphjustificationinfo}
        set-option window java_awt_font_glyphjustificationinfo_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.font\.GlyphMetrics)<ret>'
        evaluate-commands %opt{java_awt_font_glyphmetrics_active}
        set-option -add window static_words %opt{java_awt_font_glyphmetrics}
        set-option window java_awt_font_glyphmetrics_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.font\.GlyphVector)<ret>'
        evaluate-commands %opt{java_awt_font_glyphvector_active}
        set-option -add window static_words %opt{java_awt_font_glyphvector}
        set-option window java_awt_font_glyphvector_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.font\.GraphicAttribute)<ret>'
        evaluate-commands %opt{java_awt_font_graphicattribute_active}
        set-option -add window static_words %opt{java_awt_font_graphicattribute}
        set-option window java_awt_font_graphicattribute_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.font\.NumericShaper)<ret>'
        evaluate-commands %opt{java_awt_font_numericshaper_active}
        set-option -add window static_words %opt{java_awt_font_numericshaper}
        set-option window java_awt_font_numericshaper_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.font\.OpenType)<ret>'
        evaluate-commands %opt{java_awt_font_opentype_active}
        set-option -add window static_words %opt{java_awt_font_opentype}
        set-option window java_awt_font_opentype_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.font\.ShapeGraphicAttribute)<ret>'
        evaluate-commands %opt{java_awt_font_shapegraphicattribute_active}
        set-option -add window static_words %opt{java_awt_font_shapegraphicattribute}
        set-option window java_awt_font_shapegraphicattribute_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:java\.awt\.geom\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:java\.awt\.geom\.AffineTransform)<ret>'
        evaluate-commands %opt{java_awt_geom_affinetransform_active}
        set-option -add window static_words %opt{java_awt_geom_affinetransform}
        set-option window java_awt_geom_affinetransform_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.geom\.Arc2D)<ret>'
        evaluate-commands %opt{java_awt_geom_arc2d_active}
        set-option -add window static_words %opt{java_awt_geom_arc2d}
        set-option window java_awt_geom_arc2d_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.geom\.Path2D)<ret>'
        evaluate-commands %opt{java_awt_geom_path2d_active}
        set-option -add window static_words %opt{java_awt_geom_path2d}
        set-option window java_awt_geom_path2d_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.geom\.PathIterator)<ret>'
        evaluate-commands %opt{java_awt_geom_pathiterator_active}
        set-option -add window static_words %opt{java_awt_geom_pathiterator}
        set-option window java_awt_geom_pathiterator_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.geom\.Rectangle2D)<ret>'
        evaluate-commands %opt{java_awt_geom_rectangle2d_active}
        set-option -add window static_words %opt{java_awt_geom_rectangle2d}
        set-option window java_awt_geom_rectangle2d_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:java\.awt\.im\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:java\.awt\.im\.InputMethodHighlight)<ret>'
        evaluate-commands %opt{java_awt_im_inputmethodhighlight_active}
        set-option -add window static_words %opt{java_awt_im_inputmethodhighlight}
        set-option window java_awt_im_inputmethodhighlight_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:java\.awt\.image\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:java\.awt\.image\.AffineTransformOp)<ret>'
        evaluate-commands %opt{java_awt_image_affinetransformop_active}
        set-option -add window static_words %opt{java_awt_image_affinetransformop}
        set-option window java_awt_image_affinetransformop_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.image\.BufferedImage)<ret>'
        evaluate-commands %opt{java_awt_image_bufferedimage_active}
        set-option -add window static_words %opt{java_awt_image_bufferedimage}
        set-option window java_awt_image_bufferedimage_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.image\.ConvolveOp)<ret>'
        evaluate-commands %opt{java_awt_image_convolveop_active}
        set-option -add window static_words %opt{java_awt_image_convolveop}
        set-option window java_awt_image_convolveop_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.image\.DataBuffer)<ret>'
        evaluate-commands %opt{java_awt_image_databuffer_active}
        set-option -add window static_words %opt{java_awt_image_databuffer}
        set-option window java_awt_image_databuffer_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.image\.ImageConsumer)<ret>'
        evaluate-commands %opt{java_awt_image_imageconsumer_active}
        set-option -add window static_words %opt{java_awt_image_imageconsumer}
        set-option window java_awt_image_imageconsumer_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.image\.ImageObserver)<ret>'
        evaluate-commands %opt{java_awt_image_imageobserver_active}
        set-option -add window static_words %opt{java_awt_image_imageobserver}
        set-option window java_awt_image_imageobserver_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.image\.VolatileImage)<ret>'
        evaluate-commands %opt{java_awt_image_volatileimage_active}
        set-option -add window static_words %opt{java_awt_image_volatileimage}
        set-option window java_awt_image_volatileimage_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:java\.awt\.image\.[a-z])<ret>'
      try %{
        execute-keys -draft '%s (?:java\.awt\.image\.renderable\.RenderableImage)<ret>'
        evaluate-commands %opt{java_awt_image_renderable_renderableimage_active}
        set-option -add window static_words %opt{java_awt_image_renderable_renderableimage}
        set-option window java_awt_image_renderable_renderableimage_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:java\.awt\.print\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:java\.awt\.print\.Pageable)<ret>'
        evaluate-commands %opt{java_awt_print_pageable_active}
        set-option -add window static_words %opt{java_awt_print_pageable}
        set-option window java_awt_print_pageable_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.print\.PageFormat)<ret>'
        evaluate-commands %opt{java_awt_print_pageformat_active}
        set-option -add window static_words %opt{java_awt_print_pageformat}
        set-option window java_awt_print_pageformat_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.awt\.print\.Printable)<ret>'
        evaluate-commands %opt{java_awt_print_printable_active}
        set-option -add window static_words %opt{java_awt_print_printable}
        set-option window java_awt_print_printable_active "nay"
      }
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.beans\.[A-Z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.beans\.BeanInfo)<ret>'
      evaluate-commands %opt{java_beans_beaninfo_active}
      set-option -add window static_words %opt{java_beans_beaninfo}
      set-option window java_beans_beaninfo_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.beans\.DesignMode)<ret>'
      evaluate-commands %opt{java_beans_designmode_active}
      set-option -add window static_words %opt{java_beans_designmode}
      set-option window java_beans_designmode_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.beans\.Introspector)<ret>'
      evaluate-commands %opt{java_beans_introspector_active}
      set-option -add window static_words %opt{java_beans_introspector}
      set-option window java_beans_introspector_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.io\.[A-Z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.io\.ObjectStreamConstants)<ret>'
      evaluate-commands %opt{java_io_objectstreamconstants_active}
      set-option -add window static_words %opt{java_io_objectstreamconstants}
      set-option window java_io_objectstreamconstants_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.io\.PipedInputStream)<ret>'
      evaluate-commands %opt{java_io_pipedinputstream_active}
      set-option -add window static_words %opt{java_io_pipedinputstream}
      set-option window java_io_pipedinputstream_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.io\.StreamTokenizer)<ret>'
      evaluate-commands %opt{java_io_streamtokenizer_active}
      set-option -add window static_words %opt{java_io_streamtokenizer}
      set-option window java_io_streamtokenizer_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.lang\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.lang\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:java\.lang\.Byte)<ret>'
        evaluate-commands %opt{java_lang_byte_active}
        set-option -add window static_words %opt{java_lang_byte}
        set-option window java_lang_byte_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.lang\.Character)<ret>'
        evaluate-commands %opt{java_lang_character_active}
        set-option -add window static_words %opt{java_lang_character}
        set-option window java_lang_character_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.lang\.Double)<ret>'
        evaluate-commands %opt{java_lang_double_active}
        set-option -add window static_words %opt{java_lang_double}
        set-option window java_lang_double_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.lang\.Float)<ret>'
        evaluate-commands %opt{java_lang_float_active}
        set-option -add window static_words %opt{java_lang_float}
        set-option window java_lang_float_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.lang\.Integer)<ret>'
        evaluate-commands %opt{java_lang_integer_active}
        set-option -add window static_words %opt{java_lang_integer}
        set-option window java_lang_integer_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.lang\.Long)<ret>'
        evaluate-commands %opt{java_lang_long_active}
        set-option -add window static_words %opt{java_lang_long}
        set-option window java_lang_long_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.lang\.Math)<ret>'
        evaluate-commands %opt{java_lang_math_active}
        set-option -add window static_words %opt{java_lang_math}
        set-option window java_lang_math_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.lang\.Short)<ret>'
        evaluate-commands %opt{java_lang_short_active}
        set-option -add window static_words %opt{java_lang_short}
        set-option window java_lang_short_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.lang\.StrictMath)<ret>'
        evaluate-commands %opt{java_lang_strictmath_active}
        set-option -add window static_words %opt{java_lang_strictmath}
        set-option window java_lang_strictmath_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.lang\.Thread)<ret>'
        evaluate-commands %opt{java_lang_thread_active}
        set-option -add window static_words %opt{java_lang_thread}
        set-option window java_lang_thread_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:java\.lang\.[a-z]+\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:java\.lang\.constant\.[A-Z])<ret>'
        try %{
          execute-keys -draft '%s (?:java\.lang\.constant\.ConstantDescs)<ret>'
          evaluate-commands %opt{java_lang_constant_constantdescs_active}
          set-option -add window static_words %opt{java_lang_constant_constantdescs}
          set-option window java_lang_constant_constantdescs_active "nay"
        }
      }
      # -------------------------------- SECTION BREAK --------------------------------------------- #
      try %{
        execute-keys -draft '%s (?:java\.lang\.invoke\.[A-Z])<ret>'
        try %{
          execute-keys -draft '%s (?:java\.lang\.invoke\.LambdaMetafactory)<ret>'
          evaluate-commands %opt{java_lang_invoke_lambdametafactory_active}
          set-option -add window static_words %opt{java_lang_invoke_lambdametafactory}
          set-option window java_lang_invoke_lambdametafactory_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:java\.lang\.invoke\.MethodHandleInfo)<ret>'
          evaluate-commands %opt{java_lang_invoke_methodhandleinfo_active}
          set-option -add window static_words %opt{java_lang_invoke_methodhandleinfo}
          set-option window java_lang_invoke_methodhandleinfo_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:java\.lang\.invoke\.MethodHandles\.Lookup)<ret>'
          evaluate-commands %opt{java_lang_invoke_methodhandles_lookup_active}
          set-option -add window static_words %opt{java_lang_invoke_methodhandles_lookup}
          set-option window java_lang_invoke_methodhandles_lookup_active "nay"
        }
      }
      # -------------------------------- SECTION BREAK --------------------------------------------- #
      try %{
        execute-keys -draft '%s (?:java\.lang\.management\.[A-Z])<ret>'
        try %{
          execute-keys -draft '%s (?:java\.lang\.management\.ManagementFactory)<ret>'
          evaluate-commands %opt{java_lang_management_managementfactory_active}
          set-option -add window static_words %opt{java_lang_management_managementfactory}
          set-option window java_lang_management_managementfactory_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:java\.lang\.management\.MemoryNotificationInfo)<ret>'
          evaluate-commands %opt{java_lang_management_memorynotificationinfo_active}
          set-option -add window static_words %opt{java_lang_management_memorynotificationinfo}
          set-option window java_lang_management_memorynotificationinfo_active "nay"
        }
      }
      # -------------------------------- SECTION BREAK --------------------------------------------- #
      try %{
        execute-keys -draft '%s (?:java\.lang\.reflect\.[A-Z])<ret>'
        try %{
          execute-keys -draft '%s (?:java\.lang\.reflect\.Member)<ret>'
          evaluate-commands %opt{java_lang_reflect_member_active}
          set-option -add window static_words %opt{java_lang_reflect_member}
          set-option window java_lang_reflect_member_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:java\.lang\.reflect\.Modifier)<ret>'
          evaluate-commands %opt{java_lang_reflect_modifier_active}
          set-option -add window static_words %opt{java_lang_reflect_modifier}
          set-option window java_lang_reflect_modifier_active "nay"
        }
      }
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.math\.[A-Z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.math\.BigDecimal)<ret>'
      evaluate-commands %opt{java_math_bigdecimal_active}
      set-option -add window static_words %opt{java_math_bigdecimal}
      set-option window java_math_bigdecimal_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.net\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.net\.HttpURLConnection)<ret>'
      evaluate-commands %opt{java_net_httpurlconnection_active}
      set-option -add window static_words %opt{java_net_httpurlconnection}
      set-option window java_net_httpurlconnection_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.net\.IDN)<ret>'
      evaluate-commands %opt{java_net_idn_active}
      set-option -add window static_words %opt{java_net_idn}
      set-option window java_net_idn_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.net\.SocketOptions)<ret>'
      evaluate-commands %opt{java_net_socketoptions_active}
      set-option -add window static_words %opt{java_net_socketoptions}
      set-option window java_net_socketoptions_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.net\.http\.WebSocket)<ret>'
      evaluate-commands %opt{java_net_http_websocket_active}
      set-option -add window static_words %opt{java_net_http_websocket}
      set-option window java_net_http_websocket_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.nio\.[a-z]+\.[A-Z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.nio\.channels\.SelectionKey)<ret>'
      evaluate-commands %opt{java_nio_channels_selectionkey_active}
      set-option -add window static_words %opt{java_nio_channels_selectionkey}
      set-option window java_nio_channels_selectionkey_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.rmi\.[a-z]+\.[A-Z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.rmi\.activation\.ActivationSystem)<ret>'
      evaluate-commands %opt{java_rmi_activation_activationsystem_active}
      set-option -add window static_words %opt{java_rmi_activation_activationsystem}
      set-option window java_rmi_activation_activationsystem_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.rmi\.registry\.Registry)<ret>'
      evaluate-commands %opt{java_rmi_registry_registry_active}
      set-option -add window static_words %opt{java_rmi_registry_registry}
      set-option window java_rmi_registry_registry_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.rmi\.server\.LoaderHandler)<ret>'
      evaluate-commands %opt{java_rmi_server_loaderhandler_active}
      set-option -add window static_words %opt{java_rmi_server_loaderhandler}
      set-option window java_rmi_server_loaderhandler_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.rmi\.server\.LogStream)<ret>'
      evaluate-commands %opt{java_rmi_server_logstream_active}
      set-option -add window static_words %opt{java_rmi_server_logstream}
      set-option window java_rmi_server_logstream_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.rmi\.server\.ObjID)<ret>'
      evaluate-commands %opt{java_rmi_server_objid_active}
      set-option -add window static_words %opt{java_rmi_server_objid}
      set-option window java_rmi_server_objid_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.rmi\.server\.RemoteRef)<ret>'
      evaluate-commands %opt{java_rmi_server_remoteref_active}
      set-option -add window static_words %opt{java_rmi_server_remoteref}
      set-option window java_rmi_server_remoteref_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.rmi\.server\.ServerRef)<ret>'
      evaluate-commands %opt{java_rmi_server_serverref_active}
      set-option -add window static_words %opt{java_rmi_server_serverref}
      set-option window java_rmi_server_serverref_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.security\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.security\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:java\.security\.Key)<ret>'
        evaluate-commands %opt{java_security_key_active}
        set-option -add window static_words %opt{java_security_key}
        set-option window java_security_key_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.security\.PrivateKey)<ret>'
        evaluate-commands %opt{java_security_privatekey_active}
        set-option -add window static_words %opt{java_security_privatekey}
        set-option window java_security_privatekey_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.security\.PublicKey)<ret>'
        evaluate-commands %opt{java_security_publickey_active}
        set-option -add window static_words %opt{java_security_publickey}
        set-option window java_security_publickey_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.security\.Signature)<ret>'
        evaluate-commands %opt{java_security_signature_active}
        set-option -add window static_words %opt{java_security_signature}
        set-option window java_security_signature_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:java\.security\.interfaces\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:java\.security\.interfaces\.DSAPrivateKey)<ret>'
        evaluate-commands %opt{java_security_interfaces_dsaprivatekey_active}
        set-option -add window static_words %opt{java_security_interfaces_dsaprivatekey}
        set-option window java_security_interfaces_dsaprivatekey_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.security\.interfaces\.DSAPublicKey)<ret>'
        evaluate-commands %opt{java_security_interfaces_dsapublickey_active}
        set-option -add window static_words %opt{java_security_interfaces_dsapublickey}
        set-option window java_security_interfaces_dsapublickey_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.security\.interfaces\.ECPrivateKey)<ret>'
        evaluate-commands %opt{java_security_interfaces_ecprivatekey_active}
        set-option -add window static_words %opt{java_security_interfaces_ecprivatekey}
        set-option window java_security_interfaces_ecprivatekey_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.security\.interfaces\.ECPublicKey)<ret>'
        evaluate-commands %opt{java_security_interfaces_ecpublickey_active}
        set-option -add window static_words %opt{java_security_interfaces_ecpublickey}
        set-option window java_security_interfaces_ecpublickey_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.security\.interfaces\.RSAMultiPrimePrivateCrtKey)<ret>'
        evaluate-commands %opt{java_security_interfaces_rsamultiprimeprivatecrtkey_active}
        set-option -add window static_words %opt{java_security_interfaces_rsamultiprimeprivatecrtkey}
        set-option window java_security_interfaces_rsamultiprimeprivatecrtkey_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.security\.interfaces\.RSAPrivateCrtKey)<ret>'
        evaluate-commands %opt{java_security_interfaces_rsaprivatecrtkey_active}
        set-option -add window static_words %opt{java_security_interfaces_rsaprivatecrtkey}
        set-option window java_security_interfaces_rsaprivatecrtkey_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.security\.interfaces\.RSAPrivateKey)<ret>'
        evaluate-commands %opt{java_security_interfaces_rsaprivatekey_active}
        set-option -add window static_words %opt{java_security_interfaces_rsaprivatekey}
        set-option window java_security_interfaces_rsaprivatekey_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.security\.interfaces\.RSAPublicKey)<ret>'
        evaluate-commands %opt{java_security_interfaces_rsapublickey_active}
        set-option -add window static_words %opt{java_security_interfaces_rsapublickey}
        set-option window java_security_interfaces_rsapublickey_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:java\.security\.spec\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:java\.security\.spec\.PSSParameterSpec)<ret>'
        evaluate-commands %opt{java_security_spec_pssparameterspec_active}
        set-option -add window static_words %opt{java_security_spec_pssparameterspec}
        set-option window java_security_spec_pssparameterspec_active "nay"
      }
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.sql\.[A-Z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.sql\.Connection)<ret>'
      evaluate-commands %opt{java_sql_connection_active}
      set-option -add window static_words %opt{java_sql_connection}
      set-option window java_sql_connection_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.sql\.DatabaseMetaData)<ret>'
      evaluate-commands %opt{java_sql_databasemetadata_active}
      set-option -add window static_words %opt{java_sql_databasemetadata}
      set-option window java_sql_databasemetadata_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.sql\.ParameterMetaData)<ret>'
      evaluate-commands %opt{java_sql_parametermetadata_active}
      set-option -add window static_words %opt{java_sql_parametermetadata}
      set-option window java_sql_parametermetadata_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.sql\.ResultSet)<ret>'
      evaluate-commands %opt{java_sql_resultset_active}
      set-option -add window static_words %opt{java_sql_resultset}
      set-option window java_sql_resultset_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.sql\.ResultSetMetaData)<ret>'
      evaluate-commands %opt{java_sql_resultsetmetadata_active}
      set-option -add window static_words %opt{java_sql_resultsetmetadata}
      set-option window java_sql_resultsetmetadata_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.sql\.Statement)<ret>'
      evaluate-commands %opt{java_sql_statement_active}
      set-option -add window static_words %opt{java_sql_statement}
      set-option window java_sql_statement_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.sql\.Types)<ret>'
      evaluate-commands %opt{java_sql_types_active}
      set-option -add window static_words %opt{java_sql_types}
      set-option window java_sql_types_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.text\.[A-Z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.text\.Bidi)<ret>'
      evaluate-commands %opt{java_text_bidi_active}
      set-option -add window static_words %opt{java_text_bidi}
      set-option window java_text_bidi_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.text\.BreakIterator)<ret>'
      evaluate-commands %opt{java_text_breakiterator_active}
      set-option -add window static_words %opt{java_text_breakiterator}
      set-option window java_text_breakiterator_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.text\.CharacterIterator)<ret>'
      evaluate-commands %opt{java_text_characteriterator_active}
      set-option -add window static_words %opt{java_text_characteriterator}
      set-option window java_text_characteriterator_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.text\.CollationElementIterator)<ret>'
      evaluate-commands %opt{java_text_collationelementiterator_active}
      set-option -add window static_words %opt{java_text_collationelementiterator}
      set-option window java_text_collationelementiterator_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.text\.Collator)<ret>'
      evaluate-commands %opt{java_text_collator_active}
      set-option -add window static_words %opt{java_text_collator}
      set-option window java_text_collator_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.text\.DateFormat)<ret>'
      evaluate-commands %opt{java_text_dateformat_active}
      set-option -add window static_words %opt{java_text_dateformat}
      set-option window java_text_dateformat_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.text\.NumberFormat)<ret>'
      evaluate-commands %opt{java_text_numberformat_active}
      set-option -add window static_words %opt{java_text_numberformat}
      set-option window java_text_numberformat_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.time\.[A-Z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.time\.Year)<ret>'
      evaluate-commands %opt{java_time_year_active}
      set-option -add window static_words %opt{java_time_year}
      set-option window java_time_year_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.util\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.util\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:java\.util\.Calendar)<ret>'
        evaluate-commands %opt{java_util_calendar_active}
        set-option -add window static_words %opt{java_util_calendar}
        set-option window java_util_calendar_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.util\.FormattableFlags)<ret>'
        evaluate-commands %opt{java_util_formattableflags_active}
        set-option -add window static_words %opt{java_util_formattableflags}
        set-option window java_util_formattableflags_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.util\.GregorianCalendar)<ret>'
        evaluate-commands %opt{java_util_gregoriancalendar_active}
        set-option -add window static_words %opt{java_util_gregoriancalendar}
        set-option window java_util_gregoriancalendar_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.util\.Locale)<ret>'
        evaluate-commands %opt{java_util_locale_active}
        set-option -add window static_words %opt{java_util_locale}
        set-option window java_util_locale_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.util\.Locale\.LanguageRange)<ret>'
        evaluate-commands %opt{java_util_locale_languagerange_active}
        set-option -add window static_words %opt{java_util_locale_languagerange}
        set-option window java_util_locale_languagerange_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.util\.ResourceBundle\.Control)<ret>'
        evaluate-commands %opt{java_util_resourcebundle_control_active}
        set-option -add window static_words %opt{java_util_resourcebundle_control}
        set-option window java_util_resourcebundle_control_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.util\.SimpleTimeZone)<ret>'
        evaluate-commands %opt{java_util_simpletimezone_active}
        set-option -add window static_words %opt{java_util_simpletimezone}
        set-option window java_util_simpletimezone_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.util\.Spliterator)<ret>'
        evaluate-commands %opt{}
        java_util_spliterator_active -add window static_words %opt{java_util_spliterator}
        set-option window java_util_spliterator_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:java\.util\.TimeZone)<ret>'
        evaluate-commands %opt{java_util_timezone_active}
        set-option -add window static_words %opt{java_util_timezone}
        set-option window java_util_timezone_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:java\.util\.[a-z]+\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:java\.util\.jar\.[A-Z])<ret>'
        try %{
          execute-keys -draft '%s (?:java\.util\.jar\.JarEntry)<ret>'
          evaluate-commands %opt{java_util_jar_jarentry_active}
          set-option -add window static_words %opt{java_util_jar_jarentry}
          set-option window java_util_jar_jarentry_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:java\.util\.jar\.JarFile)<ret>'
          evaluate-commands %opt{java_util_jar_jarfile_active}
          set-option -add window static_words %opt{java_util_jar_jarfile}
          set-option window java_util_jar_jarfile_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:java\.util\.jar\.JarInputStream)<ret>'
          evaluate-commands %opt{java_util_jar_jarinputstream_active}
          set-option -add window static_words %opt{java_util_jar_jarinputstream}
          set-option window java_util_jar_jarinputstream_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:java\.util\.jar\.JarOutputStream)<ret>'
          evaluate-commands %opt{java_util_jar_jaroutputstream_active}
          set-option -add window static_words %opt{java_util_jar_jaroutputstream}
          set-option window java_util_jar_jaroutputstream_active "nay"
        }
      }
      # -------------------------------- SECTION BREAK --------------------------------------------- #
      try %{
        execute-keys -draft '%s (?:java\.util\.logging\.[A-Z])<ret>'
        try %{
          execute-keys -draft '%s (?:java\.util\.logging\.ErrorManager)<ret>'
          evaluate-commands %opt{java_util_logging_errormanager_active}
          set-option -add window static_words %opt{java_util_logging_errormanager}
          set-option window java_util_logging_errormanager_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:java\.util\.logging\.Logger)<ret>'
          evaluate-commands %opt{java_util_logging_logger_active}
          set-option -add window static_words %opt{java_util_logging_logger}
          set-option window java_util_logging_logger_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:java\.util\.logging\.LogManager)<ret>'
          evaluate-commands %opt{java_util_logging_logmanager_active}
          set-option -add window static_words %opt{java_util_logging_logmanager}
          set-option window java_util_logging_logmanager_active "nay"
        }
      }
      # -------------------------------- SECTION BREAK --------------------------------------------- #
      try %{
        execute-keys -draft '%s (?:java\.util\.prefs\.[A-Z])<ret>'
        try %{
          execute-keys -draft '%s (?:java\.util\.prefs\.Preferences)<ret>'
          evaluate-commands %opt{java_util_prefs_preferences_active}
          set-option -add window static_words %opt{java_util_prefs_preferences}
          set-option window java_util_prefs_preferences_active "nay"
        }
      }
      # -------------------------------- SECTION BREAK --------------------------------------------- #
      try %{
        execute-keys -draft '%s (?:java\.util\.regex\.[A-Z])<ret>'
        try %{
          execute-keys -draft '%s (?:java\.util\.regex\.Pattern)<ret>'
          evaluate-commands %opt{java_util_regex_pattern_active}
          set-option -add window static_words %opt{java_util_regex_pattern}
          set-option window java_util_regex_pattern_active "nay"
        }
      }
      # -------------------------------- SECTION BREAK --------------------------------------------- #
      try %{
        execute-keys -draft '%s (?:java\.util\.zip\.[A-Z])<ret>'
        try %{
          execute-keys -draft '%s (?:java\.util\.zip\.Deflater)<ret>'
          evaluate-commands %opt{java_util_zip_deflater_active}
          set-option -add window static_words %opt{java_util_zip_deflater}
          set-option window java_util_zip_deflater_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:java\.util\.zip\.GZIPInputStream)<ret>'
          evaluate-commands %opt{java_util_zip_gzipinputstream_active}
          set-option -add window static_words %opt{java_util_zip_gzipinputstream}
          set-option window java_util_zip_gzipinputstream_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:java\.util\.zip\.ZipEntry)<ret>'
          evaluate-commands %opt{java_util_zip_zipentry_active}
          set-option -add window static_words %opt{java_util_zip_zipentry}
          set-option window java_util_zip_zipentry_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:java\.util\.zip\.ZipFile)<ret>'
          evaluate-commands %opt{java_util_zip_zipfile_active}
          set-option -add window static_words %opt{java_util_zip_zipfile}
          set-option window java_util_zip_zipfile_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:java\.util\.zip\.ZipInputStream)<ret>'
          evaluate-commands %opt{java_util_zip_zipinputstream_active}
          set-option -add window static_words %opt{java_util_zip_zipinputstream}
          set-option window java_util_zip_zipinputstream_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:java\.util\.zip\.ZipOutputStream)<ret>'
          evaluate-commands %opt{java_util_zip_zipoutputstream_active}
          set-option -add window static_words %opt{java_util_zip_zipoutputstream}
          set-option window java_util_zip_zipoutputstream_active "nay"
        }
      }
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:javax\.accessibility\.[A-Z])<ret>'
    try %{
      execute-keys -draft '%s (?:javax\.accessibility\.AccessibleContext)<ret>'
      evaluate-commands %opt{javax_accessibility_accessiblecontext_active}
      set-option -add window static_words %opt{javax_accessibility_accessiblecontext}
      set-option window javax_accessibility_accessiblecontext_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.accessibility\.AccessibleExtendedText)<ret>'
      evaluate-commands %opt{javax_accessibility_accessibleextendedtext_active}
      set-option -add window static_words %opt{javax_accessibility_accessibleextendedtext}
      set-option window javax_accessibility_accessibleextendedtext_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.accessibility\.AccessibleRelation)<ret>'
      evaluate-commands %opt{javax_accessibility_accessiblerelation_active}
      set-option -add window static_words %opt{javax_accessibility_accessiblerelation}
      set-option window javax_accessibility_accessiblerelation_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.accessibility\.AccessibleTableModelChange)<ret>'
      evaluate-commands %opt{javax_accessibility_accessibletablemodelchange_active}
      set-option -add window static_words %opt{javax_accessibility_accessibletablemodelchange}
      set-option window javax_accessibility_accessibletablemodelchange_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.accessibility\.AccessibleText)<ret>'
      evaluate-commands %opt{javax_accessibility_accessibletext_active}
      set-option -add window static_words %opt{javax_accessibility_accessibletext}
      set-option window javax_accessibility_accessibletext_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:javax\.crypto\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:javax\.crypto\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.crypto\.Cipher)<ret>'
        evaluate-commands %opt{javax_crypto_cipher_active}
        set-option -add window static_words %opt{javax_crypto_cipher}
        set-option window javax_crypto_cipher_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.crypto\.SecretKey)<ret>'
        evaluate-commands %opt{javax_crypto_secretkey_active}
        set-option -add window static_words %opt{javax_crypto_secretkey}
        set-option window javax_crypto_secretkey_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.crypto\.interfaces\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.crypto\.interfaces\.DHPrivateKey)<ret>'
        evaluate-commands %opt{javax_crypto_interfaces_dhprivatekey_active}
        set-option -add window static_words %opt{javax_crypto_interfaces_dhprivatekey}
        set-option window javax_crypto_interfaces_dhprivatekey_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.crypto\.interfaces\.DHPublicKey)<ret>'
        evaluate-commands %opt{javax_crypto_interfaces_dhpublickey_active}
        set-option -add window static_words %opt{javax_crypto_interfaces_dhpublickey}
        set-option window javax_crypto_interfaces_dhpublickey_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.crypto\.interfaces\.PBEKey)<ret>'
        evaluate-commands %opt{javax_crypto_interfaces_pbekey_active}
        set-option -add window static_words %opt{javax_crypto_interfaces_pbekey}
        set-option window javax_crypto_interfaces_pbekey_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.crypto\.spec\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.crypto\.spec\.DESedeKeySpec)<ret>'
        evaluate-commands %opt{javax_crypto_spec_desedekeyspec_active}
        set-option -add window static_words %opt{javax_crypto_spec_desedekeyspec}
        set-option window javax_crypto_spec_desedekeyspec_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.crypto\.spec\.DESKeySpec)<ret>'
        evaluate-commands %opt{javax_crypto_spec_deskeyspec_active}
        set-option -add window static_words %opt{javax_crypto_spec_deskeyspec}
        set-option window javax_crypto_spec_deskeyspec_active "nay"
      }
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:javax\.imageio\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:javax\.imageio\.ImageWriteParam)<ret>'
      evaluate-commands %opt{javax_imageio_imagewriteparam_active}
      set-option -add window static_words %opt{javax_imageio_imagewriteparam}
      set-option window javax_imageio_imagewriteparam_active "nay"
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.imageio\.metadata\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.imageio\.metadata\.IIOMetadataFormat)<ret>'
        evaluate-commands %opt{javax_imageio_metadata_iiometadataformat_active}
        set-option -add window static_words %opt{javax_imageio_metadata_iiometadataformat}
        set-option window javax_imageio_metadata_iiometadataformat_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.imageio\.metadata\.IIOMetadataFormatImpl)<ret>'
        evaluate-commands %opt{javax_imageio_metadata_iiometadataformatimpl_active}
        set-option -add window static_words %opt{javax_imageio_metadata_iiometadataformatimpl}
        set-option window javax_imageio_metadata_iiometadataformatimpl_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.imageio\.plugins\.tiff\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.imageio\.plugins\.tiff\.BaselineTIFFTagSet)<ret>'
        evaluate-commands %opt{javax_imageio_plugins_tiff_baselinetifftagset_active}
        set-option -add window static_words %opt{javax_imageio_plugins_tiff_baselinetifftagset}
        set-option window javax_imageio_plugins_tiff_baselinetifftagset_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.imageio\.plugins\.tiff\.ExifGPSTagSet)<ret>'
        evaluate-commands %opt{javax_imageio_plugins_tiff_exifgpstagset_active}
        set-option -add window static_words %opt{javax_imageio_plugins_tiff_exifgpstagset}
        set-option window javax_imageio_plugins_tiff_exifgpstagset_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.imageio\.plugins\.tiff\.ExifInteroperabilityTagSet)<ret>'
        evaluate-commands %opt{javax_imageio_plugins_tiff_exifinteroperabilitytagset_active}
        set-option -add window static_words %opt{javax_imageio_plugins_tiff_exifinteroperabilitytagset}
        set-option window javax_imageio_plugins_tiff_exifinteroperabilitytagset_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.imageio\.plugins\.tiff\.ExifParentTIFFTagSet)<ret>'
        evaluate-commands %opt{javax_imageio_plugins_tiff_exifparenttifftagset_active}
        set-option -add window static_words %opt{javax_imageio_plugins_tiff_exifparenttifftagset}
        set-option window javax_imageio_plugins_tiff_exifparenttifftagset_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.imageio\.plugins\.tiff\.ExifTIFFTagSet)<ret>'
        evaluate-commands %opt{javax_imageio_plugins_tiff_exiftifftagset_active}
        set-option -add window static_words %opt{javax_imageio_plugins_tiff_exiftifftagset}
        set-option window javax_imageio_plugins_tiff_exiftifftagset_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.imageio\.plugins\.tiff\.FaxTIFFTagSet)<ret>'
        evaluate-commands %opt{javax_imageio_plugins_tiff_faxtifftagset_active}
        set-option -add window static_words %opt{javax_imageio_plugins_tiff_faxtifftagset}
        set-option window javax_imageio_plugins_tiff_faxtifftagset_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.imageio\.plugins\.tiff\.GeoTIFFTagSet)<ret>'
        evaluate-commands %opt{javax_imageio_plugins_tiff_geotifftagset_active}
        set-option -add window static_words %opt{javax_imageio_plugins_tiff_geotifftagset}
        set-option window javax_imageio_plugins_tiff_geotifftagset_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.imageio\.plugins\.tiff\.TIFFTag)<ret>'
        evaluate-commands %opt{javax_imageio_plugins_tiff_tifftag_active}
        set-option -add window static_words %opt{javax_imageio_plugins_tiff_tifftag}
        set-option window javax_imageio_plugins_tiff_tifftag_active "nay"
      }
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:javax\.management\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:javax\.management\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.management\.AttributeChangeNotification)<ret>'
        evaluate-commands %opt{javax_management_attributechangenotification_active}
        set-option -add window static_words %opt{javax_management_attributechangenotification}
        set-option window javax_management_attributechangenotification_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.management\.JMX)<ret>'
        evaluate-commands %opt{javax_management_jmx_active}
        set-option -add window static_words %opt{javax_management_jmx}
        set-option window javax_management_jmx_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.management\.MBeanOperationInfo)<ret>'
        evaluate-commands %opt{javax_management_mbeanoperationinfo_active}
        set-option -add window static_words %opt{javax_management_mbeanoperationinfo}
        set-option window javax_management_mbeanoperationinfo_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.management\.MBeanServerNotification)<ret>'
        evaluate-commands %opt{javax_management_mbeanservernotification_active}
        set-option -add window static_words %opt{javax_management_mbeanservernotification}
        set-option window javax_management_mbeanservernotification_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.management\.Query)<ret>'
        evaluate-commands %opt{javax_management_query_active}
        set-option -add window static_words %opt{javax_management_query}
        set-option window javax_management_query_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.management\.monitor\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.management\.monitor\.Monitor)<ret>'
        evaluate-commands %opt{javax_management_monitor_monitor_active}
        set-option -add window static_words %opt{javax_management_monitor_monitor}
        set-option window javax_management_monitor_monitor_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.management\.monitor\.MonitorNotification)<ret>'
        evaluate-commands %opt{javax_management_monitor_monitornotification_active}
        set-option -add window static_words %opt{javax_management_monitor_monitornotification}
        set-option window javax_management_monitor_monitornotification_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.management\.relation\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.management\.relation\.RelationNotification)<ret>'
        evaluate-commands %opt{javax_management_relation_relationnotification_active}
        set-option -add window static_words %opt{javax_management_relation_relationnotification}
        set-option window javax_management_relation_relationnotification_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.management\.relation\.RoleInfo)<ret>'
        evaluate-commands %opt{javax_management_relation_roleinfo_active}
        set-option -add window static_words %opt{javax_management_relation_roleinfo}
        set-option window javax_management_relation_roleinfo_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.management\.relation\.RoleStatus)<ret>'
        evaluate-commands %opt{javax_management_relation_rolestatus_active}
        set-option -add window static_words %opt{javax_management_relation_rolestatus}
        set-option window javax_management_relation_rolestatus_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.management\.remote\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.management\.remote\.JMXConnectionNotification)<ret>'
        evaluate-commands %opt{javax_management_remote_jmxconnectionnotification_active}
        set-option -add window static_words %opt{javax_management_remote_jmxconnectionnotification}
        set-option window javax_management_remote_jmxconnectionnotification_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.management\.remote\.JMXConnector)<ret>'
        evaluate-commands %opt{javax_management_remote_jmxconnector_active}
        set-option -add window static_words %opt{javax_management_remote_jmxconnector}
        set-option window javax_management_remote_jmxconnector_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.management\.remote\.JMXConnectorFactory)<ret>'
        evaluate-commands %opt{javax_management_remote_jmxconnectorfactory_active}
        set-option -add window static_words %opt{javax_management_remote_jmxconnectorfactory}
        set-option window javax_management_remote_jmxconnectorfactory_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.management\.remote\.JMXConnectorServer)<ret>'
        evaluate-commands %opt{javax_management_remote_jmxconnectorserver_active}
        set-option -add window static_words %opt{javax_management_remote_jmxconnectorserver}
        set-option window javax_management_remote_jmxconnectorserver_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.management\.remote\.JMXConnectorServerFactory)<ret>'
        evaluate-commands %opt{javax_management_remote_jmxconnectorserverfactory_active}
        set-option -add window static_words %opt{javax_management_remote_jmxconnectorserverfactory}
        set-option window javax_management_remote_jmxconnectorserverfactory_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.management\.remote\.rmi\.RMIConnectorServer)<ret>'
      evaluate-commands %opt{javax_management_remote_rmi_rmiconnectorserver_active}
      set-option -add window static_words %opt{javax_management_remote_rmi_rmiconnectorserver}
      set-option window javax_management_remote_rmi_rmiconnectorserver_active "nay"
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.management\.timer\.Timer)<ret>'
      evaluate-commands %opt{javax_management_timer_timer_active}
      set-option -add window static_words %opt{javax_management_timer_timer}
      set-option window javax_management_timer_timer_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:javax\.naming\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:javax\.naming\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.naming\.Context)<ret>'
        evaluate-commands %opt{javax_naming_context_active}
        set-option -add window static_words %opt{javax_naming_context}
        set-option window javax_naming_context_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.naming\.Name)<ret>'
        evaluate-commands %opt{javax_naming_name_active}
        set-option -add window static_words %opt{javax_naming_name}
        set-option window javax_naming_name_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.naming\.directory\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.naming\.directory\.Attribute)<ret>'
        evaluate-commands %opt{javax_naming_directory_attribute_active}
        set-option -add window static_words %opt{javax_naming_directory_attribute}
        set-option window javax_naming_directory_attribute_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.naming\.directory\.DirContext)<ret>'
        evaluate-commands %opt{javax_naming_directory_dircontext_active}
        set-option -add window static_words %opt{javax_naming_directory_dircontext}
        set-option window javax_naming_directory_dircontext_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.naming\.directory\.SearchControls)<ret>'
        evaluate-commands %opt{javax_naming_directory_searchcontrols_active}
        set-option -add window static_words %opt{javax_naming_directory_searchcontrols}
        set-option window javax_naming_directory_searchcontrols_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.naming\.event\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.naming\.event\.EventContext)<ret>'
        evaluate-commands %opt{javax_naming_event_eventcontext_active}
        set-option -add window static_words %opt{javax_naming_event_eventcontext}
        set-option window javax_naming_event_eventcontext_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.naming\.event\.NamingEvent)<ret>'
        evaluate-commands %opt{javax_naming_event_namingevent_active}
        set-option -add window static_words %opt{javax_naming_event_namingevent}
        set-option window javax_naming_event_namingevent_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.naming\.ldap\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.naming\.ldap\.Control)<ret>'
        evaluate-commands %opt{javax_naming_ldap_control_active}
        set-option -add window static_words %opt{javax_naming_ldap_control}
        set-option window javax_naming_ldap_control_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.naming\.ldap\.LdapContext)<ret>'
        evaluate-commands %opt{javax_naming_ldap_ldapcontext_active}
        set-option -add window static_words %opt{javax_naming_ldap_ldapcontext}
        set-option window javax_naming_ldap_ldapcontext_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.naming\.ldap\.ManageReferralControl)<ret>'
        evaluate-commands %opt{javax_naming_ldap_managereferralcontrol_active}
        set-option -add window static_words %opt{javax_naming_ldap_managereferralcontrol}
        set-option window javax_naming_ldap_managereferralcontrol_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.naming\.ldap\.PagedResultsControl)<ret>'
        evaluate-commands %opt{javax_naming_ldap_pagedresultscontrol_active}
        set-option -add window static_words %opt{javax_naming_ldap_pagedresultscontrol}
        set-option window javax_naming_ldap_pagedresultscontrol_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.naming\.ldap\.PagedResultsResponseControl)<ret>'
        evaluate-commands %opt{javax_naming_ldap_pagedresultsresponsecontrol_active}
        set-option -add window static_words %opt{javax_naming_ldap_pagedresultsresponsecontrol}
        set-option window javax_naming_ldap_pagedresultsresponsecontrol_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.naming\.ldap\.SortControl)<ret>'
        evaluate-commands %opt{javax_naming_ldap_sortcontrol_active}
        set-option -add window static_words %opt{javax_naming_ldap_sortcontrol}
        set-option window javax_naming_ldap_sortcontrol_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.naming\.ldap\.SortResponseControl)<ret>'
        evaluate-commands %opt{javax_naming_ldap_sortresponsecontrol_active}
        set-option -add window static_words %opt{javax_naming_ldap_sortresponsecontrol}
        set-option window javax_naming_ldap_sortresponsecontrol_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.naming\.ldap\.StartTlsRequest)<ret>'
        evaluate-commands %opt{javax_naming_ldap_starttlsrequest_active}
        set-option -add window static_words %opt{javax_naming_ldap_starttlsrequest}
        set-option window javax_naming_ldap_starttlsrequest_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.naming\.ldap\.StartTlsResponse)<ret>'
        evaluate-commands %opt{javax_naming_ldap_starttlsresponse_active}
        set-option -add window static_words %opt{javax_naming_ldap_starttlsresponse}
        set-option window javax_naming_ldap_starttlsresponse_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.naming\.spi\.NamingManager)<ret>'
      evaluate-commands %opt{javax_naming_spi_namingmanager_active}
      set-option -add window static_words %opt{javax_naming_spi_namingmanager}
      set-option window javax_naming_spi_namingmanager_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:javax\.net\.[a-z]+\.[A-Z])<ret>'
    try %{
      execute-keys -draft '%s (?:javax\.net\.ssl\.StandardConstants)<ret>'
      evaluate-commands %opt{javax_net_ssl_standardconstants_active}
      set-option -add window static_words %opt{javax_net_ssl_standardconstants}
      set-option window javax_net_ssl_standardconstants_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:javax\.print\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:javax\.print\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.print\.ServiceUIFactory)<ret>'
        evaluate-commands %opt{javax_print_serviceuifactory_active}
        set-option -add window static_words %opt{javax_print_serviceuifactory}
        set-option window javax_print_serviceuifactory_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.print\.URIException)<ret>'
        evaluate-commands %opt{javax_print_uriexception_active}
        set-option -add window static_words %opt{javax_print_uriexception}
        set-option window javax_print_uriexception_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.print\.attribute\.[A-Za-z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.print\.attribute\.ResolutionSyntax)<ret>'
        evaluate-commands %opt{javax_print_attribute_resolutionsyntax_active}
        set-option -add window static_words %opt{javax_print_attribute_resolutionsyntax}
        set-option window javax_print_attribute_resolutionsyntax_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.print\.attribute\.Size2DSyntax)<ret>'
        evaluate-commands %opt{javax_print_attribute_size2dsyntax_active}
        set-option -add window static_words %opt{javax_print_attribute_size2dsyntax}
        set-option window javax_print_attribute_size2dsyntax_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.print\.attribute\.standard\.MediaPrintableArea)<ret>'
        evaluate-commands %opt{javax_print_attribute_standard_mediaprintablearea_active}
        set-option -add window static_words %opt{javax_print_attribute_standard_mediaprintablearea}
        set-option window javax_print_attribute_standard_mediaprintablearea_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.print\.event\.PrintJobEvent)<ret>'
      evaluate-commands %opt{javax_print_event_printjobevent_active}
      set-option -add window static_words %opt{javax_print_event_printjobevent}
      set-option window javax_print_event_printjobevent_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:javax\.script\.[A-Z])<ret>'
    try %{
      execute-keys -draft '%s (?:javax\.script\.ScriptContext)<ret>'
      evaluate-commands %opt{javax_script_scriptcontext_active}
      set-option -add window static_words %opt{javax_script_scriptcontext}
      set-option window javax_script_scriptcontext_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.script\.ScriptEngine)<ret>'
      evaluate-commands %opt{javax_script_scriptengine_active}
      set-option -add window static_words %opt{javax_script_scriptengine}
      set-option window javax_script_scriptengine_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:javax\.security\.[a-z]+\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:javax\.security\.auth\.callback\.ConfirmationCallback)<ret>'
      evaluate-commands %opt{javax_security_auth_callback_confirmationcallback_active}
      set-option -add window static_words %opt{javax_security_auth_callback_confirmationcallback}
      set-option window javax_security_auth_callback_confirmationcallback_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.security\.auth\.callback\.TextOutputCallback)<ret>'
      evaluate-commands %opt{javax_security_auth_callback_textoutputcallback_active}
      set-option -add window static_words %opt{javax_security_auth_callback_textoutputcallback}
      set-option window javax_security_auth_callback_textoutputcallback_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.security\.auth\.kerberos\.KerberosPrincipal)<ret>'
      evaluate-commands %opt{javax_security_auth_kerberos_kerberosprincipal_active}
      set-option -add window static_words %opt{javax_security_auth_kerberos_kerberosprincipal}
      set-option window javax_security_auth_kerberos_kerberosprincipal_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.security\.auth\.x500\.X500Principal)<ret>'
      evaluate-commands %opt{javax_security_auth_x500_x500principal_active}
      set-option -add window static_words %opt{javax_security_auth_x500_x500principal}
      set-option window javax_security_auth_x500_x500principal_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.security\.sasl\.Sasl)<ret>'
      evaluate-commands %opt{javax_security_sasl_sasl_active}
      set-option -add window static_words %opt{javax_security_sasl_sasl}
      set-option window javax_security_sasl_sasl_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:javax\.sound\.[a-z]+\.[A-Z])<ret>'
    try %{
      execute-keys -draft '%s (?:javax\.sound\.midi\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.sound\.midi\.MetaMessage)<ret>'
        evaluate-commands %opt{javax_sound_midi_metamessage_active}
        set-option -add window static_words %opt{javax_sound_midi_metamessage}
        set-option window javax_sound_midi_metamessage_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.sound\.midi\.MidiFileFormat)<ret>'
        evaluate-commands %opt{javax_sound_midi_midifileformat_active}
        set-option -add window static_words %opt{javax_sound_midi_midifileformat}
        set-option window javax_sound_midi_midifileformat_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.sound\.midi\.Sequence)<ret>'
        evaluate-commands %opt{javax_sound_midi_sequence_active}
        set-option -add window static_words %opt{javax_sound_midi_sequence}
        set-option window javax_sound_midi_sequence_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.sound\.midi\.Sequencer)<ret>'
        evaluate-commands %opt{javax_sound_midi_sequencer_active}
        set-option -add window static_words %opt{javax_sound_midi_sequencer}
        set-option window javax_sound_midi_sequencer_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.sound\.midi\.ShortMessage)<ret>'
        evaluate-commands %opt{javax_sound_midi_shortmessage_active}
        set-option -add window static_words %opt{javax_sound_midi_shortmessage}
        set-option window javax_sound_midi_shortmessage_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.sound\.midi\.SysexMessage)<ret>'
        evaluate-commands %opt{javax_sound_midi_sysexmessage_active}
        set-option -add window static_words %opt{javax_sound_midi_sysexmessage}
        set-option window javax_sound_midi_sysexmessage_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.sound\.sampled\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.sound\.sampled\.AudioSystem)<ret>'
        evaluate-commands %opt{javax_sound_sampled_audiosystem_active}
        set-option -add window static_words %opt{javax_sound_sampled_audiosystem}
        set-option window javax_sound_sampled_audiosystem_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.sound\.sampled\.Clip)<ret>'
        evaluate-commands %opt{javax_sound_sampled_clip_active}
        set-option -add window static_words %opt{javax_sound_sampled_clip}
        set-option window javax_sound_sampled_clip_active "nay"
      }
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:javax\.sql\.rowset\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:javax\.sql\.rowset\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.sql\.rowset\.BaseRowSet)<ret>'
        evaluate-commands %opt{javax_sql_rowset_baserowset_active}
        set-option -add window static_words %opt{javax_sql_rowset_baserowset}
        set-option window javax_sql_rowset_baserowset_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.sql\.rowset\.CachedRowSet)<ret>'
        evaluate-commands %opt{javax_sql_rowset_cachedrowset_active}
        set-option -add window static_words %opt{javax_sql_rowset_cachedrowset}
        set-option window javax_sql_rowset_cachedrowset_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.sql\.rowset\.JoinRowSet)<ret>'
        evaluate-commands %opt{javax_sql_rowset_joinrowset_active}
        set-option -add window static_words %opt{javax_sql_rowset_joinrowset}
        set-option window javax_sql_rowset_joinrowset_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.sql\.rowset\.WebRowSet)<ret>'
        evaluate-commands %opt{javax_sql_rowset_webrowset_active}
        set-option -add window static_words %opt{javax_sql_rowset_webrowset}
        set-option window javax_sql_rowset_webrowset_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.sql\.rowset\.spi\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.sql\.rowset\.spi\.SyncFactory)<ret>'
        evaluate-commands %opt{javax_sql_rowset_spi_syncfactory_active}
        set-option -add window static_words %opt{javax_sql_rowset_spi_syncfactory}
        set-option window javax_sql_rowset_spi_syncfactory_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.sql\.rowset\.spi\.SyncProvider)<ret>'
        evaluate-commands %opt{javax_sql_rowset_spi_syncprovider_active}
        set-option -add window static_words %opt{javax_sql_rowset_spi_syncprovider}
        set-option window javax_sql_rowset_spi_syncprovider_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.sql\.rowset\.spi\.SyncResolver)<ret>'
        evaluate-commands %opt{javax_sql_rowset_spi_syncresolver_active}
        set-option -add window static_words %opt{javax_sql_rowset_spi_syncresolver}
        set-option window javax_sql_rowset_spi_syncresolver_active "nay"
      }
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:javax\.swing\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:javax\.swing\.[A-I])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.swing\.AbstractButton)<ret>'
        evaluate-commands %opt{javax_swing_abstractbutton_active}
        set-option -add window static_words %opt{javax_swing_abstractbutton}
        set-option window javax_swing_abstractbutton_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.Action)<ret>'
        evaluate-commands %opt{javax_swing_action_active}
        set-option -add window static_words %opt{javax_swing_action}
        set-option window javax_swing_action_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.BoxLayout)<ret>'
        evaluate-commands %opt{javax_swing_boxlayout_active}
        set-option -add window static_words %opt{javax_swing_boxlayout}
        set-option window javax_swing_boxlayout_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.DebugGraphics)<ret>'
        evaluate-commands %opt{javax_swing_debuggraphics_active}
        set-option -add window static_words %opt{javax_swing_debuggraphics}
        set-option window javax_swing_debuggraphics_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.DefaultButtonModel)<ret>'
        evaluate-commands %opt{javax_swing_defaultbuttonmodel_active}
        set-option -add window static_words %opt{javax_swing_defaultbuttonmodel}
        set-option window javax_swing_defaultbuttonmodel_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.FocusManager)<ret>'
        evaluate-commands %opt{javax_swing_focusmanager_active}
        set-option -add window static_words %opt{javax_swing_focusmanager}
        set-option window javax_swing_focusmanager_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.GroupLayout)<ret>'
        evaluate-commands %opt{javax_swing_grouplayout_active}
        set-option -add window static_words %opt{javax_swing_grouplayout}
        set-option window javax_swing_grouplayout_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.swing\.[J-K])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.swing\.JCheckBox)<ret>'
        evaluate-commands %opt{javax_swing_jcheckbox_active}
        set-option -add window static_words %opt{javax_swing_jcheckbox}
        set-option window javax_swing_jcheckbox_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.JColorChooser)<ret>'
        evaluate-commands %opt{javax_swing_jcolorchooser_active}
        set-option -add window static_words %opt{javax_swing_jcolorchooser}
        set-option window javax_swing_jcolorchooser_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.JComponent)<ret>'
        evaluate-commands %opt{javax_swing_jcomponent_active}
        set-option -add window static_words %opt{javax_swing_jcomponent}
        set-option window javax_swing_jcomponent_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.JDesktopPane)<ret>'
        evaluate-commands %opt{javax_swing_jdesktoppane_active}
        set-option -add window static_words %opt{javax_swing_jdesktoppane}
        set-option window javax_swing_jdesktoppane_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.JEditorPane)<ret>'
        evaluate-commands %opt{javax_swing_jeditorpane_active}
        set-option -add window static_words %opt{javax_swing_jeditorpane}
        set-option window javax_swing_jeditorpane_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.JFileChooser)<ret>'
        evaluate-commands %opt{javax_swing_jfilechooser_active}
        set-option -add window static_words %opt{javax_swing_jfilechooser}
        set-option window javax_swing_jfilechooser_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.JFormattedTextField)<ret>'
        evaluate-commands %opt{javax_swing_jformattedtextfield_active}
        set-option -add window static_words %opt{javax_swing_jformattedtextfield}
        set-option window javax_swing_jformattedtextfield_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.JInternalFrame)<ret>'
        evaluate-commands %opt{javax_swing_jinternalframe_active}
        set-option -add window static_words %opt{javax_swing_jinternalframe}
        set-option window javax_swing_jinternalframe_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.JLayeredPane)<ret>'
        evaluate-commands %opt{javax_swing_jlayeredpane_active}
        set-option -add window static_words %opt{javax_swing_jlayeredpane}
        set-option window javax_swing_jlayeredpane_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.JList)<ret>'
        evaluate-commands %opt{javax_swing_jlist_active}
        set-option -add window static_words %opt{javax_swing_jlist}
        set-option window javax_swing_jlist_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.JOptionPane)<ret>'
        evaluate-commands %opt{javax_swing_joptionpane_active}
        set-option -add window static_words %opt{javax_swing_joptionpane}
        set-option window javax_swing_joptionpane_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.JRootPane)<ret>'
        evaluate-commands %opt{javax_swing_jrootpane_active}
        set-option -add window static_words %opt{javax_swing_jrootpane}
        set-option window javax_swing_jrootpane_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.JSplitPane)<ret>'
        evaluate-commands %opt{javax_swing_jsplitpane_active}
        set-option -add window static_words %opt{javax_swing_jsplitpane}
        set-option window javax_swing_jsplitpane_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.JTabbedPane)<ret>'
        evaluate-commands %opt{javax_swing_jtabbedpane_active}
        set-option -add window static_words %opt{javax_swing_jtabbedpane}
        set-option window javax_swing_jtabbedpane_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.JTable)<ret>'
        evaluate-commands %opt{javax_swing_jtable_active}
        set-option -add window static_words %opt{javax_swing_jtable}
        set-option window javax_swing_jtable_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.JTextField)<ret>'
        evaluate-commands %opt{javax_swing_jtextfield_active}
        set-option -add window static_words %opt{javax_swing_jtextfield}
        set-option window javax_swing_jtextfield_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.JTree)<ret>'
        evaluate-commands %opt{javax_swing_jtree_active}
        set-option -add window static_words %opt{javax_swing_jtree}
        set-option window javax_swing_jtree_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.JViewport)<ret>'
        evaluate-commands %opt{javax_swing_jviewport_active}
        set-option -add window static_words %opt{javax_swing_jviewport}
        set-option window javax_swing_jviewport_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.swing\.[L-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.swing\.ListSelectionModel)<ret>'
        evaluate-commands %opt{javax_swing_listselectionmodel_active}
        set-option -add window static_words %opt{javax_swing_listselectionmodel}
        set-option window javax_swing_listselectionmodel_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.ScrollPaneConstants)<ret>'
        evaluate-commands %opt{javax_swing_scrollpaneconstants_active}
        set-option -add window static_words %opt{javax_swing_scrollpaneconstants}
        set-option window javax_swing_scrollpaneconstants_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.Spring)<ret>'
        evaluate-commands %opt{javax_swing_spring_active}
        set-option -add window static_words %opt{javax_swing_spring}
        set-option window javax_swing_spring_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.SpringLayout)<ret>'
        evaluate-commands %opt{javax_swing_springlayout_active}
        set-option -add window static_words %opt{javax_swing_springlayout}
        set-option window javax_swing_springlayout_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.SwingConstants)<ret>'
        evaluate-commands %opt{javax_swing_swingconstants_active}
        set-option -add window static_words %opt{javax_swing_swingconstants}
        set-option window javax_swing_swingconstants_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.TransferHandler)<ret>'
        evaluate-commands %opt{javax_swing_transferhandler_active}
        set-option -add window static_words %opt{javax_swing_transferhandler}
        set-option window javax_swing_transferhandler_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.WindowConstants)<ret>'
        evaluate-commands %opt{javax_swing_windowconstants_active}
        set-option -add window static_words %opt{javax_swing_windowconstants}
        set-option window javax_swing_windowconstants_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.swing\.[a-z]+\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.swing\.border\.[A-Z])<ret>'
        try %{
          execute-keys -draft '%s (?:javax\.swing\.border\.BevelBorder)<ret>'
          evaluate-commands %opt{javax_swing_border_bevelborder_active}
          set-option -add window static_words %opt{javax_swing_border_bevelborder}
          set-option window javax_swing_border_bevelborder_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.border\.EtchedBorder)<ret>'
          evaluate-commands %opt{javax_swing_border_etchedborder_active}
          set-option -add window static_words %opt{javax_swing_border_etchedborder}
          set-option window javax_swing_border_etchedborder_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.border\.TitledBorder)<ret>'
          evaluate-commands %opt{javax_swing_border_titledborder_active}
          set-option -add window static_words %opt{javax_swing_border_titledborder}
          set-option window javax_swing_border_titledborder_active "nay"
        }
      }
      # -------------------------------- SECTION BREAK --------------------------------------------- #
      try %{
        execute-keys -draft '%s (?:javax\.swing\.colorchooser\.[A-Z])<ret>'
        try %{
          execute-keys -draft '%s (?:javax\.swing\.colorchooser\.AbstractColorChooserPanel)<ret>'
          evaluate-commands %opt{javax_swing_colorchooser_abstractcolorchooserpanel_active}
          set-option -add window static_words %opt{javax_swing_colorchooser_abstractcolorchooserpanel}
          set-option window javax_swing_colorchooser_abstractcolorchooserpanel_active "nay"
        }
      }
      # -------------------------------- SECTION BREAK --------------------------------------------- #
      try %{
        execute-keys -draft '%s (?:javax\.swing\.event\.[A-Z])<ret>'
        try %{
          execute-keys -draft '%s (?:javax\.swing\.event\.AncestorEvent)<ret>'
          evaluate-commands %opt{javax_swing_event_ancestorevent_active}
          set-option -add window static_words %opt{javax_swing_event_ancestorevent}
          set-option window javax_swing_event_ancestorevent_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.event\.InternalFrameEvent)<ret>'
          evaluate-commands %opt{javax_swing_event_internalframeevent_active}
          set-option -add window static_words %opt{javax_swing_event_internalframeevent}
          set-option window javax_swing_event_internalframeevent_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.event\.ListDataEvent)<ret>'
          evaluate-commands %opt{javax_swing_event_listdataevent_active}
          set-option -add window static_words %opt{javax_swing_event_listdataevent}
          set-option window javax_swing_event_listdataevent_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.event\.TableModelEvent)<ret>'
          evaluate-commands %opt{javax_swing_event_tablemodelevent_active}
          set-option -add window static_words %opt{javax_swing_event_tablemodelevent}
          set-option window javax_swing_event_tablemodelevent_active "nay"
        }
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.swing\.plaf\.[a-z]+\.[A-Za-z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.swing\.plaf\.basic\.[A-Z])<ret>'
        try %{
          execute-keys -draft '%s (?:javax\.swing\.plaf\.basic\.BasicComboPopup)<ret>'
          evaluate-commands %opt{javax_swing_plaf_basic_basiccombopopup_active}
          set-option -add window static_words %opt{javax_swing_plaf_basic_basiccombopopup}
          set-option window javax_swing_plaf_basic_basiccombopopup_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.plaf\.basic\.BasicHTML)<ret>'
          evaluate-commands %opt{javax_swing_plaf_basic_basichtml_active}
          set-option -add window static_words %opt{javax_swing_plaf_basic_basichtml}
          set-option window javax_swing_plaf_basic_basichtml_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.plaf\.basic\.BasicInternalFrameUI\.BorderListener)<ret>'
          evaluate-commands %opt{javax_swing_plaf_basic_basicinternalframeui_borderlistener_active}
          set-option -add window static_words %opt{javax_swing_plaf_basic_basicinternalframeui_borderlistener}
          set-option window javax_swing_plaf_basic_basicinternalframeui_borderlistener_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.plaf\.basic\.BasicListUI)<ret>'
          evaluate-commands %opt{javax_swing_plaf_basic_basiclistui_active}
          set-option -add window static_words %opt{javax_swing_plaf_basic_basiclistui}
          set-option window javax_swing_plaf_basic_basiclistui_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.plaf\.basic\.BasicOptionPaneUI)<ret>'
          evaluate-commands %opt{javax_swing_plaf_basic_basicoptionpaneui_active}
          set-option -add window static_words %opt{javax_swing_plaf_basic_basicoptionpaneui}
          set-option window javax_swing_plaf_basic_basicoptionpaneui_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.plaf\.basic\.BasicScrollBarUI)<ret>'
          evaluate-commands %opt{javax_swing_plaf_basic_basicscrollbarui_active}
          set-option -add window static_words %opt{javax_swing_plaf_basic_basicscrollbarui}
          set-option window javax_swing_plaf_basic_basicscrollbarui_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.plaf\.basic\.BasicSliderUI)<ret>'
          evaluate-commands %opt{javax_swing_plaf_basic_basicsliderui_active}
          set-option -add window static_words %opt{javax_swing_plaf_basic_basicsliderui}
          set-option window javax_swing_plaf_basic_basicsliderui_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.plaf\.basic\.BasicSplitPaneDivider)<ret>'
          evaluate-commands %opt{javax_swing_plaf_basic_basicsplitpanedivider_active}
          set-option -add window static_words %opt{javax_swing_plaf_basic_basicsplitpanedivider}
          set-option window javax_swing_plaf_basic_basicsplitpanedivider_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.plaf\.basic\.BasicSplitPaneUI)<ret>'
          evaluate-commands %opt{javax_swing_plaf_basic_basicsplitpaneui_active}
          set-option -add window static_words %opt{javax_swing_plaf_basic_basicsplitpaneui}
          set-option window javax_swing_plaf_basic_basicsplitpaneui_active "nay"
        }
      }
      # -------------------------------- SECTION BREAK --------------------------------------------- #
      try %{
        execute-keys -draft '%s (?:javax\.swing\.plaf\.metal\.[A-Z])<ret>'
        try %{
          execute-keys -draft '%s (?:javax\.swing\.plaf\.metal\.MetalIconFactory)<ret>'
          evaluate-commands %opt{javax_swing_plaf_metal_metaliconfactory_active}
          set-option -add window static_words %opt{javax_swing_plaf_metal_metaliconfactory}
          set-option window javax_swing_plaf_metal_metaliconfactory_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.plaf\.metal\.MetalScrollBarUI)<ret>'
          evaluate-commands %opt{javax_swing_plaf_metal_metalscrollbarui_active}
          set-option -add window static_words %opt{javax_swing_plaf_metal_metalscrollbarui}
          set-option window javax_swing_plaf_metal_metalscrollbarui_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.plaf\.metal\.MetalSliderUI)<ret>'
          evaluate-commands %opt{javax_swing_plaf_metal_metalsliderui_active}
          set-option -add window static_words %opt{javax_swing_plaf_metal_metalsliderui}
          set-option window javax_swing_plaf_metal_metalsliderui_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.plaf\.metal\.MetalToolTipUI)<ret>'
          evaluate-commands %opt{javax_swing_plaf_metal_metaltooltipui_active}
          set-option -add window static_words %opt{javax_swing_plaf_metal_metaltooltipui}
          set-option window javax_swing_plaf_metal_metaltooltipui_active "nay"
        }
      }
      # -------------------------------- SECTION BREAK --------------------------------------------- #
      try %{
        execute-keys -draft '%s (?:javax\.swing\.plaf\.nimbus\.NimbusStyle)<ret>'
        evaluate-commands %opt{javax_swing_plaf_nimbus_nimbusstyle_active}
        set-option -add window static_words %opt{javax_swing_plaf_nimbus_nimbusstyle}
        set-option window javax_swing_plaf_nimbus_nimbusstyle_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.plaf\.synth\.SynthConstants)<ret>'
        evaluate-commands %opt{javax_swing_plaf_synth_synthconstants_active}
        set-option -add window static_words %opt{javax_swing_plaf_synth_synthconstants}
        set-option window javax_swing_plaf_synth_synthconstants_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.swing\.table\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.swing\.table\.TableColumn)<ret>'
        evaluate-commands %opt{javax_swing_table_tablecolumn_active}
        set-option -add window static_words %opt{javax_swing_table_tablecolumn}
        set-option window javax_swing_table_tablecolumn_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.swing\.text\.[A-Za-z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.swing\.text\.[A-Z])<ret>'
        try %{
          execute-keys -draft '%s (?:javax\.swing\.text\.AbstractDocument)<ret>'
          evaluate-commands %opt{javax_swing_text_abstractdocument_active}
          set-option -add window static_words %opt{javax_swing_text_abstractdocument}
          set-option window javax_swing_text_abstractdocument_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.text\.AbstractWriter)<ret>'
          evaluate-commands %opt{javax_swing_text_abstractwriter_active}
          set-option -add window static_words %opt{javax_swing_text_abstractwriter}
          set-option window javax_swing_text_abstractwriter_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.text\.DefaultCaret)<ret>'
          evaluate-commands %opt{javax_swing_text_defaultcaret_active}
          set-option -add window static_words %opt{javax_swing_text_defaultcaret}
          set-option window javax_swing_text_defaultcaret_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.text\.DefaultEditorKit)<ret>'
          evaluate-commands %opt{javax_swing_text_defaulteditorkit_active}
          set-option -add window static_words %opt{javax_swing_text_defaulteditorkit}
          set-option window javax_swing_text_defaulteditorkit_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.text\.DefaultStyledDocument)<ret>'
          evaluate-commands %opt{javax_swing_text_defaultstyleddocument_active}
          set-option -add window static_words %opt{javax_swing_text_defaultstyleddocument}
          set-option window javax_swing_text_defaultstyleddocument_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.text\.DefaultStyledDocument\.ElementSpec)<ret>'
          evaluate-commands %opt{javax_swing_text_defaultstyleddocument_elementspec_active}
          set-option -add window static_words %opt{javax_swing_text_defaultstyleddocument_elementspec}
          set-option window javax_swing_text_defaultstyleddocument_elementspec_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.text\.Document)<ret>'
          evaluate-commands %opt{javax_swing_text_document_active}
          set-option -add window static_words %opt{javax_swing_text_document}
          set-option window javax_swing_text_document_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.text\.JTextComponent)<ret>'
          evaluate-commands %opt{javax_swing_text_jtextcomponent_active}
          set-option -add window static_words %opt{javax_swing_text_jtextcomponent}
          set-option window javax_swing_text_jtextcomponent_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.text\.PlainDocument)<ret>'
          evaluate-commands %opt{javax_swing_text_plaindocument_active}
          set-option -add window static_words %opt{javax_swing_text_plaindocument}
          set-option window javax_swing_text_plaindocument_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.text\.StyleConstants)<ret>'
          evaluate-commands %opt{javax_swing_text_styleconstants_active}
          set-option -add window static_words %opt{javax_swing_text_styleconstants}
          set-option window javax_swing_text_styleconstants_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.text\.StyleContext)<ret>'
          evaluate-commands %opt{javax_swing_text_stylecontext_active}
          set-option -add window static_words %opt{javax_swing_text_stylecontext}
          set-option window javax_swing_text_stylecontext_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.text\.TabStop)<ret>'
          evaluate-commands %opt{javax_swing_text_tabstop_active}
          set-option -add window static_words %opt{javax_swing_text_tabstop}
          set-option window javax_swing_text_tabstop_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.text\.View)<ret>'
          evaluate-commands %opt{javax_swing_text_view_active}
          set-option -add window static_words %opt{javax_swing_text_view}
          set-option window javax_swing_text_view_active "nay"
        }
      }
      # ------------------------------ SECTION BREAK ----------------------------------------------- #
      try %{
        execute-keys -draft '%s (?:javax\.swing\.text\.html\.[A-Za-z])<ret>'
        try %{
          execute-keys -draft '%s (?:javax\.swing\.text\.html\.HTML)<ret>'
          evaluate-commands %opt{javax_swing_text_html_html_active}
          set-option -add window static_words %opt{javax_swing_text_html_html}
          set-option window javax_swing_text_html_html_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.text\.html\.HTMLDocument)<ret>'
          evaluate-commands %opt{javax_swing_text_html_htmldocument_active}
          set-option -add window static_words %opt{javax_swing_text_html_htmldocument}
          set-option window javax_swing_text_html_htmldocument_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.text\.html\.HTMLEditorKit)<ret>'
          evaluate-commands %opt{javax_swing_text_html_htmleditorkit_active}
          set-option -add window static_words %opt{javax_swing_text_html_htmleditorkit}
          set-option window javax_swing_text_html_htmleditorkit_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.text\.html\.parser\.DTD)<ret>'
          evaluate-commands %opt{javax_swing_text_html_parser_dtd_active}
          set-option -add window static_words %opt{javax_swing_text_html_parser_dtd}
          set-option window javax_swing_text_html_parser_dtd_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.swing\.text\.html\.parser\.DTDConstants)<ret>'
          evaluate-commands %opt{javax_swing_text_html_parser_dtdconstants_active}
          set-option -add window static_words %opt{javax_swing_text_html_parser_dtdconstants}
          set-option window javax_swing_text_html_parser_dtdconstants_active "nay"
        }
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.swing\.tree\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.swing\.tree\.DefaultTreeSelectionModel)<ret>'
        evaluate-commands %opt{javax_swing_tree_defaulttreeselectionmodel_active}
        set-option -add window static_words %opt{javax_swing_tree_defaulttreeselectionmodel}
        set-option window javax_swing_tree_defaulttreeselectionmodel_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.tree\.TreeSelectionModel)<ret>'
        evaluate-commands %opt{javax_swing_tree_treeselectionmodel_active}
        set-option -add window static_words %opt{javax_swing_tree_treeselectionmodel}
        set-option window javax_swing_tree_treeselectionmodel_active "nay"
      }
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.swing\.undo\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.swing\.undo\.AbstractUndoableEdit)<ret>'
        evaluate-commands %opt{javax_swing_undo_abstractundoableedit_active}
        set-option -add window static_words %opt{javax_swing_undo_abstractundoableedit}
        set-option window javax_swing_undo_abstractundoableedit_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.undo\.StateEdit)<ret>'
        evaluate-commands %opt{javax_swing_undo_stateedit_active}
        set-option -add window static_words %opt{javax_swing_undo_stateedit}
        set-option window javax_swing_undo_stateedit_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.swing\.undo\.StateEditable)<ret>'
        evaluate-commands %opt{javax_swing_undo_stateeditable_active}
        set-option -add window static_words %opt{javax_swing_undo_stateeditable}
        set-option window javax_swing_undo_stateeditable_active "nay"
      }
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:javax\.tools\.[A-Z])<ret>'
    try %{
      execute-keys -draft '%s (?:javax\.tools\.Diagnostic)<ret>'
      evaluate-commands %opt{javax_tools_diagnostic_active}
      set-option -add window static_words %opt{javax_tools_diagnostic}
      set-option window javax_tools_diagnostic_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:javax\.transaction\.xa\.[A-Z])<ret>'
    try %{
      execute-keys -draft '%s (?:javax\.transaction\.xa\.XAException)<ret>'
      evaluate-commands %opt{javax_transaction_xa_xaexception_active}
      set-option -add window static_words %opt{javax_transaction_xa_xaexception}
      set-option window javax_transaction_xa_xaexception_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.transaction\.xa\.XAResource)<ret>'
      evaluate-commands %opt{javax_transaction_xa_xaresource_active}
      set-option -add window static_words %opt{javax_transaction_xa_xaresource}
      set-option window javax_transaction_xa_xaresource_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.transaction\.xa\.Xid)<ret>'
      evaluate-commands %opt{javax_transaction_xa_xid_active}
      set-option -add window static_words %opt{javax_transaction_xa_xid}
      set-option window javax_transaction_xa_xid_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:javax\.xml\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:javax\.xml\.XMLConstants)<ret>'
      evaluate-commands %opt{javax_xml_xmlconstants_active}
      set-option -add window static_words %opt{javax_xml_xmlconstants}
      set-option window javax_xml_xmlconstants_active "nay"
    }
    # -------------------------------- SECTION BREAK ----------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.[A-Za-z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.[A-Z])<ret>'
        try %{
          execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.CanonicalizationMethod)<ret>'
          evaluate-commands %opt{javax_xml_crypto_dsig_canonicalizationmethod_active}
          set-option -add window static_words %opt{javax_xml_crypto_dsig_canonicalizationmethod}
          set-option window javax_xml_crypto_dsig_canonicalizationmethod_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.DigestMethod)<ret>'
          evaluate-commands %opt{javax_xml_crypto_dsig_digestmethod_active}
          set-option -add window static_words %opt{javax_xml_crypto_dsig_digestmethod}
          set-option window javax_xml_crypto_dsig_digestmethod_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.Manifest)<ret>'
          evaluate-commands %opt{javax_xml_crypto_dsig_manifest_active}
          set-option -add window static_words %opt{javax_xml_crypto_dsig_manifest}
          set-option window javax_xml_crypto_dsig_manifest_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.SignatureMethod)<ret>'
          evaluate-commands %opt{javax_xml_crypto_dsig_signaturemethod_active}
          set-option -add window static_words %opt{javax_xml_crypto_dsig_signaturemethod}
          set-option window javax_xml_crypto_dsig_signaturemethod_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.SignatureProperties)<ret>'
          evaluate-commands %opt{javax_xml_crypto_dsig_signatureproperties_active}
          set-option -add window static_words %opt{javax_xml_crypto_dsig_signatureproperties}
          set-option window javax_xml_crypto_dsig_signatureproperties_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.Transform)<ret>'
          evaluate-commands %opt{javax_xml_crypto_dsig_transform_active}
          set-option -add window static_words %opt{javax_xml_crypto_dsig_transform}
          set-option window javax_xml_crypto_dsig_transform_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.XMLObject)<ret>'
          evaluate-commands %opt{javax_xml_crypto_dsig_xmlobject_active}
          set-option -add window static_words %opt{javax_xml_crypto_dsig_xmlobject}
          set-option window javax_xml_crypto_dsig_xmlobject_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.XMLSignature)<ret>'
          evaluate-commands %opt{javax_xml_crypto_dsig_xmlsignature_active}
          set-option -add window static_words %opt{javax_xml_crypto_dsig_xmlsignature}
          set-option window javax_xml_crypto_dsig_xmlsignature_active "nay"
        }
      }
      # -------------------------------- SECTION BREAK --------------------------------------------- #
      try %{
        execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.keyinfo\.[A-Z])<ret>'
        try %{
          execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.keyinfo\.KeyValue)<ret>'
          evaluate-commands %opt{javax_xml_crypto_dsig_keyinfo_keyvalue_active}
          set-option -add window static_words %opt{javax_xml_crypto_dsig_keyinfo_keyvalue}
          set-option window javax_xml_crypto_dsig_keyinfo_keyvalue_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.keyinfo\.PGPData)<ret>'
          evaluate-commands %opt{javax_xml_crypto_dsig_keyinfo_pgpdata_active}
          set-option -add window static_words %opt{javax_xml_crypto_dsig_keyinfo_pgpdata}
          set-option window javax_xml_crypto_dsig_keyinfo_pgpdata_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.keyinfo\.X509Data)<ret>'
          evaluate-commands %opt{javax_xml_crypto_dsig_keyinfo_x509data_active}
          set-option -add window static_words %opt{javax_xml_crypto_dsig_keyinfo_x509data}
          set-option window javax_xml_crypto_dsig_keyinfo_x509data_active "nay"
        }
      }
      # -------------------------------- SECTION BREAK --------------------------------------------- #
      try %{
        execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.spec\.ExcC14NParameterSpec)<ret>'
        evaluate-commands %opt{javax_xml_crypto_dsig_spec_excc14nparameterspec_active}
        set-option -add window static_words %opt{javax_xml_crypto_dsig_spec_excc14nparameterspec}
        set-option window javax_xml_crypto_dsig_spec_excc14nparameterspec_active "nay"
      }
    }
    # ---------------------------------- SECTION BREAK --------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.xml\.datatype\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.xml\.datatype\.DatatypeConstants)<ret>'
        evaluate-commands %opt{javax_xml_datatype_datatypeconstants_active}
        set-option -add window static_words %opt{javax_xml_datatype_datatypeconstants}
        set-option window javax_xml_datatype_datatypeconstants_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.xml\.datatype\.DatatypeFactory)<ret>'
        evaluate-commands %opt{javax_xml_datatype_datatypefactory_active}
        set-option -add window static_words %opt{javax_xml_datatype_datatypefactory}
        set-option window javax_xml_datatype_datatypefactory_active "nay"
      }
    }
    # ---------------------------------- SECTION BREAK --------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.xml\.stream\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.xml\.stream\.XMLInputFactory)<ret>'
        evaluate-commands %opt{javax_xml_stream_xmlinputfactory_active}
        set-option -add window static_words %opt{javax_xml_stream_xmlinputfactory}
        set-option window javax_xml_stream_xmlinputfactory_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.xml\.stream\.XMLOutputFactory)<ret>'
        evaluate-commands %opt{javax_xml_stream_xmloutputfactory_active}
        set-option -add window static_words %opt{javax_xml_stream_xmloutputfactory}
        set-option window javax_xml_stream_xmloutputfactory_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.xml\.stream\.XMLStreamConstants)<ret>'
        evaluate-commands %opt{javax_xml_stream_xmlstreamconstants_active}
        set-option -add window static_words %opt{javax_xml_stream_xmlstreamconstants}
        set-option window javax_xml_stream_xmlstreamconstants_active "nay"
      }
    }
    # ---------------------------------- SECTION BREAK --------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.xml\.transform\.[A-Za-z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.xml\.transform\.[A-Z])<ret>'
        try %{
          execute-keys -draft '%s (?:javax\.xml\.transform\.OutputKeys)<ret>'
          evaluate-commands %opt{javax_xml_transform_outputkeys_active}
          set-option -add window static_words %opt{javax_xml_transform_outputkeys}
          set-option window javax_xml_transform_outputkeys_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.xml\.transform\.Result)<ret>'
          evaluate-commands %opt{javax_xml_transform_result_active}
          set-option -add window static_words %opt{javax_xml_transform_result}
          set-option window javax_xml_transform_result_active "nay"
        }
      }
      # ---------------------------------- SECTION BREAK ------------------------------------------- #
      try %{
        execute-keys -draft '%s (?:javax\.xml\.transform\.dom\.[A-Z])<ret>'
        try %{
          execute-keys -draft '%s (?:javax\.xml\.transform\.dom\.DOMResult)<ret>'
          evaluate-commands %opt{javax_xml_transform_dom_domresult_active}
          set-option -add window static_words %opt{javax_xml_transform_dom_domresult}
          set-option window javax_xml_transform_dom_domresult_active "nay"
        }
        try %{
          execute-keys -draft '%s (?:javax\.xml\.transform\.dom\.DOMSource)<ret>'
          evaluate-commands %opt{javax_xml_transform_dom_domsource_active}
          set-option -add window static_words %opt{javax_xml_transform_dom_domsource}
          set-option window javax_xml_transform_dom_domsource_active "nay"
        }
      }
      # ---------------------------------- SECTION BREAK ------------------------------------------- #
      try %{
        execute-keys -draft '%s (?:javax\.xml\.transform\.s[a-z]+\.[A-Z])<ret>'
        try %{
          execute-keys -draft '%s (?:javax\.xml\.transform\.sax\.[A-Z])<ret>'
          try %{
            execute-keys -draft '%s (?:javax\.xml\.transform\.sax\.SAXResult)<ret>'
            evaluate-commands %opt{javax_xml_transform_sax_saxresult_active}
            set-option -add window static_words %opt{javax_xml_transform_sax_saxresult}
            set-option window javax_xml_transform_sax_saxresult_active "nay"
          }
          try %{
            execute-keys -draft '%s (?:javax\.xml\.transform\.sax\.SAXSource)<ret>'
            evaluate-commands %opt{javax_xml_transform_sax_saxsource_active}
            set-option -add window static_words %opt{javax_xml_transform_sax_saxsource}
            set-option window javax_xml_transform_sax_saxsource_active "nay"
          }
          try %{
            execute-keys -draft '%s (?:javax\.xml\.transform\.sax\.SAXTransformerFactory)<ret>'
            evaluate-commands %opt{javax_xml_transform_sax_saxtransformerfactory_active}
            set-option -add window static_words %opt{javax_xml_transform_sax_saxtransformerfactory}
            set-option window javax_xml_transform_sax_saxtransformerfactory_active "nay"
          }
        }
        # -------------------------------- SECTION BREAK ------------------------------------------- #
        try %{
          execute-keys -draft '%s (?:javax\.xml\.transform\.stax\.[A-Z])<ret>'
          try %{
            execute-keys -draft '%s (?:javax\.xml\.transform\.stax\.StAXResult)<ret>'
            evaluate-commands %opt{javax_xml_transform_stax_staxresult_active}
            set-option -add window static_words %opt{javax_xml_transform_stax_staxresult}
            set-option window javax_xml_transform_stax_staxresult_active "nay"
          }
          try %{
            execute-keys -draft '%s (?:javax\.xml\.transform\.stax\.StAXSource)<ret>'
            evaluate-commands %opt{javax_xml_transform_stax_staxsource_active}
            set-option -add window static_words %opt{javax_xml_transform_stax_staxsource}
            set-option window javax_xml_transform_stax_staxsource_active "nay"
          }
        }
        # -------------------------------- SECTION BREAK ------------------------------------------- #
        try %{
          execute-keys -draft '%s (?:javax\.xml\.transform\.stream\.[A-Z])<ret>'
          try %{
            execute-keys -draft '%s (?:javax\.xml\.transform\.stream\.StreamResult)<ret>'
            evaluate-commands %opt{javax_xml_transform_stream_streamresult_active}
            set-option -add window static_words %opt{javax_xml_transform_stream_streamresult}
            set-option window javax_xml_transform_stream_streamresult_active "nay"
          }
          try %{
            execute-keys -draft '%s (?:javax\.xml\.transform\.stream\.StreamSource)<ret>'
            evaluate-commands %opt{javax_xml_transform_stream_streamsource_active}
            set-option -add window static_words %opt{javax_xml_transform_stream_streamsource}
            set-option window javax_xml_transform_stream_streamsource_active "nay"
          }
        }
      }
    }
    # ------------------------------------ SECTION BREAK ------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:javax\.xml\.xpath\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:javax\.xml\.xpath\.XPathConstants)<ret>'
        evaluate-commands %opt{javax_xml_xpath_xpathconstants_active}
        set-option -add window static_words %opt{javax_xml_xpath_xpathconstants}
        set-option window javax_xml_xpath_xpathconstants_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:javax\.xml\.xpath\.XPathFactory)<ret>'
        evaluate-commands %opt{javax_xml_xpath_xpathfactory_active}
        set-option -add window static_words %opt{javax_xml_xpath_xpathfactory}
        set-option window javax_xml_xpath_xpathfactory_active "nay"
      }
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:jdk\.dynalink\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:jdk\.dynalink\.SecureLookupSupplier)<ret>'
      evaluate-commands %opt{jdk_dynalink_securelookupsupplier_active}
      set-option -add window static_words %opt{jdk_dynalink_securelookupsupplier}
      set-option window jdk_dynalink_securelookupsupplier_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:jdk\.dynalink\.linker\.GuardingDynamicLinkerExporter)<ret>'
      evaluate-commands %opt{jdk_dynalink_linker_guardingdynamiclinkerexporter_active}
      set-option -add window static_words %opt{jdk_dynalink_linker_guardingdynamiclinkerexporter}
      set-option window jdk_dynalink_linker_guardingdynamiclinkerexporter_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:jdk\.incubator\.foreign\.[A-Z])<ret>'
    try %{
      execute-keys -draft '%s (?:jdk\.incubator\.foreign\.MemoryLayout)<ret>'
      evaluate-commands %opt{jdk_incubator_foreign_memorylayout_active}
      set-option -add window static_words %opt{jdk_incubator_foreign_memorylayout}
      set-option window jdk_incubator_foreign_memorylayout_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:jdk\.incubator\.foreign\.MemorySegment)<ret>'
      evaluate-commands %opt{jdk_incubator_foreign_memorysegment_active}
      set-option -add window static_words %opt{jdk_incubator_foreign_memorysegment}
      set-option window jdk_incubator_foreign_memorysegment_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:jdk\.jfr\.[A-Z])<ret>'
    try %{
      execute-keys -draft '%s (?:jdk\.jfr\.DataAmount)<ret>'
      evaluate-commands %opt{jdk_jfr_dataamount_active}
      set-option -add window static_words %opt{jdk_jfr_dataamount}
      set-option window jdk_jfr_dataamount_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:jdk\.jfr\.Enabled)<ret>'
      evaluate-commands %opt{jdk_jfr_enabled_active}
      set-option -add window static_words %opt{jdk_jfr_enabled}
      set-option window jdk_jfr_enabled_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:jdk\.jfr\.Period)<ret>'
      evaluate-commands %opt{jdk_jfr_period_active}
      set-option -add window static_words %opt{jdk_jfr_period}
      set-option window jdk_jfr_period_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:jdk\.jfr\.StackTrace)<ret>'
      evaluate-commands %opt{jdk_jfr_stacktrace_active}
      set-option -add window static_words %opt{jdk_jfr_stacktrace}
      set-option window jdk_jfr_stacktrace_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:jdk\.jfr\.Threshold)<ret>'
      evaluate-commands %opt{jdk_jfr_threshold_active}
      set-option -add window static_words %opt{jdk_jfr_threshold}
      set-option window jdk_jfr_threshold_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:jdk\.jfr\.Timespan)<ret>'
      evaluate-commands %opt{jdk_jfr_timespan_active}
      set-option -add window static_words %opt{jdk_jfr_timespan}
      set-option window jdk_jfr_timespan_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:jdk\.jfr\.Timestamp)<ret>'
      evaluate-commands %opt{jdk_jfr_timestamp_active}
      set-option -add window static_words %opt{jdk_jfr_timestamp}
      set-option window jdk_jfr_timestamp_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:jdk\.jshell\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:jdk\.jshell\.Diag)<ret>'
      evaluate-commands %opt{jdk_jshell_diag_active}
      set-option -add window static_words %opt{jdk_jshell_diag}
      set-option window jdk_jshell_diag_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:jdk\.jshell\.execution\.JdiExecutionControlProvider)<ret>'
      evaluate-commands %opt{jdk_jshell_execution_jdiexecutioncontrolprovider_active}
      set-option -add window static_words %opt{jdk_jshell_execution_jdiexecutioncontrolprovider}
      set-option window jdk_jshell_execution_jdiexecutioncontrolprovider_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:jdk\.management\.jfr\.[A-Z])<ret>'
    try %{
      execute-keys -draft '%s (?:jdk\.management\.jfr\.FlightRecorderMXBean)<ret>'
      evaluate-commands %opt{jdk_management_jfr_flightrecordermxbean_active}
      set-option -add window static_words %opt{jdk_management_jfr_flightrecordermxbean}
      set-option window jdk_management_jfr_flightrecordermxbean_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:org\.ietf\.jgss\.[A-Z])<ret>'
    try %{
      execute-keys -draft '%s (?:org\.ietf\.jgss\.GSSContext)<ret>'
      evaluate-commands %opt{org_ietf_jgss_gsscontext_active}
      set-option -add window static_words %opt{org_ietf_jgss_gsscontext}
      set-option window org_ietf_jgss_gsscontext_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:org\.ietf\.jgss\.GSSCredential)<ret>'
      evaluate-commands %opt{org_ietf_jgss_gsscredential_active}
      set-option -add window static_words %opt{org_ietf_jgss_gsscredential}
      set-option window org_ietf_jgss_gsscredential_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:org\.ietf\.jgss\.GSSException)<ret>'
      evaluate-commands %opt{org_ietf_jgss_gssexception_active}
      set-option -add window static_words %opt{org_ietf_jgss_gssexception}
      set-option window org_ietf_jgss_gssexception_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:org\.w3c\.dom\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:org\.w3c\.dom\.DOMError)<ret>'
        evaluate-commands %opt{org_w3c_dom_domerror_active}
        set-option -add window static_words %opt{org_w3c_dom_domerror}
        set-option window org_w3c_dom_domerror_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:org\.w3c\.dom\.DOMException)<ret>'
        evaluate-commands %opt{org_w3c_dom_domexception_active}
        set-option -add window static_words %opt{org_w3c_dom_domexception}
        set-option window org_w3c_dom_domexception_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:org\.w3c\.dom\.Node)<ret>'
        evaluate-commands %opt{org_w3c_dom_node_active}
        set-option -add window static_words %opt{org_w3c_dom_node}
        set-option window org_w3c_dom_node_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:org\.w3c\.dom\.TypeInfo)<ret>'
        evaluate-commands %opt{org_w3c_dom_typeinfo_active}
        set-option -add window static_words %opt{org_w3c_dom_typeinfo}
        set-option window org_w3c_dom_typeinfo_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:org\.w3c\.dom\.UserDataHandler)<ret>'
        evaluate-commands %opt{org_w3c_dom_userdatahandler_active}
        set-option -add window static_words %opt{org_w3c_dom_userdatahandler}
        set-option window org_w3c_dom_userdatahandler_active "nay"
      }
    }
    # ------------------------------------ SECTION BREAK ------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.bootstrap\.DOMImplementationRegistry)<ret>'
      evaluate-commands %opt{org_w3c_dom_bootstrap_domimplementationregistry_active}
      set-option -add window static_words %opt{org_w3c_dom_bootstrap_domimplementationregistry}
      set-option window org_w3c_dom_bootstrap_domimplementationregistry_active "nay"
    }
    # ------------------------------------ SECTION BREAK ------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.css\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:org\.w3c\.dom\.css\.CSSPrimitiveValue)<ret>'
        evaluate-commands %opt{org_w3c_dom_css_cssprimitivevalue_active}
        set-option -add window static_words %opt{org_w3c_dom_css_cssprimitivevalue}
        set-option window org_w3c_dom_css_cssprimitivevalue_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:org\.w3c\.dom\.css\.CSSRule)<ret>'
        evaluate-commands %opt{org_w3c_dom_css_cssrule_active}
        set-option -add window static_words %opt{org_w3c_dom_css_cssrule}
        set-option window org_w3c_dom_css_cssrule_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:org\.w3c\.dom\.css\.CSSValue)<ret>'
        evaluate-commands %opt{org_w3c_dom_css_cssvalue_active}
        set-option -add window static_words %opt{org_w3c_dom_css_cssvalue}
        set-option window org_w3c_dom_css_cssvalue_active "nay"
      }
    }
    # ------------------------------------ SECTION BREAK ------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.events\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:org\.w3c\.dom\.events\.Event)<ret>'
        evaluate-commands %opt{org_w3c_dom_events_event_active}
        set-option -add window static_words %opt{org_w3c_dom_events_event}
        set-option window org_w3c_dom_events_event_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:org\.w3c\.dom\.events\.EventException)<ret>'
        evaluate-commands %opt{org_w3c_dom_events_eventexception_active}
        set-option -add window static_words %opt{org_w3c_dom_events_eventexception}
        set-option window org_w3c_dom_events_eventexception_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:org\.w3c\.dom\.events\.MutationEvent)<ret>'
        evaluate-commands %opt{org_w3c_dom_events_mutationevent_active}
        set-option -add window static_words %opt{org_w3c_dom_events_mutationevent}
        set-option window org_w3c_dom_events_mutationevent_active "nay"
      }
    }
    # ------------------------------------ SECTION BREAK ------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.ls\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:org\.w3c\.dom\.ls\.DOMImplementationLS)<ret>'
        evaluate-commands %opt{org_w3c_dom_ls_domimplementationls_active}
        set-option -add window static_words %opt{org_w3c_dom_ls_domimplementationls}
        set-option window org_w3c_dom_ls_domimplementationls_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:org\.w3c\.dom\.ls\.LSException)<ret>'
        evaluate-commands %opt{org_w3c_dom_ls_lsexception_active}
        set-option -add window static_words %opt{org_w3c_dom_ls_lsexception}
        set-option window org_w3c_dom_ls_lsexception_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:org\.w3c\.dom\.ls\.LSParser)<ret>'
        evaluate-commands %opt{org_w3c_dom_ls_lsparser_active}
        set-option -add window static_words %opt{org_w3c_dom_ls_lsparser}
        set-option window org_w3c_dom_ls_lsparser_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:org\.w3c\.dom\.ls\.LSParserFilter)<ret>'
        evaluate-commands %opt{org_w3c_dom_ls_lsparserfilter_active}
        set-option -add window static_words %opt{org_w3c_dom_ls_lsparserfilter}
        set-option window org_w3c_dom_ls_lsparserfilter_active "nay"
      }
    }
    # ------------------------------------ SECTION BREAK ------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.ranges\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:org\.w3c\.dom\.ranges\.Range)<ret>'
        evaluate-commands %opt{org_w3c_dom_ranges_range_active}
        set-option -add window static_words %opt{org_w3c_dom_ranges_range}
        set-option window org_w3c_dom_ranges_range_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:org\.w3c\.dom\.ranges\.RangeException)<ret>'
        evaluate-commands %opt{org_w3c_dom_ranges_rangeexception_active}
        set-option -add window static_words %opt{org_w3c_dom_ranges_rangeexception}
        set-option window org_w3c_dom_ranges_rangeexception_active "nay"
      }
    }
    # ------------------------------------ SECTION BREAK ------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.traversal\.NodeFilter)<ret>'
      evaluate-commands %opt{org_w3c_dom_traversal_nodefilter_active}
      set-option -add window static_words %opt{org_w3c_dom_traversal_nodefilter}
      set-option window org_w3c_dom_traversal_nodefilter_active "nay"
    }
    # ------------------------------------ SECTION BREAK ------------------------------------------- #
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.xpath\.[A-Z])<ret>'
      try %{
        execute-keys -draft '%s (?:org\.w3c\.dom\.xpath\.XPathException)<ret>'
        evaluate-commands %opt{org_w3c_dom_xpath_xpathexception_active}
        set-option -add window static_words %opt{org_w3c_dom_xpath_xpathexception}
        set-option window org_w3c_dom_xpath_xpathexception_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:org\.w3c\.dom\.xpath\.XPathNamespace)<ret>'
        evaluate-commands %opt{org_w3c_dom_xpath_xpathnamespace_active}
        set-option -add window static_words %opt{org_w3c_dom_xpath_xpathnamespace}
        set-option window org_w3c_dom_xpath_xpathnamespace_active "nay"
      }
      try %{
        execute-keys -draft '%s (?:org\.w3c\.dom\.xpath\.XPathResult)<ret>'
        evaluate-commands %opt{org_w3c_dom_xpath_xpathresult_active}
        set-option -add window static_words %opt{org_w3c_dom_xpath_xpathresult}
        set-option window org_w3c_dom_xpath_xpathresult_active "nay"
      }
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:org\.xml\.sax\.helpers\.NamespaceSupport)<ret>'
    evaluate-commands %opt{org_xml_sax_helpers_namespacesupport_active}
    set-option -add window static_words %opt{org_xml_sax_helpers_namespacesupport}
    set-option window org_xml_sax_helpers_namespacesupport_active "nay"
  }
}
# -------------------------------------------------------------------------------------------------- #
# ------------------------------------- JAVA CONSTANTS REMOVE -------------------------------------- #
define-command -hidden java-constants-remove %{
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{com_sun_java_accessibility_util_eventid_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.java\.accessibility\.util\.EventID)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_java_accessibility_util_eventid}
      set-option window com_sun_java_accessibility_util_eventid_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{com_sun_jdi_classtype_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.jdi\.ClassType)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_jdi_classtype}
      set-option window com_sun_jdi_classtype_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{com_sun_jdi_objectreference_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.jdi\.ObjectReference)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_jdi_objectreference}
      set-option window com_sun_jdi_objectreference_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{com_sun_jdi_threadreference_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.jdi\.ThreadReference)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_jdi_threadreference}
      set-option window com_sun_jdi_threadreference_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{com_sun_jdi_virtualmachine_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.jdi\.VirtualMachine)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_jdi_virtualmachine}
      set-option window com_sun_jdi_virtualmachine_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{com_sun_jdi_request_eventrequest_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.jdi\.request\.EventRequest)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_jdi_request_eventrequest}
      set-option window com_sun_jdi_request_eventrequest_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{com_sun_jdi_request_steprequest_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.jdi\.request\.StepRequest)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_jdi_request_steprequest}
      set-option window com_sun_jdi_request_steprequest_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{com_sun_management_garbagecollectionnotificationinfo_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.management\.GarbageCollectionNotificationInfo)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_management_garbagecollectionnotificationinfo}
      set-option window com_sun_management_garbagecollectionnotificationinfo_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{com_sun_security_auth_module_jndiloginmodule_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.security\.auth\.module\.JndiLoginModule)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_security_auth_module_jndiloginmodule}
      set-option window com_sun_security_auth_module_jndiloginmodule_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{com_sun_tools_jconsole_jconsolecontext_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.tools\.jconsole\.JConsoleContext)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_tools_jconsole_jconsolecontext}
      set-option window com_sun_tools_jconsole_jconsolecontext_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{java_awt_adjustable_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.Adjustable)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_adjustable}
      set-option window java_awt_adjustable_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_alphacomposite_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.AlphaComposite)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_alphacomposite}
      set-option window java_awt_alphacomposite_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_awtevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.AWTEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_awtevent}
      set-option window java_awt_awtevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_basicstroke_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.BasicStroke)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_basicstroke}
      set-option window java_awt_basicstroke_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_borderlayout_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.BorderLayout)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_borderlayout}
      set-option window java_awt_borderlayout_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_component_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.Component)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_component}
      set-option window java_awt_component_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_cursor_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.Cursor)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_cursor}
      set-option window java_awt_cursor_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_displaymode_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.DisplayMode)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_displaymode}
      set-option window java_awt_displaymode_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_event_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.Event)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_event}
      set-option window java_awt_event_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_filedialog_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.FileDialog)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_filedialog}
      set-option window java_awt_filedialog_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_flowlayout_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.FlowLayout)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_flowlayout}
      set-option window java_awt_flowlayout_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_font_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.Font)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_font}
      set-option window java_awt_font_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_frame_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.Frame)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_frame}
      set-option window java_awt_frame_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_graphicsconfigtemplate_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.GraphicsConfigTemplate)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_graphicsconfigtemplate}
      set-option window java_awt_graphicsconfigtemplate_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_graphicsdevice_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.GraphicsDevice)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_graphicsdevice}
      set-option window java_awt_graphicsdevice_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_gridbagconstraints_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.GridBagConstraints)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_gridbagconstraints}
      set-option window java_awt_gridbagconstraints_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_gridbaglayout_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.GridBagLayout)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_gridbaglayout}
      set-option window java_awt_gridbaglayout_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_image_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.Image)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_image}
      set-option window java_awt_image_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_keyboardfocusmanager_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.KeyboardFocusManager)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_keyboardfocusmanager}
      set-option window java_awt_keyboardfocusmanager_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_label_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.Label)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_label}
      set-option window java_awt_label_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_mediatracker_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.MediaTracker)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_mediatracker}
      set-option window java_awt_mediatracker_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_scrollbar_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.Scrollbar)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_scrollbar}
      set-option window java_awt_scrollbar_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_scrollpane_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.ScrollPane)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_scrollpane}
      set-option window java_awt_scrollpane_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_systemcolor_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.SystemColor)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_systemcolor}
      set-option window java_awt_systemcolor_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_textarea_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.TextArea)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_textarea}
      set-option window java_awt_textarea_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_transparency_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.Transparency)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_transparency}
      set-option window java_awt_transparency_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_color_colorspace_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.color\.ColorSpace)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_color_colorspace}
      set-option window java_awt_color_colorspace_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_color_icc_profile_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.color\.ICC_Profile)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_color_icc_profile}
      set-option window java_awt_color_icc_profile_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_color_icc_profilergb_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.color\.ICC_ProfileRGB)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_color_icc_profilergb}
      set-option window java_awt_color_icc_profilergb_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_datatransfer_dataflavor_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.datatransfer\.DataFlavor)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_datatransfer_dataflavor}
      set-option window java_awt_datatransfer_dataflavor_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_dnd_dndconstants_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.dnd\.DnDConstants)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_dnd_dndconstants}
      set-option window java_awt_dnd_dndconstants_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_dnd_dragsourcecontext_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.dnd\.DragSourceContext)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_dnd_dragsourcecontext}
      set-option window java_awt_dnd_dragsourcecontext_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_event_actionevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.event\.ActionEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_event_actionevent}
      set-option window java_awt_event_actionevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_event_adjustmentevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.event\.AdjustmentEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_event_adjustmentevent}
      set-option window java_awt_event_adjustmentevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_event_componentevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.event\.ComponentEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_event_componentevent}
      set-option window java_awt_event_componentevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_event_containerevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.event\.ContainerEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_event_containerevent}
      set-option window java_awt_event_containerevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_event_focusevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.event\.FocusEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_event_focusevent}
      set-option window java_awt_event_focusevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_event_hierarchyevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.event\.HierarchyEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_event_hierarchyevent}
      set-option window java_awt_event_hierarchyevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_event_inputevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.event\.InputEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_event_inputevent}
      set-option window java_awt_event_inputevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_event_inputmethodevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.event\.InputMethodEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_event_inputmethodevent}
      set-option window java_awt_event_inputmethodevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_event_invocationevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.event\.InvocationEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_event_invocationevent}
      set-option window java_awt_event_invocationevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_event_itemevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.event\.ItemEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_event_itemevent}
      set-option window java_awt_event_itemevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_event_keyevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.event\.KeyEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_event_keyevent}
      set-option window java_awt_event_keyevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_event_mouseevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.event\.MouseEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_event_mouseevent}
      set-option window java_awt_event_mouseevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_event_mousewheelevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.event\.MouseWheelEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_event_mousewheelevent}
      set-option window java_awt_event_mousewheelevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_event_paintevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.event\.PaintEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_event_paintevent}
      set-option window java_awt_event_paintevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_event_textevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.event\.TextEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_event_textevent}
      set-option window java_awt_event_textevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_event_windowevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.event\.WindowEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_event_windowevent}
      set-option window java_awt_event_windowevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_font_glyphjustificationinfo_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.font\.GlyphJustificationInfo)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_font_glyphjustificationinfo}
      set-option window java_awt_font_glyphjustificationinfo_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_font_glyphmetrics_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.font\.GlyphMetrics)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_font_glyphmetrics}
      set-option window java_awt_font_glyphmetrics_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_font_glyphvector_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.font\.GlyphVector)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_font_glyphvector}
      set-option window java_awt_font_glyphvector_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_font_graphicattribute_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.font\.GraphicAttribute)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_font_graphicattribute}
      set-option window java_awt_font_graphicattribute_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_font_numericshaper_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.font\.NumericShaper)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_font_numericshaper}
      set-option window java_awt_font_numericshaper_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_font_opentype_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.font\.OpenType)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_font_opentype}
      set-option window java_awt_font_opentype_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_font_shapegraphicattribute_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.font\.ShapeGraphicAttribute)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_font_shapegraphicattribute}
      set-option window java_awt_font_shapegraphicattribute_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_geom_affinetransform_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.geom\.AffineTransform)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_geom_affinetransform}
      set-option window java_awt_geom_affinetransform_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_geom_arc2d_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.geom\.Arc2D)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_geom_arc2d}
      set-option window java_awt_geom_arc2d_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_geom_path2d_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.geom\.Path2D)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_geom_path2d}
      set-option window java_awt_geom_path2d_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_geom_pathiterator_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.geom\.PathIterator)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_geom_pathiterator}
      set-option window java_awt_geom_pathiterator_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_geom_rectangle2d_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.geom\.Rectangle2D)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_geom_rectangle2d}
      set-option window java_awt_geom_rectangle2d_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_im_inputmethodhighlight_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.im\.InputMethodHighlight)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_im_inputmethodhighlight}
      set-option window java_awt_im_inputmethodhighlight_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_image_affinetransformop_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.image\.AffineTransformOp)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_image_affinetransformop}
      set-option window java_awt_image_affinetransformop_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_image_bufferedimage_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.image\.BufferedImage)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_image_bufferedimage}
      set-option window java_awt_image_bufferedimage_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_image_convolveop_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.image\.ConvolveOp)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_image_convolveop}
      set-option window java_awt_image_convolveop_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_image_databuffer_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.image\.DataBuffer)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_image_databuffer}
      set-option window java_awt_image_databuffer_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_image_imageconsumer_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.image\.ImageConsumer)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_image_imageconsumer}
      set-option window java_awt_image_imageconsumer_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_image_imageobserver_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.image\.ImageObserver)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_image_imageobserver}
      set-option window java_awt_image_imageobserver_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_image_volatileimage_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.image\.VolatileImage)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_image_volatileimage}
      set-option window java_awt_image_volatileimage_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_image_renderable_renderableimage_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.image\.renderable\.RenderableImage)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_image_renderable_renderableimage}
      set-option window java_awt_image_renderable_renderableimage_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_print_pageable_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.print\.Pageable)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_print_pageable}
      set-option window java_awt_print_pageable_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_print_pageformat_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.print\.PageFormat)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_print_pageformat}
      set-option window java_awt_print_pageformat_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_print_printable_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.print\.Printable)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_print_printable}
      set-option window java_awt_print_printable_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{java_beans_beaninfo_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.beans\.BeanInfo)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_beans_beaninfo}
      set-option window java_beans_beaninfo_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_beans_designmode_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.beans\.DesignMode)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_beans_designmode}
      set-option window java_beans_designmode_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_beans_introspector_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.beans\.Introspector)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_beans_introspector}
      set-option window java_beans_introspector_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{java_io_objectstreamconstants_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.io\.ObjectStreamConstants)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_io_objectstreamconstants}
      set-option window java_io_objectstreamconstants_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_io_pipedinputstream_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.io\.PipedInputStream)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_io_pipedinputstream}
      set-option window java_io_pipedinputstream_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_io_streamtokenizer_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.io\.StreamTokenizer)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_io_streamtokenizer}
      set-option window java_io_streamtokenizer_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{java_lang_byte_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.Byte)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_byte}
      set-option window java_lang_byte_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_character_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.Character)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_character}
      set-option window java_lang_character_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_double_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.Double)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_double}
      set-option window java_lang_double_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_float_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.Float)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_float}
      set-option window java_lang_float_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_integer_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.Integer)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_integer}
      set-option window java_lang_integer_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_long_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.Long)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_long}
      set-option window java_lang_long_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_math_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.Math)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_math}
      set-option window java_lang_math_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_short_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.Short)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_short}
      set-option window java_lang_short_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_strictmath_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.StrictMath)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_strictmath}
      set-option window java_lang_strictmath_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_thread_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.Thread)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_thread}
      set-option window java_lang_thread_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_constant_constantdescs_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.constant\.ConstantDescs)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_constant_constantdescs}
      set-option window java_lang_constant_constantdescs_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_invoke_lambdametafactory_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.invoke\.LambdaMetafactory)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_invoke_lambdametafactory}
      set-option window java_lang_invoke_lambdametafactory_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_invoke_methodhandleinfo_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.invoke\.MethodHandleInfo)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_invoke_methodhandleinfo}
      set-option window java_lang_invoke_methodhandleinfo_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_invoke_methodhandles_lookup_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.invoke\.MethodHandles\.Lookup)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_invoke_methodhandles_lookup}
      set-option window java_lang_invoke_methodhandles_lookup_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_management_managementfactory_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.management\.ManagementFactory)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_management_managementfactory}
      set-option window java_lang_management_managementfactory_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_management_memorynotificationinfo_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.management\.MemoryNotificationInfo)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_management_memorynotificationinfo}
      set-option window java_lang_management_memorynotificationinfo_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_reflect_member_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.reflect\.Member)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_reflect_member}
      set-option window java_lang_reflect_member_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_reflect_modifier_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.reflect\.Modifier)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_reflect_modifier}
      set-option window java_lang_reflect_modifier_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{java_math_bigdecimal_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.math\.BigDecimal)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_math_bigdecimal}
      set-option window java_math_bigdecimal_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{java_net_httpurlconnection_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.net\.HttpURLConnection)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_net_httpurlconnection}
      set-option window java_net_httpurlconnection_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_net_idn_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.net\.IDN)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_net_idn}
      set-option window java_net_idn_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_net_socketoptions_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.net\.SocketOptions)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_net_socketoptions}
      set-option window java_net_socketoptions_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_net_http_websocket_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.net\.http\.WebSocket)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_net_http_websocket}
      set-option window java_net_http_websocket_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{java_nio_channels_selectionkey_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.nio\.channels\.SelectionKey)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_nio_channels_selectionkey}
      set-option window java_nio_channels_selectionkey_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{java_rmi_activation_activationsystem_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.rmi\.activation\.ActivationSystem)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_rmi_activation_activationsystem}
      set-option window java_rmi_activation_activationsystem_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_rmi_registry_registry_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.rmi\.registry\.Registry)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_rmi_registry_registry}
      set-option window java_rmi_registry_registry_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_rmi_server_loaderhandler_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.rmi\.server\.LoaderHandler)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_rmi_server_loaderhandler}
      set-option window java_rmi_server_loaderhandler_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_rmi_server_logstream_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.rmi\.server\.LogStream)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_rmi_server_logstream}
      set-option window java_rmi_server_logstream_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_rmi_server_objid_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.rmi\.server\.ObjID)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_rmi_server_objid}
      set-option window java_rmi_server_objid_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_rmi_server_remoteref_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.rmi\.server\.RemoteRef)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_rmi_server_remoteref}
      set-option window java_rmi_server_remoteref_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_rmi_server_serverref_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.rmi\.server\.ServerRef)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_rmi_server_serverref}
      set-option window java_rmi_server_serverref_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{java_security_key_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.security\.Key)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_security_key}
      set-option window java_security_key_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_security_privatekey_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.security\.PrivateKey)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_security_privatekey}
      set-option window java_security_privatekey_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_security_publickey_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.security\.PublicKey)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_security_publickey}
      set-option window java_security_publickey_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_security_signature_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.security\.Signature)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_security_signature}
      set-option window java_security_signature_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_security_interfaces_dsaprivatekey_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.security\.interfaces\.DSAPrivateKey)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_security_interfaces_dsaprivatekey}
      set-option window java_security_interfaces_dsaprivatekey_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_security_interfaces_dsapublickey_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.security\.interfaces\.DSAPublicKey)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_security_interfaces_dsapublickey}
      set-option window java_security_interfaces_dsapublickey_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_security_interfaces_ecprivatekey_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.security\.interfaces\.ECPrivateKey)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_security_interfaces_ecprivatekey}
      set-option window java_security_interfaces_ecprivatekey_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_security_interfaces_ecpublickey_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.security\.interfaces\.ECPublicKey)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_security_interfaces_ecpublickey}
      set-option window java_security_interfaces_ecpublickey_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_security_interfaces_rsamultiprimeprivatecrtkey_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.security\.interfaces\.RSAMultiPrimePrivateCrtKey)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_security_interfaces_rsamultiprimeprivatecrtkey}
      set-option window java_security_interfaces_rsamultiprimeprivatecrtkey_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_security_interfaces_rsaprivatecrtkey_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.security\.interfaces\.RSAPrivateCrtKey)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_security_interfaces_rsaprivatecrtkey}
      set-option window java_security_interfaces_rsaprivatecrtkey_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_security_interfaces_rsaprivatekey_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.security\.interfaces\.RSAPrivateKey)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_security_interfaces_rsaprivatekey}
      set-option window java_security_interfaces_rsaprivatekey_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_security_interfaces_rsapublickey_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.security\.interfaces\.RSAPublicKey)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_security_interfaces_rsapublickey}
      set-option window java_security_interfaces_rsapublickey_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_security_spec_pssparameterspec_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.security\.spec\.PSSParameterSpec)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_security_spec_pssparameterspec}
      set-option window java_security_spec_pssparameterspec_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{java_sql_connection_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.sql\.Connection)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_sql_connection}
      set-option window java_sql_connection_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_sql_databasemetadata_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.sql\.DatabaseMetaData)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_sql_databasemetadata}
      set-option window java_sql_databasemetadata_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_sql_parametermetadata_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.sql\.ParameterMetaData)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_sql_parametermetadata}
      set-option window java_sql_parametermetadata_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_sql_resultset_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.sql\.ResultSet)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_sql_resultset}
      set-option window java_sql_resultset_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_sql_resultsetmetadata_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.sql\.ResultSetMetaData)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_sql_resultsetmetadata}
      set-option window java_sql_resultsetmetadata_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_sql_statement_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.sql\.Statement)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_sql_statement}
      set-option window java_sql_statement_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_sql_types_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.sql\.Types)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_sql_types}
      set-option window java_sql_types_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{java_text_bidi_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.text\.Bidi)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_text_bidi}
      set-option window java_text_bidi_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_text_breakiterator_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.text\.BreakIterator)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_text_breakiterator}
      set-option window java_text_breakiterator_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_text_characteriterator_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.text\.CharacterIterator)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_text_characteriterator}
      set-option window java_text_characteriterator_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_text_collationelementiterator_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.text\.CollationElementIterator)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_text_collationelementiterator}
      set-option window java_text_collationelementiterator_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_text_collator_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.text\.Collator)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_text_collator}
      set-option window java_text_collator_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_text_dateformat_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.text\.DateFormat)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_text_dateformat}
      set-option window java_text_dateformat_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_text_numberformat_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.text\.NumberFormat)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_text_numberformat}
      set-option window java_text_numberformat_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{java_time_year_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.time\.Year)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_time_year}
      set-option window java_time_year_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{java_util_calendar_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.Calendar)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_calendar}
      set-option window java_util_calendar_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_formattableflags_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.FormattableFlags)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_formattableflags}
      set-option window java_util_formattableflags_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_gregoriancalendar_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.GregorianCalendar)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_gregoriancalendar}
      set-option window java_util_gregoriancalendar_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_locale_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.Locale)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_locale}
      set-option window java_util_locale_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_locale_languagerange_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.Locale\.LanguageRange)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_locale_languagerange}
      set-option window java_util_locale_languagerange_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_resourcebundle_control_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.ResourceBundle\.Control)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_resourcebundle_control}
      set-option window java_util_resourcebundle_control_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_simpletimezone_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.SimpleTimeZone)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_simpletimezone}
      set-option window java_util_simpletimezone_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_spliterator_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.Spliterator)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_spliterator}
      set-option window java_util_spliterator_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_timezone_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.TimeZone)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_timezone}
      set-option window java_util_timezone_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_jar_jarentry_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.jar\.JarEntry)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_jar_jarentry}
      set-option window java_util_jar_jarentry_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_jar_jarfile_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.jar\.JarFile)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_jar_jarfile}
      set-option window java_util_jar_jarfile_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_jar_jarinputstream_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.jar\.JarInputStream)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_jar_jarinputstream}
      set-option window java_util_jar_jarinputstream_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_jar_jaroutputstream_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.jar\.JarOutputStream)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_jar_jaroutputstream}
      set-option window java_util_jar_jaroutputstream_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_logging_errormanager_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.logging\.ErrorManager)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_logging_errormanager}
      set-option window java_util_logging_errormanager_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_logging_logger_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.logging\.Logger)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_logging_logger}
      set-option window java_util_logging_logger_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_logging_logmanager_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.logging\.LogManager)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_logging_logmanager}
      set-option window java_util_logging_logmanager_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_prefs_preferences_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.prefs\.Preferences)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_prefs_preferences}
      set-option window java_util_prefs_preferences_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_regex_pattern_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.regex\.Pattern)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_regex_pattern}
      set-option window java_util_regex_pattern_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_zip_deflater_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.zip\.Deflater)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_zip_deflater}
      set-option window java_util_zip_deflater_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_zip_gzipinputstream_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.zip\.GZIPInputStream)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_zip_gzipinputstream}
      set-option window java_util_zip_gzipinputstream_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_zip_zipentry_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.zip\.ZipEntry)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_zip_zipentry}
      set-option window java_util_zip_zipentry_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_zip_zipfile_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.zip\.ZipFile)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_zip_zipfile}
      set-option window java_util_zip_zipfile_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_zip_zipinputstream_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.zip\.ZipInputStream)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_zip_zipinputstream}
      set-option window java_util_zip_zipinputstream_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_zip_zipoutputstream_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.zip\.ZipOutputStream)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_zip_zipoutputstream}
      set-option window java_util_zip_zipoutputstream_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{javax_accessibility_accessiblecontext_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.accessibility\.AccessibleContext)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_accessibility_accessiblecontext}
      set-option window javax_accessibility_accessiblecontext_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_accessibility_accessibleextendedtext_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.accessibility\.AccessibleExtendedText)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_accessibility_accessibleextendedtext}
      set-option window javax_accessibility_accessibleextendedtext_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_accessibility_accessiblerelation_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.accessibility\.AccessibleRelation)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_accessibility_accessiblerelation}
      set-option window javax_accessibility_accessiblerelation_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_accessibility_accessibletablemodelchange_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.accessibility\.AccessibleTableModelChange)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_accessibility_accessibletablemodelchange}
      set-option window javax_accessibility_accessibletablemodelchange_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_accessibility_accessibletext_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.accessibility\.AccessibleText)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_accessibility_accessibletext}
      set-option window javax_accessibility_accessibletext_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{javax_crypto_cipher_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.crypto\.Cipher)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_crypto_cipher}
      set-option window javax_crypto_cipher_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_crypto_secretkey_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.crypto\.SecretKey)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_crypto_secretkey}
      set-option window javax_crypto_secretkey_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_crypto_interfaces_dhprivatekey_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.crypto\.interfaces\.DHPrivateKey)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_crypto_interfaces_dhprivatekey}
      set-option window javax_crypto_interfaces_dhprivatekey_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_crypto_interfaces_dhpublickey_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.crypto\.interfaces\.DHPublicKey)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_crypto_interfaces_dhpublickey}
      set-option window javax_crypto_interfaces_dhpublickey_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_crypto_interfaces_pbekey_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.crypto\.interfaces\.PBEKey)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_crypto_interfaces_pbekey}
      set-option window javax_crypto_interfaces_pbekey_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_crypto_spec_desedekeyspec_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.crypto\.spec\.DESedeKeySpec)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_crypto_spec_desedekeyspec}
      set-option window javax_crypto_spec_desedekeyspec_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_crypto_spec_deskeyspec_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.crypto\.spec\.DESKeySpec)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_crypto_spec_deskeyspec}
      set-option window javax_crypto_spec_deskeyspec_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{javax_imageio_imagewriteparam_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.imageio\.ImageWriteParam)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_imageio_imagewriteparam}
      set-option window javax_imageio_imagewriteparam_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_imageio_metadata_iiometadataformat_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.imageio\.metadata\.IIOMetadataFormat)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_imageio_metadata_iiometadataformat}
      set-option window javax_imageio_metadata_iiometadataformat_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_imageio_metadata_iiometadataformatimpl_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.imageio\.metadata\.IIOMetadataFormatImpl)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_imageio_metadata_iiometadataformatimpl}
      set-option window javax_imageio_metadata_iiometadataformatimpl_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_imageio_plugins_tiff_baselinetifftagset_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.imageio\.plugins\.tiff\.BaselineTIFFTagSet)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_imageio_plugins_tiff_baselinetifftagset}
      set-option window javax_imageio_plugins_tiff_baselinetifftagset_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_imageio_plugins_tiff_exifgpstagset_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.imageio\.plugins\.tiff\.ExifGPSTagSet)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_imageio_plugins_tiff_exifgpstagset}
      set-option window javax_imageio_plugins_tiff_exifgpstagset_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_imageio_plugins_tiff_exifinteroperabilitytagset_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.imageio\.plugins\.tiff\.ExifInteroperabilityTagSet)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_imageio_plugins_tiff_exifinteroperabilitytagset}
      set-option window javax_imageio_plugins_tiff_exifinteroperabilitytagset_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_imageio_plugins_tiff_exifparenttifftagset_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.imageio\.plugins\.tiff\.ExifParentTIFFTagSet)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_imageio_plugins_tiff_exifparenttifftagset}
      set-option window javax_imageio_plugins_tiff_exifparenttifftagset_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_imageio_plugins_tiff_exiftifftagset_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.imageio\.plugins\.tiff\.ExifTIFFTagSet)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_imageio_plugins_tiff_exiftifftagset}
      set-option window javax_imageio_plugins_tiff_exiftifftagset_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_imageio_plugins_tiff_faxtifftagset_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.imageio\.plugins\.tiff\.FaxTIFFTagSet)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_imageio_plugins_tiff_faxtifftagset}
      set-option window javax_imageio_plugins_tiff_faxtifftagset_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_imageio_plugins_tiff_geotifftagset_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.imageio\.plugins\.tiff\.GeoTIFFTagSet)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_imageio_plugins_tiff_geotifftagset}
      set-option window javax_imageio_plugins_tiff_geotifftagset_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_imageio_plugins_tiff_tifftag_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.imageio\.plugins\.tiff\.TIFFTag)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_imageio_plugins_tiff_tifftag}
      set-option window javax_imageio_plugins_tiff_tifftag_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{javax_management_attributechangenotification_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.management\.AttributeChangeNotification)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_management_attributechangenotification}
      set-option window javax_management_attributechangenotification_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_management_jmx_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.management\.JMX)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_management_jmx}
      set-option window javax_management_jmx_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_management_mbeanoperationinfo_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.management\.MBeanOperationInfo)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_management_mbeanoperationinfo}
      set-option window javax_management_mbeanoperationinfo_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_management_mbeanservernotification_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.management\.MBeanServerNotification)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_management_mbeanservernotification}
      set-option window javax_management_mbeanservernotification_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_management_query_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.management\.Query)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_management_query}
      set-option window javax_management_query_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_management_monitor_monitor_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.management\.monitor\.Monitor)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_management_monitor_monitor}
      set-option window javax_management_monitor_monitor_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_management_monitor_monitornotification_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.management\.monitor\.MonitorNotification)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_management_monitor_monitornotification}
      set-option window javax_management_monitor_monitornotification_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_management_relation_relationnotification_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.management\.relation\.RelationNotification)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_management_relation_relationnotification}
      set-option window javax_management_relation_relationnotification_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_management_relation_roleinfo_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.management\.relation\.RoleInfo)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_management_relation_roleinfo}
      set-option window javax_management_relation_roleinfo_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_management_relation_rolestatus_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.management\.relation\.RoleStatus)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_management_relation_rolestatus}
      set-option window javax_management_relation_rolestatus_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_management_remote_jmxconnectionnotification_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.management\.remote\.JMXConnectionNotification)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_management_remote_jmxconnectionnotification}
      set-option window javax_management_remote_jmxconnectionnotification_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_management_remote_jmxconnector_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.management\.remote\.JMXConnector)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_management_remote_jmxconnector}
      set-option window javax_management_remote_jmxconnector_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_management_remote_jmxconnectorfactory_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.management\.remote\.JMXConnectorFactory)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_management_remote_jmxconnectorfactory}
      set-option window javax_management_remote_jmxconnectorfactory_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_management_remote_jmxconnectorserver_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.management\.remote\.JMXConnectorServer)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_management_remote_jmxconnectorserver}
      set-option window javax_management_remote_jmxconnectorserver_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_management_remote_jmxconnectorserverfactory_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.management\.remote\.JMXConnectorServerFactory)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_management_remote_jmxconnectorserverfactory}
      set-option window javax_management_remote_jmxconnectorserverfactory_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_management_remote_rmi_rmiconnectorserver_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.management\.remote\.rmi\.RMIConnectorServer)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_management_remote_rmi_rmiconnectorserver}
      set-option window javax_management_remote_rmi_rmiconnectorserver_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_management_timer_timer_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.management\.timer\.Timer)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_management_timer_timer}
      set-option window javax_management_timer_timer_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{javax_naming_context_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.naming\.Context)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_naming_context}
      set-option window javax_naming_context_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_naming_name_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.naming\.Name)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_naming_name}
      set-option window javax_naming_name_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_naming_directory_attribute_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.naming\.directory\.Attribute)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_naming_directory_attribute}
      set-option window javax_naming_directory_attribute_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_naming_directory_dircontext_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.naming\.directory\.DirContext)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_naming_directory_dircontext}
      set-option window javax_naming_directory_dircontext_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_naming_directory_searchcontrols_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.naming\.directory\.SearchControls)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_naming_directory_searchcontrols}
      set-option window javax_naming_directory_searchcontrols_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_naming_event_eventcontext_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.naming\.event\.EventContext)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_naming_event_eventcontext}
      set-option window javax_naming_event_eventcontext_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_naming_event_namingevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.naming\.event\.NamingEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_naming_event_namingevent}
      set-option window javax_naming_event_namingevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_naming_ldap_control_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.naming\.ldap\.Control)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_naming_ldap_control}
      set-option window javax_naming_ldap_control_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_naming_ldap_ldapcontext_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.naming\.ldap\.LdapContext)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_naming_ldap_ldapcontext}
      set-option window javax_naming_ldap_ldapcontext_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_naming_ldap_managereferralcontrol_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.naming\.ldap\.ManageReferralControl)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_naming_ldap_managereferralcontrol}
      set-option window javax_naming_ldap_managereferralcontrol_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_naming_ldap_pagedresultscontrol_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.naming\.ldap\.PagedResultsControl)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_naming_ldap_pagedresultscontrol}
      set-option window javax_naming_ldap_pagedresultscontrol_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_naming_ldap_pagedresultsresponsecontrol_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.naming\.ldap\.PagedResultsResponseControl)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_naming_ldap_pagedresultsresponsecontrol}
      set-option window javax_naming_ldap_pagedresultsresponsecontrol_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_naming_ldap_sortcontrol_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.naming\.ldap\.SortControl)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_naming_ldap_sortcontrol}
      set-option window javax_naming_ldap_sortcontrol_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_naming_ldap_sortresponsecontrol_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.naming\.ldap\.SortResponseControl)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_naming_ldap_sortresponsecontrol}
      set-option window javax_naming_ldap_sortresponsecontrol_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_naming_ldap_starttlsrequest_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.naming\.ldap\.StartTlsRequest)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_naming_ldap_starttlsrequest}
      set-option window javax_naming_ldap_starttlsrequest_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_naming_ldap_starttlsresponse_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.naming\.ldap\.StartTlsResponse)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_naming_ldap_starttlsresponse}
      set-option window javax_naming_ldap_starttlsresponse_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_naming_spi_namingmanager_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.naming\.spi\.NamingManager)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_naming_spi_namingmanager}
      set-option window javax_naming_spi_namingmanager_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{javax_net_ssl_standardconstants_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.net\.ssl\.StandardConstants)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_net_ssl_standardconstants}
      set-option window javax_net_ssl_standardconstants_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{javax_print_serviceuifactory_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.print\.ServiceUIFactory)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_print_serviceuifactory}
      set-option window javax_print_serviceuifactory_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_print_uriexception_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.print\.URIException)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_print_uriexception}
      set-option window javax_print_uriexception_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_print_attribute_resolutionsyntax_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.print\.attribute\.ResolutionSyntax)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_print_attribute_resolutionsyntax}
      set-option window javax_print_attribute_resolutionsyntax_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_print_attribute_size2dsyntax_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.print\.attribute\.Size2DSyntax)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_print_attribute_size2dsyntax}
      set-option window javax_print_attribute_size2dsyntax_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_print_attribute_standard_mediaprintablearea_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.print\.attribute\.standard\.MediaPrintableArea)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_print_attribute_standard_mediaprintablearea}
      set-option window javax_print_attribute_standard_mediaprintablearea_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_print_event_printjobevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.print\.event\.PrintJobEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_print_event_printjobevent}
      set-option window javax_print_event_printjobevent_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{javax_script_scriptcontext_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.script\.ScriptContext)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_script_scriptcontext}
      set-option window javax_script_scriptcontext_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_script_scriptengine_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.script\.ScriptEngine)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_script_scriptengine}
      set-option window javax_script_scriptengine_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{javax_security_auth_callback_confirmationcallback_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.security\.auth\.callback\.ConfirmationCallback)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_security_auth_callback_confirmationcallback}
      set-option window javax_security_auth_callback_confirmationcallback_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_security_auth_callback_textoutputcallback_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.security\.auth\.callback\.TextOutputCallback)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_security_auth_callback_textoutputcallback}
      set-option window javax_security_auth_callback_textoutputcallback_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_security_auth_kerberos_kerberosprincipal_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.security\.auth\.kerberos\.KerberosPrincipal)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_security_auth_kerberos_kerberosprincipal}
      set-option window javax_security_auth_kerberos_kerberosprincipal_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_security_auth_x500_x500principal_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.security\.auth\.x500\.X500Principal)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_security_auth_x500_x500principal}
      set-option window javax_security_auth_x500_x500principal_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_security_sasl_sasl_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.security\.sasl\.Sasl)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_security_sasl_sasl}
      set-option window javax_security_sasl_sasl_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{javax_sound_midi_metamessage_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.sound\.midi\.MetaMessage)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_sound_midi_metamessage}
      set-option window javax_sound_midi_metamessage_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_sound_midi_midifileformat_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.sound\.midi\.MidiFileFormat)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_sound_midi_midifileformat}
      set-option window javax_sound_midi_midifileformat_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_sound_midi_sequence_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.sound\.midi\.Sequence)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_sound_midi_sequence}
      set-option window javax_sound_midi_sequence_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_sound_midi_sequencer_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.sound\.midi\.Sequencer)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_sound_midi_sequencer}
      set-option window javax_sound_midi_sequencer_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_sound_midi_shortmessage_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.sound\.midi\.ShortMessage)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_sound_midi_shortmessage}
      set-option window javax_sound_midi_shortmessage_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_sound_midi_sysexmessage_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.sound\.midi\.SysexMessage)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_sound_midi_sysexmessage}
      set-option window javax_sound_midi_sysexmessage_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_sound_sampled_audiosystem_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.sound\.sampled\.AudioSystem)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_sound_sampled_audiosystem}
      set-option window javax_sound_sampled_audiosystem_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_sound_sampled_clip_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.sound\.sampled\.Clip)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_sound_sampled_clip}
      set-option window javax_sound_sampled_clip_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{javax_sql_rowset_baserowset_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.sql\.rowset\.BaseRowSet)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_sql_rowset_baserowset}
      set-option window javax_sql_rowset_baserowset_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_sql_rowset_cachedrowset_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.sql\.rowset\.CachedRowSet)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_sql_rowset_cachedrowset}
      set-option window javax_sql_rowset_cachedrowset_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_sql_rowset_joinrowset_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.sql\.rowset\.JoinRowSet)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_sql_rowset_joinrowset}
      set-option window javax_sql_rowset_joinrowset_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_sql_rowset_webrowset_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.sql\.rowset\.WebRowSet)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_sql_rowset_webrowset}
      set-option window javax_sql_rowset_webrowset_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_sql_rowset_spi_syncfactory_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.sql\.rowset\.spi\.SyncFactory)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_sql_rowset_spi_syncfactory}
      set-option window javax_sql_rowset_spi_syncfactory_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_sql_rowset_spi_syncprovider_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.sql\.rowset\.spi\.SyncProvider)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_sql_rowset_spi_syncprovider}
      set-option window javax_sql_rowset_spi_syncprovider_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_sql_rowset_spi_syncresolver_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.sql\.rowset\.spi\.SyncResolver)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_sql_rowset_spi_syncresolver}
      set-option window javax_sql_rowset_spi_syncresolver_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{javax_swing_abstractbutton_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.AbstractButton)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_abstractbutton}
      set-option window javax_swing_abstractbutton_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_action_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.Action)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_action}
      set-option window javax_swing_action_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_boxlayout_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.BoxLayout)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_boxlayout}
      set-option window javax_swing_boxlayout_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_debuggraphics_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.DebugGraphics)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_debuggraphics}
      set-option window javax_swing_debuggraphics_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_defaultbuttonmodel_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.DefaultButtonModel)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_defaultbuttonmodel}
      set-option window javax_swing_defaultbuttonmodel_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_focusmanager_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.FocusManager)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_focusmanager}
      set-option window javax_swing_focusmanager_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_grouplayout_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.GroupLayout)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_grouplayout}
      set-option window javax_swing_grouplayout_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_jcheckbox_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.JCheckBox)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_jcheckbox}
      set-option window javax_swing_jcheckbox_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_jcolorchooser_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.JColorChooser)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_jcolorchooser}
      set-option window javax_swing_jcolorchooser_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_jcomponent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.JComponent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_jcomponent}
      set-option window javax_swing_jcomponent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_jdesktoppane_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.JDesktopPane)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_jdesktoppane}
      set-option window javax_swing_jdesktoppane_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_jeditorpane_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.JEditorPane)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_jeditorpane}
      set-option window javax_swing_jeditorpane_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_jfilechooser_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.JFileChooser)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_jfilechooser}
      set-option window javax_swing_jfilechooser_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_jformattedtextfield_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.JFormattedTextField)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_jformattedtextfield}
      set-option window javax_swing_jformattedtextfield_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_jinternalframe_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.JInternalFrame)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_jinternalframe}
      set-option window javax_swing_jinternalframe_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_jlayeredpane_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.JLayeredPane)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_jlayeredpane}
      set-option window javax_swing_jlayeredpane_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_jlist_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.JList)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_jlist}
      set-option window javax_swing_jlist_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_joptionpane_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.JOptionPane)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_joptionpane}
      set-option window javax_swing_joptionpane_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_jrootpane_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.JRootPane)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_jrootpane}
      set-option window javax_swing_jrootpane_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_jsplitpane_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.JSplitPane)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_jsplitpane}
      set-option window javax_swing_jsplitpane_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_jtabbedpane_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.JTabbedPane)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_jtabbedpane}
      set-option window javax_swing_jtabbedpane_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_jtable_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.JTable)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_jtable}
      set-option window javax_swing_jtable_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_jtextfield_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.JTextField)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_jtextfield}
      set-option window javax_swing_jtextfield_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_jtree_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.JTree)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_jtree}
      set-option window javax_swing_jtree_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_jviewport_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.JViewport)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_jviewport}
      set-option window javax_swing_jviewport_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_listselectionmodel_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.ListSelectionModel)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_listselectionmodel}
      set-option window javax_swing_listselectionmodel_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_scrollpaneconstants_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.ScrollPaneConstants)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_scrollpaneconstants}
      set-option window javax_swing_scrollpaneconstants_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_spring_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.Spring)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_spring}
      set-option window javax_swing_spring_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_springlayout_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.SpringLayout)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_springlayout}
      set-option window javax_swing_springlayout_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_swingconstants_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.SwingConstants)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_swingconstants}
      set-option window javax_swing_swingconstants_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_transferhandler_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.TransferHandler)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_transferhandler}
      set-option window javax_swing_transferhandler_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_windowconstants_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.WindowConstants)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_windowconstants}
      set-option window javax_swing_windowconstants_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_border_bevelborder_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.border\.BevelBorder)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_border_bevelborder}
      set-option window javax_swing_border_bevelborder_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_border_etchedborder_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.border\.EtchedBorder)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_border_etchedborder}
      set-option window javax_swing_border_etchedborder_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_border_titledborder_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.border\.TitledBorder)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_border_titledborder}
      set-option window javax_swing_border_titledborder_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_colorchooser_abstractcolorchooserpanel_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.colorchooser\.AbstractColorChooserPanel)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_colorchooser_abstractcolorchooserpanel}
      set-option window javax_swing_colorchooser_abstractcolorchooserpanel_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_event_ancestorevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.event\.AncestorEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_event_ancestorevent}
      set-option window javax_swing_event_ancestorevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_event_internalframeevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.event\.InternalFrameEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_event_internalframeevent}
      set-option window javax_swing_event_internalframeevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_event_listdataevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.event\.ListDataEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_event_listdataevent}
      set-option window javax_swing_event_listdataevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_event_tablemodelevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.event\.TableModelEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_event_tablemodelevent}
      set-option window javax_swing_event_tablemodelevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_plaf_basic_basiccombopopup_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.plaf\.basic\.BasicComboPopup)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_plaf_basic_basiccombopopup}
      set-option window javax_swing_plaf_basic_basiccombopopup_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_plaf_basic_basichtml_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.plaf\.basic\.BasicHTML)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_plaf_basic_basichtml}
      set-option window javax_swing_plaf_basic_basichtml_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_plaf_basic_basicinternalframeui_borderlistener_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.plaf\.basic\.BasicInternalFrameUI\.BorderListener)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_plaf_basic_basicinternalframeui_borderlistener}
      set-option window javax_swing_plaf_basic_basicinternalframeui_borderlistener_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_plaf_basic_basiclistui_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.plaf\.basic\.BasicListUI)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_plaf_basic_basiclistui}
      set-option window javax_swing_plaf_basic_basiclistui_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_plaf_basic_basicoptionpaneui_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.plaf\.basic\.BasicOptionPaneUI)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_plaf_basic_basicoptionpaneui}
      set-option window javax_swing_plaf_basic_basicoptionpaneui_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_plaf_basic_basicscrollbarui_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.plaf\.basic\.BasicScrollBarUI)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_plaf_basic_basicscrollbarui}
      set-option window javax_swing_plaf_basic_basicscrollbarui_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_plaf_basic_basicsliderui_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.plaf\.basic\.BasicSliderUI)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_plaf_basic_basicsliderui}
      set-option window javax_swing_plaf_basic_basicsliderui_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_plaf_basic_basicsplitpanedivider_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.plaf\.basic\.BasicSplitPaneDivider)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_plaf_basic_basicsplitpanedivider}
      set-option window javax_swing_plaf_basic_basicsplitpanedivider_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_plaf_basic_basicsplitpaneui_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.plaf\.basic\.BasicSplitPaneUI)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_plaf_basic_basicsplitpaneui}
      set-option window javax_swing_plaf_basic_basicsplitpaneui_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_plaf_metal_metaliconfactory_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.plaf\.metal\.MetalIconFactory)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_plaf_metal_metaliconfactory}
      set-option window javax_swing_plaf_metal_metaliconfactory_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_plaf_metal_metalscrollbarui_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.plaf\.metal\.MetalScrollBarUI)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_plaf_metal_metalscrollbarui}
      set-option window javax_swing_plaf_metal_metalscrollbarui_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_plaf_metal_metalsliderui_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.plaf\.metal\.MetalSliderUI)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_plaf_metal_metalsliderui}
      set-option window javax_swing_plaf_metal_metalsliderui_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_plaf_metal_metaltooltipui_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.plaf\.metal\.MetalToolTipUI)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_plaf_metal_metaltooltipui}
      set-option window javax_swing_plaf_metal_metaltooltipui_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_plaf_nimbus_nimbusstyle_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.plaf\.nimbus\.NimbusStyle)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_plaf_nimbus_nimbusstyle}
      set-option window javax_swing_plaf_nimbus_nimbusstyle_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_plaf_synth_synthconstants_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.plaf\.synth\.SynthConstants)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_plaf_synth_synthconstants}
      set-option window javax_swing_plaf_synth_synthconstants_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_table_tablecolumn_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.table\.TableColumn)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_table_tablecolumn}
      set-option window javax_swing_table_tablecolumn_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_text_abstractdocument_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.text\.AbstractDocument)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_text_abstractdocument}
      set-option window javax_swing_text_abstractdocument_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_text_abstractwriter_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.text\.AbstractWriter)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_text_abstractwriter}
      set-option window javax_swing_text_abstractwriter_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_text_defaultcaret_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.text\.DefaultCaret)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_text_defaultcaret}
      set-option window javax_swing_text_defaultcaret_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_text_defaulteditorkit_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.text\.DefaultEditorKit)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_text_defaulteditorkit}
      set-option window javax_swing_text_defaulteditorkit_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_text_defaultstyleddocument_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.text\.DefaultStyledDocument)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_text_defaultstyleddocument}
      set-option window javax_swing_text_defaultstyleddocument_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_text_defaultstyleddocument_elementspec_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.text\.DefaultStyledDocument\.ElementSpec)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_text_defaultstyleddocument_elementspec}
      set-option window javax_swing_text_defaultstyleddocument_elementspec_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_text_document_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.text\.Document)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_text_document}
      set-option window javax_swing_text_document_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_text_jtextcomponent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.text\.JTextComponent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_text_jtextcomponent}
      set-option window javax_swing_text_jtextcomponent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_text_plaindocument_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.text\.PlainDocument)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_text_plaindocument}
      set-option window javax_swing_text_plaindocument_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_text_styleconstants_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.text\.StyleConstants)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_text_styleconstants}
      set-option window javax_swing_text_styleconstants_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_text_stylecontext_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.text\.StyleContext)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_text_stylecontext}
      set-option window javax_swing_text_stylecontext_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_text_tabstop_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.text\.TabStop)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_text_tabstop}
      set-option window javax_swing_text_tabstop_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_text_view_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.text\.View)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_text_view}
      set-option window javax_swing_text_view_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_text_html_html_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.text\.html\.HTML)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_text_html_html}
      set-option window javax_swing_text_html_html_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_text_html_htmldocument_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.text\.html\.HTMLDocument)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_text_html_htmldocument}
      set-option window javax_swing_text_html_htmldocument_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_text_html_htmleditorkit_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.text\.html\.HTMLEditorKit)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_text_html_htmleditorkit}
      set-option window javax_swing_text_html_htmleditorkit_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_text_html_parser_dtd_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.text\.html\.parser\.DTD)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_text_html_parser_dtd}
      set-option window javax_swing_text_html_parser_dtd_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_text_html_parser_dtdconstants_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.text\.html\.parser\.DTDConstants)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_text_html_parser_dtdconstants}
      set-option window javax_swing_text_html_parser_dtdconstants_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_tree_defaulttreeselectionmodel_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.tree\.DefaultTreeSelectionModel)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_tree_defaulttreeselectionmodel}
      set-option window javax_swing_tree_defaulttreeselectionmodel_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_tree_treeselectionmodel_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.tree\.TreeSelectionModel)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_tree_treeselectionmodel}
      set-option window javax_swing_tree_treeselectionmodel_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_undo_abstractundoableedit_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.undo\.AbstractUndoableEdit)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_undo_abstractundoableedit}
      set-option window javax_swing_undo_abstractundoableedit_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_undo_stateedit_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.undo\.StateEdit)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_undo_stateedit}
      set-option window javax_swing_undo_stateedit_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_undo_stateeditable_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.undo\.StateEditable)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_undo_stateeditable}
      set-option window javax_swing_undo_stateeditable_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{javax_tools_diagnostic_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.tools\.Diagnostic)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_tools_diagnostic}
      set-option window javax_tools_diagnostic_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{javax_transaction_xa_xaexception_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.transaction\.xa\.XAException)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_transaction_xa_xaexception}
      set-option window javax_transaction_xa_xaexception_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_transaction_xa_xaresource_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.transaction\.xa\.XAResource)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_transaction_xa_xaresource}
      set-option window javax_transaction_xa_xaresource_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_transaction_xa_xid_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.transaction\.xa\.Xid)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_transaction_xa_xid}
      set-option window javax_transaction_xa_xid_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{javax_xml_xmlconstants_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.XMLConstants)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_xmlconstants}
      set-option window javax_xml_xmlconstants_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_crypto_dsig_canonicalizationmethod_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.CanonicalizationMethod)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_crypto_dsig_canonicalizationmethod}
      set-option window javax_xml_crypto_dsig_canonicalizationmethod_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_crypto_dsig_digestmethod_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.DigestMethod)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_crypto_dsig_digestmethod}
      set-option window javax_xml_crypto_dsig_digestmethod_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_crypto_dsig_manifest_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.Manifest)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_crypto_dsig_manifest}
      set-option window javax_xml_crypto_dsig_manifest_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_crypto_dsig_signaturemethod_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.SignatureMethod)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_crypto_dsig_signaturemethod}
      set-option window javax_xml_crypto_dsig_signaturemethod_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_crypto_dsig_signatureproperties_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.SignatureProperties)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_crypto_dsig_signatureproperties}
      set-option window javax_xml_crypto_dsig_signatureproperties_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_crypto_dsig_transform_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.Transform)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_crypto_dsig_transform}
      set-option window javax_xml_crypto_dsig_transform_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_crypto_dsig_xmlobject_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.XMLObject)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_crypto_dsig_xmlobject}
      set-option window javax_xml_crypto_dsig_xmlobject_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_crypto_dsig_xmlsignature_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.XMLSignature)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_crypto_dsig_xmlsignature}
      set-option window javax_xml_crypto_dsig_xmlsignature_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_crypto_dsig_keyinfo_keyvalue_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.keyinfo\.KeyValue)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_crypto_dsig_keyinfo_keyvalue}
      set-option window javax_xml_crypto_dsig_keyinfo_keyvalue_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_crypto_dsig_keyinfo_pgpdata_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.keyinfo\.PGPData)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_crypto_dsig_keyinfo_pgpdata}
      set-option window javax_xml_crypto_dsig_keyinfo_pgpdata_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_crypto_dsig_keyinfo_x509data_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.keyinfo\.X509Data)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_crypto_dsig_keyinfo_x509data}
      set-option window javax_xml_crypto_dsig_keyinfo_x509data_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_crypto_dsig_spec_excc14nparameterspec_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.crypto\.dsig\.spec\.ExcC14NParameterSpec)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_crypto_dsig_spec_excc14nparameterspec}
      set-option window javax_xml_crypto_dsig_spec_excc14nparameterspec_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_datatype_datatypeconstants_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.datatype\.DatatypeConstants)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_datatype_datatypeconstants}
      set-option window javax_xml_datatype_datatypeconstants_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_datatype_datatypefactory_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.datatype\.DatatypeFactory)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_datatype_datatypefactory}
      set-option window javax_xml_datatype_datatypefactory_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_stream_xmlinputfactory_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.stream\.XMLInputFactory)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_stream_xmlinputfactory}
      set-option window javax_xml_stream_xmlinputfactory_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_stream_xmloutputfactory_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.stream\.XMLOutputFactory)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_stream_xmloutputfactory}
      set-option window javax_xml_stream_xmloutputfactory_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_stream_xmlstreamconstants_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.stream\.XMLStreamConstants)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_stream_xmlstreamconstants}
      set-option window javax_xml_stream_xmlstreamconstants_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_transform_outputkeys_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.transform\.OutputKeys)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_transform_outputkeys}
      set-option window javax_xml_transform_outputkeys_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_transform_result_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.transform\.Result)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_transform_result}
      set-option window javax_xml_transform_result_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_transform_dom_domresult_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.transform\.dom\.DOMResult)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_transform_dom_domresult}
      set-option window javax_xml_transform_dom_domresult_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_transform_dom_domsource_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.transform\.dom\.DOMSource)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_transform_dom_domsource}
      set-option window javax_xml_transform_dom_domsource_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_transform_sax_saxresult_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.transform\.sax\.SAXResult)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_transform_sax_saxresult}
      set-option window javax_xml_transform_sax_saxresult_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_transform_sax_saxsource_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.transform\.sax\.SAXSource)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_transform_sax_saxsource}
      set-option window javax_xml_transform_sax_saxsource_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_transform_sax_saxtransformerfactory_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.transform\.sax\.SAXTransformerFactory)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_transform_sax_saxtransformerfactory}
      set-option window javax_xml_transform_sax_saxtransformerfactory_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_transform_stax_staxresult_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.transform\.stax\.StAXResult)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_transform_stax_staxresult}
      set-option window javax_xml_transform_stax_staxresult_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_transform_stax_staxsource_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.transform\.stax\.StAXSource)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_transform_stax_staxsource}
      set-option window javax_xml_transform_stax_staxsource_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_transform_stream_streamresult_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.transform\.stream\.StreamResult)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_transform_stream_streamresult}
      set-option window javax_xml_transform_stream_streamresult_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_transform_stream_streamsource_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.transform\.stream\.StreamSource)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_transform_stream_streamsource}
      set-option window javax_xml_transform_stream_streamsource_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_xpath_xpathconstants_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.xpath\.XPathConstants)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_xpath_xpathconstants}
      set-option window javax_xml_xpath_xpathconstants_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_xpath_xpathfactory_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.xpath\.XPathFactory)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_xpath_xpathfactory}
      set-option window javax_xml_xpath_xpathfactory_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{jdk_dynalink_securelookupsupplier_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.dynalink\.SecureLookupSupplier)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_dynalink_securelookupsupplier}
      set-option window jdk_dynalink_securelookupsupplier_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{jdk_dynalink_linker_guardingdynamiclinkerexporter_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.dynalink\.linker\.GuardingDynamicLinkerExporter)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_dynalink_linker_guardingdynamiclinkerexporter}
      set-option window jdk_dynalink_linker_guardingdynamiclinkerexporter_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{jdk_incubator_foreign_memorylayout_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.incubator\.foreign\.MemoryLayout)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_incubator_foreign_memorylayout}
      set-option window jdk_incubator_foreign_memorylayout_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{jdk_incubator_foreign_memorysegment_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.incubator\.foreign\.MemorySegment)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_incubator_foreign_memorysegment}
      set-option window jdk_incubator_foreign_memorysegment_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{jdk_jfr_dataamount_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.jfr\.DataAmount)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_jfr_dataamount}
      set-option window jdk_jfr_dataamount_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{jdk_jfr_enabled_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.jfr\.Enabled)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_jfr_enabled}
      set-option window jdk_jfr_enabled_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{jdk_jfr_period_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.jfr\.Period)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_jfr_period}
      set-option window jdk_jfr_period_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{jdk_jfr_stacktrace_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.jfr\.StackTrace)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_jfr_stacktrace}
      set-option window jdk_jfr_stacktrace_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{jdk_jfr_threshold_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.jfr\.Threshold)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_jfr_threshold}
      set-option window jdk_jfr_threshold_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{jdk_jfr_timespan_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.jfr\.Timespan)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_jfr_timespan}
      set-option window jdk_jfr_timespan_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{jdk_jfr_timestamp_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.jfr\.Timestamp)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_jfr_timestamp}
      set-option window jdk_jfr_timestamp_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{jdk_jshell_diag_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.jshell\.Diag)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_jshell_diag}
      set-option window jdk_jshell_diag_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{jdk_jshell_execution_jdiexecutioncontrolprovider_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.jshell\.execution\.JdiExecutionControlProvider)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_jshell_execution_jdiexecutioncontrolprovider}
      set-option window jdk_jshell_execution_jdiexecutioncontrolprovider_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{jdk_management_jfr_flightrecordermxbean_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.management\.jfr\.FlightRecorderMXBean)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_management_jfr_flightrecordermxbean}
      set-option window jdk_management_jfr_flightrecordermxbean_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{org_ietf_jgss_gsscontext_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.ietf\.jgss\.GSSContext)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_ietf_jgss_gsscontext}
      set-option window org_ietf_jgss_gsscontext_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{org_ietf_jgss_gsscredential_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.ietf\.jgss\.GSSCredential)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_ietf_jgss_gsscredential}
      set-option window org_ietf_jgss_gsscredential_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{org_ietf_jgss_gssexception_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.ietf\.jgss\.GSSException)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_ietf_jgss_gssexception}
      set-option window org_ietf_jgss_gssexception_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{org_w3c_dom_domerror_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.DOMError)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_w3c_dom_domerror}
      set-option window org_w3c_dom_domerror_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{org_w3c_dom_domexception_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.DOMException)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_w3c_dom_domexception}
      set-option window org_w3c_dom_domexception_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{org_w3c_dom_node_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.Node)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_w3c_dom_node}
      set-option window org_w3c_dom_node_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{org_w3c_dom_typeinfo_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.TypeInfo)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_w3c_dom_typeinfo}
      set-option window org_w3c_dom_typeinfo_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{org_w3c_dom_userdatahandler_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.UserDataHandler)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_w3c_dom_userdatahandler}
      set-option window org_w3c_dom_userdatahandler_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{org_w3c_dom_bootstrap_domimplementationregistry_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.bootstrap\.DOMImplementationRegistry)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_w3c_dom_bootstrap_domimplementationregistry}
      set-option window org_w3c_dom_bootstrap_domimplementationregistry_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{org_w3c_dom_css_cssprimitivevalue_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.css\.CSSPrimitiveValue)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_w3c_dom_css_cssprimitivevalue}
      set-option window org_w3c_dom_css_cssprimitivevalue_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{org_w3c_dom_css_cssrule_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.css\.CSSRule)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_w3c_dom_css_cssrule}
      set-option window org_w3c_dom_css_cssrule_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{org_w3c_dom_css_cssvalue_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.css\.CSSValue)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_w3c_dom_css_cssvalue}
      set-option window org_w3c_dom_css_cssvalue_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{org_w3c_dom_events_event_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.events\.Event)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_w3c_dom_events_event}
      set-option window org_w3c_dom_events_event_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{org_w3c_dom_events_eventexception_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.events\.EventException)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_w3c_dom_events_eventexception}
      set-option window org_w3c_dom_events_eventexception_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{org_w3c_dom_events_mutationevent_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.events\.MutationEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_w3c_dom_events_mutationevent}
      set-option window org_w3c_dom_events_mutationevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{org_w3c_dom_ls_domimplementationls_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.ls\.DOMImplementationLS)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_w3c_dom_ls_domimplementationls}
      set-option window org_w3c_dom_ls_domimplementationls_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{org_w3c_dom_ls_lsexception_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.ls\.LSException)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_w3c_dom_ls_lsexception}
      set-option window org_w3c_dom_ls_lsexception_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{org_w3c_dom_ls_lsparser_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.ls\.LSParser)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_w3c_dom_ls_lsparser}
      set-option window org_w3c_dom_ls_lsparser_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{org_w3c_dom_ls_lsparserfilter_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.ls\.LSParserFilter)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_w3c_dom_ls_lsparserfilter}
      set-option window org_w3c_dom_ls_lsparserfilter_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{org_w3c_dom_ranges_range_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.ranges\.Range)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_w3c_dom_ranges_range}
      set-option window org_w3c_dom_ranges_range_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{org_w3c_dom_ranges_rangeexception_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.ranges\.RangeException)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_w3c_dom_ranges_rangeexception}
      set-option window org_w3c_dom_ranges_rangeexception_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{org_w3c_dom_traversal_nodefilter_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.traversal\.NodeFilter)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_w3c_dom_traversal_nodefilter}
      set-option window org_w3c_dom_traversal_nodefilter_active "nop"
    }
  }
 try %{
    evaluate-commands %opt{org_w3c_dom_xpath_xpathexception_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.xpath\.XPathException)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_w3c_dom_xpath_xpathexception}
      set-option window org_w3c_dom_xpath_xpathexception_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{org_w3c_dom_xpath_xpathnamespace_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.xpath\.XPathNamespace)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_w3c_dom_xpath_xpathnamespace}
      set-option window org_w3c_dom_xpath_xpathnamespace_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{org_w3c_dom_xpath_xpathresult_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.w3c\.dom\.xpath\.XPathResult)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_w3c_dom_xpath_xpathresult}
      set-option window org_w3c_dom_xpath_xpathresult_active "nop"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{org_xml_sax_helpers_namespacesupport_active}
  } catch %{
    try %{
      execute-keys -draft '%s (?:org\.xml\.sax\.helpers\.NamespaceSupport)<ret>'
    } catch %{
      set-option -remove window static_words %opt{org_xml_sax_helpers_namespacesupport}
      set-option window org_xml_sax_helpers_namespacesupport_active "nop"
    }
  }
}
# -------------------------------------------------------------------------------------------------- #
# ------------------------------------- JAVA ENUMS ADD --------------------------------------------- #
define-command -hidden java-enums-add %{
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:com\.sun\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:com\.sun\.management\.VMOption\.Origin)<ret>'
      evaluate-commands %opt{com_sun_management_vmoption_origin_active}
      set-option -add window static_words %opt{com_sun_management_vmoption_origin}
      set-option window com_sun_management_vmoption_origin_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:com\.sun\.nio\.sctp\.AssociationChangeNotification\.AssocChangeEvent)<ret>'
      evaluate-commands %opt{com_sun_nio_sctp_associationchangenotification_assocchangeevent_active}
      set-option -add window static_words %opt{com_sun_nio_sctp_associationchangenotification_assocchangeevent}
      set-option window com_sun_nio_sctp_associationchangenotification_assocchangeevent_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:com\.sun\.nio\.sctp\.HandlerResult)<ret>'
      evaluate-commands %opt{com_sun_nio_sctp_handlerresult_active}
      set-option -add window static_words %opt{com_sun_nio_sctp_handlerresult}
      set-option window com_sun_nio_sctp_handlerresult_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:com\.sun\.nio\.sctp\.PeerAddressChangeNotification\.AddressChangeEvent)<ret>'
      evaluate-commands %opt{com_sun_nio_sctp_peeraddresschangenotification_addresschangeevent_active}
      set-option -add window static_words %opt{com_sun_nio_sctp_peeraddresschangenotification_addresschangeevent}
      set-option window com_sun_nio_sctp_peeraddresschangenotification_addresschangeevent_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:com\.sun\.security\.jgss\.InquireType)<ret>'
      evaluate-commands %opt{com_sun_security_jgss_inquiretype_active}
      set-option -add window static_words %opt{com_sun_security_jgss_inquiretype}
      set-option window com_sun_security_jgss_inquiretype_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:com\.sun\.source\.doctree\.AttributeTree\.ValueKind)<ret>'
      evaluate-commands %opt{com_sun_source_doctree_attributetree_valuekind_active}
      set-option -add window static_words %opt{com_sun_source_doctree_attributetree_valuekind}
      set-option window com_sun_source_doctree_attributetree_valuekind_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:com\.sun\.source\.doctree\.DocTree\.Kind)<ret>'
      evaluate-commands %opt{com_sun_source_doctree_doctree_kind_active}
      set-option -add window static_words %opt{com_sun_source_doctree_doctree_kind}
      set-option window com_sun_source_doctree_doctree_kind_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:com\.sun\.source\.tree\.CaseTree\.CaseKind)<ret>'
      evaluate-commands %opt{com_sun_source_tree_casetree_casekind_active}
      set-option -add window static_words %opt{com_sun_source_tree_casetree_casekind}
      set-option window com_sun_source_tree_casetree_casekind_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:com\.sun\.source\.tree\.LambdaExpressionTree\.BodyKind)<ret>'
      evaluate-commands %opt{com_sun_source_tree_lambdaexpressiontree_bodykind_active}
      set-option -add window static_words %opt{com_sun_source_tree_lambdaexpressiontree_bodykind}
      set-option window com_sun_source_tree_lambdaexpressiontree_bodykind_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:com\.sun\.source\.tree\.MemberReferenceTree\.ReferenceMode)<ret>'
      evaluate-commands %opt{com_sun_source_tree_memberreferencetree_referencemode_active}
      set-option -add window static_words %opt{com_sun_source_tree_memberreferencetree_referencemode}
      set-option window com_sun_source_tree_memberreferencetree_referencemode_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:com\.sun\.source\.tree\.ModuleTree\.ModuleKind)<ret>'
      evaluate-commands %opt{com_sun_source_tree_moduletree_modulekind_active}
      set-option -add window static_words %opt{com_sun_source_tree_moduletree_modulekind}
      set-option window com_sun_source_tree_moduletree_modulekind_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:com\.sun\.source\.tree\.Tree\.Kind)<ret>'
      evaluate-commands %opt{com_sun_source_tree_tree_kind_active}
      set-option -add window static_words %opt{com_sun_source_tree_tree_kind}
      set-option window com_sun_source_tree_tree_kind_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:com\.sun\.source\.util\.TaskEvent\.Kind)<ret>'
      evaluate-commands %opt{com_sun_source_util_taskevent_kind_active}
      set-option -add window static_words %opt{com_sun_source_util_taskevent_kind}
      set-option window com_sun_source_util_taskevent_kind_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:com\.sun\.tools\.jconsole\.JConsoleContext\.ConnectionState)<ret>'
      evaluate-commands %opt{com_sun_tools_jconsole_jconsolecontext_connectionstate_active}
      set-option -add window static_words %opt{com_sun_tools_jconsole_jconsolecontext_connectionstate}
      set-option window com_sun_tools_jconsole_jconsolecontext_connectionstate_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.awt\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.awt\.Component\.BaselineResizeBehavior)<ret>'
      evaluate-commands %opt{java_awt_component_baselineresizebehavior_active}
      set-option -add window static_words %opt{java_awt_component_baselineresizebehavior}
      set-option window java_awt_component_baselineresizebehavior_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.awt\.Desktop\.Action)<ret>'
      evaluate-commands %opt{java_awt_desktop_action_active}
      set-option -add window static_words %opt{java_awt_desktop_action}
      set-option window java_awt_desktop_action_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.awt\.desktop\.QuitStrategy)<ret>'
      evaluate-commands %opt{java_awt_desktop_quitstrategy_active}
      set-option -add window static_words %opt{java_awt_desktop_quitstrategy}
      set-option window java_awt_desktop_quitstrategy_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.awt\.desktop\.UserSessionEvent\.Reason)<ret>'
      evaluate-commands %opt{java_awt_desktop_usersessionevent_reason_active}
      set-option -add window static_words %opt{java_awt_desktop_usersessionevent_reason}
      set-option window java_awt_desktop_usersessionevent_reason_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.awt\.Dialog\.ModalExclusionType)<ret>'
      evaluate-commands %opt{java_awt_dialog_modalexclusiontype_active}
      set-option -add window static_words %opt{java_awt_dialog_modalexclusiontype}
      set-option window java_awt_dialog_modalexclusiontype_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.awt\.Dialog\.ModalityType)<ret>'
      evaluate-commands %opt{java_awt_dialog_modalitytype_active}
      set-option -add window static_words %opt{java_awt_dialog_modalitytype}
      set-option window java_awt_dialog_modalitytype_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.awt\.event\.FocusEvent\.Cause)<ret>'
      evaluate-commands %opt{java_awt_event_focusevent_cause_active}
      set-option -add window static_words %opt{java_awt_event_focusevent_cause}
      set-option window java_awt_event_focusevent_cause_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.awt\.font\.NumericShaper\.Range)<ret>'
      evaluate-commands %opt{java_awt_font_numericshaper_range_active}
      set-option -add window static_words %opt{java_awt_font_numericshaper_range}
      set-option window java_awt_font_numericshaper_range_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.awt\.GraphicsDevice\.WindowTranslucency)<ret>'
      evaluate-commands %opt{java_awt_graphicsdevice_windowtranslucency_active}
      set-option -add window static_words %opt{java_awt_graphicsdevice_windowtranslucency}
      set-option window java_awt_graphicsdevice_windowtranslucency_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.awt\.MultipleGradientPaint\.ColorSpaceType)<ret>'
      evaluate-commands %opt{java_awt_multiplegradientpaint_colorspacetype_active}
      set-option -add window static_words %opt{java_awt_multiplegradientpaint_colorspacetype}
      set-option window java_awt_multiplegradientpaint_colorspacetype_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.awt\.MultipleGradientPaint\.CycleMethod)<ret>'
      evaluate-commands %opt{java_awt_multiplegradientpaint_cyclemethod_active}
      set-option -add window static_words %opt{java_awt_multiplegradientpaint_cyclemethod}
      set-option window java_awt_multiplegradientpaint_cyclemethod_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.awt\.Taskbar\.Feature)<ret>'
      evaluate-commands %opt{java_awt_taskbar_feature_active}
      set-option -add window static_words %opt{java_awt_taskbar_feature}
      set-option window java_awt_taskbar_feature_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.awt\.Taskbar\.State)<ret>'
      evaluate-commands %opt{java_awt_taskbar_state_active}
      set-option -add window static_words %opt{java_awt_taskbar_state}
      set-option window java_awt_taskbar_state_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.awt\.TrayIcon\.MessageType)<ret>'
      evaluate-commands %opt{java_awt_trayicon_messagetype_active}
      set-option -add window static_words %opt{java_awt_trayicon_messagetype}
      set-option window java_awt_trayicon_messagetype_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.awt\.Window\.Type)<ret>'
      evaluate-commands %opt{java_awt_window_type_active}
      set-option -add window static_words %opt{java_awt_window_type}
      set-option window java_awt_window_type_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.io\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.io\.ObjectInputFilter\.Status)<ret>'
      evaluate-commands %opt{java_io_objectinputfilter_status_active}
      set-option -add window static_words %opt{java_io_objectinputfilter_status}
      set-option window java_io_objectinputfilter_status_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.lang\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.lang\.annotation\.ElementType)<ret>'
      evaluate-commands %opt{java_lang_annotation_elementtype_active}
      set-option -add window static_words %opt{java_lang_annotation_elementtype}
      set-option window java_lang_annotation_elementtype_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.lang\.annotation\.RetentionPolicy)<ret>'
      evaluate-commands %opt{java_lang_annotation_retentionpolicy_active}
      set-option -add window static_words %opt{java_lang_annotation_retentionpolicy}
      set-option window java_lang_annotation_retentionpolicy_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.lang\.Character\.UnicodeScript)<ret>'
      evaluate-commands %opt{java_lang_character_unicodescript_active}
      set-option -add window static_words %opt{java_lang_character_unicodescript}
      set-option window java_lang_character_unicodescript_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.lang\.constant\.DirectMethodHandleDesc\.Kind)<ret>'
      evaluate-commands %opt{java_lang_constant_directmethodhandledesc_kind_active}
      set-option -add window static_words %opt{java_lang_constant_directmethodhandledesc_kind}
      set-option window java_lang_constant_directmethodhandledesc_kind_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.lang\.invoke\.MethodHandles\.Lookup\.ClassOption)<ret>'
      evaluate-commands %opt{java_lang_invoke_methodhandles_lookup_classoption_active}
      set-option -add window static_words %opt{java_lang_invoke_methodhandles_lookup_classoption}
      set-option window java_lang_invoke_methodhandles_lookup_classoption_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.lang\.invoke\.VarHandle\.AccessMode)<ret>'
      evaluate-commands %opt{java_lang_invoke_varhandle_accessmode_active}
      set-option -add window static_words %opt{java_lang_invoke_varhandle_accessmode}
      set-option window java_lang_invoke_varhandle_accessmode_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.lang\.management\.MemoryType)<ret>'
      evaluate-commands %opt{java_lang_management_memorytype_active}
      set-option -add window static_words %opt{java_lang_management_memorytype}
      set-option window java_lang_management_memorytype_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.lang\.module\.ModuleDescriptor\.Exports\.Modifier)<ret>'
      evaluate-commands %opt{java_lang_module_moduledescriptor_exports_modifier_active}
      set-option -add window static_words %opt{java_lang_module_moduledescriptor_exports_modifier}
      set-option window java_lang_module_moduledescriptor_exports_modifier_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.lang\.module\.ModuleDescriptor\.Modifier)<ret>'
      evaluate-commands %opt{java_lang_module_moduledescriptor_modifier_active}
      set-option -add window static_words %opt{java_lang_module_moduledescriptor_modifier}
      set-option window java_lang_module_moduledescriptor_modifier_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.lang\.module\.ModuleDescriptor\.Opens\.Modifier)<ret>'
      evaluate-commands %opt{java_lang_module_moduledescriptor_opens_modifier_active}
      set-option -add window static_words %opt{java_lang_module_moduledescriptor_opens_modifier}
      set-option window java_lang_module_moduledescriptor_opens_modifier_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.lang\.module\.ModuleDescriptor\.Requires\.Modifier)<ret>'
      evaluate-commands %opt{java_lang_module_moduledescriptor_requires_modifier_active}
      set-option -add window static_words %opt{java_lang_module_moduledescriptor_requires_modifier}
      set-option window java_lang_module_moduledescriptor_requires_modifier_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.lang\.ProcessBuilder\.Redirect\.Type)<ret>'
      evaluate-commands %opt{java_lang_processbuilder_redirect_type_active}
      set-option -add window static_words %opt{java_lang_processbuilder_redirect_type}
      set-option window java_lang_processbuilder_redirect_type_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.lang\.StackWalker\.Option)<ret>'
      evaluate-commands %opt{java_lang_stackwalker_option_active}
      set-option -add window static_words %opt{java_lang_stackwalker_option}
      set-option window java_lang_stackwalker_option_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.lang\.System\.Logger\.Level)<ret>'
      evaluate-commands %opt{java_lang_system_logger_level_active}
      set-option -add window static_words %opt{java_lang_system_logger_level}
      set-option window java_lang_system_logger_level_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.lang\.Thread\.State)<ret>'
      evaluate-commands %opt{java_lang_thread_state_active}
      set-option -add window static_words %opt{java_lang_thread_state}
      set-option window java_lang_thread_state_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.math\.[A-Z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.math\.RoundingMode)<ret>'
      evaluate-commands %opt{java_math_roundingmode_active}
      set-option -add window static_words %opt{java_math_roundingmode}
      set-option window java_math_roundingmode_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.net\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.net\.Authenticator\.RequestorType)<ret>'
      evaluate-commands %opt{java_net_authenticator_requestortype_active}
      set-option -add window static_words %opt{java_net_authenticator_requestortype}
      set-option window java_net_authenticator_requestortype_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.net\.http\.HttpClient\.Redirect)<ret>'
      evaluate-commands %opt{java_net_http_httpclient_redirect_active}
      set-option -add window static_words %opt{java_net_http_httpclient_redirect}
      set-option window java_net_http_httpclient_redirect_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.net\.http\.HttpClient\.Version)<ret>'
      evaluate-commands %opt{java_net_http_httpclient_version_active}
      set-option -add window static_words %opt{java_net_http_httpclient_version}
      set-option window java_net_http_httpclient_version_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.net\.Proxy\.Type)<ret>'
      evaluate-commands %opt{java_net_proxy_type_active}
      set-option -add window static_words %opt{java_net_proxy_type}
      set-option window java_net_proxy_type_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.net\.StandardProtocolFamily)<ret>'
      evaluate-commands %opt{java_net_standardprotocolfamily_active}
      set-option -add window static_words %opt{java_net_standardprotocolfamily}
      set-option window java_net_standardprotocolfamily_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.nio\.[a-z]+\.[A-Z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.nio\.file\.AccessMode)<ret>'
      evaluate-commands %opt{java_nio_file_accessmode_active}
      set-option -add window static_words %opt{java_nio_file_accessmode}
      set-option window java_nio_file_accessmode_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.nio\.file\.attribute\.AclEntryFlag)<ret>'
      evaluate-commands %opt{java_nio_file_attribute_aclentryflag_active}
      set-option -add window static_words %opt{java_nio_file_attribute_aclentryflag}
      set-option window java_nio_file_attribute_aclentryflag_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.nio\.file\.attribute\.AclEntryPermission)<ret>'
      evaluate-commands %opt{java_nio_file_attribute_aclentrypermission_active}
      set-option -add window static_words %opt{java_nio_file_attribute_aclentrypermission}
      set-option window java_nio_file_attribute_aclentrypermission_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.nio\.file\.attribute\.AclEntryType)<ret>'
      evaluate-commands %opt{java_nio_file_attribute_aclentrytype_active}
      set-option -add window static_words %opt{java_nio_file_attribute_aclentrytype}
      set-option window java_nio_file_attribute_aclentrytype_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.nio\.file\.attribute\.PosixFilePermission)<ret>'
      evaluate-commands %opt{java_nio_file_attribute_posixfilepermission_active}
      set-option -add window static_words %opt{java_nio_file_attribute_posixfilepermission}
      set-option window java_nio_file_attribute_posixfilepermission_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.nio\.file\.FileVisitOption)<ret>'
      evaluate-commands %opt{java_nio_file_filevisitoption_active}
      set-option -add window static_words %opt{java_nio_file_filevisitoption}
      set-option window java_nio_file_filevisitoption_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.nio\.file\.FileVisitResult)<ret>'
      evaluate-commands %opt{java_nio_file_filevisitresult_active}
      set-option -add window static_words %opt{java_nio_file_filevisitresult}
      set-option window java_nio_file_filevisitresult_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.nio\.file\.LinkOption)<ret>'
      evaluate-commands %opt{java_nio_file_linkoption_active}
      set-option -add window static_words %opt{java_nio_file_linkoption}
      set-option window java_nio_file_linkoption_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.nio\.file\.StandardCopyOption)<ret>'
      evaluate-commands %opt{java_nio_file_standardcopyoption_active}
      set-option -add window static_words %opt{java_nio_file_standardcopyoption}
      set-option window java_nio_file_standardcopyoption_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.nio\.file\.StandardOpenOption)<ret>'
      evaluate-commands %opt{java_nio_file_standardopenoption_active}
      set-option -add window static_words %opt{java_nio_file_standardopenoption}
      set-option window java_nio_file_standardopenoption_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.security\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.security\.cert\.CertPathValidatorException\.BasicReason)<ret>'
      evaluate-commands %opt{java_security_cert_certpathvalidatorexception_basicreason_active}
      set-option -add window static_words %opt{java_security_cert_certpathvalidatorexception_basicreason}
      set-option window java_security_cert_certpathvalidatorexception_basicreason_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.security\.cert\.CRLReason)<ret>'
      evaluate-commands %opt{java_security_cert_crlreason_active}
      set-option -add window static_words %opt{java_security_cert_crlreason}
      set-option window java_security_cert_crlreason_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.security\.cert\.PKIXReason)<ret>'
      evaluate-commands %opt{java_security_cert_pkixreason_active}
      set-option -add window static_words %opt{java_security_cert_pkixreason}
      set-option window java_security_cert_pkixreason_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.security\.cert\.PKIXRevocationChecker\.Option)<ret>'
      evaluate-commands %opt{java_security_cert_pkixrevocationchecker_option_active}
      set-option -add window static_words %opt{java_security_cert_pkixrevocationchecker_option}
      set-option window java_security_cert_pkixrevocationchecker_option_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.security\.CryptoPrimitive)<ret>'
      evaluate-commands %opt{java_security_cryptoprimitive_active}
      set-option -add window static_words %opt{java_security_cryptoprimitive}
      set-option window java_security_cryptoprimitive_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.security\.DrbgParameters\.Capability)<ret>'
      evaluate-commands %opt{java_security_drbgparameters_capability_active}
      set-option -add window static_words %opt{java_security_drbgparameters_capability}
      set-option window java_security_drbgparameters_capability_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.security\.KeyRep\.Type)<ret>'
      evaluate-commands %opt{java_security_keyrep_type_active}
      set-option -add window static_words %opt{java_security_keyrep_type}
      set-option window java_security_keyrep_type_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.sql\.[A-Z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.sql\.ClientInfoStatus)<ret>'
      evaluate-commands %opt{java_sql_clientinfostatus_active}
      set-option -add window static_words %opt{java_sql_clientinfostatus}
      set-option window java_sql_clientinfostatus_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.sql\.JDBCType)<ret>'
      evaluate-commands %opt{java_sql_jdbctype_active}
      set-option -add window static_words %opt{java_sql_jdbctype}
      set-option window java_sql_jdbctype_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.sql\.PseudoColumnUsage)<ret>'
      evaluate-commands %opt{java_sql_pseudocolumnusage_active}
      set-option -add window static_words %opt{java_sql_pseudocolumnusage}
      set-option window java_sql_pseudocolumnusage_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.sql\.RowIdLifetime)<ret>'
      evaluate-commands %opt{java_sql_rowidlifetime_active}
      set-option -add window static_words %opt{java_sql_rowidlifetime}
      set-option window java_sql_rowidlifetime_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.text\.[A-Z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.text\.Normalizer\.Form)<ret>'
      evaluate-commands %opt{java_text_normalizer_form_active}
      set-option -add window static_words %opt{java_text_normalizer_form}
      set-option window java_text_normalizer_form_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.text\.NumberFormat\.Style)<ret>'
      evaluate-commands %opt{java_text_numberformat_style_active}
      set-option -add window static_words %opt{java_text_numberformat_style}
      set-option window java_text_numberformat_style_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.time\.[A-Z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.time\.chrono\.HijrahEra)<ret>'
      evaluate-commands %opt{java_time_chrono_hijrahera_active}
      set-option -add window static_words %opt{java_time_chrono_hijrahera}
      set-option window java_time_chrono_hijrahera_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.time\.chrono\.IsoEra)<ret>'
      evaluate-commands %opt{java_time_chrono_isoera_active}
      set-option -add window static_words %opt{java_time_chrono_isoera}
      set-option window java_time_chrono_isoera_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.time\.chrono\.MinguoEra)<ret>'
      evaluate-commands %opt{java_time_chrono_minguoera_active}
      set-option -add window static_words %opt{java_time_chrono_minguoera}
      set-option window java_time_chrono_minguoera_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.time\.chrono\.ThaiBuddhistEra)<ret>'
      evaluate-commands %opt{java_time_chrono_thaibuddhistera_active}
      set-option -add window static_words %opt{java_time_chrono_thaibuddhistera}
      set-option window java_time_chrono_thaibuddhistera_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.time\.DayOfWeek)<ret>'
      evaluate-commands %opt{java_time_dayofweek_active}
      set-option -add window static_words %opt{java_time_dayofweek}
      set-option window java_time_dayofweek_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.time\.format\.FormatStyle)<ret>'
      evaluate-commands %opt{java_time_format_formatstyle_active}
      set-option -add window static_words %opt{java_time_format_formatstyle}
      set-option window java_time_format_formatstyle_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.time\.format\.ResolverStyle)<ret>'
      evaluate-commands %opt{java_time_format_resolverstyle_active}
      set-option -add window static_words %opt{java_time_format_resolverstyle}
      set-option window java_time_format_resolverstyle_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.time\.format\.SignStyle)<ret>'
      evaluate-commands %opt{java_time_format_signstyle_active}
      set-option -add window static_words %opt{java_time_format_signstyle}
      set-option window java_time_format_signstyle_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.time\.format\.TextStyle)<ret>'
      evaluate-commands %opt{java_time_format_textstyle_active}
      set-option -add window static_words %opt{java_time_format_textstyle}
      set-option window java_time_format_textstyle_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.time\.Month)<ret>'
      evaluate-commands %opt{java_time_month_active}
      set-option -add window static_words %opt{java_time_month}
      set-option window java_time_month_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.time\.temporal\.ChronoField)<ret>'
      evaluate-commands %opt{java_time_temporal_chronofield_active}
      set-option -add window static_words %opt{java_time_temporal_chronofield}
      set-option window java_time_temporal_chronofield_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.time\.temporal\.ChronoUnit)<ret>'
      evaluate-commands %opt{java_time_temporal_chronounit_active}
      set-option -add window static_words %opt{java_time_temporal_chronounit}
      set-option window java_time_temporal_chronounit_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.time\.zone\.ZoneOffsetTransitionRule\.TimeDefinition)<ret>'
      evaluate-commands %opt{java_time_zone_zoneoffsettransitionrule_timedefinition_active}
      set-option -add window static_words %opt{java_time_zone_zoneoffsettransitionrule_timedefinition}
      set-option window java_time_zone_zoneoffsettransitionrule_timedefinition_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:java\.util\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:java\.util\.concurrent\.TimeUnit)<ret>'
      evaluate-commands %opt{java_util_concurrent_timeunit_active}
      set-option -add window static_words %opt{java_util_concurrent_timeunit}
      set-option window java_util_concurrent_timeunit_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.util\.Formatter\.BigDecimalLayoutForm)<ret>'
      evaluate-commands %opt{java_util_formatter_bigdecimallayoutform_active}
      set-option -add window static_words %opt{java_util_formatter_bigdecimallayoutform}
      set-option window java_util_formatter_bigdecimallayoutform_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.util\.Locale\.Category)<ret>'
      evaluate-commands %opt{java_util_locale_category_active}
      set-option -add window static_words %opt{java_util_locale_category}
      set-option window java_util_locale_category_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.util\.Locale\.FilteringMode)<ret>'
      evaluate-commands %opt{java_util_locale_filteringmode_active}
      set-option -add window static_words %opt{java_util_locale_filteringmode}
      set-option window java_util_locale_filteringmode_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.util\.Locale\.IsoCountryCode)<ret>'
      evaluate-commands %opt{java_util_locale_isocountrycode_active}
      set-option -add window static_words %opt{java_util_locale_isocountrycode}
      set-option window java_util_locale_isocountrycode_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:java\.util\.stream\.Collector\.Characteristics)<ret>'
      evaluate-commands %opt{java_util_stream_collector_characteristics_active}
      set-option -add window static_words %opt{java_util_stream_collector_characteristics}
      set-option window java_util_stream_collector_characteristics_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:javax\.lang\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:javax\.lang\.model\.element\.ElementKind)<ret>'
      evaluate-commands %opt{javax_lang_model_element_elementkind_active}
      set-option -add window static_words %opt{javax_lang_model_element_elementkind}
      set-option window javax_lang_model_element_elementkind_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.lang\.model\.element\.Modifier)<ret>'
      evaluate-commands %opt{javax_lang_model_element_modifier_active}
      set-option -add window static_words %opt{javax_lang_model_element_modifier}
      set-option window javax_lang_model_element_modifier_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.lang\.model\.element\.ModuleElement\.DirectiveKind)<ret>'
      evaluate-commands %opt{javax_lang_model_element_moduleelement_directivekind_active}
      set-option -add window static_words %opt{javax_lang_model_element_moduleelement_directivekind}
      set-option window javax_lang_model_element_moduleelement_directivekind_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.lang\.model\.element\.NestingKind)<ret>'
      evaluate-commands %opt{javax_lang_model_element_nestingkind_active}
      set-option -add window static_words %opt{javax_lang_model_element_nestingkind}
      set-option window javax_lang_model_element_nestingkind_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.lang\.model\.SourceVersion)<ret>'
      evaluate-commands %opt{javax_lang_model_sourceversion_active}
      set-option -add window static_words %opt{javax_lang_model_sourceversion}
      set-option window javax_lang_model_sourceversion_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.lang\.model\.type\.TypeKind)<ret>'
      evaluate-commands %opt{javax_lang_model_type_typekind_active}
      set-option -add window static_words %opt{javax_lang_model_type_typekind}
      set-option window javax_lang_model_type_typekind_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.lang\.model\.util\.Elements\.Origin)<ret>'
      evaluate-commands %opt{javax_lang_model_util_elements_origin_active}
      set-option -add window static_words %opt{javax_lang_model_util_elements_origin}
      set-option window javax_lang_model_util_elements_origin_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:javax\.net\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:javax\.net\.ssl\.SSLEngineResult\.HandshakeStatus)<ret>'
      evaluate-commands %opt{javax_net_ssl_sslengineresult_handshakestatus_active}
      set-option -add window static_words %opt{javax_net_ssl_sslengineresult_handshakestatus}
      set-option window javax_net_ssl_sslengineresult_handshakestatus_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.net\.ssl\.SSLEngineResult\.Status)<ret>'
      evaluate-commands %opt{javax_net_ssl_sslengineresult_status_active}
      set-option -add window static_words %opt{javax_net_ssl_sslengineresult_status}
      set-option window javax_net_ssl_sslengineresult_status_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:javax\.smartcardio\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:javax\.smartcardio\.CardTerminals\.State)<ret>'
      evaluate-commands %opt{javax_smartcardio_cardterminals_state_active}
      set-option -add window static_words %opt{javax_smartcardio_cardterminals_state}
      set-option window javax_smartcardio_cardterminals_state_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:javax\.swing\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:javax\.swing\.DropMode)<ret>'
      evaluate-commands %opt{javax_swing_dropmode_active}
      set-option -add window static_words %opt{javax_swing_dropmode}
      set-option window javax_swing_dropmode_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.swing\.event\.RowSorterEvent\.Type)<ret>'
      evaluate-commands %opt{javax_swing_event_rowsorterevent_type_active}
      set-option -add window static_words %opt{javax_swing_event_rowsorterevent_type}
      set-option window javax_swing_event_rowsorterevent_type_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.swing\.GroupLayout\.Alignment)<ret>'
      evaluate-commands %opt{javax_swing_grouplayout_alignment_active}
      set-option -add window static_words %opt{javax_swing_grouplayout_alignment}
      set-option window javax_swing_grouplayout_alignment_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.swing\.JTable\.PrintMode)<ret>'
      evaluate-commands %opt{javax_swing_jtable_printmode_active}
      set-option -add window static_words %opt{javax_swing_jtable_printmode}
      set-option window javax_swing_jtable_printmode_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.swing\.LayoutStyle\.ComponentPlacement)<ret>'
      evaluate-commands %opt{javax_swing_layoutstyle_componentplacement_active}
      set-option -add window static_words %opt{javax_swing_layoutstyle_componentplacement}
      set-option window javax_swing_layoutstyle_componentplacement_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.swing\.plaf\.nimbus\.AbstractRegionPainter\.PaintContext\.CacheMode)<ret>'
      evaluate-commands %opt{javax_swing_plaf_nimbus_abstractregionpainter_paintcontext_cachemode_active}
      set-option -add window static_words %opt{javax_swing_plaf_nimbus_abstractregionpainter_paintcontext_cachemode}
      set-option window javax_swing_plaf_nimbus_abstractregionpainter_paintcontext_cachemode_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.swing\.RowFilter\.ComparisonType)<ret>'
      evaluate-commands %opt{javax_swing_rowfilter_comparisontype_active}
      set-option -add window static_words %opt{javax_swing_rowfilter_comparisontype}
      set-option window javax_swing_rowfilter_comparisontype_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.swing\.SortOrder)<ret>'
      evaluate-commands %opt{javax_swing_sortorder_active}
      set-option -add window static_words %opt{javax_swing_sortorder}
      set-option window javax_swing_sortorder_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.swing\.SwingWorker\.StateValue)<ret>'
      evaluate-commands %opt{javax_swing_swingworker_statevalue_active}
      set-option -add window static_words %opt{javax_swing_swingworker_statevalue}
      set-option window javax_swing_swingworker_statevalue_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.swing\.text\.html\.FormSubmitEvent\.MethodType)<ret>'
      evaluate-commands %opt{javax_swing_text_html_formsubmitevent_methodtype_active}
      set-option -add window static_words %opt{javax_swing_text_html_formsubmitevent_methodtype}
      set-option window javax_swing_text_html_formsubmitevent_methodtype_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:javax\.tools\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:javax\.tools\.Diagnostic\.Kind)<ret>'
      evaluate-commands %opt{javax_tools_diagnostic_kind_active}
      set-option -add window static_words %opt{javax_tools_diagnostic_kind}
      set-option window javax_tools_diagnostic_kind_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.tools\.DocumentationTool\.Location)<ret>'
      evaluate-commands %opt{javax_tools_documentationtool_location_active}
      set-option -add window static_words %opt{javax_tools_documentationtool_location}
      set-option window javax_tools_documentationtool_location_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.tools\.JavaFileObject\.Kind)<ret>'
      evaluate-commands %opt{javax_tools_javafileobject_kind_active}
      set-option -add window static_words %opt{javax_tools_javafileobject_kind}
      set-option window javax_tools_javafileobject_kind_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.tools\.StandardLocation)<ret>'
      evaluate-commands %opt{javax_tools_standardlocation_active}
      set-option -add window static_words %opt{javax_tools_standardlocation}
      set-option window javax_tools_standardlocation_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:javax\.xml\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:javax\.xml\.catalog\.CatalogFeatures\.Feature)<ret>'
      evaluate-commands %opt{javax_xml_catalog_catalogfeatures_feature_active}
      set-option -add window static_words %opt{javax_xml_catalog_catalogfeatures_feature}
      set-option window javax_xml_catalog_catalogfeatures_feature_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:javax\.xml\.xpath\.XPathEvaluationResult\.XPathResultType)<ret>'
      evaluate-commands %opt{javax_xml_xpath_xpathevaluationresult_xpathresulttype_active}
      set-option -add window static_words %opt{javax_xml_xpath_xpathevaluationresult_xpathresulttype}
      set-option window javax_xml_xpath_xpathevaluationresult_xpathresulttype_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:jdk\.dynalink\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:jdk\.dynalink\.linker\.ConversionComparator\.Comparison)<ret>'
      evaluate-commands %opt{jdk_dynalink_linker_conversioncomparator_comparison_active}
      set-option -add window static_words %opt{jdk_dynalink_linker_conversioncomparator_comparison}
      set-option window jdk_dynalink_linker_conversioncomparator_comparison_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:jdk\.dynalink\.StandardNamespace)<ret>'
      evaluate-commands %opt{jdk_dynalink_standardnamespace_active}
      set-option -add window static_words %opt{jdk_dynalink_standardnamespace}
      set-option window jdk_dynalink_standardnamespace_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:jdk\.dynalink\.StandardOperation)<ret>'
      evaluate-commands %opt{jdk_dynalink_standardoperation_active}
      set-option -add window static_words %opt{jdk_dynalink_standardoperation}
      set-option window jdk_dynalink_standardoperation_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:jdk\.incubator\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:jdk\.incubator\.foreign\.CLinker\.TypeKind)<ret>'
      evaluate-commands %opt{jdk_incubator_foreign_clinker_typekind_active}
      set-option -add window static_words %opt{jdk_incubator_foreign_clinker_typekind}
      set-option window jdk_incubator_foreign_clinker_typekind_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:jdk\.incubator\.vector\.VectorShape)<ret>'
      evaluate-commands %opt{jdk_incubator_vector_vectorshape_active}
      set-option -add window static_words %opt{jdk_incubator_vector_vectorshape}
      set-option window jdk_incubator_vector_vectorshape_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:jdk\.javadoc\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:jdk\.javadoc\.doclet\.DocletEnvironment\.ModuleMode)<ret>'
      evaluate-commands %opt{jdk_javadoc_doclet_docletenvironment_modulemode_active}
      set-option -add window static_words %opt{jdk_javadoc_doclet_docletenvironment_modulemode}
      set-option window jdk_javadoc_doclet_docletenvironment_modulemode_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:jdk\.javadoc\.doclet\.Doclet\.Option\.Kind)<ret>'
      evaluate-commands %opt{jdk_javadoc_doclet_doclet_option_kind_active}
      set-option -add window static_words %opt{jdk_javadoc_doclet_doclet_option_kind}
      set-option window jdk_javadoc_doclet_doclet_option_kind_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:jdk\.javadoc\.doclet\.Taglet\.Location)<ret>'
      evaluate-commands %opt{jdk_javadoc_doclet_taglet_location_active}
      set-option -add window static_words %opt{jdk_javadoc_doclet_taglet_location}
      set-option window jdk_javadoc_doclet_taglet_location_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:jdk\.jfr\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:jdk\.jfr\.RecordingState)<ret>'
      evaluate-commands %opt{jdk_jfr_recordingstate_active}
      set-option -add window static_words %opt{jdk_jfr_recordingstate}
      set-option window jdk_jfr_recordingstate_active "nay"
    }
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    execute-keys -draft '%s (?:jdk\.jshell\.[A-Za-z])<ret>'
    try %{
      execute-keys -draft '%s (?:jdk\.jshell\.Snippet\.Kind)<ret>'
      evaluate-commands %opt{jdk_jshell_snippet_kind_active}
      set-option -add window static_words %opt{jdk_jshell_snippet_kind}
      set-option window jdk_jshell_snippet_kind_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:jdk\.jshell\.Snippet\.Status)<ret>'
      evaluate-commands %opt{jdk_jshell_snippet_status_active}
      set-option -add window static_words %opt{jdk_jshell_snippet_status}
      set-option window jdk_jshell_snippet_status_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:jdk\.jshell\.Snippet\.SubKind)<ret>'
      evaluate-commands %opt{jdk_jshell_snippet_subkind_active}
      set-option -add window static_words %opt{jdk_jshell_snippet_subkind}
      set-option window jdk_jshell_snippet_subkind_active "nay"
    }
    try %{
      execute-keys -draft '%s (?:jdk\.jshell\.SourceCodeAnalysis\.Completeness)<ret>'
      evaluate-commands %opt{jdk_jshell_sourcecodeanalysis_completeness_active}
      set-option -add window static_words %opt{jdk_jshell_sourcecodeanalysis_completeness}
      set-option window jdk_jshell_sourcecodeanalysis_completeness_active "nay"
    }
  }
}
# -------------------------------------------------------------------------------------------------- #
# ------------------------------------- JAVA ENUMS REMOVE ------------------------------------------ #
define-command -hidden java-enums-remove %{
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  try %{
    evaluate-commands %opt{com_sun_management_vmoption_origin_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.management\.VMOption\.Origin)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_management_vmoption_origin}
      set-option window com_sun_management_vmoption_origin_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{com_sun_nio_sctp_associationchangenotification_assocchangeevent_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.nio\.sctp\.AssociationChangeNotification\.AssocChangeEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_nio_sctp_associationchangenotification_assocchangeevent}
      set-option window com_sun_nio_sctp_associationchangenotification_assocchangeevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{com_sun_nio_sctp_handlerresult_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.nio\.sctp\.HandlerResult)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_nio_sctp_handlerresult}
      set-option window com_sun_nio_sctp_handlerresult_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{com_sun_nio_sctp_peeraddresschangenotification_addresschangeevent_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.nio\.sctp\.PeerAddressChangeNotification\.AddressChangeEvent)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_nio_sctp_peeraddresschangenotification_addresschangeevent}
      set-option window com_sun_nio_sctp_peeraddresschangenotification_addresschangeevent_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{com_sun_security_jgss_inquiretype_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.security\.jgss\.InquireType)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_security_jgss_inquiretype}
      set-option window com_sun_security_jgss_inquiretype_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{com_sun_source_doctree_attributetree_valuekind_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.source\.doctree\.AttributeTree\.ValueKind)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_source_doctree_attributetree_valuekind}
      set-option window com_sun_source_doctree_attributetree_valuekind_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{com_sun_source_doctree_doctree_kind_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.source\.doctree\.DocTree\.Kind)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_source_doctree_doctree_kind}
      set-option window com_sun_source_doctree_doctree_kind_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{com_sun_source_tree_casetree_casekind_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.source\.tree\.CaseTree\.CaseKind)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_source_tree_casetree_casekind}
      set-option window com_sun_source_tree_casetree_casekind_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{com_sun_source_tree_lambdaexpressiontree_bodykind_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.source\.tree\.LambdaExpressionTree\.BodyKind)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_source_tree_lambdaexpressiontree_bodykind}
      set-option window com_sun_source_tree_lambdaexpressiontree_bodykind_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{com_sun_source_tree_memberreferencetree_referencemode_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.source\.tree\.MemberReferenceTree\.ReferenceMode)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_source_tree_memberreferencetree_referencemode}
      set-option window com_sun_source_tree_memberreferencetree_referencemode_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{com_sun_source_tree_moduletree_modulekind_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.source\.tree\.ModuleTree\.ModuleKind)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_source_tree_moduletree_modulekind}
      set-option window com_sun_source_tree_moduletree_modulekind_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{com_sun_source_tree_tree_kind_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.source\.tree\.Tree\.Kind)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_source_tree_tree_kind}
      set-option window com_sun_source_tree_tree_kind_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{com_sun_source_util_taskevent_kind_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.source\.util\.TaskEvent\.Kind)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_source_util_taskevent_kind}
      set-option window com_sun_source_util_taskevent_kind_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{com_sun_tools_jconsole_jconsolecontext_connectionstate_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:com\.sun\.tools\.jconsole\.JConsoleContext\.ConnectionState)<ret>'
    } catch %{
      set-option -remove window static_words %opt{com_sun_tools_jconsole_jconsolecontext_connectionstate}
      set-option window com_sun_tools_jconsole_jconsolecontext_connectionstate_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_component_baselineresizebehavior_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.Component\.BaselineResizeBehavior)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_component_baselineresizebehavior}
      set-option window java_awt_component_baselineresizebehavior_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_desktop_action_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.Desktop\.Action)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_desktop_action}
      set-option window java_awt_desktop_action_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_desktop_quitstrategy_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.desktop\.QuitStrategy)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_desktop_quitstrategy}
      set-option window java_awt_desktop_quitstrategy_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_desktop_usersessionevent_reason_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.desktop\.UserSessionEvent\.Reason)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_desktop_usersessionevent_reason}
      set-option window java_awt_desktop_usersessionevent_reason_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_dialog_modalexclusiontype_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.Dialog\.ModalExclusionType)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_dialog_modalexclusiontype}
      set-option window java_awt_dialog_modalexclusiontype_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_dialog_modalitytype_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.Dialog\.ModalityType)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_dialog_modalitytype}
      set-option window java_awt_dialog_modalitytype_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_event_focusevent_cause_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.event\.FocusEvent\.Cause)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_event_focusevent_cause}
      set-option window java_awt_event_focusevent_cause_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_font_numericshaper_range_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.font\.NumericShaper\.Range)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_font_numericshaper_range}
      set-option window java_awt_font_numericshaper_range_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_graphicsdevice_windowtranslucency_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.GraphicsDevice\.WindowTranslucency)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_graphicsdevice_windowtranslucency}
      set-option window java_awt_graphicsdevice_windowtranslucency_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_multiplegradientpaint_colorspacetype_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.MultipleGradientPaint\.ColorSpaceType)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_multiplegradientpaint_colorspacetype}
      set-option window java_awt_multiplegradientpaint_colorspacetype_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_multiplegradientpaint_cyclemethod_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.MultipleGradientPaint\.CycleMethod)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_multiplegradientpaint_cyclemethod}
      set-option window java_awt_multiplegradientpaint_cyclemethod_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_taskbar_feature_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.Taskbar\.Feature)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_taskbar_feature}
      set-option window java_awt_taskbar_feature_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_taskbar_state_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.Taskbar\.State)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_taskbar_state}
      set-option window java_awt_taskbar_state_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_trayicon_messagetype_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.TrayIcon\.MessageType)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_trayicon_messagetype}
      set-option window java_awt_trayicon_messagetype_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_awt_window_type_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.awt\.Window\.Type)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_awt_window_type}
      set-option window java_awt_window_type_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_io_objectinputfilter_status_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.io\.ObjectInputFilter\.Status)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_io_objectinputfilter_status}
      set-option window java_io_objectinputfilter_status_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_annotation_elementtype_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.annotation\.ElementType)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_annotation_elementtype}
      set-option window java_lang_annotation_elementtype_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_annotation_retentionpolicy_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.annotation\.RetentionPolicy)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_annotation_retentionpolicy}
      set-option window java_lang_annotation_retentionpolicy_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_character_unicodescript_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.Character\.UnicodeScript)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_character_unicodescript}
      set-option window java_lang_character_unicodescript_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_constant_directmethodhandledesc_kind_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.constant\.DirectMethodHandleDesc\.Kind)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_constant_directmethodhandledesc_kind}
      set-option window java_lang_constant_directmethodhandledesc_kind_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_invoke_methodhandles_lookup_classoption_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.invoke\.MethodHandles\.Lookup\.ClassOption)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_invoke_methodhandles_lookup_classoption}
      set-option window java_lang_invoke_methodhandles_lookup_classoption_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_invoke_varhandle_accessmode_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.invoke\.VarHandle\.AccessMode)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_invoke_varhandle_accessmode}
      set-option window java_lang_invoke_varhandle_accessmode_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_management_memorytype_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.management\.MemoryType)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_management_memorytype}
      set-option window java_lang_management_memorytype_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_module_moduledescriptor_exports_modifier_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.module\.ModuleDescriptor\.Exports\.Modifier)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_module_moduledescriptor_exports_modifier}
      set-option window java_lang_module_moduledescriptor_exports_modifier_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_module_moduledescriptor_modifier_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.module\.ModuleDescriptor\.Modifier)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_module_moduledescriptor_modifier}
      set-option window java_lang_module_moduledescriptor_modifier_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_module_moduledescriptor_opens_modifier_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.module\.ModuleDescriptor\.Opens\.Modifier)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_module_moduledescriptor_opens_modifier}
      set-option window java_lang_module_moduledescriptor_opens_modifier_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_module_moduledescriptor_requires_modifier_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.module\.ModuleDescriptor\.Requires\.Modifier)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_module_moduledescriptor_requires_modifier}
      set-option window java_lang_module_moduledescriptor_requires_modifier_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_processbuilder_redirect_type_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.ProcessBuilder\.Redirect\.Type)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_processbuilder_redirect_type}
      set-option window java_lang_processbuilder_redirect_type_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_stackwalker_option_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.StackWalker\.Option)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_stackwalker_option}
      set-option window java_lang_stackwalker_option_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_system_logger_level_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.System\.Logger\.Level)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_system_logger_level}
      set-option window java_lang_system_logger_level_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_lang_thread_state_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.lang\.Thread\.State)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_lang_thread_state}
      set-option window java_lang_thread_state_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_math_roundingmode_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.math\.RoundingMode)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_math_roundingmode}
      set-option window java_math_roundingmode_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_net_authenticator_requestortype_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.net\.Authenticator\.RequestorType)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_net_authenticator_requestortype}
      set-option window java_net_authenticator_requestortype_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_net_http_httpclient_redirect_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.net\.http\.HttpClient\.Redirect)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_net_http_httpclient_redirect}
      set-option window java_net_http_httpclient_redirect_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_net_http_httpclient_version_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.net\.http\.HttpClient\.Version)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_net_http_httpclient_version}
      set-option window java_net_http_httpclient_version_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_net_proxy_type_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.net\.Proxy\.Type)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_net_proxy_type}
      set-option window java_net_proxy_type_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_net_standardprotocolfamily_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.net\.StandardProtocolFamily)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_net_standardprotocolfamily}
      set-option window java_net_standardprotocolfamily_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_nio_file_accessmode_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.nio\.file\.AccessMode)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_nio_file_accessmode}
      set-option window java_nio_file_accessmode_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_nio_file_attribute_aclentryflag_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.nio\.file\.attribute\.AclEntryFlag)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_nio_file_attribute_aclentryflag}
      set-option window java_nio_file_attribute_aclentryflag_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_nio_file_attribute_aclentrypermission_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.nio\.file\.attribute\.AclEntryPermission)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_nio_file_attribute_aclentrypermission}
      set-option window java_nio_file_attribute_aclentrypermission_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_nio_file_attribute_aclentrytype_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.nio\.file\.attribute\.AclEntryType)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_nio_file_attribute_aclentrytype}
      set-option window java_nio_file_attribute_aclentrytype_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_nio_file_attribute_posixfilepermission_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.nio\.file\.attribute\.PosixFilePermission)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_nio_file_attribute_posixfilepermission}
      set-option window java_nio_file_attribute_posixfilepermission_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_nio_file_filevisitoption_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.nio\.file\.FileVisitOption)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_nio_file_filevisitoption}
      set-option window java_nio_file_filevisitoption_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_nio_file_filevisitresult_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.nio\.file\.FileVisitResult)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_nio_file_filevisitresult}
      set-option window java_nio_file_filevisitresult_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_nio_file_linkoption_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.nio\.file\.LinkOption)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_nio_file_linkoption}
      set-option window java_nio_file_linkoption_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_nio_file_standardcopyoption_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.nio\.file\.StandardCopyOption)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_nio_file_standardcopyoption}
      set-option window java_nio_file_standardcopyoption_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_nio_file_standardopenoption_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.nio\.file\.StandardOpenOption)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_nio_file_standardopenoption}
      set-option window java_nio_file_standardopenoption_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_security_cert_certpathvalidatorexception_basicreason_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.security\.cert\.CertPathValidatorException\.BasicReason)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_security_cert_certpathvalidatorexception_basicreason}
      set-option window java_security_cert_certpathvalidatorexception_basicreason_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_security_cert_crlreason_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.security\.cert\.CRLReason)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_security_cert_crlreason}
      set-option window java_security_cert_crlreason_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_security_cert_pkixreason_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.security\.cert\.PKIXReason)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_security_cert_pkixreason}
      set-option window java_security_cert_pkixreason_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_security_cert_pkixrevocationchecker_option_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.security\.cert\.PKIXRevocationChecker\.Option)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_security_cert_pkixrevocationchecker_option}
      set-option window java_security_cert_pkixrevocationchecker_option_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_security_cryptoprimitive_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.security\.CryptoPrimitive)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_security_cryptoprimitive}
      set-option window java_security_cryptoprimitive_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_security_drbgparameters_capability_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.security\.DrbgParameters\.Capability)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_security_drbgparameters_capability}
      set-option window java_security_drbgparameters_capability_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_security_keyrep_type_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.security\.KeyRep\.Type)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_security_keyrep_type}
      set-option window java_security_keyrep_type_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_sql_clientinfostatus_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.sql\.ClientInfoStatus)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_sql_clientinfostatus}
      set-option window java_sql_clientinfostatus_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_sql_jdbctype_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.sql\.JDBCType)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_sql_jdbctype}
      set-option window java_sql_jdbctype_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_sql_pseudocolumnusage_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.sql\.PseudoColumnUsage)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_sql_pseudocolumnusage}
      set-option window java_sql_pseudocolumnusage_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_sql_rowidlifetime_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.sql\.RowIdLifetime)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_sql_rowidlifetime}
      set-option window java_sql_rowidlifetime_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_text_normalizer_form_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.text\.Normalizer\.Form)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_text_normalizer_form}
      set-option window java_text_normalizer_form_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_text_numberformat_style_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.text\.NumberFormat\.Style)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_text_numberformat_style}
      set-option window java_text_numberformat_style_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_time_chrono_hijrahera_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.time\.chrono\.HijrahEra)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_time_chrono_hijrahera}
      set-option window java_time_chrono_hijrahera_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_time_chrono_isoera_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.time\.chrono\.IsoEra)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_time_chrono_isoera}
      set-option window java_time_chrono_isoera_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_time_chrono_minguoera_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.time\.chrono\.MinguoEra)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_time_chrono_minguoera}
      set-option window java_time_chrono_minguoera_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_time_chrono_thaibuddhistera_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.time\.chrono\.ThaiBuddhistEra)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_time_chrono_thaibuddhistera}
      set-option window java_time_chrono_thaibuddhistera_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_time_dayofweek_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.time\.DayOfWeek)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_time_dayofweek}
      set-option window java_time_dayofweek_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_time_format_formatstyle_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.time\.format\.FormatStyle)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_time_format_formatstyle}
      set-option window java_time_format_formatstyle_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_time_format_resolverstyle_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.time\.format\.ResolverStyle)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_time_format_resolverstyle}
      set-option window java_time_format_resolverstyle_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_time_format_signstyle_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.time\.format\.SignStyle)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_time_format_signstyle}
      set-option window java_time_format_signstyle_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_time_format_textstyle_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.time\.format\.TextStyle)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_time_format_textstyle}
      set-option window java_time_format_textstyle_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_time_month_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.time\.Month)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_time_month}
      set-option window java_time_month_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_time_temporal_chronofield_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.time\.temporal\.ChronoField)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_time_temporal_chronofield}
      set-option window java_time_temporal_chronofield_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_time_temporal_chronounit_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.time\.temporal\.ChronoUnit)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_time_temporal_chronounit}
      set-option window java_time_temporal_chronounit_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_time_zone_zoneoffsettransitionrule_timedefinition_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.time\.zone\.ZoneOffsetTransitionRule\.TimeDefinition)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_time_zone_zoneoffsettransitionrule_timedefinition}
      set-option window java_time_zone_zoneoffsettransitionrule_timedefinition_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_concurrent_timeunit_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.concurrent\.TimeUnit)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_concurrent_timeunit}
      set-option window java_util_concurrent_timeunit_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_formatter_bigdecimallayoutform_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.Formatter\.BigDecimalLayoutForm)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_formatter_bigdecimallayoutform}
      set-option window java_util_formatter_bigdecimallayoutform_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_locale_category_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.Locale\.Category)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_locale_category}
      set-option window java_util_locale_category_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_locale_filteringmode_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.Locale\.FilteringMode)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_locale_filteringmode}
      set-option window java_util_locale_filteringmode_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_locale_isocountrycode_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.Locale\.IsoCountryCode)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_locale_isocountrycode}
      set-option window java_util_locale_isocountrycode_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{java_util_stream_collector_characteristics_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:java\.util\.stream\.Collector\.Characteristics)<ret>'
    } catch %{
      set-option -remove window static_words %opt{java_util_stream_collector_characteristics}
      set-option window java_util_stream_collector_characteristics_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_lang_model_element_elementkind_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.lang\.model\.element\.ElementKind)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_lang_model_element_elementkind}
      set-option window javax_lang_model_element_elementkind_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_lang_model_element_modifier_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.lang\.model\.element\.Modifier)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_lang_model_element_modifier}
      set-option window javax_lang_model_element_modifier_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_lang_model_element_moduleelement_directivekind_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.lang\.model\.element\.ModuleElement\.DirectiveKind)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_lang_model_element_moduleelement_directivekind}
      set-option window javax_lang_model_element_moduleelement_directivekind_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_lang_model_element_nestingkind_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.lang\.model\.element\.NestingKind)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_lang_model_element_nestingkind}
      set-option window javax_lang_model_element_nestingkind_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_lang_model_sourceversion_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.lang\.model\.SourceVersion)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_lang_model_sourceversion}
      set-option window javax_lang_model_sourceversion_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_lang_model_type_typekind_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.lang\.model\.type\.TypeKind)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_lang_model_type_typekind}
      set-option window javax_lang_model_type_typekind_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_lang_model_util_elements_origin_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.lang\.model\.util\.Elements\.Origin)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_lang_model_util_elements_origin}
      set-option window javax_lang_model_util_elements_origin_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_net_ssl_sslengineresult_handshakestatus_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.net\.ssl\.SSLEngineResult\.HandshakeStatus)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_net_ssl_sslengineresult_handshakestatus}
      set-option window javax_net_ssl_sslengineresult_handshakestatus_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_net_ssl_sslengineresult_status_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.net\.ssl\.SSLEngineResult\.Status)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_net_ssl_sslengineresult_status}
      set-option window javax_net_ssl_sslengineresult_status_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_smartcardio_cardterminals_state_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.smartcardio\.CardTerminals\.State)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_smartcardio_cardterminals_state}
      set-option window javax_smartcardio_cardterminals_state_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_dropmode_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.DropMode)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_dropmode}
      set-option window javax_swing_dropmode_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_event_rowsorterevent_type_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.event\.RowSorterEvent\.Type)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_event_rowsorterevent_type}
      set-option window javax_swing_event_rowsorterevent_type_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_grouplayout_alignment_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.GroupLayout\.Alignment)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_grouplayout_alignment}
      set-option window javax_swing_grouplayout_alignment_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_jtable_printmode_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.JTable\.PrintMode)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_jtable_printmode}
      set-option window javax_swing_jtable_printmode_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_layoutstyle_componentplacement_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.LayoutStyle\.ComponentPlacement)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_layoutstyle_componentplacement}
      set-option window javax_swing_layoutstyle_componentplacement_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_plaf_nimbus_abstractregionpainter_paintcontext_cachemode_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.plaf\.nimbus\.AbstractRegionPainter\.PaintContext\.CacheMode)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_plaf_nimbus_abstractregionpainter_paintcontext_cachemode}
      set-option window javax_swing_plaf_nimbus_abstractregionpainter_paintcontext_cachemode_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_rowfilter_comparisontype_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.RowFilter\.ComparisonType)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_rowfilter_comparisontype}
      set-option window javax_swing_rowfilter_comparisontype_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_sortorder_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.SortOrder)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_sortorder}
      set-option window javax_swing_sortorder_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_swingworker_statevalue_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.SwingWorker\.StateValue)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_swingworker_statevalue}
      set-option window javax_swing_swingworker_statevalue_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_swing_text_html_formsubmitevent_methodtype_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.swing\.text\.html\.FormSubmitEvent\.MethodType)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_swing_text_html_formsubmitevent_methodtype}
      set-option window javax_swing_text_html_formsubmitevent_methodtype_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_tools_diagnostic_kind_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.tools\.Diagnostic\.Kind)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_tools_diagnostic_kind}
      set-option window javax_tools_diagnostic_kind_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_tools_documentationtool_location_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.tools\.DocumentationTool\.Location)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_tools_documentationtool_location}
      set-option window javax_tools_documentationtool_location_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_tools_javafileobject_kind_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.tools\.JavaFileObject\.Kind)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_tools_javafileobject_kind}
      set-option window javax_tools_javafileobject_kind_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_tools_standardlocation_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.tools\.StandardLocation)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_tools_standardlocation}
      set-option window javax_tools_standardlocation_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_catalog_catalogfeatures_feature_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.catalog\.CatalogFeatures\.Feature)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_catalog_catalogfeatures_feature}
      set-option window javax_xml_catalog_catalogfeatures_feature_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{javax_xml_xpath_xpathevaluationresult_xpathresulttype_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:javax\.xml\.xpath\.XPathEvaluationResult\.XPathResultType)<ret>'
    } catch %{
      set-option -remove window static_words %opt{javax_xml_xpath_xpathevaluationresult_xpathresulttype}
      set-option window javax_xml_xpath_xpathevaluationresult_xpathresulttype_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{jdk_dynalink_linker_conversioncomparator_comparison_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.dynalink\.linker\.ConversionComparator\.Comparison)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_dynalink_linker_conversioncomparator_comparison}
      set-option window jdk_dynalink_linker_conversioncomparator_comparison_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{jdk_dynalink_standardnamespace_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.dynalink\.StandardNamespace)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_dynalink_standardnamespace}
      set-option window jdk_dynalink_standardnamespace_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{jdk_dynalink_standardoperation_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.dynalink\.StandardOperation)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_dynalink_standardoperation}
      set-option window jdk_dynalink_standardoperation_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{jdk_incubator_foreign_clinker_typekind_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.incubator\.foreign\.CLinker\.TypeKind)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_incubator_foreign_clinker_typekind}
      set-option window jdk_incubator_foreign_clinker_typekind_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{jdk_incubator_vector_vectorshape_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.incubator\.vector\.VectorShape)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_incubator_vector_vectorshape}
      set-option window jdk_incubator_vector_vectorshape_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{jdk_javadoc_doclet_docletenvironment_modulemode_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.javadoc\.doclet\.DocletEnvironment\.ModuleMode)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_javadoc_doclet_docletenvironment_modulemode}
      set-option window jdk_javadoc_doclet_docletenvironment_modulemode_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{jdk_javadoc_doclet_doclet_option_kind_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.javadoc\.doclet\.Doclet\.Option\.Kind)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_javadoc_doclet_doclet_option_kind}
      set-option window jdk_javadoc_doclet_doclet_option_kind_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{jdk_javadoc_doclet_taglet_location_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.javadoc\.doclet\.Taglet\.Location)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_javadoc_doclet_taglet_location}
      set-option window jdk_javadoc_doclet_taglet_location_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{jdk_jfr_recordingstate_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.jfr\.RecordingState)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_jfr_recordingstate}
      set-option window jdk_jfr_recordingstate_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{jdk_jshell_snippet_kind_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.jshell\.Snippet\.Kind)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_jshell_snippet_kind}
      set-option window jdk_jshell_snippet_kind_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{jdk_jshell_snippet_status_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.jshell\.Snippet\.Status)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_jshell_snippet_status}
      set-option window jdk_jshell_snippet_status_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{jdk_jshell_snippet_subkind_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.jshell\.Snippet\.SubKind)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_jshell_snippet_subkind}
      set-option window jdk_jshell_snippet_subkind_active "nop"
    }
  }
  try %{
    evaluate-commands %opt{jdk_jshell_sourcecodeanalysis_completeness_active}  
  } catch %{
    try %{
      execute-keys -draft '%s (?:jdk\.jshell\.SourceCodeAnalysis\.Completeness)<ret>'
    } catch %{
      set-option -remove window static_words %opt{jdk_jshell_sourcecodeanalysis_completeness}
      set-option window jdk_jshell_sourcecodeanalysis_completeness_active "nop"
    }
  }  
}
# ---------------------------------------- PAGE BREAK ---------------------------------------------- #
# ------------------------------------------ SHELL ------------------------------------------------- #
# tail to head
# split(".*", name);
# macro: glqdmqR<a;>lr=lf"l<a-l>dghjj<esc><esc>
# 411<s-^>
#
evaluate-commands %sh{
  
  comSunJavaAccessibilityUtilEventID='ACTION ADJUSTMENT ANCESTOR CARET CELLEDITOR CHANGE COLUMNMODEL
    COMPONENT CONTAINER DOCUMENT FOCUS INTERNALFRAME ITEM KEY LISTDATA LISTSELECTION MENU MOTION MOUSE
    POPUPMENU PROPERTYCHANGE TABLEMODEL TEXT TREEEXPANSION TREEMODEL TREESELECTION UNDOABLEEDIT
    VETOABLECHANGE WINDOW'
  
  comSunJdiClassType='INVOKE_SINGLE_THREADED'
  
  comSunJdiObjectReference='INVOKE_NONVIRTUAL INVOKE_SINGLE_THREADED'
  
  comSunJdiThreadReference='THREAD_STATUS_MONITOR THREAD_STATUS_NOT_STARTED THREAD_STATUS_RUNNING
    THREAD_STATUS_SLEEPING THREAD_STATUS_UNKNOWN THREAD_STATUS_WAIT THREAD_STATUS_ZOMBIE'
  
  comSunJdiVirtualMachine='TRACE_ALL TRACE_EVENTS TRACE_NONE TRACE_OBJREFS TRACE_RECEIVES
    TRACE_REFTYPES TRACE_SENDS'
  
  comSunJdiRequestEventRequest='SUSPEND_ALL SUSPEND_EVENT_THREAD SUSPEND_NONE'
  
  comSunJdiRequestStepRequest='STEP_INTO STEP_LINE STEP_MIN STEP_OUT STEP_OVER'
  
  comSunManagementGarbageCollectionNotificationInfo='GARBAGE_COLLECTION_NOTIFICATION'
  
  comSunSecurityAuthModuleJndiLoginModule='GROUP_PROVIDER USER_PROVIDER'
  
  comSunToolsJconsoleJConsoleContext='CONNECTION_STATE_PROPERTY'
  
  javaAwtAdjustable='HORIZONTAL NO_ORIENTATION VERTICAL'
  
  javaAwtAlphaComposite='CLEAR DST DST_ATOP DST_IN DST_OUT DST_OVER SRC SRC_ATOP SRC_IN SRC_OUT
  SRC_OVER XOR'
  
  javaAwtAWTEvent='ACTION_EVENT_MASKL ADJUSTMENT_EVENT_MASKL COMPONENT_EVENT_MASKL
    CONTAINER_EVENT_MASKL FOCUS_EVENT_MASKL HIERARCHY_BOUNDS_EVENT_MASKL HIERARCHY_EVENT_MASKL
    INPUT_METHOD_EVENT_MASKL INVOCATION_EVENT_MASKL ITEM_EVENT_MASKL KEY_EVENT_MASKL MOUSE_EVENT_MASKL
    MOUSE_MOTION_EVENT_MASKL MOUSE_WHEEL_EVENT_MASKL PAINT_EVENT_MASKL RESERVED_ID_MAX TEXT_EVENT_MASKL
    WINDOW_EVENT_MASKL WINDOW_FOCUS_EVENT_MASKL WINDOW_STATE_EVENT_MASKL'
  
  javaAwtBasicStroke='CAP_BUTT CAP_ROUND CAP_SQUARE JOIN_BEVEL JOIN_MITER JOIN_ROUND'
  
  javaAwtBorderLayout='AFTER_LAST_LINE AFTER_LINE_ENDS BEFORE_FIRST_LINE BEFORE_LINE_BEGINS CENTER
    EAST LINE_END LINE_START NORTH PAGE_END PAGE_START SOUTH WEST'
  
  javaAwtComponent='BOTTOM_ALIGNMENT CENTER_ALIGNMENT LEFT_ALIGNMENT RIGHT_ALIGNMENT TOP_ALIGNMENT'
  
  javaAwtCursor='CROSSHAIR_CURSOR CUSTOM_CURSOR DEFAULT_CURSOR E_RESIZE_CURSOR HAND_CURSOR MOVE_CURSOR
    N_RESIZE_CURSOR NE_RESIZE_CURSOR NW_RESIZE_CURSOR S_RESIZE_CURSOR SE_RESIZE_CURSOR SW_RESIZE_CURSOR
    TEXT_CURSOR W_RESIZE_CURSOR WAIT_CURSOR'
  
  javaAwtDisplayMode='BIT_DEPTH_MULTI REFRESH_RATE_UNKNOWN'
  
  javaAwtEvent='ACTION_EVENT ALT_MASK BACK_SPACE CAPS_LOCK CTRL_MASK DELETE DOWN END ENTER ESCAPE F1
    F10 F11 F12 F2 F3 F4 F5 F6 F7 F8 F9 GOT_FOCUS HOME INSERT KEY_ACTION KEY_ACTION_RELEASE KEY_PRESS
    KEY_RELEASE LEFT LIST_DESELECT LIST_SELECT LOAD_FILE LOST_FOCUS META_MASK MOUSE_DOWN MOUSE_DRAG
    MOUSE_ENTER MOUSE_EXIT MOUSE_MOVE MOUSE_UP NUM_LOCK PAUSE PGDN PGUP PRINT_SCREEN RIGHT SAVE_FILE
    SCROLL_ABSOLUTE SCROLL_BEGIN SCROLL_END SCROLL_LINE_DOWN SCROLL_LINE_UP SCROLL_LOCK
    SCROLL_PAGE_DOWN SCROLL_PAGE_UP SHIFT_MASK TAB UP WINDOW_DEICONIFY WINDOW_DESTROY WINDOW_EXPOSE
    WINDOW_ICONIFY WINDOW_MOVED'
  
  javaAwtFileDialog='LOAD SAVE'
  
  javaAwtFlowLayout='CENTER LEADING LEFT RIGHT TRAILING'
  
  javaAwtFont='BOLD CENTER_BASELINE DIALOG DIALOG_INPUT HANGING_BASELINE ITALIC LAYOUT_LEFT_TO_RIGHT
    LAYOUT_NO_LIMIT_CONTEXT LAYOUT_NO_START_CONTEXT LAYOUT_RIGHT_TO_LEFT MONOSPACED PLAIN
    ROMAN_BASELINE SANS_SERIF SERIF TRUETYPE_FONT TYPE1_FONT'
  
  javaAwtFrame='CROSSHAIR_CURSOR DEFAULT_CURSOR E_RESIZE_CURSOR HAND_CURSOR ICONIFIED MAXIMIZED_BOTH
    MAXIMIZED_HORIZ MAXIMIZED_VERT MOVE_CURSOR N_RESIZE_CURSOR NE_RESIZE_CURSOR NORMAL NW_RESIZE_CURSOR
    S_RESIZE_CURSOR SE_RESIZE_CURSOR SW_RESIZE_CURSOR TEXT_CURSOR W_RESIZE_CURSOR WAIT_CURSOR'
  
  javaAwtGraphicsConfigTemplate='PREFERRED REQUIRED UNNECESSARY'
  
  javaAwtGraphicsDevice='TYPE_IMAGE_BUFFER TYPE_PRINTER TYPE_RASTER_SCREEN'
  
  javaAwtGridBagConstraints='ABOVE_BASELINE ABOVE_BASELINE_LEADING ABOVE_BASELINE_TRAILING BASELINE
    BASELINE_LEADING BASELINE_TRAILING BELOW_BASELINE BELOW_BASELINE_LEADING BELOW_BASELINE_TRAILING
    BOTH CENTER EAST FIRST_LINE_END FIRST_LINE_START HORIZONTAL LAST_LINE_END LAST_LINE_START LINE_END
    LINE_START NONE NORTH NORTHEAST NORTHWEST PAGE_END PAGE_START RELATIVE REMAINDER SOUTH SOUTHEAST
    SOUTHWEST VERTICAL WEST'
  
  javaAwtGridBagLayout='MAXGRIDSIZE MINSIZE PREFERREDSIZE'
  
  javaAwtImage='SCALE_AREA_AVERAGING SCALE_DEFAULT SCALE_FAST SCALE_REPLICATE SCALE_SMOOTH'
  
  javaAwtKeyboardFocusManager='BACKWARD_TRAVERSAL_KEYS DOWN_CYCLE_TRAVERSAL_KEYS
    FORWARD_TRAVERSAL_KEYS UP_CYCLE_TRAVERSAL_KEYS'
  
  javaAwtLabel='CENTER LEFT RIGHT'
  
  javaAwtMediaTracker='ABORTED COMPLETE ERRORED LOADING'
  
  javaAwtScrollbar='HORIZONTAL VERTICAL'
  
  javaAwtScrollPane='SCROLLBARS_ALWAYS SCROLLBARS_AS_NEEDED SCROLLBARS_NEVER'
  
  javaAwtSystemColor='ACTIVE_CAPTION ACTIVE_CAPTION_BORDER ACTIVE_CAPTION_TEXT CONTROL
    CONTROL_DK_SHADOW CONTROL_HIGHLIGHT CONTROL_LT_HIGHLIGHT CONTROL_SHADOW CONTROL_TEXT DESKTOP
    INACTIVE_CAPTION INACTIVE_CAPTION_BORDER INACTIVE_CAPTION_TEXT INFO INFO_TEXT MENU MENU_TEXT
    NUM_COLORS SCROLLBAR TEXT TEXT_HIGHLIGHT TEXT_HIGHLIGHT_TEXT TEXT_INACTIVE_TEXT TEXT_TEXT WINDOW
    WINDOW_BORDER WINDOW_TEXT'
  
  javaAwtTextArea='SCROLLBARS_BOTH SCROLLBARS_HORIZONTAL_ONLY SCROLLBARS_NONE SCROLLBARS_VERTICAL_ONLY'
  
  javaAwtTransparency='BITMASK OPAQUE TRANSLUCENT'
  
  javaAwtColorColorSpace='CS_CIEXYZ CS_GRAY CS_LINEAR_RGB CS_PYCC CS_sRGB TYPE_2CLR TYPE_3CLR
    TYPE_4CLR TYPE_5CLR TYPE_6CLR TYPE_7CLR TYPE_8CLR TYPE_9CLR TYPE_ACLR TYPE_BCLR TYPE_CCLR TYPE_CMY
    TYPE_CMYK TYPE_DCLR TYPE_ECLR TYPE_FCLR TYPE_GRAY TYPE_HLS TYPE_HSV TYPE_Lab TYPE_Luv TYPE_RGB
    TYPE_XYZ TYPE_YCbCr TYPE_Yxy'
  
  javaAwtColorICC_Profile='CLASS_ABSTRACT CLASS_COLORSPACECONVERSION CLASS_DEVICELINK CLASS_DISPLAY
    CLASS_INPUT CLASS_NAMEDCOLOR CLASS_OUTPUT icAbsoluteColorimetric icCurveCount icCurveData
    icHdrAttributes icHdrCmmId icHdrColorSpace icHdrCreator icHdrDate icHdrDeviceClass icHdrFlags
    icHdrIlluminant icHdrMagic icHdrManufacturer icHdrModel icHdrPcs icHdrPlatform icHdrProfileID
    icHdrRenderingIntent icHdrSize icHdrVersion icICCAbsoluteColorimetric icMediaRelativeColorimetric
    icPerceptual icRelativeColorimetric icSaturation icSigAbstractClass icSigAToB0Tag icSigAToB1Tag
    icSigAToB2Tag icSigBlueColorantTag icSigBlueMatrixColumnTag icSigBlueTRCTag icSigBToA0Tag
    icSigBToA1Tag icSigBToA2Tag icSigCalibrationDateTimeTag icSigCharTargetTag
    icSigChromaticAdaptationTag icSigChromaticityTag icSigCmyData icSigCmykData icSigColorantOrderTag
    icSigColorantTableTag icSigColorSpaceClass icSigCopyrightTag icSigCrdInfoTag icSigDeviceMfgDescTag
    icSigDeviceModelDescTag icSigDeviceSettingsTag icSigDisplayClass icSigGamutTag icSigGrayData
    icSigGrayTRCTag icSigGreenColorantTag icSigGreenMatrixColumnTag icSigGreenTRCTag icSigHead
    icSigHlsData icSigHsvData icSigInputClass icSigLabData icSigLinkClass icSigLuminanceTag
    icSigLuvData icSigMeasurementTag icSigMediaBlackPointTag icSigMediaWhitePointTag
    icSigNamedColor2Tag icSigNamedColorClass icSigOutputClass icSigOutputResponseTag icSigPreview0Tag
    icSigPreview1Tag icSigPreview2Tag icSigProfileDescriptionTag icSigProfileSequenceDescTag
    icSigPs2CRD0Tag icSigPs2CRD1Tag icSigPs2CRD2Tag icSigPs2CRD3Tag icSigPs2CSATag
    icSigPs2RenderingIntentTag icSigRedColorantTag icSigRedMatrixColumnTag icSigRedTRCTag icSigRgbData
    icSigScreeningDescTag icSigScreeningTag icSigSpace2CLR icSigSpace3CLR icSigSpace4CLR icSigSpace5CLR
    icSigSpace6CLR icSigSpace7CLR icSigSpace8CLR icSigSpace9CLR icSigSpaceACLR icSigSpaceBCLR
    icSigSpaceCCLR icSigSpaceDCLR icSigSpaceECLR icSigSpaceFCLR icSigTechnologyTag icSigUcrBgTag
    icSigViewingCondDescTag icSigViewingConditionsTag icSigXYZData icSigYCbCrData icSigYxyData
    icTagReserved icTagType icXYZNumberX'
  
  javaAwtColorICC_ProfileRGB='BLUECOMPONENT GREENCOMPONENT REDCOMPONENT'
  
  javaAwtDatatransferDataFlavor='javaJVMLocalObjectMimeType javaRemoteObjectMimeType javaSerializedObjectMimeType'
  
  javaAwtDndDnDConstants='ACTION_COPY ACTION_COPY_OR_MOVE ACTION_LINK ACTION_MOVE ACTION_NONE ACTION_REFERENCE'
  
  javaAwtDndDragSourceContext='CHANGED DEFAULT ENTER OVER'
  
  javaAwtEventActionEvent='ACTION_FIRST ACTION_LAST ACTION_PERFORMED ALT_MASK CTRL_MASK META_MASK SHIFT_MASK'
  
  javaAwtEventAdjustmentEvent='ADJUSTMENT_FIRST ADJUSTMENT_LAST ADJUSTMENT_VALUE_CHANGED
    BLOCK_DECREMENT BLOCK_INCREMENT TRACK UNIT_DECREMENT UNIT_INCREMENT'
  
  javaAwtEventComponentEvent='COMPONENT_FIRST COMPONENT_HIDDEN COMPONENT_LAST COMPONENT_MOVED
    COMPONENT_RESIZED COMPONENT_SHOWN'
  
  javaAwtEventContainerEvent='COMPONENT_ADDED COMPONENT_REMOVED CONTAINER_FIRST CONTAINER_LAST'
  
  javaAwtEventFocusEvent='FOCUS_FIRST FOCUS_GAINED FOCUS_LAST FOCUS_LOST'
  
  javaAwtEventHierarchyEvent='ANCESTOR_MOVED ANCESTOR_RESIZED DISPLAYABILITY_CHANGED
    HIERARCHY_CHANGED HIERARCHY_FIRST HIERARCHY_LAST PARENT_CHANGED SHOWING_CHANGED'
  
  javaAwtEventInputEvent='ALT_DOWN_MASK ALT_GRAPH_DOWN_MASK ALT_GRAPH_MASK ALT_MASK BUTTON1_DOWN_MASK
    BUTTON1_MASK BUTTON2_DOWN_MASK BUTTON2_MASK BUTTON3_DOWN_MASK BUTTON3_MASK CTRL_DOWN_MASK CTRL_MASK
    META_DOWN_MASK META_MASK SHIFT_DOWN_MASK SHIFT_MASK'
  
  javaAwtEventInputMethodEvent='CARET_POSITION_CHANGED INPUT_METHOD_FIRST INPUT_METHOD_LAST
    INPUT_METHOD_TEXT_CHANGED'
  
  javaAwtEventInvocationEvent='INVOCATION_DEFAULT INVOCATION_FIRST INVOCATION_LAST'
  
  javaAwtEventItemEvent='DESELECTED ITEM_FIRST ITEM_LAST ITEM_STATE_CHANGED SELECTED'
  
  javaAwtEventKeyEvent='CHAR_UNDEFINED KEY_FIRST KEY_LAST KEY_LOCATION_LEFT KEY_LOCATION_NUMPAD
    KEY_LOCATION_RIGHT KEY_LOCATION_STANDARD KEY_LOCATION_UNKNOWN KEY_PRESSED KEY_RELEASED KEY_TYPED
    VK_0 VK_1 VK_2 VK_3 VK_4 VK_5 VK_6 VK_7 VK_8 VK_9 VK_A VK_ACCEPT VK_ADD VK_AGAIN VK_ALL_CANDIDATES
    VK_ALPHANUMERIC VK_ALT VK_ALT_GRAPH VK_AMPERSAND VK_ASTERISK VK_AT VK_B VK_BACK_QUOTE VK_BACK_SLASH
    VK_BACK_SPACE VK_BEGIN VK_BRACELEFT VK_BRACERIGHT VK_C VK_CANCEL VK_CAPS_LOCK VK_CIRCUMFLEX
    VK_CLEAR VK_CLOSE_BRACKET VK_CODE_INPUT VK_COLON VK_COMMA VK_COMPOSE VK_CONTEXT_MENU VK_CONTROL
    VK_CONVERT VK_COPY VK_CUT VK_D VK_DEAD_ABOVEDOT VK_DEAD_ABOVERING VK_DEAD_ACUTE VK_DEAD_BREVE
    VK_DEAD_CARON VK_DEAD_CEDILLA VK_DEAD_CIRCUMFLEX VK_DEAD_DIAERESIS VK_DEAD_DOUBLEACUTE
    VK_DEAD_GRAVE VK_DEAD_IOTA VK_DEAD_MACRON VK_DEAD_OGONEK VK_DEAD_SEMIVOICED_SOUND VK_DEAD_TILDE
    VK_DEAD_VOICED_SOUND VK_DECIMAL VK_DELETE VK_DIVIDE VK_DOLLAR VK_DOWN VK_E VK_END VK_ENTER
    VK_EQUALS VK_ESCAPE VK_EURO_SIGN VK_EXCLAMATION_MARK VK_F VK_F1 VK_F10 VK_F11 VK_F12 VK_F13 VK_F14
    VK_F15 VK_F16 VK_F17 VK_F18 VK_F19 VK_F2 VK_F20 VK_F21 VK_F22 VK_F23 VK_F24 VK_F3 VK_F4 VK_F5 VK_F6
    VK_F7 VK_F8 VK_F9 VK_FINAL VK_FIND VK_FULL_WIDTH VK_G VK_GREATER VK_H VK_HALF_WIDTH VK_HELP
    VK_HIRAGANA VK_HOME VK_I VK_INPUT_METHOD_ON_OFF VK_INSERT VK_INVERTED_EXCLAMATION_MARK VK_J
    VK_JAPANESE_HIRAGANA VK_JAPANESE_KATAKANA VK_JAPANESE_ROMAN VK_K VK_KANA VK_KANA_LOCK VK_KANJI
    VK_KATAKANA VK_KP_DOWN VK_KP_LEFT VK_KP_RIGHT VK_KP_UP VK_L VK_LEFT VK_LEFT_PARENTHESIS VK_LESS
    VK_M VK_META VK_MINUS VK_MODECHANGE VK_MULTIPLY VK_N VK_NONCONVERT VK_NUM_LOCK VK_NUMBER_SIGN
    VK_NUMPAD0 VK_NUMPAD1 VK_NUMPAD2 VK_NUMPAD3 VK_NUMPAD4 VK_NUMPAD5 VK_NUMPAD6 VK_NUMPAD7 VK_NUMPAD8
    VK_NUMPAD9 VK_O VK_OPEN_BRACKET VK_P VK_PAGE_DOWN VK_PAGE_UP VK_PASTE VK_PAUSE VK_PERIOD VK_PLUS
    VK_PREVIOUS_CANDIDATE VK_PRINTSCREEN VK_PROPS VK_Q VK_QUOTE VK_QUOTEDBL VK_R VK_RIGHT
    VK_RIGHT_PARENTHESIS VK_ROMAN_CHARACTERS VK_S VK_SCROLL_LOCK VK_SEMICOLON VK_SEPARATER VK_SEPARATOR
    VK_SHIFT VK_SLASH VK_SPACE VK_STOP VK_SUBTRACT VK_T VK_TAB VK_U VK_UNDEFINED VK_UNDERSCORE VK_UNDO
    VK_UP VK_V VK_W VK_WINDOWS VK_X VK_Y VK_Z'
  
  javaAwtEventMouseEvent='BUTTON1 BUTTON2 BUTTON3 MOUSE_CLICKED MOUSE_DRAGGED MOUSE_ENTERED
    MOUSE_EXITED MOUSE_FIRST MOUSE_LAST MOUSE_MOVED MOUSE_PRESSED MOUSE_RELEASED MOUSE_WHEEL NOBUTTON'
  
  javaAwtEventMouseWheelEvent='WHEEL_BLOCK_SCROLL WHEEL_UNIT_SCROLL'
  
  javaAwtEventPaintEvent='PAINT PAINT_FIRST PAINT_LAST UPDATE'
  
  javaAwtEventTextEvent='TEXT_FIRST TEXT_LAST TEXT_VALUE_CHANGED'
  
  javaAwtEventWindowEvent='WINDOW_ACTIVATED WINDOW_CLOSED WINDOW_CLOSING WINDOW_DEACTIVATED
    WINDOW_DEICONIFIED WINDOW_FIRST WINDOW_GAINED_FOCUS WINDOW_ICONIFIED WINDOW_LAST WINDOW_LOST_FOCUS
    WINDOW_OPENED WINDOW_STATE_CHANGED'
  
  javaAwtFontGlyphJustificationInfo='PRIORITY_INTERCHAR PRIORITY_KASHIDA PRIORITY_NONE PRIORITY_WHITESPACE'
  
  javaAwtFontGlyphMetrics='COMBINING COMPONENT LIGATURE STANDARD WHITESPACE'
  
  javaAwtFontGlyphVector='FLAG_COMPLEX_GLYPHS FLAG_HAS_POSITION_ADJUSTMENTS FLAG_HAS_TRANSFORMS
    FLAG_MASK FLAG_RUN_RTL'
  
  javaAwtFontGraphicAttribute='BOTTOM_ALIGNMENT CENTER_BASELINE HANGING_BASELINE ROMAN_BASELINE TOP_ALIGNMENT'
  
  javaAwtFontNumericShaper='ALL_RANGES ARABIC BENGALI DEVANAGARI EASTERN_ARABIC ETHIOPIC EUROPEAN
    GUJARATI GURMUKHI KANNADA KHMER LAO MALAYALAM MONGOLIAN MYANMAR ORIYA TAMIL TELUGU THAI TIBETAN'
  
  javaAwtFontOpenType='TAG_ACNT TAG_AVAR TAG_BASE TAG_BDAT TAG_BLOC TAG_BSLN TAG_CFF TAG_CMAP
    TAG_CVAR TAG_CVT TAG_DSIG TAG_EBDT TAG_EBLC TAG_EBSC TAG_FDSC TAG_FEAT TAG_FMTX TAG_FPGM TAG_FVAR
    TAG_GASP TAG_GDEF TAG_GLYF TAG_GPOS TAG_GSUB TAG_GVAR TAG_HDMX TAG_HEAD TAG_HHEA TAG_HMTX TAG_JSTF
    TAG_JUST TAG_KERN TAG_LCAR TAG_LOCA TAG_LTSH TAG_MAXP TAG_MMFX TAG_MMSD TAG_MORT TAG_NAME TAG_OPBD
    TAG_OS2 TAG_PCLT TAG_POST TAG_PREP TAG_PROP TAG_TRAK TAG_TYP1 TAG_VDMX TAG_VHEA TAG_VMTX'
  
  javaAwtFontShapeGraphicAttribute='FILL STROKE'
  
  javaAwtGeomAffineTransform='TYPE_FLIP TYPE_GENERAL_ROTATION TYPE_GENERAL_SCALE
    TYPE_GENERAL_TRANSFORM TYPE_IDENTITY TYPE_MASK_ROTATION TYPE_MASK_SCALE TYPE_QUADRANT_ROTATION
    TYPE_TRANSLATION TYPE_UNIFORM_SCALE'
  
  javaAwtGeomArc2D='CHORD OPEN PIE'
  
  javaAwtGeomPath2D='WIND_EVEN_ODD WIND_NON_ZERO'
  
  javaAwtGeomPathIterator='SEG_CLOSE SEG_CUBICTO SEG_LINETO SEG_MOVETO SEG_QUADTO WIND_EVEN_ODD WIND_NON_ZERO'
  
  javaAwtGeomRectangle2D='OUT_BOTTOM OUT_LEFT OUT_RIGHT OUT_TOP'
  
  javaAwtImInputMethodHighlight='CONVERTED_TEXT RAW_TEXT'
  
  javaAwtImageAffineTransformOp='TYPE_BICUBIC TYPE_BILINEAR TYPE_NEAREST_NEIGHBOR'
  
  javaAwtImageBufferedImage='TYPE_3BYTE_BGR TYPE_4BYTE_ABGR TYPE_4BYTE_ABGR_PRE TYPE_BYTE_BINARY
    TYPE_BYTE_GRAY TYPE_BYTE_INDEXED TYPE_CUSTOM TYPE_INT_ARGB TYPE_INT_ARGB_PRE TYPE_INT_BGR
    TYPE_INT_RGB TYPE_USHORT_555_RGB TYPE_USHORT_565_RGB TYPE_USHORT_GRAY'
  
  javaAwtImageConvolveOp='EDGE_NO_OP EDGE_ZERO_FILL'
  
  javaAwtImageDataBuffer='TYPE_BYTE TYPE_DOUBLE TYPE_FLOAT TYPE_INT TYPE_SHORT TYPE_UNDEFINED TYPE_USHORT'
  
  javaAwtImageImageConsumer='COMPLETESCANLINES IMAGEABORTED IMAGEERROR RANDOMPIXELORDER SINGLEFRAME
    SINGLEFRAMEDONE SINGLEPASS STATICIMAGEDONE TOPDOWNLEFTRIGHT'
  
  javaAwtImageImageObserver='ABORT ALLBITS ERROR FRAMEBITS HEIGHT PROPERTIES SOMEBITS WIDTH'
  
  javaAwtImageVolatileImage='IMAGE_INCOMPATIBLE IMAGE_OK IMAGE_RESTORED'
  
  javaAwtImageRenderableRenderableImage='HINTS_OBSERVED'
  
  javaAwtPrintPageable='UNKNOWN_NUMBER_OF_PAGES'
  
  javaAwtPrintPageFormat='LANDSCAPE PORTRAIT REVERSE_LANDSCAPE'
  
  javaAwtPrintPrintable='NO_SUCH_PAGE PAGE_EXISTS'
  
  javaBeansBeanInfo='ICON_COLOR_16x16 ICON_COLOR_32x32 ICON_MONO_16x16 ICON_MONO_32x32'
  
  javaBeansDesignMode='PROPERTYNAME'
  
  javaBeansIntrospector='IGNORE_ALL_BEANINFO IGNORE_IMMEDIATE_BEANINFO USE_ALL_BEANINFO'
  
  javaIoObjectStreamConstants='baseWireHandle PROTOCOL_VERSION_1 PROTOCOL_VERSION_2 SC_BLOCK_DATA
    SC_ENUM SC_EXTERNALIZABLE SC_SERIALIZABLE SC_WRITE_METHOD STREAM_MAGIC STREAM_VERSION TC_ARRAY
    TC_BASE TC_BLOCKDATA TC_BLOCKDATALONG TC_CLASS TC_CLASSDESC TC_ENDBLOCKDATA TC_ENUM TC_EXCEPTION
    TC_LONGSTRING TC_MAX TC_NULL TC_OBJECT TC_PROXYCLASSDESC TC_REFERENCE TC_RESET TC_STRING'
  
  javaIoPipedInputStream='PIPE_SIZE'
  
  javaIoStreamTokenizer='TT_EOF TT_EOL TT_NUMBER TT_WORD'
  
  javaLangByte='BYTES MAX_VALUE MIN_VALUE SIZE'
  
  javaLangCharacter='BYTES COMBINING_SPACING_MARK CONNECTOR_PUNCTUATION CONTROL CURRENCY_SYMBOL
    DASH_PUNCTUATION DECIMAL_DIGIT_NUMBER DIRECTIONALITY_ARABIC_NUMBER DIRECTIONALITY_BOUNDARY_NEUTRAL
    DIRECTIONALITY_COMMON_NUMBER_SEPARATOR DIRECTIONALITY_EUROPEAN_NUMBER
    DIRECTIONALITY_EUROPEAN_NUMBER_SEPARATOR DIRECTIONALITY_EUROPEAN_NUMBER_TERMINATOR
    DIRECTIONALITY_FIRST_STRONG_ISOLATE DIRECTIONALITY_LEFT_TO_RIGHT
    DIRECTIONALITY_LEFT_TO_RIGHT_EMBEDDING DIRECTIONALITY_LEFT_TO_RIGHT_ISOLATE
    DIRECTIONALITY_LEFT_TO_RIGHT_OVERRIDE DIRECTIONALITY_NONSPACING_MARK DIRECTIONALITY_OTHER_NEUTRALS
    DIRECTIONALITY_PARAGRAPH_SEPARATOR DIRECTIONALITY_POP_DIRECTIONAL_FORMAT
    DIRECTIONALITY_POP_DIRECTIONAL_ISOLATE DIRECTIONALITY_RIGHT_TO_LEFT
    DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC DIRECTIONALITY_RIGHT_TO_LEFT_EMBEDDING
    DIRECTIONALITY_RIGHT_TO_LEFT_ISOLATE DIRECTIONALITY_RIGHT_TO_LEFT_OVERRIDE
    DIRECTIONALITY_SEGMENT_SEPARATOR DIRECTIONALITY_UNDEFINED DIRECTIONALITY_WHITESPACE ENCLOSING_MARK
    END_PUNCTUATION FINAL_QUOTE_PUNCTUATION FORMAT INITIAL_QUOTE_PUNCTUATION LETTER_NUMBER
    LINE_SEPARATOR LOWERCASE_LETTER MATH_SYMBOL MAX_CODE_POINT MAX_HIGH_SURROGATE MAX_LOW_SURROGATE
    MAX_RADIX MAX_SURROGATE MAX_VALUE MIN_CODE_POINT MIN_HIGH_SURROGATE MIN_LOW_SURROGATE MIN_RADIX
    MIN_SUPPLEMENTARY_CODE_POINT MIN_SURROGATE MIN_VALUE MODIFIER_LETTER MODIFIER_SYMBOL
    NON_SPACING_MARK OTHER_LETTER OTHER_NUMBER OTHER_PUNCTUATION OTHER_SYMBOL PARAGRAPH_SEPARATOR
    PRIVATE_USE SIZE SPACE_SEPARATOR START_PUNCTUATION SURROGATE TITLECASE_LETTER UNASSIGNED
    UPPERCASE_LETTER'
  
  javaLangDouble='BYTES MAX_EXPONENT MAX_VALUE MIN_EXPONENT MIN_NORMAL MIN_VALUE NaN
    NEGATIVE_INFINITY POSITIVE_INFINITY SIZE'
  
  javaLangFloat='BYTES MAX_EXPONENT MAX_VALUE MIN_EXPONENT MIN_NORMAL MIN_VALUE NaN NEGATIVE_INFINITY
    POSITIVE_INFINITY SIZE'
  
  javaLangInteger='BYTES MAX_VALUE MIN_VALUE SIZE'
  
  javaLangLong='BYTES MAX_VALUEL MIN_VALUEL SIZE'
  
  javaLangMath='E PI'
  
  javaLangShort='BYTES MAX_VALUE MIN_VALUE SIZE'
  
  javaLangStrictMath='E PI'
  
  javaLangThread='MAX_PRIORITY MIN_PRIORITY NORM_PRIORITY'
  
  javaLangConstantConstantDescs='DEFAULT_NAME'
  
  javaLangInvokeLambdaMetafactory='FLAG_BRIDGES FLAG_MARKERS FLAG_SERIALIZABLE'
  
  javaLangInvokeMethodHandleInfo='REF_getField REF_getStatic REF_invokeInterface REF_invokeSpecial
    REF_invokeStatic REF_invokeVirtual REF_newInvokeSpecial REF_putField REF_putStatic'
  
  javaLangInvokeMethodhandlesLookup='MODULE PACKAGE PRIVATE PROTECTED PUBLIC UNCONDITIONAL'
  
  javaLangManagementManagementFactory='CLASS_LOADING_MXBEAN_NAME COMPILATION_MXBEAN_NAME
    GARBAGE_COLLECTOR_MXBEAN_DOMAIN_TYPE MEMORY_MANAGER_MXBEAN_DOMAIN_TYPE MEMORY_MXBEAN_NAME
    MEMORY_POOL_MXBEAN_DOMAIN_TYPE OPERATING_SYSTEM_MXBEAN_NAME RUNTIME_MXBEAN_NAME THREAD_MXBEAN_NAME'
  
  javaLangManagementMemoryNotificationInfo='MEMORY_COLLECTION_THRESHOLD_EXCEEDED MEMORY_THRESHOLD_EXCEEDED'
  
  javaLangReflectMember='DECLARED PUBLIC'
  
  javaLangReflectModifier='ABSTRACT FINAL INTERFACE NATIVE PRIVATE PROTECTED PUBLIC STATIC STRICT
    SYNCHRONIZED TRANSIENT VOLATILE'
  
  javaMathBigDecimal='ROUND_CEILING ROUND_DOWN ROUND_FLOOR ROUND_HALF_DOWN ROUND_HALF_EVEN
    ROUND_HALF_UP ROUND_UNNECESSARY ROUND_UP'
  
  javaNetHttpURLConnection='HTTP_ACCEPTED HTTP_BAD_GATEWAY HTTP_BAD_METHOD HTTP_BAD_REQUEST
    HTTP_CLIENT_TIMEOUT HTTP_CONFLICT HTTP_CREATED HTTP_ENTITY_TOO_LARGE HTTP_FORBIDDEN
    HTTP_GATEWAY_TIMEOUT HTTP_GONE HTTP_INTERNAL_ERROR HTTP_LENGTH_REQUIRED HTTP_MOVED_PERM
    HTTP_MOVED_TEMP HTTP_MULT_CHOICE HTTP_NO_CONTENT HTTP_NOT_ACCEPTABLE HTTP_NOT_AUTHORITATIVE
    HTTP_NOT_FOUND HTTP_NOT_IMPLEMENTED HTTP_NOT_MODIFIED HTTP_OK HTTP_PARTIAL HTTP_PAYMENT_REQUIRED
    HTTP_PRECON_FAILED HTTP_PROXY_AUTH HTTP_REQ_TOO_LONG HTTP_RESET HTTP_SEE_OTHER HTTP_SERVER_ERROR
    HTTP_UNAUTHORIZED HTTP_UNAVAILABLE HTTP_UNSUPPORTED_TYPE HTTP_USE_PROXY HTTP_VERSION'
  
  javaNetIDN='ALLOW_UNASSIGNED USE_STD3_ASCII_RULES'
  
  javaNetSocketOptions='IP_MULTICAST_IF IP_MULTICAST_IF2 IP_MULTICAST_LOOP IP_TOS SO_BINDADDR
    SO_BROADCAST SO_KEEPALIVE SO_LINGER SO_OOBINLINE SO_RCVBUF SO_REUSEADDR SO_REUSEPORT SO_SNDBUF
    SO_TIMEOUT TCP_NODELAY'
  
  javaNetHttpWebSocket='NORMAL_CLOSURE'
  
  javaNioChannelsSelectionKey='OP_ACCEPT OP_CONNECT OP_READ OP_WRITE'
  
  javaRmiActivationActivationSystem='SYSTEM_PORT'
  
  javaRmiRegistryRegistry='REGISTRY_PORT'
  
  javaRmiServerLoaderHandler='packagePrefix'
  
  javaRmiServerLogStream='BRIEF SILENT VERBOSE'
  
  javaRmiServerObjID='ACTIVATOR_ID DGC_ID REGISTRY_ID'
  
  javaRmiServerRemoteRef='packagePrefix serialVersionUIDL'
  
  javaRmiServerServerRef='serialVersionUIDL'
  
  javaSecurityKey='serialVersionUIDL'
  
  javaSecurityPrivateKey='serialVersionUIDL'
  
  javaSecurityPublicKey='serialVersionUIDL'
  
  javaSecuritySignature='SIGN UNINITIALIZED VERIFY'
  
  javaSecurityInterfacesDSAPrivateKey='serialVersionUIDL'
  
  javaSecurityInterfacesDSAPublicKey='serialVersionUIDL'
  
  javaSecurityInterfacesECPrivateKey='serialVersionUIDL'
  
  javaSecurityInterfacesECPublicKey='serialVersionUIDL'
  
  javaSecurityInterfacesRSAMultiPrimePrivateCrtKey='serialVersionUIDL'
  
  javaSecurityInterfacesRSAPrivateCrtKey='serialVersionUIDL'
  
  javaSecurityInterfacesRSAPrivateKey='serialVersionUIDL'
  
  javaSecurityInterfacesRSAPublicKey='serialVersionUIDL'
  
  javaSecuritySpecPSSParameterSpec='TRAILER_FIELD_BC'
  
  javaSqlConnection='TRANSACTION_NONE TRANSACTION_READ_COMMITTED TRANSACTION_READ_UNCOMMITTED
    TRANSACTION_REPEATABLE_READ TRANSACTION_SERIALIZABLE'
  
  javaSqlDatabaseMetaData='attributeNoNulls attributeNullable attributeNullableUnknown
    bestRowNotPseudo bestRowPseudo bestRowSession bestRowTemporary bestRowTransaction bestRowUnknown
    columnNoNulls columnNullable columnNullableUnknown functionColumnIn functionColumnInOut
    functionColumnOut functionColumnResult functionColumnUnknown functionNoNulls functionNoTable
    functionNullable functionNullableUnknown functionResultUnknown functionReturn functionReturnsTable
    importedKeyCascade importedKeyInitiallyDeferred importedKeyInitiallyImmediate importedKeyNoAction
    importedKeyNotDeferrable importedKeyRestrict importedKeySetDefault importedKeySetNull
    procedureColumnIn procedureColumnInOut procedureColumnOut procedureColumnResult
    procedureColumnReturn procedureColumnUnknown procedureNoNulls procedureNoResult procedureNullable
    procedureNullableUnknown procedureResultUnknown procedureReturnsResult sqlStateSQL sqlStateSQL99
    sqlStateXOpen tableIndexClustered tableIndexHashed tableIndexOther tableIndexStatistic typeNoNulls
    typeNullable typeNullableUnknown typePredBasic typePredChar typePredNone typeSearchable
    versionColumnNotPseudo versionColumnPseudo versionColumnUnknown'
  
  javaSqlParameterMetaData='parameterModeIn parameterModeInOut parameterModeOut parameterModeUnknown
    parameterNoNulls parameterNullable parameterNullableUnknown'
  
  javaSqlResultSet='CLOSE_CURSORS_AT_COMMIT CONCUR_READ_ONLY CONCUR_UPDATABLE FETCH_FORWARD
    FETCH_REVERSE FETCH_UNKNOWN HOLD_CURSORS_OVER_COMMIT TYPE_FORWARD_ONLY TYPE_SCROLL_INSENSITIVE
    TYPE_SCROLL_SENSITIVE'
  
  javaSqlResultSetMetaData='columnNoNulls columnNullable columnNullableUnknown'
  
  javaSqlStatement='CLOSE_ALL_RESULTS CLOSE_CURRENT_RESULT EXECUTE_FAILED KEEP_CURRENT_RESULT
    NO_GENERATED_KEYS RETURN_GENERATED_KEYS SUCCESS_NO_INFO'
  
  javaSqlTypes='ARRAY BIGINT BINARY BIT BLOB BOOLEAN CHAR CLOB DATALINK DATE DECIMAL DISTINCT DOUBLE
    FLOAT INTEGER JAVA_OBJECT LONGNVARCHAR LONGVARBINARY LONGVARCHAR NCHAR NCLOB NULL NUMERIC NVARCHAR
    OTHER REAL REF REF_CURSOR ROWID SMALLINT SQLXML STRUCT TIME TIME_WITH_TIMEZONE TIMESTAMP
    TIMESTAMP_WITH_TIMEZONE TINYINT VARBINARY VARCHAR'
  
  javaTextBidi='DIRECTION_DEFAULT_LEFT_TO_RIGHT DIRECTION_DEFAULT_RIGHT_TO_LEFT
    DIRECTION_LEFT_TO_RIGHT DIRECTION_RIGHT_TO_LEFT'
  
  javaTextBreakIterator='DONE'
  
  javaTextCharacterIterator='DONE'
  
  javaTextCollationElementIterator='NULLORDER'
  
  javaTextCollator='CANONICAL_DECOMPOSITION FULL_DECOMPOSITION IDENTICAL NO_DECOMPOSITION PRIMARY
    SECONDARY TERTIARY'
  
  javaTextDateFormat='AM_PM_FIELD DATE_FIELD DAY_OF_WEEK_FIELD DAY_OF_WEEK_IN_MONTH_FIELD
    DAY_OF_YEAR_FIELD DEFAULT ERA_FIELD FULL HOUR_OF_DAY0_FIELD HOUR_OF_DAY1_FIELD HOUR0_FIELD
    HOUR1_FIELD LONG MEDIUM MILLISECOND_FIELD MINUTE_FIELD MONTH_FIELD SECOND_FIELD SHORT
    TIMEZONE_FIELD WEEK_OF_MONTH_FIELD WEEK_OF_YEAR_FIELD YEAR_FIELD'
  
  javaTextNumberFormat='FRACTION_FIELD INTEGER_FIELD'
  
  javaTimeYear='MAX_VALUE MIN_VALUE'
  
  javaUtilCalendar='ALL_STYLES AM AM_PM APRIL AUGUST DATE DAY_OF_MONTH DAY_OF_WEEK
    DAY_OF_WEEK_IN_MONTH DAY_OF_YEAR DECEMBER DST_OFFSET ERA FEBRUARY FIELD_COUNT FRIDAY HOUR
    HOUR_OF_DAY JANUARY JULY JUNE LONG LONG_FORMAT LONG_STANDALONE MARCH MAY MILLISECOND MINUTE MONDAY
    MONTH NARROW_FORMAT NARROW_STANDALONE NOVEMBER OCTOBER PM SATURDAY SECOND SEPTEMBER SHORT
    SHORT_FORMAT SHORT_STANDALONE SUNDAY THURSDAY TUESDAY UNDECIMBER WEDNESDAY WEEK_OF_MONTH
    WEEK_OF_YEAR YEAR ZONE_OFFSET'
  
  javaUtilFormattableFlags='ALTERNATE LEFT_JUSTIFY UPPERCASE'
  
  javaUtilGregorianCalendar='AD BC'
  
  javaUtilLocale='PRIVATE_USE_EXTENSION UNICODE_LOCALE_EXTENSION'
  
  javaUtilLocaleLanguageRange='MAX_WEIGHT MIN_WEIGHT'
  
  javaUtilResourcebundleControl='TTL_DONT_CACHEL TTL_NO_EXPIRATION_CONTROLL'
  
  javaUtilSimpleTimeZone='STANDARD_TIME UTC_TIME WALL_TIME'
  
  javaUtilSpliterator='CONCURRENT DISTINCT IMMUTABLE NONNULL ORDERED SIZED SORTED SUBSIZED'
  
  javaUtilTimeZone='LONG SHORT'
  
  javaUtilJarJarEntry='CENATT CENATX CENCOM CENCRC CENDSK CENEXT CENFLG CENHDR CENHOW CENLEN CENNAM
    CENOFF CENSIGL CENSIZ CENTIM CENVEM CENVER ENDCOM ENDHDR ENDOFF ENDSIGL ENDSIZ ENDSUB ENDTOT EXTCRC
    EXTHDR EXTLEN EXTSIGL EXTSIZ LOCCRC LOCEXT LOCFLG LOCHDR LOCHOW LOCLEN LOCNAM LOCSIGL LOCSIZ LOCTIM
    LOCVER'
  
  javaUtilJarJarFile='CENATT CENATX CENCOM CENCRC CENDSK CENEXT CENFLG CENHDR CENHOW CENLEN CENNAM
    CENOFF CENSIGL CENSIZ CENTIM CENVEM CENVER ENDCOM ENDHDR ENDOFF ENDSIGL ENDSIZ ENDSUB ENDTOT EXTCRC
    EXTHDR EXTLEN EXTSIGL EXTSIZ LOCCRC LOCEXT LOCFLG LOCHDR LOCHOW LOCLEN LOCNAM LOCSIGL LOCSIZ LOCTIM
    LOCVER MANIFEST_NAME'
  
  javaUtilJarJarInputStream='CENATT CENATX CENCOM CENCRC CENDSK CENEXT CENFLG CENHDR CENHOW CENLEN
    CENNAM CENOFF CENSIGL CENSIZ CENTIM CENVEM CENVER ENDCOM ENDHDR ENDOFF ENDSIGL ENDSIZ ENDSUB ENDTOT
    EXTCRC EXTHDR EXTLEN EXTSIGL EXTSIZ LOCCRC LOCEXT LOCFLG LOCHDR LOCHOW LOCLEN LOCNAM LOCSIGL LOCSIZ
    LOCTIM LOCVER'
  
  javaUtilJarJarOutputStream='CENATT CENATX CENCOM CENCRC CENDSK CENEXT CENFLG CENHDR CENHOW CENLEN
    CENNAM CENOFF CENSIGL CENSIZ CENTIM CENVEM CENVER ENDCOM ENDHDR ENDOFF ENDSIGL ENDSIZ ENDSUB ENDTOT
    EXTCRC EXTHDR EXTLEN EXTSIGL EXTSIZ LOCCRC LOCEXT LOCFLG LOCHDR LOCHOW LOCLEN LOCNAM LOCSIGL LOCSIZ
    LOCTIM LOCVER'
  
  javaUtilLoggingErrorManager='CLOSE_FAILURE FLUSH_FAILURE FORMAT_FAILURE GENERIC_FAILURE
    OPEN_FAILURE WRITE_FAILURE'
  
  javaUtilLoggingLogger='GLOBAL_LOGGER_NAME'
  
  javaUtilLoggingLogManager='LOGGING_MXBEAN_NAME'
  
  javaUtilPrefsPreferences='MAX_KEY_LENGTH MAX_NAME_LENGTH MAX_VALUE_LENGTH'
  
  javaUtilRegexPattern='CANON_EQ CASE_INSENSITIVE COMMENTS DOTALL LITERAL MULTILINE UNICODE_CASE
    UNICODE_CHARACTER_CLASS UNIX_LINES'
  
  javaUtilZipDeflater='BEST_COMPRESSION BEST_SPEED DEFAULT_COMPRESSION DEFAULT_STRATEGY DEFLATED
    FILTERED FULL_FLUSH HUFFMAN_ONLY NO_COMPRESSION NO_FLUSH SYNC_FLUSH'
  
  javaUtilZipGZIPInputStream='GZIP_MAGIC'
  
  javaUtilZipZipEntry='CENATT CENATX CENCOM CENCRC CENDSK CENEXT CENFLG CENHDR CENHOW CENLEN CENNAM
    CENOFF CENSIGL CENSIZ CENTIM CENVEM CENVER DEFLATED ENDCOM ENDHDR ENDOFF ENDSIGL ENDSIZ ENDSUB
    ENDTOT EXTCRC EXTHDR EXTLEN EXTSIGL EXTSIZ LOCCRC LOCEXT LOCFLG LOCHDR LOCHOW LOCLEN LOCNAM LOCSIGL
    LOCSIZ LOCTIM LOCVER STORED'
  
  javaUtilZipZipFile='CENATT CENATX CENCOM CENCRC CENDSK CENEXT CENFLG
    CENHDR CENHOW CENLEN CENNAM CENOFF CENSIGL CENSIZ CENTIM CENVEM CENVER ENDCOM ENDHDR ENDOFF ENDSIGL
    ENDSIZ ENDSUB ENDTOT EXTCRC EXTHDR EXTLEN EXTSIGL EXTSIZ LOCCRC LOCEXT LOCFLG LOCHDR LOCHOW LOCLEN
    LOCNAM LOCSIGL LOCSIZ LOCTIM LOCVER OPEN_DELETE OPEN_READ'
  
  javaUtilZipZipInputStream='CENATT CENATX CENCOM CENCRC CENDSK CENEXT CENFLG CENHDR CENHOW CENLEN
    CENNAM CENOFF CENSIGL CENSIZ CENTIM CENVEM CENVER ENDCOM ENDHDR ENDOFF ENDSIGL ENDSIZ ENDSUB ENDTOT
    EXTCRC EXTHDR EXTLEN EXTSIGL EXTSIZ LOCCRC LOCEXT LOCFLG LOCHDR LOCHOW LOCLEN LOCNAM LOCSIGL LOCSIZ
    LOCTIM LOCVER'
  
  javaUtilZipZipOutputStream='CENATT CENATX CENCOM CENCRC CENDSK CENEXT CENFLG CENHDR CENHOW CENLEN
    CENNAM CENOFF CENSIGL CENSIZ CENTIM CENVEM CENVER DEFLATED ENDCOM ENDHDR ENDOFF ENDSIGL ENDSIZ
    ENDSUB ENDTOT EXTCRC EXTHDR EXTLEN EXTSIGL EXTSIZ LOCCRC LOCEXT LOCFLG LOCHDR LOCHOW LOCLEN LOCNAM
    LOCSIGL LOCSIZ LOCTIM LOCVER STORED'
  
  javaxAccessibilityAccessibleContext='ACCESSIBLE_ACTION_PROPERTY
    ACCESSIBLE_ACTIVE_DESCENDANT_PROPERTY ACCESSIBLE_CARET_PROPERTY ACCESSIBLE_CHILD_PROPERTY
    ACCESSIBLE_COMPONENT_BOUNDS_CHANGED ACCESSIBLE_DESCRIPTION_PROPERTY ACCESSIBLE_HYPERTEXT_OFFSET
    ACCESSIBLE_INVALIDATE_CHILDREN ACCESSIBLE_NAME_PROPERTY ACCESSIBLE_SELECTION_PROPERTY
    ACCESSIBLE_STATE_PROPERTY ACCESSIBLE_TABLE_CAPTION_CHANGED
    ACCESSIBLE_TABLE_COLUMN_DESCRIPTION_CHANGED ACCESSIBLE_TABLE_COLUMN_HEADER_CHANGED
    ACCESSIBLE_TABLE_MODEL_CHANGED ACCESSIBLE_TABLE_ROW_DESCRIPTION_CHANGED
    ACCESSIBLE_TABLE_ROW_HEADER_CHANGED ACCESSIBLE_TABLE_SUMMARY_CHANGED
    ACCESSIBLE_TEXT_ATTRIBUTES_CHANGED ACCESSIBLE_TEXT_PROPERTY ACCESSIBLE_VALUE_PROPERTY
    ACCESSIBLE_VISIBLE_DATA_PROPERTY'
  
  javaxAccessibilityAccessibleExtendedText='ATTRIBUTE_RUN LINE'
  
  javaxAccessibilityAccessibleRelation='CHILD_NODE_OF CHILD_NODE_OF_PROPERTY CONTROLLED_BY_PROPERTY
    CONTROLLER_FOR_PROPERTY EMBEDDED_BY EMBEDDED_BY_PROPERTY EMBEDS EMBEDS_PROPERTY FLOWS_FROM
    FLOWS_FROM_PROPERTY FLOWS_TO FLOWS_TO_PROPERTY LABEL_FOR_PROPERTY LABELED_BY_PROPERTY
    MEMBER_OF_PROPERTY PARENT_WINDOW_OF PARENT_WINDOW_OF_PROPERTY SUBWINDOW_OF SUBWINDOW_OF_PROPERTY'
  
  javaxAccessibilityAccessibleTableModelChange='DELETE INSERT UPDATE'
  
  javaxAccessibilityAccessibleText='CHARACTER SENTENCE WORD'
  
  javaxCryptoCipher='DECRYPT_MODE ENCRYPT_MODE PRIVATE_KEY PUBLIC_KEY SECRET_KEY UNWRAP_MODE WRAP_MODE'
  
  javaxCryptoSecretKey='serialVersionUIDL'
  
  javaxCryptoInterfacesDHPrivateKey='serialVersionUIDL'
  
  javaxCryptoInterfacesDHPublicKey='serialVersionUIDL'
  
  javaxCryptoInterfacesPBEKey='serialVersionUIDL'
  
  javaxCryptoSpecDESedeKeySpec='DES_EDE_KEY_LEN'
  
  javaxCryptoSpecDESKeySpec='DES_KEY_LEN'
  
  javaxImageioImageWriteParam='MODE_COPY_FROM_METADATA MODE_DEFAULT MODE_DISABLED MODE_EXPLICIT'
  
  javaxImageioMetadataIIOMetadataFormat='CHILD_POLICY_ALL CHILD_POLICY_CHOICE CHILD_POLICY_EMPTY
    CHILD_POLICY_MAX CHILD_POLICY_REPEAT CHILD_POLICY_SEQUENCE CHILD_POLICY_SOME DATATYPE_BOOLEAN
    DATATYPE_DOUBLE DATATYPE_FLOAT DATATYPE_INTEGER DATATYPE_STRING VALUE_ARBITRARY VALUE_ENUMERATION
    VALUE_LIST VALUE_NONE VALUE_RANGE VALUE_RANGE_MAX_INCLUSIVE VALUE_RANGE_MAX_INCLUSIVE_MASK
    VALUE_RANGE_MIN_INCLUSIVE VALUE_RANGE_MIN_INCLUSIVE_MASK VALUE_RANGE_MIN_MAX_INCLUSIVE'
  
  javaxImageioMetadataIIOMetadataFormatImpl='standardMetadataFormatName'
  
  javaxImageioPluginsTiffBaselineTIFFTagSet='COMPRESSION_CCITT_RLE COMPRESSION_CCITT_T_4
    COMPRESSION_CCITT_T_6 COMPRESSION_DEFLATE COMPRESSION_JPEG COMPRESSION_LZW COMPRESSION_NONE
    COMPRESSION_OLD_JPEG COMPRESSION_PACKBITS COMPRESSION_ZLIB EXTRA_SAMPLES_ASSOCIATED_ALPHA
    EXTRA_SAMPLES_UNASSOCIATED_ALPHA EXTRA_SAMPLES_UNSPECIFIED FILL_ORDER_LEFT_TO_RIGHT
    FILL_ORDER_RIGHT_TO_LEFT GRAY_RESPONSE_UNIT_HUNDRED_THOUSANDTHS GRAY_RESPONSE_UNIT_HUNDREDTHS
    GRAY_RESPONSE_UNIT_TEN_THOUSANDTHS GRAY_RESPONSE_UNIT_TENTHS GRAY_RESPONSE_UNIT_THOUSANDTHS
    INK_SET_CMYK INK_SET_NOT_CMYK JPEG_PROC_BASELINE JPEG_PROC_LOSSLESS
    NEW_SUBFILE_TYPE_REDUCED_RESOLUTION NEW_SUBFILE_TYPE_SINGLE_PAGE NEW_SUBFILE_TYPE_TRANSPARENCY
    ORIENTATION_ROW_0_BOTTOM_COLUMN_0_LEFT ORIENTATION_ROW_0_BOTTOM_COLUMN_0_RIGHT
    ORIENTATION_ROW_0_LEFT_COLUMN_0_BOTTOM ORIENTATION_ROW_0_LEFT_COLUMN_0_TOP
    ORIENTATION_ROW_0_RIGHT_COLUMN_0_BOTTOM ORIENTATION_ROW_0_RIGHT_COLUMN_0_TOP
    ORIENTATION_ROW_0_TOP_COLUMN_0_LEFT ORIENTATION_ROW_0_TOP_COLUMN_0_RIGHT
    PHOTOMETRIC_INTERPRETATION_BLACK_IS_ZERO PHOTOMETRIC_INTERPRETATION_CIELAB
    PHOTOMETRIC_INTERPRETATION_CMYK PHOTOMETRIC_INTERPRETATION_ICCLAB
    PHOTOMETRIC_INTERPRETATION_PALETTE_COLOR PHOTOMETRIC_INTERPRETATION_RGB
    PHOTOMETRIC_INTERPRETATION_TRANSPARENCY_MASK PHOTOMETRIC_INTERPRETATION_WHITE_IS_ZERO
    PHOTOMETRIC_INTERPRETATION_Y_CB_CR PLANAR_CONFIGURATION_CHUNKY PLANAR_CONFIGURATION_PLANAR
    PREDICTOR_HORIZONTAL_DIFFERENCING PREDICTOR_NONE RESOLUTION_UNIT_CENTIMETER RESOLUTION_UNIT_INCH
    RESOLUTION_UNIT_NONE SAMPLE_FORMAT_FLOATING_POINT SAMPLE_FORMAT_SIGNED_INTEGER
    SAMPLE_FORMAT_UNDEFINED SAMPLE_FORMAT_UNSIGNED_INTEGER SUBFILE_TYPE_FULL_RESOLUTION
    SUBFILE_TYPE_REDUCED_RESOLUTION SUBFILE_TYPE_SINGLE_PAGE T4_OPTIONS_2D_CODING
    T4_OPTIONS_EOL_BYTE_ALIGNED T4_OPTIONS_UNCOMPRESSED T6_OPTIONS_UNCOMPRESSED TAG_ARTIST
    TAG_BITS_PER_SAMPLE TAG_CELL_LENGTH TAG_CELL_WIDTH TAG_COLOR_MAP TAG_COMPRESSION TAG_COPYRIGHT
    TAG_DATE_TIME TAG_DOCUMENT_NAME TAG_DOT_RANGE TAG_EXTRA_SAMPLES TAG_FILL_ORDER TAG_FREE_BYTE_COUNTS
    TAG_FREE_OFFSETS TAG_GRAY_RESPONSE_CURVE TAG_GRAY_RESPONSE_UNIT TAG_HALFTONE_HINTS
    TAG_HOST_COMPUTER TAG_ICC_PROFILE TAG_IMAGE_DESCRIPTION TAG_IMAGE_LENGTH TAG_IMAGE_WIDTH
    TAG_INK_NAMES TAG_INK_SET TAG_JPEG_AC_TABLES TAG_JPEG_DC_TABLES TAG_JPEG_INTERCHANGE_FORMAT
    TAG_JPEG_INTERCHANGE_FORMAT_LENGTH TAG_JPEG_LOSSLESS_PREDICTORS TAG_JPEG_POINT_TRANSFORMS
    TAG_JPEG_PROC TAG_JPEG_Q_TABLES TAG_JPEG_RESTART_INTERVAL TAG_JPEG_TABLES TAG_MAKE
    TAG_MAX_SAMPLE_VALUE TAG_MIN_SAMPLE_VALUE TAG_MODEL TAG_NEW_SUBFILE_TYPE TAG_NUMBER_OF_INKS
    TAG_ORIENTATION TAG_PAGE_NAME TAG_PAGE_NUMBER TAG_PHOTOMETRIC_INTERPRETATION
    TAG_PLANAR_CONFIGURATION TAG_PREDICTOR TAG_PRIMARY_CHROMATICITES TAG_REFERENCE_BLACK_WHITE
    TAG_RESOLUTION_UNIT TAG_ROWS_PER_STRIP TAG_S_MAX_SAMPLE_VALUE TAG_S_MIN_SAMPLE_VALUE
    TAG_SAMPLE_FORMAT TAG_SAMPLES_PER_PIXEL TAG_SOFTWARE TAG_STRIP_BYTE_COUNTS TAG_STRIP_OFFSETS
    TAG_SUBFILE_TYPE TAG_T4_OPTIONS TAG_T6_OPTIONS TAG_TARGET_PRINTER TAG_THRESHHOLDING
    TAG_TILE_BYTE_COUNTS TAG_TILE_LENGTH TAG_TILE_OFFSETS TAG_TILE_WIDTH TAG_TRANSFER_FUNCTION
    TAG_TRANSFER_RANGE TAG_WHITE_POINT TAG_X_POSITION TAG_X_RESOLUTION TAG_Y_CB_CR_COEFFICIENTS
    TAG_Y_CB_CR_POSITIONING TAG_Y_CB_CR_SUBSAMPLING TAG_Y_POSITION TAG_Y_RESOLUTION THRESHHOLDING_NONE
    THRESHHOLDING_ORDERED_DITHER THRESHHOLDING_RANDOMIZED_DITHER Y_CB_CR_POSITIONING_CENTERED
    Y_CB_CR_POSITIONING_COSITED'
  
  javaxImageioPluginsTiffExifGPSTagSet='ALTITUDE_REF_SEA_LEVEL ALTITUDE_REF_SEA_LEVEL_REFERENCE
    DEST_DISTANCE_REF_KILOMETERS DEST_DISTANCE_REF_KNOTS DEST_DISTANCE_REF_MILES
    DIFFERENTIAL_CORRECTION_APPLIED DIFFERENTIAL_CORRECTION_NONE DIRECTION_REF_MAGNETIC
    DIRECTION_REF_TRUE GPS_VERSION_2_2 LATITUDE_REF_NORTH LATITUDE_REF_SOUTH LONGITUDE_REF_EAST
    LONGITUDE_REF_WEST MEASURE_MODE_2D MEASURE_MODE_3D SPEED_REF_KILOMETERS_PER_HOUR SPEED_REF_KNOTS
    SPEED_REF_MILES_PER_HOUR STATUS_MEASUREMENT_IN_PROGRESS STATUS_MEASUREMENT_INTEROPERABILITY
    TAG_GPS_ALTITUDE TAG_GPS_ALTITUDE_REF TAG_GPS_AREA_INFORMATION TAG_GPS_DATE_STAMP
    TAG_GPS_DEST_BEARING TAG_GPS_DEST_BEARING_REF TAG_GPS_DEST_DISTANCE TAG_GPS_DEST_DISTANCE_REF
    TAG_GPS_DEST_LATITUDE TAG_GPS_DEST_LATITUDE_REF TAG_GPS_DEST_LONGITUDE TAG_GPS_DEST_LONGITUDE_REF
    TAG_GPS_DIFFERENTIAL TAG_GPS_DOP TAG_GPS_IMG_DIRECTION TAG_GPS_IMG_DIRECTION_REF TAG_GPS_LATITUDE
    TAG_GPS_LATITUDE_REF TAG_GPS_LONGITUDE TAG_GPS_LONGITUDE_REF TAG_GPS_MAP_DATUM TAG_GPS_MEASURE_MODE
    TAG_GPS_PROCESSING_METHOD TAG_GPS_SATELLITES TAG_GPS_SPEED TAG_GPS_SPEED_REF TAG_GPS_STATUS
    TAG_GPS_TIME_STAMP TAG_GPS_TRACK TAG_GPS_TRACK_REF TAG_GPS_VERSION_ID'
  
  javaxImageioPluginsTiffExifInteroperabilityTagSet='INTEROPERABILITY_INDEX_R98
    INTEROPERABILITY_INDEX_THM TAG_INTEROPERABILITY_INDEX'
  
  javaxImageioPluginsTiffExifParentTIFFTagSet='TAG_EXIF_IFD_POINTER TAG_GPS_INFO_IFD_POINTER'
  
  javaxImageioPluginsTiffExifTIFFTagSet='COLOR_SPACE_SRGB COLOR_SPACE_UNCALIBRATED
    COMPONENTS_CONFIGURATION_B COMPONENTS_CONFIGURATION_CB COMPONENTS_CONFIGURATION_CR
    COMPONENTS_CONFIGURATION_DOES_NOT_EXIST COMPONENTS_CONFIGURATION_G COMPONENTS_CONFIGURATION_R
    COMPONENTS_CONFIGURATION_Y CONTRAST_HARD CONTRAST_NORMAL CONTRAST_SOFT CUSTOM_RENDERED_CUSTOM
    CUSTOM_RENDERED_NORMAL EXIF_VERSION_2_1 EXIF_VERSION_2_2 EXPOSURE_MODE_AUTO_BRACKET
    EXPOSURE_MODE_AUTO_EXPOSURE EXPOSURE_MODE_MANUAL_EXPOSURE EXPOSURE_PROGRAM_ACTION_PROGRAM
    EXPOSURE_PROGRAM_APERTURE_PRIORITY EXPOSURE_PROGRAM_CREATIVE_PROGRAM
    EXPOSURE_PROGRAM_LANDSCAPE_MODE EXPOSURE_PROGRAM_MANUAL EXPOSURE_PROGRAM_MAX_RESERVED
    EXPOSURE_PROGRAM_NORMAL_PROGRAM EXPOSURE_PROGRAM_NOT_DEFINED EXPOSURE_PROGRAM_PORTRAIT_MODE
    EXPOSURE_PROGRAM_SHUTTER_PRIORITY FILE_SOURCE_DSC FLASH_DID_NOT_FIRE FLASH_FIRED FLASH_MASK_FIRED
    FLASH_MASK_FUNCTION_NOT_PRESENT FLASH_MASK_MODE_AUTO FLASH_MASK_MODE_FLASH_FIRING
    FLASH_MASK_MODE_FLASH_SUPPRESSION FLASH_MASK_RED_EYE_REDUCTION FLASH_MASK_RETURN_DETECTED
    FLASH_MASK_RETURN_NOT_DETECTED FLASH_STROBE_RETURN_LIGHT_DETECTED
    FLASH_STROBE_RETURN_LIGHT_NOT_DETECTED FOCAL_PLANE_RESOLUTION_UNIT_CENTIMETER
    FOCAL_PLANE_RESOLUTION_UNIT_INCH FOCAL_PLANE_RESOLUTION_UNIT_NONE GAIN_CONTROL_HIGH_GAIN_DOWN
    GAIN_CONTROL_HIGH_GAIN_UP GAIN_CONTROL_LOW_GAIN_DOWN GAIN_CONTROL_LOW_GAIN_UP GAIN_CONTROL_NONE
    LIGHT_SOURCE_CLOUDY_WEATHER LIGHT_SOURCE_COOL_WHITE_FLUORESCENT LIGHT_SOURCE_D50 LIGHT_SOURCE_D55
    LIGHT_SOURCE_D65 LIGHT_SOURCE_D75 LIGHT_SOURCE_DAY_WHITE_FLUORESCENT LIGHT_SOURCE_DAYLIGHT
    LIGHT_SOURCE_DAYLIGHT_FLUORESCENT LIGHT_SOURCE_FINE_WEATHER LIGHT_SOURCE_FLASH
    LIGHT_SOURCE_FLUORESCENT LIGHT_SOURCE_ISO_STUDIO_TUNGSTEN LIGHT_SOURCE_OTHER LIGHT_SOURCE_SHADE
    LIGHT_SOURCE_STANDARD_LIGHT_A LIGHT_SOURCE_STANDARD_LIGHT_B LIGHT_SOURCE_STANDARD_LIGHT_C
    LIGHT_SOURCE_TUNGSTEN LIGHT_SOURCE_UNKNOWN LIGHT_SOURCE_WHITE_FLUORESCENT METERING_MODE_AVERAGE
    METERING_MODE_CENTER_WEIGHTED_AVERAGE METERING_MODE_MAX_RESERVED METERING_MODE_MIN_RESERVED
    METERING_MODE_MULTI_SPOT METERING_MODE_OTHER METERING_MODE_PARTIAL METERING_MODE_PATTERN
    METERING_MODE_SPOT METERING_MODE_UNKNOWN SATURATION_HIGH SATURATION_LOW SATURATION_NORMAL
    SCENE_CAPTURE_TYPE_LANDSCAPE SCENE_CAPTURE_TYPE_NIGHT_SCENE SCENE_CAPTURE_TYPE_PORTRAIT
    SCENE_CAPTURE_TYPE_STANDARD SCENE_TYPE_DSC SENSING_METHOD_COLOR_SEQUENTIAL_AREA_SENSOR
    SENSING_METHOD_COLOR_SEQUENTIAL_LINEAR_SENSOR SENSING_METHOD_NOT_DEFINED
    SENSING_METHOD_ONE_CHIP_COLOR_AREA_SENSOR SENSING_METHOD_THREE_CHIP_COLOR_AREA_SENSOR
    SENSING_METHOD_TRILINEAR_SENSOR SENSING_METHOD_TWO_CHIP_COLOR_AREA_SENSOR SHARPNESS_HARD
    SHARPNESS_NORMAL SHARPNESS_SOFT SUBJECT_DISTANCE_RANGE_CLOSE_VIEW
    SUBJECT_DISTANCE_RANGE_DISTANT_VIEW SUBJECT_DISTANCE_RANGE_MACRO SUBJECT_DISTANCE_RANGE_UNKNOWN
    TAG_APERTURE_VALUE TAG_BRIGHTNESS_VALUE TAG_CFA_PATTERN TAG_COLOR_SPACE
    TAG_COMPONENTS_CONFIGURATION TAG_COMPRESSED_BITS_PER_PIXEL TAG_CONTRAST TAG_CUSTOM_RENDERED
    TAG_DATE_TIME_DIGITIZED TAG_DATE_TIME_ORIGINAL TAG_DEVICE_SETTING_DESCRIPTION
    TAG_DIGITAL_ZOOM_RATIO TAG_EXIF_VERSION TAG_EXPOSURE_BIAS_VALUE TAG_EXPOSURE_INDEX
    TAG_EXPOSURE_MODE TAG_EXPOSURE_PROGRAM TAG_EXPOSURE_TIME TAG_F_NUMBER TAG_FILE_SOURCE TAG_FLASH
    TAG_FLASH_ENERGY TAG_FLASHPIX_VERSION TAG_FOCAL_LENGTH TAG_FOCAL_LENGTH_IN_35MM_FILM
    TAG_FOCAL_PLANE_RESOLUTION_UNIT TAG_FOCAL_PLANE_X_RESOLUTION TAG_FOCAL_PLANE_Y_RESOLUTION
    TAG_GAIN_CONTROL TAG_GPS_INFO_IFD_POINTER TAG_IMAGE_UNIQUE_ID TAG_INTEROPERABILITY_IFD_POINTER
    TAG_ISO_SPEED_RATINGS TAG_LIGHT_SOURCE TAG_MAKER_NOTE TAG_MARKER_NOTE TAG_MAX_APERTURE_VALUE
    TAG_METERING_MODE TAG_OECF TAG_PIXEL_X_DIMENSION TAG_PIXEL_Y_DIMENSION TAG_RELATED_SOUND_FILE
    TAG_SATURATION TAG_SCENE_CAPTURE_TYPE TAG_SCENE_TYPE TAG_SENSING_METHOD TAG_SHARPNESS
    TAG_SHUTTER_SPEED_VALUE TAG_SPATIAL_FREQUENCY_RESPONSE TAG_SPECTRAL_SENSITIVITY TAG_SUB_SEC_TIME
    TAG_SUB_SEC_TIME_DIGITIZED TAG_SUB_SEC_TIME_ORIGINAL TAG_SUBJECT_AREA TAG_SUBJECT_DISTANCE
    TAG_SUBJECT_DISTANCE_RANGE TAG_SUBJECT_LOCATION TAG_USER_COMMENT TAG_WHITE_BALANCE
    WHITE_BALANCE_AUTO WHITE_BALANCE_MANUAL'
  
  javaxImageioPluginsTiffFaxTIFFTagSet='CLEAN_FAX_DATA_ERRORS_CORRECTED
    CLEAN_FAX_DATA_ERRORS_UNCORRECTED CLEAN_FAX_DATA_NO_ERRORS TAG_BAD_FAX_LINES TAG_CLEAN_FAX_DATA
    TAG_CONSECUTIVE_BAD_LINES'
  
  javaxImageioPluginsTiffGeoTIFFTagSet='TAG_GEO_ASCII_PARAMS TAG_GEO_DOUBLE_PARAMS
    TAG_GEO_KEY_DIRECTORY TAG_MODEL_PIXEL_SCALE TAG_MODEL_TIE_POINT TAG_MODEL_TRANSFORMATION'
  
  javaxImageioPluginsTiffTIFFTag='MAX_DATATYPE MIN_DATATYPE TIFF_ASCII TIFF_BYTE TIFF_DOUBLE
    TIFF_FLOAT TIFF_IFD_POINTER TIFF_LONG TIFF_RATIONAL TIFF_SBYTE TIFF_SHORT TIFF_SLONG TIFF_SRATIONAL
    TIFF_SSHORT TIFF_UNDEFINED UNKNOWN_TAG_NAME'
  
  javaxManagementAttributeChangeNotification='ATTRIBUTE_CHANGE'
  
  javaxManagementJMX='DEFAULT_VALUE_FIELD IMMUTABLE_INFO_FIELD INTERFACE_CLASS_NAME_FIELD
    LEGAL_VALUES_FIELD MAX_VALUE_FIELD MIN_VALUE_FIELD MXBEAN_FIELD OPEN_TYPE_FIELD ORIGINAL_TYPE_FIELD'
  
  javaxManagementMBeanOperationInfo='ACTION ACTION_INFO INFO UNKNOWN'
  
  javaxManagementMBeanServerNotification='REGISTRATION_NOTIFICATION UNREGISTRATION_NOTIFICATION'
  
  javaxManagementQuery='DIV EQ GE GT LE LT MINUS PLUS TIMES'
  
  javaxManagementMonitorMonitor='capacityIncrement OBSERVED_ATTRIBUTE_ERROR_NOTIFIED
    OBSERVED_ATTRIBUTE_TYPE_ERROR_NOTIFIED OBSERVED_OBJECT_ERROR_NOTIFIED RESET_FLAGS_ALREADY_NOTIFIED
    RUNTIME_ERROR_NOTIFIED'
  
  javaxManagementMonitorMonitorNotification='OBSERVED_ATTRIBUTE_ERROR OBSERVED_ATTRIBUTE_TYPE_ERROR
    OBSERVED_OBJECT_ERROR RUNTIME_ERROR STRING_TO_COMPARE_VALUE_DIFFERED
    STRING_TO_COMPARE_VALUE_MATCHED THRESHOLD_ERROR THRESHOLD_HIGH_VALUE_EXCEEDED
    THRESHOLD_LOW_VALUE_EXCEEDED THRESHOLD_VALUE_EXCEEDED'
  
  javaxManagementRelationRelationNotification='RELATION_BASIC_CREATION RELATION_BASIC_REMOVAL
    RELATION_BASIC_UPDATE RELATION_MBEAN_CREATION RELATION_MBEAN_REMOVAL RELATION_MBEAN_UPDATE'
  
  javaxManagementRelationRoleInfo='ROLE_CARDINALITY_INFINITY'
  
  javaxManagementRelationRoleStatus='LESS_THAN_MIN_ROLE_DEGREE MORE_THAN_MAX_ROLE_DEGREE
    NO_ROLE_WITH_NAME REF_MBEAN_NOT_REGISTERED REF_MBEAN_OF_INCORRECT_CLASS ROLE_NOT_READABLE ROLE_NOT_WRITABLE'
  
  javaxManagementRemoteJMXConnectionNotification='CLOSED FAILED NOTIFS_LOST OPENED'
  
  javaxManagementRemoteJMXConnector='CREDENTIALS'
  
  javaxManagementRemoteJMXConnectorFactory='DEFAULT_CLASS_LOADER PROTOCOL_PROVIDER_CLASS_LOADER
    PROTOCOL_PROVIDER_PACKAGES'
  
  javaxManagementRemoteJMXConnectorServer='AUTHENTICATOR'
  
  javaxManagementRemoteJMXConnectorServerFactory='DEFAULT_CLASS_LOADER DEFAULT_CLASS_LOADER_NAME
    PROTOCOL_PROVIDER_CLASS_LOADER PROTOCOL_PROVIDER_PACKAGES'
  
  javaxManagementRemoteRmiRMIConnectorServer='CREDENTIALS_FILTER_PATTERN JNDI_REBIND_ATTRIBUTE
    RMI_CLIENT_SOCKET_FACTORY_ATTRIBUTE RMI_SERVER_SOCKET_FACTORY_ATTRIBUTE SERIAL_FILTER_PATTERN'
  
  javaxManagementTimerTimer='ONE_DAYL ONE_HOURL ONE_MINUTEL ONE_SECONDL ONE_WEEKL'
  
  javaxNamingContext='APPLET AUTHORITATIVE BATCHSIZE DNS_URL INITIAL_CONTEXT_FACTORY LANGUAGE
    OBJECT_FACTORIES PROVIDER_URL REFERRAL SECURITY_AUTHENTICATION SECURITY_CREDENTIALS
    SECURITY_PRINCIPAL SECURITY_PROTOCOL STATE_FACTORIES URL_PKG_PREFIXES'
  
  javaxNamingName='serialVersionUIDL'
  
  javaxNamingDirectoryAttribute='serialVersionUIDL'
  
  javaxNamingDirectoryDirContext='ADD_ATTRIBUTE REMOVE_ATTRIBUTE REPLACE_ATTRIBUTE'
  
  javaxNamingDirectorySearchControls='OBJECT_SCOPE ONELEVEL_SCOPE SUBTREE_SCOPE'
  
  javaxNamingEventEventContext='OBJECT_SCOPE ONELEVEL_SCOPE SUBTREE_SCOPE'
  
  javaxNamingEventNamingEvent='OBJECT_ADDED OBJECT_CHANGED OBJECT_REMOVED OBJECT_RENAMED'
  
  javaxNamingLdapControl='CRITICAL NONCRITICAL'
  
  javaxNamingLdapLdapContext='CONTROL_FACTORIES'
  
  javaxNamingLdapManageReferralControl='OID'
  
  javaxNamingLdapPagedResultsControl='OID'
  
  javaxNamingLdapPagedResultsResponseControl='OID'
  
  javaxNamingLdapSortControl='OID'
  
  javaxNamingLdapSortResponseControl='OID'
  
  javaxNamingLdapStartTlsRequest='OID'
  
  javaxNamingLdapStartTlsResponse='OID'
  
  javaxNamingSpiNamingManager='CPE'
  
  javaxNetSslStandardConstants='SNI_HOST_NAME'
  
  javaxPrintServiceUIFactory='ABOUT_UIROLE ADMIN_UIROLE DIALOG_UI JCOMPONENT_UI JDIALOG_UI
    MAIN_UIROLE PANEL_UI RESERVED_UIROLE'
  
  javaxPrintURIException='URIInaccessible URIOtherProblem URISchemeNotSupported'
  
  javaxPrintAttributeResolutionSyntax='DPCM DPI'
  
  javaxPrintAttributeSize2DSyntax='INCH MM'
  
  javaxPrintAttributeStandardMediaPrintableArea='INCH MM'
  
  javaxPrintEventPrintJobEvent='DATA_TRANSFER_COMPLETE JOB_CANCELED JOB_COMPLETE JOB_FAILED
    NO_MORE_EVENTS REQUIRES_ATTENTION'
  
  javaxScriptScriptContext='ENGINE_SCOPE GLOBAL_SCOPE'
  
  javaxScriptScriptEngine='ARGV ENGINE ENGINE_VERSION FILENAME LANGUAGE LANGUAGE_VERSION NAME'
  
  javaxSecurityAuthCallbackConfirmationCallback='CANCEL ERROR INFORMATION NO OK OK_CANCEL_OPTION
    UNSPECIFIED_OPTION WARNING YES YES_NO_CANCEL_OPTION YES_NO_OPTION'
  
  javaxSecurityAuthCallbackTextOutputCallback='ERROR INFORMATION WARNING'
  
  javaxSecurityAuthKerberosKerberosPrincipal='KRB_NT_ENTERPRISE KRB_NT_PRINCIPAL KRB_NT_SRV_HST
    KRB_NT_SRV_INST KRB_NT_SRV_XHST KRB_NT_UID KRB_NT_UNKNOWN'
  
  javaxSecurityAuthX500X500Principal='CANONICAL RFC1779 RFC2253'
  
  javaxSecuritySaslSasl='BOUND_SERVER_NAME CREDENTIALS MAX_BUFFER POLICY_FORWARD_SECRECY
    POLICY_NOACTIVE POLICY_NOANONYMOUS POLICY_NODICTIONARY POLICY_NOPLAINTEXT POLICY_PASS_CREDENTIALS
    QOP RAW_SEND_SIZE REUSE SERVER_AUTH STRENGTH'
  
  javaxSoundMidiMetaMessage='META'
  
  javaxSoundMidiMidiFileFormat='UNKNOWN_LENGTH'
  
  javaxSoundMidiSequence='PPQ SMPTE_24 SMPTE_25 SMPTE_30 SMPTE_30DROP'
  
  javaxSoundMidiSequencer='LOOP_CONTINUOUSLY'
  
  javaxSoundMidiShortMessage='ACTIVE_SENSING CHANNEL_PRESSURE CONTINUE CONTROL_CHANGE
    END_OF_EXCLUSIVE MIDI_TIME_CODE NOTE_OFF NOTE_ON PITCH_BEND POLY_PRESSURE PROGRAM_CHANGE
    SONG_POSITION_POINTER SONG_SELECT START STOP SYSTEM_RESET TIMING_CLOCK TUNE_REQUEST'
  
  javaxSoundMidiSysexMessage='SPECIAL_SYSTEM_EXCLUSIVE SYSTEM_EXCLUSIVE'
  
  javaxSoundSampledAudioSystem='NOT_SPECIFIED'
  
  javaxSoundSampledClip='LOOP_CONTINUOUSLY'
  
  javaxSqlRowsetBaseRowSet='ASCII_STREAM_PARAM BINARY_STREAM_PARAM UNICODE_STREAM_PARAM'
  
  javaxSqlRowsetCachedRowSet='COMMIT_ON_ACCEPT_CHANGES'
  
  javaxSqlRowsetJoinRowSet='CROSS_JOIN FULL_JOIN INNER_JOIN LEFT_OUTER_JOIN RIGHT_OUTER_JOIN'
  
  javaxSqlRowsetWebRowSet='PUBLIC_XML_SCHEMA SCHEMA_SYSTEM_ID'
  
  javaxSqlRowsetSpiSyncFactory='ROWSET_SYNC_PROVIDER ROWSET_SYNC_PROVIDER_VERSION ROWSET_SYNC_VENDOR'
  
  javaxSqlRowsetSpiSyncProvider='DATASOURCE_DB_LOCK DATASOURCE_NO_LOCK DATASOURCE_ROW_LOCK
    DATASOURCE_TABLE_LOCK GRADE_CHECK_ALL_AT_COMMIT GRADE_CHECK_MODIFIED_AT_COMMIT
    GRADE_LOCK_WHEN_LOADED GRADE_LOCK_WHEN_MODIFIED GRADE_NONE NONUPDATABLE_VIEW_SYNC
    UPDATABLE_VIEW_SYNC'
  
  javaxSqlRowsetSpiSyncResolver='DELETE_ROW_CONFLICT INSERT_ROW_CONFLICT NO_ROW_CONFLICT UPDATE_ROW_CONFLICT'
  
  javaxSwingAbstractButton='BORDER_PAINTED_CHANGED_PROPERTY CONTENT_AREA_FILLED_CHANGED_PROPERTY
    DISABLED_ICON_CHANGED_PROPERTY DISABLED_SELECTED_ICON_CHANGED_PROPERTY
    FOCUS_PAINTED_CHANGED_PROPERTY HORIZONTAL_ALIGNMENT_CHANGED_PROPERTY
    HORIZONTAL_TEXT_POSITION_CHANGED_PROPERTY ICON_CHANGED_PROPERTY MARGIN_CHANGED_PROPERTY
    MNEMONIC_CHANGED_PROPERTY MODEL_CHANGED_PROPERTY PRESSED_ICON_CHANGED_PROPERTY
    ROLLOVER_ENABLED_CHANGED_PROPERTY ROLLOVER_ICON_CHANGED_PROPERTY
    ROLLOVER_SELECTED_ICON_CHANGED_PROPERTY SELECTED_ICON_CHANGED_PROPERTY TEXT_CHANGED_PROPERTY
    VERTICAL_ALIGNMENT_CHANGED_PROPERTY VERTICAL_TEXT_POSITION_CHANGED_PROPERTY'
  
  javaxSwingAction='ACCELERATOR_KEY ACTION_COMMAND_KEY DEFAULT DISPLAYED_MNEMONIC_INDEX_KEY
    LARGE_ICON_KEY LONG_DESCRIPTION MNEMONIC_KEY NAME SELECTED_KEY SHORT_DESCRIPTION SMALL_ICON'
  
  javaxSwingBoxLayout='LINE_AXIS PAGE_AXIS X_AXIS Y_AXIS'
  
  javaxSwingDebugGraphics='BUFFERED_OPTION FLASH_OPTION LOG_OPTION NONE_OPTION'
  
  javaxSwingDefaultButtonModel='ARMED ENABLED PRESSED ROLLOVER SELECTED'
  
  javaxSwingFocusManager='FOCUS_MANAGER_CLASS_PROPERTY'
  
  javaxSwingGroupLayout='DEFAULT_SIZE PREFERRED_SIZE'
  
  javaxSwingJCheckBox='BORDER_PAINTED_FLAT_CHANGED_PROPERTY'
  
  javaxSwingJColorChooser='CHOOSER_PANELS_PROPERTY PREVIEW_PANEL_PROPERTY SELECTION_MODEL_PROPERTY'
  
  javaxSwingJComponent='TOOL_TIP_TEXT_KEY UNDEFINED_CONDITION WHEN_ANCESTOR_OF_FOCUSED_COMPONENT
    WHEN_FOCUSED WHEN_IN_FOCUSED_WINDOW'
  
  javaxSwingJDesktopPane='LIVE_DRAG_MODE OUTLINE_DRAG_MODE'
  
  javaxSwingJEditorPane='HONOR_DISPLAY_PROPERTIES W3C_LENGTH_UNITS'
  
  javaxSwingJFileChooser='ACCEPT_ALL_FILE_FILTER_USED_CHANGED_PROPERTY ACCESSORY_CHANGED_PROPERTY
    APPROVE_BUTTON_MNEMONIC_CHANGED_PROPERTY APPROVE_BUTTON_TEXT_CHANGED_PROPERTY
    APPROVE_BUTTON_TOOL_TIP_TEXT_CHANGED_PROPERTY APPROVE_OPTION APPROVE_SELECTION CANCEL_OPTION
    CANCEL_SELECTION CHOOSABLE_FILE_FILTER_CHANGED_PROPERTY CONTROL_BUTTONS_ARE_SHOWN_CHANGED_PROPERTY
    CUSTOM_DIALOG DIALOG_TITLE_CHANGED_PROPERTY DIALOG_TYPE_CHANGED_PROPERTY DIRECTORIES_ONLY
    DIRECTORY_CHANGED_PROPERTY ERROR_OPTION FILE_FILTER_CHANGED_PROPERTY FILE_HIDING_CHANGED_PROPERTY
    FILE_SELECTION_MODE_CHANGED_PROPERTY FILE_SYSTEM_VIEW_CHANGED_PROPERTY FILE_VIEW_CHANGED_PROPERTY
    FILES_AND_DIRECTORIES FILES_ONLY MULTI_SELECTION_ENABLED_CHANGED_PROPERTY OPEN_DIALOG SAVE_DIALOG
    SELECTED_FILE_CHANGED_PROPERTY SELECTED_FILES_CHANGED_PROPERTY'
  
  javaxSwingJFormattedTextField='COMMIT COMMIT_OR_REVERT PERSIST REVERT'
  
  javaxSwingJInternalFrame='CONTENT_PANE_PROPERTY FRAME_ICON_PROPERTY GLASS_PANE_PROPERTY
    IS_CLOSED_PROPERTY IS_ICON_PROPERTY IS_MAXIMUM_PROPERTY IS_SELECTED_PROPERTY LAYERED_PANE_PROPERTY
    MENU_BAR_PROPERTY ROOT_PANE_PROPERTY TITLE_PROPERTY'
  
  javaxSwingJLayeredPane='LAYER_PROPERTY'
  
  javaxSwingJlist='HORIZONTAL_WRAP VERTICAL VERTICAL_WRAP'
  
  javaxSwingJOptionPane='CANCEL_OPTION CLOSED_OPTION DEFAULT_OPTION ERROR_MESSAGE ICON_PROPERTY
    INFORMATION_MESSAGE INITIAL_SELECTION_VALUE_PROPERTY INITIAL_VALUE_PROPERTY INPUT_VALUE_PROPERTY
    MESSAGE_PROPERTY MESSAGE_TYPE_PROPERTY NO_OPTION OK_CANCEL_OPTION OK_OPTION OPTION_TYPE_PROPERTY
    OPTIONS_PROPERTY PLAIN_MESSAGE QUESTION_MESSAGE SELECTION_VALUES_PROPERTY VALUE_PROPERTY
    WANTS_INPUT_PROPERTY WARNING_MESSAGE YES_NO_CANCEL_OPTION YES_NO_OPTION YES_OPTION'
  
  javaxSwingJRootPane='COLOR_CHOOSER_DIALOG ERROR_DIALOG FILE_CHOOSER_DIALOG FRAME INFORMATION_DIALOG
    NONE PLAIN_DIALOG QUESTION_DIALOG WARNING_DIALOG'
  
  javaxSwingJSplitPane='BOTTOM CONTINUOUS_LAYOUT_PROPERTY DIVIDER DIVIDER_LOCATION_PROPERTY
    DIVIDER_SIZE_PROPERTY HORIZONTAL_SPLIT LAST_DIVIDER_LOCATION_PROPERTY LEFT
    ONE_TOUCH_EXPANDABLE_PROPERTY ORIENTATION_PROPERTY RESIZE_WEIGHT_PROPERTY RIGHT TOP VERTICAL_SPLIT'
  
  javaxSwingJTabbedPane='SCROLL_TAB_LAYOUT WRAP_TAB_LAYOUT'
  
  javaxSwingJTable='AUTO_RESIZE_ALL_COLUMNS AUTO_RESIZE_LAST_COLUMN AUTO_RESIZE_NEXT_COLUMN
    AUTO_RESIZE_OFF AUTO_RESIZE_SUBSEQUENT_COLUMNS'
  
  javaxSwingJTextField='notifyAction'
  
  javaxSwingJTree='ANCHOR_SELECTION_PATH_PROPERTY CELL_EDITOR_PROPERTY CELL_RENDERER_PROPERTY
    EDITABLE_PROPERTY EXPANDS_SELECTED_PATHS_PROPERTY INVOKES_STOP_CELL_EDITING_PROPERTY
    LARGE_MODEL_PROPERTY LEAD_SELECTION_PATH_PROPERTY ROOT_VISIBLE_PROPERTY ROW_HEIGHT_PROPERTY
    SCROLLS_ON_EXPAND_PROPERTY SELECTION_MODEL_PROPERTY SHOWS_ROOT_HANDLES_PROPERTY
    TOGGLE_CLICK_COUNT_PROPERTY TREE_MODEL_PROPERTY VISIBLE_ROW_COUNT_PROPERTY'
  
  javaxSwingJViewport='BACKINGSTORE_SCROLL_MODE BLIT_SCROLL_MODE SIMPLE_SCROLL_MODE'
  
  javaxSwingListSelectionModel='MULTIPLE_INTERVAL_SELECTION SINGLE_INTERVAL_SELECTION SINGLE_SELECTION'
  
  javaxSwingScrollPaneConstants='COLUMN_HEADER HORIZONTAL_SCROLLBAR HORIZONTAL_SCROLLBAR_ALWAYS
    HORIZONTAL_SCROLLBAR_AS_NEEDED HORIZONTAL_SCROLLBAR_NEVER HORIZONTAL_SCROLLBAR_POLICY
    LOWER_LEADING_CORNER LOWER_LEFT_CORNER LOWER_RIGHT_CORNER LOWER_TRAILING_CORNER ROW_HEADER
    UPPER_LEADING_CORNER UPPER_LEFT_CORNER UPPER_RIGHT_CORNER UPPER_TRAILING_CORNER VERTICAL_SCROLLBAR
    VERTICAL_SCROLLBAR_ALWAYS VERTICAL_SCROLLBAR_AS_NEEDED VERTICAL_SCROLLBAR_NEVER
    VERTICAL_SCROLLBAR_POLICY VIEWPORT'
  
  javaxSwingSpring='UNSET'
  
  javaxSwingSpringLayout='BASELINE EAST HEIGHT HORIZONTAL_CENTER NORTH SOUTH VERTICAL_CENTER WEST WIDTH'
  
  javaxSwingSwingConstants='BOTTOM CENTER EAST HORIZONTAL LEADING LEFT NEXT NORTH NORTH_EAST
    NORTH_WEST PREVIOUS RIGHT SOUTH SOUTH_EAST SOUTH_WEST TOP TRAILING VERTICAL WEST'
  
  javaxSwingTransferHandler='COPY COPY_OR_MOVE LINK MOVE NONE'
  
  javaxSwingWindowConstants='DISPOSE_ON_CLOSE DO_NOTHING_ON_CLOSE EXIT_ON_CLOSE HIDE_ON_CLOSE'
  
  javaxSwingBorderBevelBorder='LOWERED RAISED'
  
  javaxSwingBorderEtchedBorder='LOWERED RAISED'
  
  javaxSwingBorderTitledBorder='ABOVE_BOTTOM ABOVE_TOP BELOW_BOTTOM BELOW_TOP BOTTOM CENTER
    DEFAULT_JUSTIFICATION DEFAULT_POSITION EDGE_SPACING LEADING LEFT RIGHT TEXT_INSET_H TEXT_SPACING
    TOP TRAILING'
  
  javaxSwingColorchooserAbstractColorChooserPanel='TRANSPARENCY_ENABLED_PROPERTY'
  
  javaxSwingEventAncestorEvent='ANCESTOR_ADDED ANCESTOR_MOVED ANCESTOR_REMOVED'
  
  javaxSwingEventInternalFrameEvent='INTERNAL_FRAME_ACTIVATED INTERNAL_FRAME_CLOSED
    INTERNAL_FRAME_CLOSING INTERNAL_FRAME_DEACTIVATED INTERNAL_FRAME_DEICONIFIED INTERNAL_FRAME_FIRST
    INTERNAL_FRAME_ICONIFIED INTERNAL_FRAME_LAST INTERNAL_FRAME_OPENED'
  
  javaxSwingEventListDataEvent='CONTENTS_CHANGED INTERVAL_ADDED INTERVAL_REMOVED'
  
  javaxSwingEventTableModelEvent='ALL_COLUMNS DELETE HEADER_ROW INSERT UPDATE'
  
  javaxSwingPlafBasicBasicComboPopup='SCROLL_DOWN SCROLL_UP'
  
  javaxSwingPlafBasicBasicHTML='documentBaseKey propertyKey'
  
  javaxSwingPlafBasicBasicinternalframeuiBorderListener='RESIZE_NONE'
  
  javaxSwingPlafBasicBasicListUI='cellRendererChanged fixedCellHeightChanged fixedCellWidthChanged
    fontChanged modelChanged prototypeCellValueChanged selectionModelChanged'
  
  javaxSwingPlafBasicBasicOptionPaneUI='MinimumHeight MinimumWidth'
  
  javaxSwingPlafBasicBasicScrollBarUI='DECREASE_HIGHLIGHT INCREASE_HIGHLIGHT NO_HIGHLIGHT'
  
  javaxSwingPlafBasicBasicSliderUI='MAX_SCROLL MIN_SCROLL NEGATIVE_SCROLL POSITIVE_SCROLL'
  
  javaxSwingPlafBasicBasicSplitPaneDivider='ONE_TOUCH_OFFSET ONE_TOUCH_SIZE'
  
  javaxSwingPlafBasicBasicSplitPaneUI='NON_CONTINUOUS_DIVIDER'
  
  javaxSwingPlafMetalMetalIconFactory='DARK LIGHT'
  
  javaxSwingPlafMetalMetalScrollBarUI='FREE_STANDING_PROP'
  
  javaxSwingPlafMetalMetalSliderUI='SLIDER_FILL TICK_BUFFER'
  
  javaxSwingPlafMetalMetalToolTipUI='padSpaceBetweenStrings'
  
  javaxSwingPlafNimbusNimbusStyle='LARGE_KEY LARGE_SCALE MINI_KEY MINI_SCALE SMALL_KEY SMALL_SCALE'
  
  javaxSwingPlafSynthSynthConstants='DEFAULT DISABLED ENABLED FOCUSED MOUSE_OVER PRESSED SELECTED'
  
  javaxSwingTableTableColumn='CELL_RENDERER_PROPERTY COLUMN_WIDTH_PROPERTY HEADER_RENDERER_PROPERTY HEADER_VALUE_PROPERTY'
  
  javaxSwingTextAbstractDocument='BAD_LOCATION BidiElementName ContentElementName
    ElementNameAttribute ParagraphElementName SectionElementName'
  
  javaxSwingTextAbstractWriter='NEWLINE'
  
  javaxSwingTextDefaultCaret='ALWAYS_UPDATE NEVER_UPDATE UPDATE_WHEN_ON_EDT'
  
  javaxSwingTextDefaultEditorKit='backwardAction beepAction beginAction beginLineAction
    beginParagraphAction beginWordAction copyAction cutAction defaultKeyTypedAction
    deleteNextCharAction deleteNextWordAction deletePrevCharAction deletePrevWordAction downAction
    endAction endLineAction EndOfLineStringProperty endParagraphAction endWordAction forwardAction
    insertBreakAction insertContentAction insertTabAction nextWordAction pageDownAction pageUpAction
    pasteAction previousWordAction readOnlyAction selectAllAction selectionBackwardAction
    selectionBeginAction selectionBeginLineAction selectionBeginParagraphAction
    selectionBeginWordAction selectionDownAction selectionEndAction selectionEndLineAction
    selectionEndParagraphAction selectionEndWordAction selectionForwardAction selectionNextWordAction
    selectionPreviousWordAction selectionUpAction selectLineAction selectParagraphAction
    selectWordAction upAction writableAction'
  
  javaxSwingTextDefaultStyledDocument='BUFFER_SIZE_DEFAULT'
  
  javaxSwingTextDefaultstyleddocumentElementSpec='ContentType EndTagType JoinFractureDirection
    JoinNextDirection JoinPreviousDirection OriginateDirection StartTagType'
  
  javaxSwingTextDocument='StreamDescriptionProperty TitleProperty'
  
  javaxSwingTextJTextComponent='DEFAULT_KEYMAP FOCUS_ACCELERATOR_KEY'
  
  javaxSwingTextPlainDocument='lineLimitAttribute tabSizeAttribute'
  
  javaxSwingTextStyleConstants='ALIGN_CENTER ALIGN_JUSTIFIED ALIGN_LEFT ALIGN_RIGHT
    ComponentElementName IconElementName'
  
  javaxSwingTextStyleContext='DEFAULT_STYLE'
  
  javaxSwingTextTabStop='ALIGN_BAR ALIGN_CENTER ALIGN_DECIMAL ALIGN_LEFT ALIGN_RIGHT LEAD_DOTS
    LEAD_EQUALS LEAD_HYPHENS LEAD_NONE LEAD_THICKLINE LEAD_UNDERLINE'
  
  javaxSwingTextView='BadBreakWeight ExcellentBreakWeight ForcedBreakWeight GoodBreakWeight X_AXIS Y_AXIS'
  
  javaxSwingTextHtmlHTML='NULL_ATTRIBUTE_VALUE'
  
  javaxSwingTextHtmlHTMLDocument='AdditionalComments'
  
  javaxSwingTextHtmlHTMLEditorKit='BOLD_ACTION COLOR_ACTION DEFAULT_CSS FONT_CHANGE_BIGGER
    FONT_CHANGE_SMALLER IMG_ALIGN_BOTTOM IMG_ALIGN_MIDDLE IMG_ALIGN_TOP IMG_BORDER ITALIC_ACTION
    LOGICAL_STYLE_ACTION PARA_INDENT_LEFT PARA_INDENT_RIGHT'
  
  javaxSwingTextHtmlParserDTD='FILE_VERSION'
  
  javaxSwingTextHtmlParserDTDConstants='ANY CDATA CONREF CURRENT DEFAULT EMPTY ENDTAG ENTITIES ENTITY
    FIXED GENERAL ID IDREF IDREFS IMPLIED MD MODEL MS NAME NAMES NMTOKEN NMTOKENS NOTATION NUMBER
    NUMBERS NUTOKEN NUTOKENS PARAMETER PI PUBLIC RCDATA REQUIRED SDATA STARTTAG SYSTEM'
  
  javaxSwingTreeDefaultTreeSelectionModel='SELECTION_MODE_PROPERTY'
  
  javaxSwingTreeTreeSelectionModel='CONTIGUOUS_TREE_SELECTION DISCONTIGUOUS_TREE_SELECTION SINGLE_TREE_SELECTION'
  
  javaxSwingUndoAbstractUndoableEdit='RedoName UndoName'
  
  javaxSwingUndoStateEdit='RCSID'
  
  javaxSwingUndoStateEditable='RCSID'
  
  javaxToolsDiagnostic='NOPOSL'
  
  javaxTransactionXaXAException='XA_HEURCOM XA_HEURHAZ XA_HEURMIX XA_HEURRB XA_NOMIGRATE XA_RBBASE
    XA_RBCOMMFAIL XA_RBDEADLOCK XA_RBEND XA_RBINTEGRITY XA_RBOTHER XA_RBPROTO XA_RBROLLBACK
    XA_RBTIMEOUT XA_RBTRANSIENT XA_RDONLY XA_RETRY XAER_ASYNC XAER_DUPID XAER_INVAL XAER_NOTA
    XAER_OUTSIDE XAER_PROTO XAER_RMERR XAER_RMFAIL'
  
  javaxTransactionXaXAResource='TMENDRSCAN TMFAIL TMJOIN TMNOFLAGS TMONEPHASE TMRESUME TMSTARTRSCAN
    TMSUCCESS TMSUSPEND XA_OK XA_RDONLY'
  
  javaxTransactionXaXid='MAXBQUALSIZE MAXGTRIDSIZE'
  
  javaxXmlXMLConstants='ACCESS_EXTERNAL_DTD ACCESS_EXTERNAL_SCHEMA ACCESS_EXTERNAL_STYLESHEET
    DEFAULT_NS_PREFIX FEATURE_SECURE_PROCESSING NULL_NS_URI RELAXNG_NS_URI USE_CATALOG
    W3C_XML_SCHEMA_INSTANCE_NS_URI W3C_XML_SCHEMA_NS_URI W3C_XPATH_DATATYPE_NS_URI XML_DTD_NS_URI
    XML_NS_PREFIX XML_NS_URI XMLNS_ATTRIBUTE XMLNS_ATTRIBUTE_NS_URI'
  
  javaxXmlCryptoDsigCanonicalizationMethod='EXCLUSIVE EXCLUSIVE_WITH_COMMENTS INCLUSIVE INCLUSIVE_11
    INCLUSIVE_11_WITH_COMMENTS INCLUSIVE_WITH_COMMENTS'
  
  javaxXmlCryptoDsigDigestMethod='RIPEMD160 SHA1 SHA224 SHA256 SHA3_224 SHA3_256 SHA3_384 SHA3_512 SHA384 SHA512'
  
  javaxXmlCryptoDsigManifest='TYPE'
  
  javaxXmlCryptoDsigSignatureMethod='DSA_SHA1 DSA_SHA256 ECDSA_SHA1 ECDSA_SHA224 ECDSA_SHA256
    ECDSA_SHA384 ECDSA_SHA512 HMAC_SHA1 HMAC_SHA224 HMAC_SHA256 HMAC_SHA384 HMAC_SHA512 RSA_SHA1
    RSA_SHA224 RSA_SHA256 RSA_SHA384 RSA_SHA512 SHA1_RSA_MGF1 SHA224_RSA_MGF1 SHA256_RSA_MGF1
    SHA384_RSA_MGF1 SHA512_RSA_MGF1'
  
  javaxXmlCryptoDsigSignatureProperties='TYPE'
  
  javaxXmlCryptoDsigTransform='BASE64 ENVELOPED XPATH XPATH2 XSLT'
  
  javaxXmlCryptoDsigXMLObject='TYPE'
  
  javaxXmlCryptoDsigXMLSignature='XMLNS'
  
  javaxXmlCryptoDsigKeyinfoKeyValue='DSA_TYPE EC_TYPE RSA_TYPE'
  
  javaxXmlCryptoDsigKeyinfoPGPData='TYPE'
  
  javaxXmlCryptoDsigKeyinfoX509Data='RAW_X509_CERTIFICATE_TYPE TYPE'
  
  javaxXmlCryptoDsigSpecExcC14NParameterSpec='DEFAULT'
  
  javaxXmlDatatypeDatatypeConstants='APRIL AUGUST DECEMBER EQUAL FEBRUARY FIELD_UNDEFINED GREATER
    INDETERMINATE JANUARY JULY JUNE LESSER MARCH MAX_TIMEZONE_OFFSET MAY MIN_TIMEZONE_OFFSET NOVEMBER
    OCTOBER SEPTEMBER'
  
  javaxXmlDatatypeDatatypeFactory='DATATYPEFACTORY_PROPERTY'
  
  javaxXmlStreamXMLInputFactory='ALLOCATOR IS_COALESCING IS_NAMESPACE_AWARE
    IS_REPLACING_ENTITY_REFERENCES IS_SUPPORTING_EXTERNAL_ENTITIES IS_VALIDATING REPORTER RESOLVER
    SUPPORT_DTD'
  
  javaxXmlStreamXMLOutputFactory='IS_REPAIRING_NAMESPACES'
  
  javaxXmlStreamXMLStreamConstants='ATTRIBUTE CDATA CHARACTERS COMMENT DTD END_DOCUMENT END_ELEMENT
    ENTITY_DECLARATION ENTITY_REFERENCE NAMESPACE NOTATION_DECLARATION PROCESSING_INSTRUCTION SPACE
    START_DOCUMENT START_ELEMENT'
  
  javaxXmlTransformOutputKeys='CDATA_SECTION_ELEMENTS DOCTYPE_PUBLIC DOCTYPE_SYSTEM ENCODING INDENT
    MEDIA_TYPE METHOD OMIT_XML_DECLARATION STANDALONE VERSION'
  
  javaxXmlTransformResult='PI_DISABLE_OUTPUT_ESCAPING PI_ENABLE_OUTPUT_ESCAPING'
  
  javaxXmlTransformDomDOMResult='FEATURE'
  
  javaxXmlTransformDomDOMSource='FEATURE'
  
  javaxXmlTransformSaxSAXResult='FEATURE'
  
  javaxXmlTransformSaxSAXSource='FEATURE'
  
  javaxXmlTransformSaxSAXTransformerFactory='FEATURE FEATURE_XMLFILTER'
  
  javaxXmlTransformStaxStAXResult='FEATURE'
  
  javaxXmlTransformStaxStAXSource='FEATURE'
  
  javaxXmlTransformStreamStreamResult='FEATURE'
  
  javaxXmlTransformStreamStreamSource='FEATURE'
  
  javaxXmlXpathXPathConstants='DOM_OBJECT_MODEL'
  
  javaxXmlXpathXPathFactory='DEFAULT_OBJECT_MODEL_URI DEFAULT_PROPERTY_NAME'
  
  jdkDynalinkSecureLookupSupplier='GET_LOOKUP_PERMISSION_NAME'
  
  jdkDynalinkLinkerGuardingDynamicLinkerExporter='AUTOLOAD_PERMISSION_NAME'
  
  jdkIncubatorForeignMemoryLayout='LAYOUT_NAME'
  
  jdkIncubatorForeignMemorySegment='ACQUIRE ALL_ACCESS CLOSE HANDOFF READ WRITE'
  
  jdkJfrDataAmount='BITS BYTES'
  
  jdkJfrEnabled='NAME'
  
  jdkJfrPeriod='NAME'
  
  jdkJfrStackTrace='NAME'
  
  jdkJfrThreshold='NAME'
  
  jdkJfrTimespan='MICROSECONDS MILLISECONDS NANOSECONDS SECONDS TICKS'
  
  jdkJfrTimestamp='MILLISECONDS_SINCE_EPOCH TICKS'
  
  jdkJshellDiag='NOPOSL'
  
  jdkJshellExecutionJdiExecutionControlProvider='PARAM_HOST_NAME PARAM_LAUNCH PARAM_REMOTE_AGENT PARAM_TIMEOUT'
  
  jdkManagementJfrFlightRecorderMXBean='MXBEAN_NAME'
  
  orgIetfJgssGSSContext='DEFAULT_LIFETIME INDEFINITE_LIFETIME'
  
  orgIetfJgssGSSCredential='ACCEPT_ONLY DEFAULT_LIFETIME INDEFINITE_LIFETIME INITIATE_AND_ACCEPT INITIATE_ONLY'
  
  orgIetfJgssGSSException='BAD_BINDINGS BAD_MECH BAD_MIC BAD_NAME BAD_NAMETYPE BAD_QOP BAD_STATUS
    CONTEXT_EXPIRED CREDENTIALS_EXPIRED DEFECTIVE_CREDENTIAL DEFECTIVE_TOKEN DUPLICATE_ELEMENT
    DUPLICATE_TOKEN FAILURE GAP_TOKEN NAME_NOT_MN NO_CONTEXT NO_CRED OLD_TOKEN UNAUTHORIZED UNAVAILABLE UNSEQ_TOKEN'
  
  orgW3cDomDOMError='SEVERITY_ERROR SEVERITY_FATAL_ERROR SEVERITY_WARNING'
  
  orgW3cDomDOMException='DOMSTRING_SIZE_ERR HIERARCHY_REQUEST_ERR INDEX_SIZE_ERR INUSE_ATTRIBUTE_ERR
    INVALID_ACCESS_ERR INVALID_CHARACTER_ERR INVALID_MODIFICATION_ERR INVALID_STATE_ERR NAMESPACE_ERR
    NO_DATA_ALLOWED_ERR NO_MODIFICATION_ALLOWED_ERR NOT_FOUND_ERR NOT_SUPPORTED_ERR SYNTAX_ERR
    TYPE_MISMATCH_ERR VALIDATION_ERR WRONG_DOCUMENT_ERR'
  
  orgW3cDomNode='ATTRIBUTE_NODE CDATA_SECTION_NODE COMMENT_NODE DOCUMENT_FRAGMENT_NODE DOCUMENT_NODE
    DOCUMENT_POSITION_CONTAINED_BY DOCUMENT_POSITION_CONTAINS DOCUMENT_POSITION_DISCONNECTED
    DOCUMENT_POSITION_FOLLOWING DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC DOCUMENT_POSITION_PRECEDING
    DOCUMENT_TYPE_NODE ELEMENT_NODE ENTITY_NODE ENTITY_REFERENCE_NODE NOTATION_NODE
    PROCESSING_INSTRUCTION_NODE TEXT_NODE'
  
  orgW3cDomTypeInfo='DERIVATION_EXTENSION DERIVATION_LIST DERIVATION_RESTRICTION DERIVATION_UNION'
  
  orgW3cDomUserDataHandler='NODE_ADOPTED NODE_CLONED NODE_DELETED NODE_IMPORTED NODE_RENAMED'
  
  orgW3cDomBootstrapDOMImplementationRegistry='PROPERTY'
  
  orgW3cDomCssCSSPrimitiveValue='CSS_ATTR CSS_CM CSS_COUNTER CSS_DEG CSS_DIMENSION CSS_EMS CSS_EXS
    CSS_GRAD CSS_HZ CSS_IDENT CSS_IN CSS_KHZ CSS_MM CSS_MS CSS_NUMBER CSS_PC CSS_PERCENTAGE CSS_PT
    CSS_PX CSS_RAD CSS_RECT CSS_RGBCOLOR CSS_S CSS_STRING CSS_UNKNOWN CSS_URI'
  
  orgW3cDomCssCSSRule='CHARSET_RULE FONT_FACE_RULE IMPORT_RULE MEDIA_RULE PAGE_RULE STYLE_RULE UNKNOWN_RULE'
  
  orgW3cDomCssCSSValue='CSS_CUSTOM CSS_INHERIT CSS_PRIMITIVE_VALUE CSS_VALUE_LIST'
  
  orgW3cDomEventsEvent='AT_TARGET BUBBLING_PHASE CAPTURING_PHASE'
  
  orgW3cDomEventsEventException='UNSPECIFIED_EVENT_TYPE_ERR'
  
  orgW3cDomEventsMutationEvent='ADDITION MODIFICATION REMOVAL'
  
  orgW3cDomLsDOMImplementationLS='MODE_ASYNCHRONOUS MODE_SYNCHRONOUS'
  
  orgW3cDomLsLSException='PARSE_ERR SERIALIZE_ERR'
  
  orgW3cDomLsLSParser='ACTION_APPEND_AS_CHILDREN ACTION_INSERT_AFTER ACTION_INSERT_BEFORE
    ACTION_REPLACE ACTION_REPLACE_CHILDREN'
  
  orgW3cDomLsLSParserFilter='FILTER_ACCEPT FILTER_INTERRUPT FILTER_REJECT FILTER_SKIP'
  
  orgW3cDomRangesRange='END_TO_END END_TO_START START_TO_END START_TO_START'
  
  orgW3cDomRangesRangeException='BAD_BOUNDARYPOINTS_ERR INVALID_NODE_TYPE_ERR'
  
  orgW3cDomTraversalNodeFilter='FILTER_ACCEPT FILTER_REJECT FILTER_SKIP SHOW_ALL SHOW_ATTRIBUTE
    SHOW_CDATA_SECTION SHOW_COMMENT SHOW_DOCUMENT SHOW_DOCUMENT_FRAGMENT SHOW_DOCUMENT_TYPE
    SHOW_ELEMENT SHOW_ENTITY SHOW_ENTITY_REFERENCE SHOW_NOTATION SHOW_PROCESSING_INSTRUCTION SHOW_TEXT'
  
  orgW3cDomXpathXPathException='INVALID_EXPRESSION_ERR TYPE_ERR'
  
  orgW3cDomXpathXPathNamespace='XPATH_NAMESPACE_NODE'
  
  orgW3cDomXpathXPathResult='ANY_TYPE ANY_UNORDERED_NODE_TYPE BOOLEAN_TYPE FIRST_ORDERED_NODE_TYPE
    NUMBER_TYPE ORDERED_NODE_ITERATOR_TYPE ORDERED_NODE_SNAPSHOT_TYPE STRING_TYPE
    UNORDERED_NODE_ITERATOR_TYPE UNORDERED_NODE_SNAPSHOT_TYPE'
  
  orgXmlSaxHelpersNamespaceSupport='NSDECL XMLNS'
  
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  print_static_words() {
    for i in $@
    do
      printf " %s" $i 
    done
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  printf "declare-option str-list com_sun_java_accessibility_util_eventid "
  print_static_words $comSunJavaAccessibilityUtilEventID
  printf "\n"

  printf "declare-option str-list com_sun_jdi_classtype "
  print_static_words $comSunJdiClassType
  printf "\n"

  printf "declare-option str-list com_sun_jdi_objectreference "
  print_static_words $comSunJdiObjectReference
  printf "\n"

  printf "declare-option str-list com_sun_jdi_threadreference "
  print_static_words $comSunJdiThreadReference
  printf "\n"

  printf "declare-option str-list com_sun_jdi_virtualmachine "
  print_static_words $comSunJdiVirtualMachine
  printf "\n"

  printf "declare-option str-list com_sun_jdi_request_eventrequest "
  print_static_words $comSunJdiRequestEventRequest
  printf "\n"

  printf "declare-option str-list com_sun_jdi_request_steprequest "
  print_static_words $comSunJdiRequestStepRequest
  printf "\n"

  printf "declare-option str-list com_sun_management_garbagecollectionnotificationinfo "
  print_static_words $comSunManagementGarbageCollectionNotificationInfo
  printf "\n"

  printf "declare-option str-list com_sun_security_auth_module_jndiloginmodule "
  print_static_words $comSunSecurityAuthModuleJndiLoginModule
  printf "\n"

  printf "declare-option str-list com_sun_tools_jconsole_jconsolecontext "
  print_static_words $comSunToolsJconsoleJConsoleContext
  printf "\n"

  printf "declare-option str-list java_awt_adjustable "
  print_static_words $javaAwtAdjustable
  printf "\n"

  printf "declare-option str-list java_awt_alphacomposite "
  print_static_words $javaAwtAlphaComposite
  printf "\n"

  printf "declare-option str-list java_awt_awtevent "
  print_static_words $javaAwtAWTEvent
  printf "\n"

  printf "declare-option str-list java_awt_basicstroke "
  print_static_words $javaAwtBasicStroke
  printf "\n"

  printf "declare-option str-list java_awt_borderlayout "
  print_static_words $javaAwtBorderLayout
  printf "\n"

  printf "declare-option str-list java_awt_component "
  print_static_words $javaAwtComponent
  printf "\n"

  printf "declare-option str-list java_awt_cursor "
  print_static_words $javaAwtCursor
  printf "\n"

  printf "declare-option str-list java_awt_displaymode "
  print_static_words $javaAwtDisplayMode
  printf "\n"

  printf "declare-option str-list java_awt_event "
  print_static_words $javaAwtEvent
  printf "\n"

  printf "declare-option str-list java_awt_filedialog "
  print_static_words $javaAwtFileDialog
  printf "\n"

  printf "declare-option str-list java_awt_flowlayout "
  print_static_words $javaAwtFlowLayout
  printf "\n"

  printf "declare-option str-list java_awt_font "
  print_static_words $javaAwtFont
  printf "\n"

  printf "declare-option str-list java_awt_frame "
  print_static_words $javaAwtFrame
  printf "\n"

  printf "declare-option str-list java_awt_graphicsconfigtemplate "
  print_static_words $javaAwtGraphicsConfigTemplate
  printf "\n"

  printf "declare-option str-list java_awt_graphicsdevice "
  print_static_words $javaAwtGraphicsDevice
  printf "\n"

  printf "declare-option str-list java_awt_gridbagconstraints "
  print_static_words $javaAwtGridBagConstraints
  printf "\n"

  printf "declare-option str-list java_awt_gridbaglayout "
  print_static_words $javaAwtGridBagLayout
  printf "\n"

  printf "declare-option str-list java_awt_image "
  print_static_words $javaAwtImage
  printf "\n"

  printf "declare-option str-list java_awt_keyboardfocusmanager "
  print_static_words $javaAwtKeyboardFocusManager
  printf "\n"

  printf "declare-option str-list java_awt_label "
  print_static_words $javaAwtLabel
  printf "\n"

  printf "declare-option str-list java_awt_mediatracker "
  print_static_words $javaAwtMediaTracker
  printf "\n"

  printf "declare-option str-list java_awt_scrollbar "
  print_static_words $javaAwtScrollbar
  printf "\n"

  printf "declare-option str-list java_awt_scrollpane "
  print_static_words $javaAwtScrollPane
  printf "\n"

  printf "declare-option str-list java_awt_systemcolor "
  print_static_words $javaAwtSystemColor
  printf "\n"

  printf "declare-option str-list java_awt_textarea "
  print_static_words $javaAwtTextArea
  printf "\n"

  printf "declare-option str-list java_awt_transparency "
  print_static_words $javaAwtTransparency
  printf "\n"

  printf "declare-option str-list java_awt_color_colorspace "
  print_static_words $javaAwtColorColorSpace
  printf "\n"

  printf "declare-option str-list java_awt_color_icc_profile "
  print_static_words $javaAwtColorICC_Profile
  printf "\n"

  printf "declare-option str-list java_awt_color_icc_profilergb "
  print_static_words $javaAwtColorICC_ProfileRGB
  printf "\n"

  printf "declare-option str-list java_awt_datatransfer_dataflavor "
  print_static_words $javaAwtDatatransferDataFlavor
  printf "\n"

  printf "declare-option str-list java_awt_dnd_dndconstants "
  print_static_words $javaAwtDndDnDConstants
  printf "\n"

  printf "declare-option str-list java_awt_dnd_dragsourcecontext "
  print_static_words $javaAwtDndDragSourceContext
  printf "\n"

  printf "declare-option str-list java_awt_event_actionevent "
  print_static_words $javaAwtEventActionEvent
  printf "\n"

  printf "declare-option str-list java_awt_event_adjustmentevent "
  print_static_words $javaAwtEventAdjustmentEvent
  printf "\n"

  printf "declare-option str-list java_awt_event_componentevent "
  print_static_words $javaAwtEventComponentEvent
  printf "\n"

  printf "declare-option str-list java_awt_event_containerevent "
  print_static_words $javaAwtEventContainerEvent
  printf "\n"

  printf "declare-option str-list java_awt_event_focusevent "
  print_static_words $javaAwtEventFocusEvent
  printf "\n"

  printf "declare-option str-list java_awt_event_hierarchyevent "
  print_static_words $javaAwtEventHierarchyEvent
  printf "\n"

  printf "declare-option str-list java_awt_event_inputevent "
  print_static_words $javaAwtEventInputEvent
  printf "\n"

  printf "declare-option str-list java_awt_event_inputmethodevent "
  print_static_words $javaAwtEventInputMethodEvent
  printf "\n"

  printf "declare-option str-list java_awt_event_invocationevent "
  print_static_words $javaAwtEventInvocationEvent
  printf "\n"

  printf "declare-option str-list java_awt_event_itemevent "
  print_static_words $javaAwtEventItemEvent
  printf "\n"

  printf "declare-option str-list java_awt_event_keyevent "
  print_static_words $javaAwtEventKeyEvent
  printf "\n"

  printf "declare-option str-list java_awt_event_mouseevent "
  print_static_words $javaAwtEventMouseEvent
  printf "\n"

  printf "declare-option str-list java_awt_event_mousewheelevent "
  print_static_words $javaAwtEventMouseWheelEvent
  printf "\n"

  printf "declare-option str-list java_awt_event_paintevent "
  print_static_words $javaAwtEventPaintEvent
  printf "\n"

  printf "declare-option str-list java_awt_event_textevent "
  print_static_words $javaAwtEventTextEvent
  printf "\n"

  printf "declare-option str-list java_awt_event_windowevent "
  print_static_words $javaAwtEventWindowEvent
  printf "\n"

  printf "declare-option str-list java_awt_font_glyphjustificationinfo "
  print_static_words $javaAwtFontGlyphJustificationInfo
  printf "\n"

  printf "declare-option str-list java_awt_font_glyphmetrics "
  print_static_words $javaAwtFontGlyphMetrics
  printf "\n"

  printf "declare-option str-list java_awt_font_glyphvector "
  print_static_words $javaAwtFontGlyphVector
  printf "\n"

  printf "declare-option str-list java_awt_font_graphicattribute "
  print_static_words $javaAwtFontGraphicAttribute
  printf "\n"

  printf "declare-option str-list java_awt_font_numericshaper "
  print_static_words $javaAwtFontNumericShaper
  printf "\n"

  printf "declare-option str-list java_awt_font_opentype "
  print_static_words $javaAwtFontOpenType
  printf "\n"

  printf "declare-option str-list java_awt_font_shapegraphicattribute "
  print_static_words $javaAwtFontShapeGraphicAttribute
  printf "\n"

  printf "declare-option str-list java_awt_geom_affinetransform "
  print_static_words $javaAwtGeomAffineTransform
  printf "\n"

  printf "declare-option str-list java_awt_geom_arc2d "
  print_static_words $javaAwtGeomArc2D
  printf "\n"

  printf "declare-option str-list java_awt_geom_path2d "
  print_static_words $javaAwtGeomPath2D
  printf "\n"

  printf "declare-option str-list java_awt_geom_pathiterator "
  print_static_words $javaAwtGeomPathIterator
  printf "\n"

  printf "declare-option str-list java_awt_geom_rectangle2d "
  print_static_words $javaAwtGeomRectangle2D
  printf "\n"

  printf "declare-option str-list java_awt_im_inputmethodhighlight "
  print_static_words $javaAwtImInputMethodHighlight
  printf "\n"

  printf "declare-option str-list java_awt_image_affinetransformop "
  print_static_words $javaAwtImageAffineTransformOp
  printf "\n"

  printf "declare-option str-list java_awt_image_bufferedimage "
  print_static_words $javaAwtImageBufferedImage
  printf "\n"

  printf "declare-option str-list java_awt_image_convolveop "
  print_static_words $javaAwtImageConvolveOp
  printf "\n"

  printf "declare-option str-list java_awt_image_databuffer "
  print_static_words $javaAwtImageDataBuffer
  printf "\n"

  printf "declare-option str-list java_awt_image_imageconsumer "
  print_static_words $javaAwtImageImageConsumer
  printf "\n"

  printf "declare-option str-list java_awt_image_imageobserver "
  print_static_words $javaAwtImageImageObserver
  printf "\n"

  printf "declare-option str-list java_awt_image_volatileimage "
  print_static_words $javaAwtImageVolatileImage
  printf "\n"

  printf "declare-option str-list java_awt_image_renderable_renderableimage "
  print_static_words $javaAwtImageRenderableRenderableImage
  printf "\n"

  printf "declare-option str-list java_awt_print_pageable "
  print_static_words $javaAwtPrintPageable
  printf "\n"

  printf "declare-option str-list java_awt_print_pageformat "
  print_static_words $javaAwtPrintPageFormat
  printf "\n"

  printf "declare-option str-list java_awt_print_printable "
  print_static_words $javaAwtPrintPrintable
  printf "\n"

  printf "declare-option str-list java_beans_beaninfo "
  print_static_words $javaBeansBeanInfo
  printf "\n"

  printf "declare-option str-list java_beans_designmode "
  print_static_words $javaBeansDesignMode
  printf "\n"

  printf "declare-option str-list java_beans_introspector "
  print_static_words $javaBeansIntrospector
  printf "\n"

  printf "declare-option str-list java_io_objectstreamconstants "
  print_static_words $javaIoObjectStreamConstants
  printf "\n"

  printf "declare-option str-list java_io_pipedinputstream "
  print_static_words $javaIoPipedInputStream
  printf "\n"

  printf "declare-option str-list java_io_streamtokenizer "
  print_static_words $javaIoStreamTokenizer
  printf "\n"

  printf "declare-option str-list java_lang_byte "
  print_static_words $javaLangByte
  printf "\n"

  printf "declare-option str-list java_lang_character "
  print_static_words $javaLangCharacter
  printf "\n"

  printf "declare-option str-list java_lang_double "
  print_static_words $javaLangDouble
  printf "\n"

  printf "declare-option str-list java_lang_float "
  print_static_words $javaLangFloat
  printf "\n"

  printf "declare-option str-list java_lang_integer "
  print_static_words $javaLangInteger
  printf "\n"

  printf "declare-option str-list java_lang_long "
  print_static_words $javaLangLong
  printf "\n"

  printf "declare-option str-list java_lang_math "
  print_static_words $javaLangMath
  printf "\n"

  printf "declare-option str-list java_lang_short "
  print_static_words $javaLangShort
  printf "\n"

  printf "declare-option str-list java_lang_strictmath "
  print_static_words $javaLangStrictMath
  printf "\n"

  printf "declare-option str-list java_lang_thread "
  print_static_words $javaLangThread
  printf "\n"

  printf "declare-option str-list java_lang_constant_constantdescs "
  print_static_words $javaLangConstantConstantDescs
  printf "\n"

  printf "declare-option str-list java_lang_invoke_lambdametafactory "
  print_static_words $javaLangInvokeLambdaMetafactory
  printf "\n"

  printf "declare-option str-list java_lang_invoke_methodhandleinfo "
  print_static_words $javaLangInvokeMethodHandleInfo
  printf "\n"

  printf "declare-option str-list java_lang_invoke_methodhandles_lookup "
  print_static_words $javaLangInvokeMethodhandlesLookup
  printf "\n"

  printf "declare-option str-list java_lang_management_managementfactory "
  print_static_words $javaLangManagementManagementFactory
  printf "\n"

  printf "declare-option str-list java_lang_management_memorynotificationinfo "
  print_static_words $javaLangManagementMemoryNotificationInfo
  printf "\n"

  printf "declare-option str-list java_lang_reflect_member "
  print_static_words $javaLangReflectMember
  printf "\n"

  printf "declare-option str-list java_lang_reflect_modifier "
  print_static_words $javaLangReflectModifier
  printf "\n"

  printf "declare-option str-list java_math_bigdecimal "
  print_static_words $javaMathBigDecimal
  printf "\n"

  printf "declare-option str-list java_net_httpurlconnection "
  print_static_words $javaNetHttpURLConnection
  printf "\n"

  printf "declare-option str-list java_net_idn "
  print_static_words $javaNetIDN
  printf "\n"

  printf "declare-option str-list java_net_socketoptions "
  print_static_words $javaNetSocketOptions
  printf "\n"

  printf "declare-option str-list java_net_http_websocket "
  print_static_words $javaNetHttpWebSocket
  printf "\n"

  printf "declare-option str-list java_nio_channels_selectionkey "
  print_static_words $javaNioChannelsSelectionKey
  printf "\n"

  printf "declare-option str-list java_rmi_activation_activationsystem "
  print_static_words $javaRmiActivationActivationSystem
  printf "\n"

  printf "declare-option str-list java_rmi_registry_registry "
  print_static_words $javaRmiRegistryRegistry
  printf "\n"

  printf "declare-option str-list java_rmi_server_loaderhandler "
  print_static_words $javaRmiServerLoaderHandler
  printf "\n"

  printf "declare-option str-list java_rmi_server_logstream "
  print_static_words $javaRmiServerLogStream
  printf "\n"

  printf "declare-option str-list java_rmi_server_objid "
  print_static_words $javaRmiServerObjID
  printf "\n"

  printf "declare-option str-list java_rmi_server_remoteref "
  print_static_words $javaRmiServerRemoteRef
  printf "\n"

  printf "declare-option str-list java_rmi_server_serverref "
  print_static_words $javaRmiServerServerRef
  printf "\n"

  printf "declare-option str-list java_security_key "
  print_static_words $javaSecurityKey
  printf "\n"

  printf "declare-option str-list java_security_privatekey "
  print_static_words $javaSecurityPrivateKey
  printf "\n"

  printf "declare-option str-list java_security_publickey "
  print_static_words $javaSecurityPublicKey
  printf "\n"

  printf "declare-option str-list java_security_signature "
  print_static_words $javaSecuritySignature
  printf "\n"

  printf "declare-option str-list java_security_interfaces_dsaprivatekey "
  print_static_words $javaSecurityInterfacesDSAPrivateKey
  printf "\n"

  printf "declare-option str-list java_security_interfaces_dsapublickey "
  print_static_words $javaSecurityInterfacesDSAPublicKey
  printf "\n"

  printf "declare-option str-list java_security_interfaces_ecprivatekey "
  print_static_words $javaSecurityInterfacesECPrivateKey
  printf "\n"

  printf "declare-option str-list java_security_interfaces_ecpublickey "
  print_static_words $javaSecurityInterfacesECPublicKey
  printf "\n"

  printf "declare-option str-list java_security_interfaces_rsamultiprimeprivatecrtkey "
  print_static_words $javaSecurityInterfacesRSAMultiPrimePrivateCrtKey
  printf "\n"

  printf "declare-option str-list java_security_interfaces_rsaprivatecrtkey "
  print_static_words $javaSecurityInterfacesRSAPrivateCrtKey
  printf "\n"

  printf "declare-option str-list java_security_interfaces_rsaprivatekey "
  print_static_words $javaSecurityInterfacesRSAPrivateKey
  printf "\n"

  printf "declare-option str-list java_security_interfaces_rsapublickey "
  print_static_words $javaSecurityInterfacesRSAPublicKey
  printf "\n"

  printf "declare-option str-list java_security_spec_pssparameterspec "
  print_static_words $javaSecuritySpecPSSParameterSpec
  printf "\n"

  printf "declare-option str-list java_sql_connection "
  print_static_words $javaSqlConnection
  printf "\n"

  printf "declare-option str-list java_sql_databasemetadata "
  print_static_words $javaSqlDatabaseMetaData
  printf "\n"

  printf "declare-option str-list java_sql_parametermetadata "
  print_static_words $javaSqlParameterMetaData
  printf "\n"

  printf "declare-option str-list java_sql_resultset "
  print_static_words $javaSqlResultSet
  printf "\n"

  printf "declare-option str-list java_sql_resultsetmetadata "
  print_static_words $javaSqlResultSetMetaData
  printf "\n"

  printf "declare-option str-list java_sql_statement "
  print_static_words $javaSqlStatement
  printf "\n"

  printf "declare-option str-list java_sql_types "
  print_static_words $javaSqlTypes
  printf "\n"

  printf "declare-option str-list java_text_bidi "
  print_static_words $javaTextBidi
  printf "\n"

  printf "declare-option str-list java_text_breakiterator "
  print_static_words $javaTextBreakIterator
  printf "\n"

  printf "declare-option str-list java_text_characteriterator "
  print_static_words $javaTextCharacterIterator
  printf "\n"

  printf "declare-option str-list java_text_collationelementiterator "
  print_static_words $javaTextCollationElementIterator
  printf "\n"

  printf "declare-option str-list java_text_collator "
  print_static_words $javaTextCollator
  printf "\n"

  printf "declare-option str-list java_text_dateformat "
  print_static_words $javaTextDateFormat
  printf "\n"

  printf "declare-option str-list java_text_numberformat "
  print_static_words $javaTextNumberFormat
  printf "\n"

  printf "declare-option str-list java_time_year "
  print_static_words $javaTimeYear
  printf "\n"

  printf "declare-option str-list java_util_calendar "
  print_static_words $javaUtilCalendar
  printf "\n"

  printf "declare-option str-list java_util_formattableflags "
  print_static_words $javaUtilFormattableFlags
  printf "\n"

  printf "declare-option str-list java_util_gregoriancalendar "
  print_static_words $javaUtilGregorianCalendar
  printf "\n"

  printf "declare-option str-list java_util_locale "
  print_static_words $javaUtilLocale
  printf "\n"

  printf "declare-option str-list java_util_locale_languagerange "
  print_static_words $javaUtilLocaleLanguageRange
  printf "\n"

  printf "declare-option str-list java_util_resourcebundle_control "
  print_static_words $javaUtilResourcebundleControl
  printf "\n"

  printf "declare-option str-list java_util_simpletimezone "
  print_static_words $javaUtilSimpleTimeZone
  printf "\n"

  printf "declare-option str-list java_util_spliterator "
  print_static_words $javaUtilSpliterator
  printf "\n"

  printf "declare-option str-list java_util_timezone "
  print_static_words $javaUtilTimeZone
  printf "\n"

  printf "declare-option str-list java_util_jar_jarentry "
  print_static_words $javaUtilJarJarEntry
  printf "\n"

  printf "declare-option str-list java_util_jar_jarfile "
  print_static_words $javaUtilJarJarFile
  printf "\n"

  printf "declare-option str-list java_util_jar_jarinputstream "
  print_static_words $javaUtilJarJarInputStream
  printf "\n"

  printf "declare-option str-list java_util_jar_jaroutputstream "
  print_static_words $javaUtilJarJarOutputStream
  printf "\n"

  printf "declare-option str-list java_util_logging_errormanager "
  print_static_words $javaUtilLoggingErrorManager
  printf "\n"

  printf "declare-option str-list java_util_logging_logger "
  print_static_words $javaUtilLoggingLogger
  printf "\n"

  printf "declare-option str-list java_util_logging_logmanager "
  print_static_words $javaUtilLoggingLogManager
  printf "\n"

  printf "declare-option str-list java_util_prefs_preferences "
  print_static_words $javaUtilPrefsPreferences
  printf "\n"

  printf "declare-option str-list java_util_regex_pattern "
  print_static_words $javaUtilRegexPattern
  printf "\n"

  printf "declare-option str-list java_util_zip_deflater "
  print_static_words $javaUtilZipDeflater
  printf "\n"

  printf "declare-option str-list java_util_zip_gzipinputstream "
  print_static_words $javaUtilZipGZIPInputStream
  printf "\n"

  printf "declare-option str-list java_util_zip_zipentry "
  print_static_words $javaUtilZipZipEntry
  printf "\n"

  printf "declare-option str-list java_util_zip_zipfile "
  print_static_words $javaUtilZipZipFile
  printf "\n"

  printf "declare-option str-list java_util_zip_zipinputstream "
  print_static_words $javaUtilZipZipInputStream
  printf "\n"

  printf "declare-option str-list java_util_zip_zipoutputstream "
  print_static_words $javaUtilZipZipOutputStream
  printf "\n"

  printf "declare-option str-list javax_accessibility_accessiblecontext "
  print_static_words $javaxAccessibilityAccessibleContext
  printf "\n"

  printf "declare-option str-list javax_accessibility_accessibleextendedtext "
  print_static_words $javaxAccessibilityAccessibleExtendedText
  printf "\n"

  printf "declare-option str-list javax_accessibility_accessiblerelation "
  print_static_words $javaxAccessibilityAccessibleRelation
  printf "\n"

  printf "declare-option str-list javax_accessibility_accessibletablemodelchange "
  print_static_words $javaxAccessibilityAccessibleTableModelChange
  printf "\n"

  printf "declare-option str-list javax_accessibility_accessibletext "
  print_static_words $javaxAccessibilityAccessibleText
  printf "\n"

  printf "declare-option str-list javax_crypto_cipher "
  print_static_words $javaxCryptoCipher
  printf "\n"

  printf "declare-option str-list javax_crypto_secretkey "
  print_static_words $javaxCryptoSecretKey
  printf "\n"

  printf "declare-option str-list javax_crypto_interfaces_dhprivatekey "
  print_static_words $javaxCryptoInterfacesDHPrivateKey
  printf "\n"

  printf "declare-option str-list javax_crypto_interfaces_dhpublickey "
  print_static_words $javaxCryptoInterfacesDHPublicKey
  printf "\n"

  printf "declare-option str-list javax_crypto_interfaces_pbekey "
  print_static_words $javaxCryptoInterfacesPBEKey
  printf "\n"

  printf "declare-option str-list javax_crypto_spec_desedekeyspec "
  print_static_words $javaxCryptoSpecDESedeKeySpec
  printf "\n"

  printf "declare-option str-list javax_crypto_spec_deskeyspec "
  print_static_words $javaxCryptoSpecDESKeySpec
  printf "\n"

  printf "declare-option str-list javax_imageio_imagewriteparam "
  print_static_words $javaxImageioImageWriteParam
  printf "\n"

  printf "declare-option str-list javax_imageio_metadata_iiometadataformat "
  print_static_words $javaxImageioMetadataIIOMetadataFormat
  printf "\n"

  printf "declare-option str-list javax_imageio_metadata_iiometadataformatimpl "
  print_static_words $javaxImageioMetadataIIOMetadataFormatImpl
  printf "\n"

  printf "declare-option str-list javax_imageio_plugins_tiff_baselinetifftagset "
  print_static_words $javaxImageioPluginsTiffBaselineTIFFTagSet
  printf "\n"

  printf "declare-option str-list javax_imageio_plugins_tiff_exifgpstagset "
  print_static_words $javaxImageioPluginsTiffExifGPSTagSet
  printf "\n"

  printf "declare-option str-list javax_imageio_plugins_tiff_exifinteroperabilitytagset "
  print_static_words $javaxImageioPluginsTiffExifInteroperabilityTagSet
  printf "\n"

  printf "declare-option str-list javax_imageio_plugins_tiff_exifparenttifftagset "
  print_static_words $javaxImageioPluginsTiffExifParentTIFFTagSet
  printf "\n"

  printf "declare-option str-list javax_imageio_plugins_tiff_exiftifftagset "
  print_static_words $javaxImageioPluginsTiffExifTIFFTagSet
  printf "\n"

  printf "declare-option str-list javax_imageio_plugins_tiff_faxtifftagset "
  print_static_words $javaxImageioPluginsTiffFaxTIFFTagSet
  printf "\n"

  printf "declare-option str-list javax_imageio_plugins_tiff_geotifftagset "
  print_static_words $javaxImageioPluginsTiffGeoTIFFTagSet
  printf "\n"

  printf "declare-option str-list javax_imageio_plugins_tiff_tifftag "
  print_static_words $javaxImageioPluginsTiffTIFFTag
  printf "\n"

  printf "declare-option str-list javax_management_attributechangenotification "
  print_static_words $javaxManagementAttributeChangeNotification
  printf "\n"

  printf "declare-option str-list javax_management_jmx "
  print_static_words $javaxManagementJMX
  printf "\n"

  printf "declare-option str-list javax_management_mbeanoperationinfo "
  print_static_words $javaxManagementMBeanOperationInfo
  printf "\n"

  printf "declare-option str-list javax_management_mbeanservernotification "
  print_static_words $javaxManagementMBeanServerNotification
  printf "\n"

  printf "declare-option str-list javax_management_query "
  print_static_words $javaxManagementQuery
  printf "\n"

  printf "declare-option str-list javax_management_monitor_monitor "
  print_static_words $javaxManagementMonitorMonitor
  printf "\n"

  printf "declare-option str-list javax_management_monitor_monitornotification "
  print_static_words $javaxManagementMonitorMonitorNotification
  printf "\n"

  printf "declare-option str-list javax_management_relation_relationnotification "
  print_static_words $javaxManagementRelationRelationNotification
  printf "\n"

  printf "declare-option str-list javax_management_relation_roleinfo "
  print_static_words $javaxManagementRelationRoleInfo
  printf "\n"

  printf "declare-option str-list javax_management_relation_rolestatus "
  print_static_words $javaxManagementRelationRoleStatus
  printf "\n"

  printf "declare-option str-list javax_management_remote_jmxconnectionnotification "
  print_static_words $javaxManagementRemoteJMXConnectionNotification
  printf "\n"

  printf "declare-option str-list javax_management_remote_jmxconnector "
  print_static_words $javaxManagementRemoteJMXConnector
  printf "\n"

  printf "declare-option str-list javax_management_remote_jmxconnectorfactory "
  print_static_words $javaxManagementRemoteJMXConnectorFactory
  printf "\n"

  printf "declare-option str-list javax_management_remote_jmxconnectorserver "
  print_static_words $javaxManagementRemoteJMXConnectorServer
  printf "\n"

  printf "declare-option str-list javax_management_remote_jmxconnectorserverfactory "
  print_static_words $javaxManagementRemoteJMXConnectorServerFactory
  printf "\n"

  printf "declare-option str-list javax_management_remote_rmi_rmiconnectorserver "
  print_static_words $javaxManagementRemoteRmiRMIConnectorServer
  printf "\n"

  printf "declare-option str-list javax_management_timer_timer "
  print_static_words $javaxManagementTimerTimer
  printf "\n"

  printf "declare-option str-list javax_naming_context "
  print_static_words $javaxNamingContext
  printf "\n"

  printf "declare-option str-list javax_naming_name "
  print_static_words $javaxNamingName
  printf "\n"

  printf "declare-option str-list javax_naming_directory_attribute "
  print_static_words $javaxNamingDirectoryAttribute
  printf "\n"

  printf "declare-option str-list javax_naming_directory_dircontext "
  print_static_words $javaxNamingDirectoryDirContext
  printf "\n"

  printf "declare-option str-list javax_naming_directory_searchcontrols "
  print_static_words $javaxNamingDirectorySearchControls
  printf "\n"

  printf "declare-option str-list javax_naming_event_eventcontext "
  print_static_words $javaxNamingEventEventContext
  printf "\n"

  printf "declare-option str-list javax_naming_event_namingevent "
  print_static_words $javaxNamingEventNamingEvent
  printf "\n"

  printf "declare-option str-list javax_naming_ldap_control "
  print_static_words $javaxNamingLdapControl
  printf "\n"

  printf "declare-option str-list javax_naming_ldap_ldapcontext "
  print_static_words $javaxNamingLdapLdapContext
  printf "\n"

  printf "declare-option str-list javax_naming_ldap_managereferralcontrol "
  print_static_words $javaxNamingLdapManageReferralControl
  printf "\n"

  printf "declare-option str-list javax_naming_ldap_pagedresultscontrol "
  print_static_words $javaxNamingLdapPagedResultsControl
  printf "\n"

  printf "declare-option str-list javax_naming_ldap_pagedresultsresponsecontrol "
  print_static_words $javaxNamingLdapPagedResultsResponseControl
  printf "\n"

  printf "declare-option str-list javax_naming_ldap_sortcontrol "
  print_static_words $javaxNamingLdapSortControl
  printf "\n"

  printf "declare-option str-list javax_naming_ldap_sortresponsecontrol "
  print_static_words $javaxNamingLdapSortResponseControl
  printf "\n"

  printf "declare-option str-list javax_naming_ldap_starttlsrequest "
  print_static_words $javaxNamingLdapStartTlsRequest
  printf "\n"

  printf "declare-option str-list javax_naming_ldap_starttlsresponse "
  print_static_words $javaxNamingLdapStartTlsResponse
  printf "\n"

  printf "declare-option str-list javax_naming_spi_namingmanager "
  print_static_words $javaxNamingSpiNamingManager
  printf "\n"

  printf "declare-option str-list javax_net_ssl_standardconstants "
  print_static_words $javaxNetSslStandardConstants
  printf "\n"

  printf "declare-option str-list javax_print_serviceuifactory "
  print_static_words $javaxPrintServiceUIFactory
  printf "\n"

  printf "declare-option str-list javax_print_uriexception "
  print_static_words $javaxPrintURIException
  printf "\n"

  printf "declare-option str-list javax_print_attribute_resolutionsyntax "
  print_static_words $javaxPrintAttributeResolutionSyntax
  printf "\n"

  printf "declare-option str-list javax_print_attribute_size2dsyntax "
  print_static_words $javaxPrintAttributeSize2DSyntax
  printf "\n"

  printf "declare-option str-list javax_print_attribute_standard_mediaprintablearea "
  print_static_words $javaxPrintAttributeStandardMediaPrintableArea
  printf "\n"

  printf "declare-option str-list javax_print_event_printjobevent "
  print_static_words $javaxPrintEventPrintJobEvent
  printf "\n"

  printf "declare-option str-list javax_script_scriptcontext "
  print_static_words $javaxScriptScriptContext
  printf "\n"

  printf "declare-option str-list javax_script_scriptengine "
  print_static_words $javaxScriptScriptEngine
  printf "\n"

  printf "declare-option str-list javax_security_auth_callback_confirmationcallback "
  print_static_words $javaxSecurityAuthCallbackConfirmationCallback
  printf "\n"

  printf "declare-option str-list javax_security_auth_callback_textoutputcallback "
  print_static_words $javaxSecurityAuthCallbackTextOutputCallback
  printf "\n"

  printf "declare-option str-list javax_security_auth_kerberos_kerberosprincipal "
  print_static_words $javaxSecurityAuthKerberosKerberosPrincipal
  printf "\n"

  printf "declare-option str-list javax_security_auth_x500_x500principal "
  print_static_words $javaxSecurityAuthX500X500Principal
  printf "\n"

  printf "declare-option str-list javax_security_sasl_sasl "
  print_static_words $javaxSecuritySaslSasl
  printf "\n"

  printf "declare-option str-list javax_sound_midi_metamessage "
  print_static_words $javaxSoundMidiMetaMessage
  printf "\n"

  printf "declare-option str-list javax_sound_midi_midifileformat "
  print_static_words $javaxSoundMidiMidiFileFormat
  printf "\n"

  printf "declare-option str-list javax_sound_midi_sequence "
  print_static_words $javaxSoundMidiSequence
  printf "\n"

  printf "declare-option str-list javax_sound_midi_sequencer "
  print_static_words $javaxSoundMidiSequencer
  printf "\n"

  printf "declare-option str-list javax_sound_midi_shortmessage "
  print_static_words $javaxSoundMidiShortMessage
  printf "\n"

  printf "declare-option str-list javax_sound_midi_sysexmessage "
  print_static_words $javaxSoundMidiSysexMessage
  printf "\n"

  printf "declare-option str-list javax_sound_sampled_audiosystem "
  print_static_words $javaxSoundSampledAudioSystem
  printf "\n"

  printf "declare-option str-list javax_sound_sampled_clip "
  print_static_words $javaxSoundSampledClip
  printf "\n"

  printf "declare-option str-list javax_sql_rowset_baserowset "
  print_static_words $javaxSqlRowsetBaseRowSet
  printf "\n"

  printf "declare-option str-list javax_sql_rowset_cachedrowset "
  print_static_words $javaxSqlRowsetCachedRowSet
  printf "\n"

  printf "declare-option str-list javax_sql_rowset_joinrowset "
  print_static_words $javaxSqlRowsetJoinRowSet
  printf "\n"

  printf "declare-option str-list javax_sql_rowset_webrowset "
  print_static_words $javaxSqlRowsetWebRowSet
  printf "\n"

  printf "declare-option str-list javax_sql_rowset_spi_syncfactory "
  print_static_words $javaxSqlRowsetSpiSyncFactory
  printf "\n"

  printf "declare-option str-list javax_sql_rowset_spi_syncprovider "
  print_static_words $javaxSqlRowsetSpiSyncProvider
  printf "\n"

  printf "declare-option str-list javax_sql_rowset_spi_syncresolver "
  print_static_words $javaxSqlRowsetSpiSyncResolver
  printf "\n"

  printf "declare-option str-list javax_swing_abstractbutton "
  print_static_words $javaxSwingAbstractButton
  printf "\n"

  printf "declare-option str-list javax_swing_action "
  print_static_words $javaxSwingAction
  printf "\n"

  printf "declare-option str-list javax_swing_boxlayout "
  print_static_words $javaxSwingBoxLayout
  printf "\n"

  printf "declare-option str-list javax_swing_debuggraphics "
  print_static_words $javaxSwingDebugGraphics
  printf "\n"

  printf "declare-option str-list javax_swing_defaultbuttonmodel "
  print_static_words $javaxSwingDefaultButtonModel
  printf "\n"

  printf "declare-option str-list javax_swing_focusmanager "
  print_static_words $javaxSwingFocusManager
  printf "\n"

  printf "declare-option str-list javax_swing_grouplayout "
  print_static_words $javaxSwingGroupLayout
  printf "\n"

  printf "declare-option str-list javax_swing_jcheckbox "
  print_static_words $javaxSwingJCheckBox
  printf "\n"

  printf "declare-option str-list javax_swing_jcolorchooser "
  print_static_words $javaxSwingJColorChooser
  printf "\n"

  printf "declare-option str-list javax_swing_jcomponent "
  print_static_words $javaxSwingJComponent
  printf "\n"

  printf "declare-option str-list javax_swing_jdesktoppane "
  print_static_words $javaxSwingJDesktopPane
  printf "\n"

  printf "declare-option str-list javax_swing_jeditorpane "
  print_static_words $javaxSwingJEditorPane
  printf "\n"

  printf "declare-option str-list javax_swing_jfilechooser "
  print_static_words $javaxSwingJFileChooser
  printf "\n"

  printf "declare-option str-list javax_swing_jformattedtextfield "
  print_static_words $javaxSwingJFormattedTextField
  printf "\n"

  printf "declare-option str-list javax_swing_jinternalframe "
  print_static_words $javaxSwingJInternalFrame
  printf "\n"

  printf "declare-option str-list javax_swing_jlayeredpane "
  print_static_words $javaxSwingJLayeredPane
  printf "\n"

  printf "declare-option str-list javax_swing_jlist "
  print_static_words $javaxSwingJlist
  printf "\n"

  printf "declare-option str-list javax_swing_joptionpane "
  print_static_words $javaxSwingJOptionPane
  printf "\n"

  printf "declare-option str-list javax_swing_jrootpane "
  print_static_words $javaxSwingJRootPane
  printf "\n"

  printf "declare-option str-list javax_swing_jsplitpane "
  print_static_words $javaxSwingJSplitPane
  printf "\n"

  printf "declare-option str-list javax_swing_jtabbedpane "
  print_static_words $javaxSwingJTabbedPane
  printf "\n"

  printf "declare-option str-list javax_swing_jtable "
  print_static_words $javaxSwingJTable
  printf "\n"

  printf "declare-option str-list javax_swing_jtextfield "
  print_static_words $javaxSwingJTextField
  printf "\n"

  printf "declare-option str-list javax_swing_jtree "
  print_static_words $javaxSwingJTree
  printf "\n"

  printf "declare-option str-list javax_swing_jviewport "
  print_static_words $javaxSwingJViewport
  printf "\n"

  printf "declare-option str-list javax_swing_listselectionmodel "
  print_static_words $javaxSwingListSelectionModel
  printf "\n"

  printf "declare-option str-list javax_swing_scrollpaneconstants "
  print_static_words $javaxSwingScrollPaneConstants
  printf "\n"

  printf "declare-option str-list javax_swing_spring "
  print_static_words $javaxSwingSpring
  printf "\n"

  printf "declare-option str-list javax_swing_springlayout "
  print_static_words $javaxSwingSpringLayout
  printf "\n"

  printf "declare-option str-list javax_swing_swingconstants "
  print_static_words $javaxSwingSwingConstants
  printf "\n"

  printf "declare-option str-list javax_swing_transferhandler "
  print_static_words $javaxSwingTransferHandler
  printf "\n"

  printf "declare-option str-list javax_swing_windowconstants "
  print_static_words $javaxSwingWindowConstants
  printf "\n"

  printf "declare-option str-list javax_swing_border_bevelborder "
  print_static_words $javaxSwingBorderBevelBorder
  printf "\n"

  printf "declare-option str-list javax_swing_border_etchedborder "
  print_static_words $javaxSwingBorderEtchedBorder
  printf "\n"

  printf "declare-option str-list javax_swing_border_titledborder "
  print_static_words $javaxSwingBorderTitledBorder
  printf "\n"

  printf "declare-option str-list javax_swing_colorchooser_abstractcolorchooserpanel "
  print_static_words $javaxSwingColorchooserAbstractColorChooserPanel
  printf "\n"

  printf "declare-option str-list javax_swing_event_ancestorevent "
  print_static_words $javaxSwingEventAncestorEvent
  printf "\n"

  printf "declare-option str-list javax_swing_event_internalframeevent "
  print_static_words $javaxSwingEventInternalFrameEvent
  printf "\n"

  printf "declare-option str-list javax_swing_event_listdataevent "
  print_static_words $javaxSwingEventListDataEvent
  printf "\n"

  printf "declare-option str-list javax_swing_event_tablemodelevent "
  print_static_words $javaxSwingEventTableModelEvent
  printf "\n"

  printf "declare-option str-list javax_swing_plaf_basic_basiccombopopup "
  print_static_words $javaxSwingPlafBasicBasicComboPopup
  printf "\n"

  printf "declare-option str-list javax_swing_plaf_basic_basichtml "
  print_static_words $javaxSwingPlafBasicBasicHTML
  printf "\n"

  printf "declare-option str-list javax_swing_plaf_basic_basicinternalframeui_borderlistener "
  print_static_words $javaxSwingPlafBasicBasicinternalframeuiBorderListener
  printf "\n"

  printf "declare-option str-list javax_swing_plaf_basic_basiclistui "
  print_static_words $javaxSwingPlafBasicBasicListUI
  printf "\n"

  printf "declare-option str-list javax_swing_plaf_basic_basicoptionpaneui "
  print_static_words $javaxSwingPlafBasicBasicOptionPaneUI
  printf "\n"

  printf "declare-option str-list javax_swing_plaf_basic_basicscrollbarui "
  print_static_words $javaxSwingPlafBasicBasicScrollBarUI
  printf "\n"

  printf "declare-option str-list javax_swing_plaf_basic_basicsliderui "
  print_static_words $javaxSwingPlafBasicBasicSliderUI
  printf "\n"

  printf "declare-option str-list javax_swing_plaf_basic_basicsplitpanedivider "
  print_static_words $javaxSwingPlafBasicBasicSplitPaneDivider
  printf "\n"

  printf "declare-option str-list javax_swing_plaf_basic_basicsplitpaneui "
  print_static_words $javaxSwingPlafBasicBasicSplitPaneUI
  printf "\n"

  printf "declare-option str-list javax_swing_plaf_metal_metaliconfactory "
  print_static_words $javaxSwingPlafMetalMetalIconFactory
  printf "\n"

  printf "declare-option str-list javax_swing_plaf_metal_metalscrollbarui "
  print_static_words $javaxSwingPlafMetalMetalScrollBarUI
  printf "\n"

  printf "declare-option str-list javax_swing_plaf_metal_metalsliderui "
  print_static_words $javaxSwingPlafMetalMetalSliderUI
  printf "\n"

  printf "declare-option str-list javax_swing_plaf_metal_metaltooltipui "
  print_static_words $javaxSwingPlafMetalMetalToolTipUI
  printf "\n"

  printf "declare-option str-list javax_swing_plaf_nimbus_nimbusstyle "
  print_static_words $javaxSwingPlafNimbusNimbusStyle
  printf "\n"

  printf "declare-option str-list javax_swing_plaf_synth_synthconstants "
  print_static_words $javaxSwingPlafSynthSynthConstants
  printf "\n"

  printf "declare-option str-list javax_swing_table_tablecolumn "
  print_static_words $javaxSwingTableTableColumn
  printf "\n"

  printf "declare-option str-list javax_swing_text_abstractdocument "
  print_static_words $javaxSwingTextAbstractDocument
  printf "\n"

  printf "declare-option str-list javax_swing_text_abstractwriter "
  print_static_words $javaxSwingTextAbstractWriter
  printf "\n"

  printf "declare-option str-list javax_swing_text_defaultcaret "
  print_static_words $javaxSwingTextDefaultCaret
  printf "\n"

  printf "declare-option str-list javax_swing_text_defaulteditorkit "
  print_static_words $javaxSwingTextDefaultEditorKit
  printf "\n"

  printf "declare-option str-list javax_swing_text_defaultstyleddocument "
  print_static_words $javaxSwingTextDefaultStyledDocument
  printf "\n"

  printf "declare-option str-list javax_swing_text_defaultstyleddocument_elementspec "
  print_static_words $javaxSwingTextDefaultstyleddocumentElementSpec
  printf "\n"

  printf "declare-option str-list javax_swing_text_document "
  print_static_words $javaxSwingTextDocument
  printf "\n"

  printf "declare-option str-list javax_swing_text_jtextcomponent "
  print_static_words $javaxSwingTextJTextComponent
  printf "\n"

  printf "declare-option str-list javax_swing_text_plaindocument "
  print_static_words $javaxSwingTextPlainDocument
  printf "\n"

  printf "declare-option str-list javax_swing_text_styleconstants "
  print_static_words $javaxSwingTextStyleConstants
  printf "\n"

  printf "declare-option str-list javax_swing_text_stylecontext "
  print_static_words $javaxSwingTextStyleContext
  printf "\n"

  printf "declare-option str-list javax_swing_text_tabstop "
  print_static_words $javaxSwingTextTabStop
  printf "\n"

  printf "declare-option str-list javax_swing_text_view "
  print_static_words $javaxSwingTextView
  printf "\n"

  printf "declare-option str-list javax_swing_text_html_html "
  print_static_words $javaxSwingTextHtmlHTML
  printf "\n"

  printf "declare-option str-list javax_swing_text_html_htmldocument "
  print_static_words $javaxSwingTextHtmlHTMLDocument
  printf "\n"

  printf "declare-option str-list javax_swing_text_html_htmleditorkit "
  print_static_words $javaxSwingTextHtmlHTMLEditorKit
  printf "\n"

  printf "declare-option str-list javax_swing_text_html_parser_dtd "
  print_static_words $javaxSwingTextHtmlParserDTD
  printf "\n"

  printf "declare-option str-list javax_swing_text_html_parser_dtdconstants "
  print_static_words $javaxSwingTextHtmlParserDTDConstants
  printf "\n"

  printf "declare-option str-list javax_swing_tree_defaulttreeselectionmodel "
  print_static_words $javaxSwingTreeDefaultTreeSelectionModel
  printf "\n"

  printf "declare-option str-list javax_swing_tree_treeselectionmodel "
  print_static_words $javaxSwingTreeTreeSelectionModel
  printf "\n"

  printf "declare-option str-list javax_swing_undo_abstractundoableedit "
  print_static_words $javaxSwingUndoAbstractUndoableEdit
  printf "\n"

  printf "declare-option str-list javax_swing_undo_stateedit "
  print_static_words $javaxSwingUndoStateEdit
  printf "\n"

  printf "declare-option str-list javax_swing_undo_stateeditable "
  print_static_words $javaxSwingUndoStateEditable
  printf "\n"

  printf "declare-option str-list javax_tools_diagnostic "
  print_static_words $javaxToolsDiagnostic
  printf "\n"

  printf "declare-option str-list javax_transaction_xa_xaexception "
  print_static_words $javaxTransactionXaXAException
  printf "\n"

  printf "declare-option str-list javax_transaction_xa_xaresource "
  print_static_words $javaxTransactionXaXAResource
  printf "\n"

  printf "declare-option str-list javax_transaction_xa_xid "
  print_static_words $javaxTransactionXaXid
  printf "\n"

  printf "declare-option str-list javax_xml_xmlconstants "
  print_static_words $javaxXmlXMLConstants
  printf "\n"

  printf "declare-option str-list javax_xml_crypto_dsig_canonicalizationmethod "
  print_static_words $javaxXmlCryptoDsigCanonicalizationMethod
  printf "\n"

  printf "declare-option str-list javax_xml_crypto_dsig_digestmethod "
  print_static_words $javaxXmlCryptoDsigDigestMethod
  printf "\n"

  printf "declare-option str-list javax_xml_crypto_dsig_manifest "
  print_static_words $javaxXmlCryptoDsigManifest
  printf "\n"

  printf "declare-option str-list javax_xml_crypto_dsig_signaturemethod "
  print_static_words $javaxXmlCryptoDsigSignatureMethod
  printf "\n"

  printf "declare-option str-list javax_xml_crypto_dsig_signatureproperties "
  print_static_words $javaxXmlCryptoDsigSignatureProperties
  printf "\n"

  printf "declare-option str-list javax_xml_crypto_dsig_transform "
  print_static_words $javaxXmlCryptoDsigTransform
  printf "\n"

  printf "declare-option str-list javax_xml_crypto_dsig_xmlobject "
  print_static_words $javaxXmlCryptoDsigXMLObject
  printf "\n"

  printf "declare-option str-list javax_xml_crypto_dsig_xmlsignature "
  print_static_words $javaxXmlCryptoDsigXMLSignature
  printf "\n"

  printf "declare-option str-list javax_xml_crypto_dsig_keyinfo_keyvalue "
  print_static_words $javaxXmlCryptoDsigKeyinfoKeyValue
  printf "\n"

  printf "declare-option str-list javax_xml_crypto_dsig_keyinfo_pgpdata "
  print_static_words $javaxXmlCryptoDsigKeyinfoPGPData
  printf "\n"

  printf "declare-option str-list javax_xml_crypto_dsig_keyinfo_x509data "
  print_static_words $javaxXmlCryptoDsigKeyinfoX509Data
  printf "\n"

  printf "declare-option str-list javax_xml_crypto_dsig_spec_excc14nparameterspec "
  print_static_words $javaxXmlCryptoDsigSpecExcC14NParameterSpec
  printf "\n"

  printf "declare-option str-list javax_xml_datatype_datatypeconstants "
  print_static_words $javaxXmlDatatypeDatatypeConstants
  printf "\n"

  printf "declare-option str-list javax_xml_datatype_datatypefactory "
  print_static_words $javaxXmlDatatypeDatatypeFactory
  printf "\n"

  printf "declare-option str-list javax_xml_stream_xmlinputfactory "
  print_static_words $javaxXmlStreamXMLInputFactory
  printf "\n"

  printf "declare-option str-list javax_xml_stream_xmloutputfactory "
  print_static_words $javaxXmlStreamXMLOutputFactory
  printf "\n"

  printf "declare-option str-list javax_xml_stream_xmlstreamconstants "
  print_static_words $javaxXmlStreamXMLStreamConstants
  printf "\n"

  printf "declare-option str-list javax_xml_transform_outputkeys "
  print_static_words $javaxXmlTransformOutputKeys
  printf "\n"

  printf "declare-option str-list javax_xml_transform_result "
  print_static_words $javaxXmlTransformResult
  printf "\n"

  printf "declare-option str-list javax_xml_transform_dom_domresult "
  print_static_words $javaxXmlTransformDomDOMResult
  printf "\n"

  printf "declare-option str-list javax_xml_transform_dom_domsource "
  print_static_words $javaxXmlTransformDomDOMSource
  printf "\n"

  printf "declare-option str-list javax_xml_transform_sax_saxresult "
  print_static_words $javaxXmlTransformSaxSAXResult
  printf "\n"

  printf "declare-option str-list javax_xml_transform_sax_saxsource "
  print_static_words $javaxXmlTransformSaxSAXSource
  printf "\n"

  printf "declare-option str-list javax_xml_transform_sax_saxtransformerfactory "
  print_static_words $javaxXmlTransformSaxSAXTransformerFactory
  printf "\n"

  printf "declare-option str-list javax_xml_transform_stax_staxresult "
  print_static_words $javaxXmlTransformStaxStAXResult
  printf "\n"

  printf "declare-option str-list javax_xml_transform_stax_staxsource "
  print_static_words $javaxXmlTransformStaxStAXSource
  printf "\n"

  printf "declare-option str-list javax_xml_transform_stream_streamresult "
  print_static_words $javaxXmlTransformStreamStreamResult
  printf "\n"

  printf "declare-option str-list javax_xml_transform_stream_streamsource "
  print_static_words $javaxXmlTransformStreamStreamSource
  printf "\n"

  printf "declare-option str-list javax_xml_xpath_xpathconstants "
  print_static_words $javaxXmlXpathXPathConstants
  printf "\n"

  printf "declare-option str-list javax_xml_xpath_xpathfactory "
  print_static_words $javaxXmlXpathXPathFactory
  printf "\n"

  printf "declare-option str-list jdk_dynalink_securelookupsupplier "
  print_static_words $jdkDynalinkSecureLookupSupplier
  printf "\n"

  printf "declare-option str-list jdk_dynalink_linker_guardingdynamiclinkerexporter "
  print_static_words $jdkDynalinkLinkerGuardingDynamicLinkerExporter
  printf "\n"

  printf "declare-option str-list jdk_incubator_foreign_memorylayout "
  print_static_words $jdkIncubatorForeignMemoryLayout
  printf "\n"

  printf "declare-option str-list jdk_incubator_foreign_memorysegment "
  print_static_words $jdkIncubatorForeignMemorySegment
  printf "\n"

  printf "declare-option str-list jdk_jfr_dataamount "
  print_static_words $jdkJfrDataAmount
  printf "\n"

  printf "declare-option str-list jdk_jfr_enabled "
  print_static_words $jdkJfrEnabled
  printf "\n"

  printf "declare-option str-list jdk_jfr_period "
  print_static_words $jdkJfrPeriod
  printf "\n"

  printf "declare-option str-list jdk_jfr_stacktrace "
  print_static_words $jdkJfrStackTrace
  printf "\n"

  printf "declare-option str-list jdk_jfr_threshold "
  print_static_words $jdkJfrThreshold
  printf "\n"

  printf "declare-option str-list jdk_jfr_timespan "
  print_static_words $jdkJfrTimespan
  printf "\n"

  printf "declare-option str-list jdk_jfr_timestamp "
  print_static_words $jdkJfrTimestamp
  printf "\n"

  printf "declare-option str-list jdk_jshell_diag "
  print_static_words $jdkJshellDiag
  printf "\n"

  printf "declare-option str-list jdk_jshell_execution_jdiexecutioncontrolprovider "
  print_static_words $jdkJshellExecutionJdiExecutionControlProvider
  printf "\n"

  printf "declare-option str-list jdk_management_jfr_flightrecordermxbean "
  print_static_words $jdkManagementJfrFlightRecorderMXBean
  printf "\n"

  printf "declare-option str-list org_ietf_jgss_gsscontext "
  print_static_words $orgIetfJgssGSSContext
  printf "\n"

  printf "declare-option str-list org_ietf_jgss_gsscredential "
  print_static_words $orgIetfJgssGSSCredential
  printf "\n"

  printf "declare-option str-list org_ietf_jgss_gssexception "
  print_static_words $orgIetfJgssGSSException
  printf "\n"

  printf "declare-option str-list org_w3c_dom_domerror "
  print_static_words $orgW3cDomDOMError
  printf "\n"

  printf "declare-option str-list org_w3c_dom_domexception "
  print_static_words $orgW3cDomDOMException
  printf "\n"

  printf "declare-option str-list org_w3c_dom_node "
  print_static_words $orgW3cDomNode
  printf "\n"

  printf "declare-option str-list org_w3c_dom_typeinfo "
  print_static_words $orgW3cDomTypeInfo
  printf "\n"

  printf "declare-option str-list org_w3c_dom_userdatahandler "
  print_static_words $orgW3cDomUserDataHandler
  printf "\n"

  printf "declare-option str-list org_w3c_dom_bootstrap_domimplementationregistry "
  print_static_words $orgW3cDomBootstrapDOMImplementationRegistry
  printf "\n"

  printf "declare-option str-list org_w3c_dom_css_cssprimitivevalue "
  print_static_words $orgW3cDomCssCSSPrimitiveValue
  printf "\n"

  printf "declare-option str-list org_w3c_dom_css_cssrule "
  print_static_words $orgW3cDomCssCSSRule
  printf "\n"

  printf "declare-option str-list org_w3c_dom_css_cssvalue "
  print_static_words $orgW3cDomCssCSSValue
  printf "\n"

  printf "declare-option str-list org_w3c_dom_events_event "
  print_static_words $orgW3cDomEventsEvent
  printf "\n"

  printf "declare-option str-list org_w3c_dom_events_eventexception "
  print_static_words $orgW3cDomEventsEventException
  printf "\n"

  printf "declare-option str-list org_w3c_dom_events_mutationevent "
  print_static_words $orgW3cDomEventsMutationEvent
  printf "\n"

  printf "declare-option str-list org_w3c_dom_ls_domimplementationls "
  print_static_words $orgW3cDomLsDOMImplementationLS
  printf "\n"

  printf "declare-option str-list org_w3c_dom_ls_lsexception "
  print_static_words $orgW3cDomLsLSException
  printf "\n"

  printf "declare-option str-list org_w3c_dom_ls_lsparser "
  print_static_words $orgW3cDomLsLSParser
  printf "\n"

  printf "declare-option str-list org_w3c_dom_ls_lsparserfilter "
  print_static_words $orgW3cDomLsLSParserFilter
  printf "\n"

  printf "declare-option str-list org_w3c_dom_ranges_range "
  print_static_words $orgW3cDomRangesRange
  printf "\n"

  printf "declare-option str-list org_w3c_dom_ranges_rangeexception "
  print_static_words $orgW3cDomRangesRangeException
  printf "\n"

  printf "declare-option str-list org_w3c_dom_traversal_nodefilter "
  print_static_words $orgW3cDomTraversalNodeFilter
  printf "\n"

  printf "declare-option str-list org_w3c_dom_xpath_xpathexception "
  print_static_words $orgW3cDomXpathXPathException
  printf "\n"

  printf "declare-option str-list org_w3c_dom_xpath_xpathnamespace "
  print_static_words $orgW3cDomXpathXPathNamespace
  printf "\n"

  printf "declare-option str-list org_w3c_dom_xpath_xpathresult "
  print_static_words $orgW3cDomXpathXPathResult
  printf "\n"

  printf "declare-option str-list org_xml_sax_helpers_namespacesupport "
  print_static_words $orgXmlSaxHelpersNamespaceSupport
  printf "\n"
}
# ---------------------------------------- PAGE BREAK ---------------------------------------------- #
# ------------------------------------------ ENUMS ------------------------------------------------- #
evaluate-commands %sh{

  comSunManagementVMOptionOrigin='ATTACH_ON_DEMAND CONFIG_FILE DEFAULT ENVIRON_VAR ERGONOMIC
    MANAGEMENT OTHER VM_CREATION'
  
  comSunNioSctpAssociationChangeNotificationAssocChangeEvent='CANT_START COMM_LOST COMM_UP RESTART SHUTDOWN'
  
  comSunNioSctpHandlerResult='CONTINUE RETURN'
  
  comSunNioSctpPeerAddressChangeNotificationAddressChangeEvent='ADDR_ADDED ADDR_AVAILABLE
    ADDR_CONFIRMED ADDR_MADE_PRIMARY ADDR_REMOVED ADDR_UNREACHABLE'
  
  comSunSecurityJgssInquireType='KRB5_GET_AUTHTIME KRB5_GET_AUTHZ_DATA KRB5_GET_KRB_CRED
    KRB5_GET_SESSION_KEY_EX KRB5_GET_TKT_FLAGS'
  
  comSunSourceDoctreeAttributeTreeValueKind='DOUBLE EMPTY SINGLE UNQUOTED'
  
  comSunSourceDoctreeDocTreeKind='ATTRIBUTE AUTHOR CODE COMMENT DEPRECATED DOC_COMMENT DOC_ROOT
    DOC_TYPE END_ELEMENT ENTITY ERRONEOUS EXCEPTION HIDDEN IDENTIFIER INDEX INHERIT_DOC LINK
    LINK_PLAIN LITERAL OTHER PARAM PROVIDES REFERENCE RETURN SEE SERIAL SERIAL_DATA SERIAL_FIELD
    SINCE START_ELEMENT SUMMARY SYSTEM_PROPERTY TEXT THROWS UNKNOWN_BLOCK_TAG UNKNOWN_INLINE_TAG USES
    VALUE VERSION'
  
  comSunSourceTreeCaseTreeCaseKind='RULE STATEMENT'
  
  comSunSourceTreeLambdaExpressionTreeBodyKind='EXPRESSION STATEMENT'
  
  comSunSourceTreeMemberReferenceTreeReferenceMode='INVOKE NEW'
  
  comSunSourceTreeModuleTreeModuleKind='OPEN STRONG'
  
  comSunSourceTreeTreeKind='AND AND_ASSIGNMENT ANNOTATED_TYPE ANNOTATION ANNOTATION_TYPE ARRAY_ACCESS
    ARRAY_TYPE ASSERT ASSIGNMENT BINDING_PATTERN BITWISE_COMPLEMENT BLOCK BOOLEAN_LITERAL BREAK CASE
    CATCH CHAR_LITERAL CLASS COMPILATION_UNIT CONDITIONAL_AND CONDITIONAL_EXPRESSION CONDITIONAL_OR
    CONTINUE DIVIDE DIVIDE_ASSIGNMENT DO_WHILE_LOOP DOUBLE_LITERAL EMPTY_STATEMENT ENHANCED_FOR_LOOP
    ENUM EQUAL_TO ERRONEOUS EXPORTS EXPRESSION_STATEMENT EXTENDS_WILDCARD FLOAT_LITERAL FOR_LOOP
    GREATER_THAN GREATER_THAN_EQUAL IDENTIFIER IF IMPORT INSTANCE_OF INT_LITERAL INTERFACE
    INTERSECTION_TYPE LABELED_STATEMENT LAMBDA_EXPRESSION LEFT_SHIFT LEFT_SHIFT_ASSIGNMENT LESS_THAN
    LESS_THAN_EQUAL LOGICAL_COMPLEMENT LONG_LITERAL MEMBER_REFERENCE MEMBER_SELECT METHOD
    METHOD_INVOCATION MINUS MINUS_ASSIGNMENT MODIFIERS MODULE MULTIPLY MULTIPLY_ASSIGNMENT NEW_ARRAY
    NEW_CLASS NOT_EQUAL_TO NULL_LITERAL OPENS OR OR_ASSIGNMENT OTHER'
  
  comSunSourceUtilTaskEventKind='ANALYZE ANNOTATION_PROCESSING ANNOTATION_PROCESSING_ROUND
    COMPILATION ENTER GENERATE PARSE'
  
  comSunToolsJconsoleJConsoleContextConnectionState='CONNECTED CONNECTING DISCONNECTED'
  
  javaAwtComponentBaselineResizeBehavior='CENTER_OFFSET CONSTANT_ASCENT CONSTANT_DESCENT OTHER'
  
  javaAwtDesktopAction='APP_ABOUT APP_EVENT_FOREGROUND APP_EVENT_HIDDEN APP_EVENT_REOPENED
    APP_EVENT_SCREEN_SLEEP APP_EVENT_SYSTEM_SLEEP APP_EVENT_USER_SESSION APP_HELP_VIEWER APP_MENU_BAR
    APP_OPEN_FILE APP_OPEN_URI APP_PREFERENCES APP_PRINT_FILE APP_QUIT_HANDLER APP_QUIT_STRATEGY
    APP_REQUEST_FOREGROUND APP_SUDDEN_TERMINATION BROWSE BROWSE_FILE_DIR EDIT MAIL MOVE_TO_TRASH OPEN PRINT'
  
  javaAwtDesktopQuitStrategy='CLOSE_ALL_WINDOWS NORMAL_EXIT'
  
  javaAwtDesktopUserSessionEventReason='CONSOLE LOCK REMOTE UNSPECIFIED'
  
  javaAwtDialogModalExclusionType='APPLICATION_EXCLUDE NO_EXCLUDE TOOLKIT_EXCLUDE'
  
  javaAwtDialogModalityType='APPLICATION_MODAL DOCUMENT_MODAL MODELESS TOOLKIT_MODAL'
  
  javaAwtEventFocusEventCause='ACTIVATION CLEAR_GLOBAL_FOCUS_OWNER MOUSE_EVENT ROLLBACK TRAVERSAL
    TRAVERSAL_BACKWARD TRAVERSAL_DOWN TRAVERSAL_FORWARD TRAVERSAL_UP UNEXPECTED UNKNOWN'
  
  javaAwtFontNumericShaperRange='ARABIC BALINESE BENGALI CHAM DEVANAGARI EASTERN_ARABIC ETHIOPIC
    EUROPEAN GUJARATI GURMUKHI JAVANESE KANNADA KAYAH_LI KHMER LAO LEPCHA LIMBU MALAYALAM
    MEETEI_MAYEK MONGOLIAN MYANMAR MYANMAR_SHAN MYANMAR_TAI_LAING NEW_TAI_LUE NKO OL_CHIKI ORIYA
    SAURASHTRA SINHALA SUNDANESE TAI_THAM_HORA TAI_THAM_THAM TAMIL TELUGU THAI TIBETAN VAI'
  
  javaAwtGraphicsDeviceWindowTranslucency='PERPIXEL_TRANSLUCENT PERPIXEL_TRANSPARENT TRANSLUCENT'
  
  javaAwtMultipleGradientPaintColorSpaceType='LINEAR_RGB SRGB'
  
  javaAwtMultipleGradientPaintCycleMethod='NO_CYCLE REFLECT REPEAT'
  
  javaAwtTaskbarFeature='ICON_BADGE_IMAGE_WINDOW ICON_BADGE_NUMBER ICON_BADGE_TEXT ICON_IMAGE MENU
    PROGRESS_STATE_WINDOW PROGRESS_VALUE PROGRESS_VALUE_WINDOW USER_ATTENTION USER_ATTENTION_WINDOW'
  
  javaAwtTaskbarState='ERROR INDETERMINATE NORMAL OFF PAUSED'
  
  javaAwtTrayIconMessageType='ERROR INFO NONE WARNING'
  
  javaAwtWindowType='NORMAL POPUP UTILITY'
  
  javaIoObjectInputFilterStatus='ALLOWED REJECTED UNDECIDED'
  
  javaLangAnnotationElementType='ANNOTATION_TYPE CONSTRUCTOR FIELD LOCAL_VARIABLE METHOD MODULE
    PACKAGE PARAMETER RECORD_COMPONENT TYPE TYPE_PARAMETER TYPE_USE'
  
  javaLangAnnotationRetentionPolicy='CLASS RUNTIME SOURCE'
  
  javaLangCharacterUnicodeScript='ADLAM AHOM ANATOLIAN_HIEROGLYPHS ARABIC ARMENIAN AVESTAN BALINESE
    BAMUM BASSA_VAH BATAK BENGALI BHAIKSUKI BOPOMOFO BRAHMI BRAILLE BUGINESE BUHID
    CANADIAN_ABORIGINAL CARIAN CAUCASIAN_ALBANIAN CHAKMA CHAM CHEROKEE CHORASMIAN COMMON COPTIC
    CUNEIFORM CYPRIOT CYRILLIC DESERET DEVANAGARI DIVES_AKURU DOGRA DUPLOYAN EGYPTIAN_HIEROGLYPHS
    ELBASAN ELYMAIC ETHIOPIC GEORGIAN GLAGOLITIC GOTHIC GRANTHA GREEK GUJARATI GUNJALA_GONDI GURMUKHI
    HAN HANGUL HANIFI_ROHINGYA HANUNOO HATRAN HEBREW HIRAGANA IMPERIAL_ARAMAIC INHERITED
    INSCRIPTIONAL_PAHLAVI INSCRIPTIONAL_PARTHIAN JAVANESE KAITHI KANNADA KATAKANA KAYAH_LI KHAROSHTHI
    KHITAN_SMALL_SCRIPT KHMER KHOJKI KHUDAWADI LAO LATIN LEPCHA LIMBU LINEAR_A LINEAR_B LISU LYCIAN
    LYDIAN MAHAJANI MAKASAR MALAYALAM MANDAIC MANICHAEAN MARCHEN MASARAM_GONDI MEDEFAIDRIN
    MEETEI_MAYEK MENDE_KIKAKUI MEROITIC_CURSIVE MEROITIC_HIEROGLYPHS MIAO MODI MONGOLIAN MRO MULTANI
    MYANMAR NABATAEAN NANDINAGARI NEW_TAI_LUE NEWA NKO NUSHU NYIAKENG_PUACHUE_HMONG OGHAM OL_CHIKI
    OLD_HUNGARIAN OLD_ITALIC OLD_NORTH_ARABIAN OLD_PERMIC OLD_PERSIAN OLD_SOGDIAN OLD_SOUTH_ARABIAN
    OLD_TURKIC ORIYA OSAGE OSMANYA PAHAWH_HMONG PALMYRENE PAU_CIN_HAU PHAGS_PA PHOENICIAN
    PSALTER_PAHLAVI REJANG RUNIC SAMARITAN SAURASHTRA SHARADA SHAVIAN SIDDHAM SIGNWRITING SINHALA
    SOGDIAN SORA_SOMPENG SOYOMBO SUNDANESE SYLOTI_NAGRI SYRIAC TAGALOG TAGBANWA TAI_LE TAI_THAM
    TAI_VIET TAKRI TAMIL TANGUT TELUGU THAANA THAI TIBETAN TIFINAGH TIRHUTA UGARITIC UNKNOWN VAI
    WANCHO WARANG_CITI YEZIDI YI ZANABAZAR_SQUARE'
  
  javaLangConstantDirectMethodHandleDescKind='CONSTRUCTOR GETTER INTERFACE_SPECIAL INTERFACE_STATIC
    INTERFACE_VIRTUAL SETTER SPECIAL STATIC STATIC_GETTER STATIC_SETTER VIRTUAL'
  
  javaLangInvokeMethodHandlesLookupClassOption='NESTMATE STRONG'
  
  javaLangInvokeVarHandleAccessMode='COMPARE_AND_EXCHANGE COMPARE_AND_EXCHANGE_ACQUIRE
    COMPARE_AND_EXCHANGE_RELEASE COMPARE_AND_SET GET GET_ACQUIRE GET_AND_ADD GET_AND_ADD_ACQUIRE
    GET_AND_ADD_RELEASE GET_AND_BITWISE_AND GET_AND_BITWISE_AND_ACQUIRE GET_AND_BITWISE_AND_RELEASE
    GET_AND_BITWISE_OR GET_AND_BITWISE_OR_ACQUIRE GET_AND_BITWISE_OR_RELEASE GET_AND_BITWISE_XOR
    GET_AND_BITWISE_XOR_ACQUIRE GET_AND_BITWISE_XOR_RELEASE GET_AND_SET GET_AND_SET_ACQUIRE
    GET_AND_SET_RELEASE GET_OPAQUE GET_VOLATILE SET SET_OPAQUE SET_RELEASE SET_VOLATILE
    WEAK_COMPARE_AND_SET WEAK_COMPARE_AND_SET_ACQUIRE WEAK_COMPARE_AND_SET_PLAIN
    WEAK_COMPARE_AND_SET_RELEASE'
  
  javaLangManagementMemoryType='HEAP NON_HEAP'
  
  javaLangModuleModuleDescriptorExportsModifier='MANDATED SYNTHETIC'
  
  javaLangModuleModuleDescriptorModifier='AUTOMATIC MANDATED OPEN SYNTHETIC'
  
  javaLangModuleModuleDescriptorOpensModifier='MANDATED SYNTHETIC'
  
  javaLangModuleModuleDescriptorRequiresModifier='MANDATED STATIC SYNTHETIC TRANSITIVE'
  
  javaLangProcessBuilderRedirectType='APPEND INHERIT PIPE READ WRITE'
  
  javaLangStackWalkerOption='RETAIN_CLASS_REFERENCE SHOW_HIDDEN_FRAMES SHOW_REFLECT_FRAMES'
  
  javaLangSystemLoggerLevel='ALL DEBUG ERROR INFO OFF TRACE WARNING'
  
  javaLangThreadState='BLOCKED NEW RUNNABLE TERMINATED TIMED_WAITING WAITING'
  
  javaMathRoundingMode='CEILING DOWN FLOOR HALF_DOWN HALF_EVEN HALF_UP UNNECESSARY UP'
  
  javaNetAuthenticatorRequestorType='PROXY SERVER'
  
  javaNetHttpHttpClientRedirect='ALWAYS NEVER NORMAL'
  
  javaNetHttpHttpClientVersion='HTTP_1_1 HTTP_2'
  
  javaNetProxyType='DIRECT HTTP SOCKS'
  
  javaNetStandardProtocolFamily='INET INET6 UNIX'
  
  javaNioFileAccessMode='EXECUTE READ WRITE'
  
  javaNioFileAttributeAclEntryFlag='DIRECTORY_INHERIT FILE_INHERIT INHERIT_ONLY NO_PROPAGATE_INHERIT'
  
  javaNioFileAttributeAclEntryPermission='APPEND_DATA DELETE DELETE_CHILD EXECUTE READ_ACL
    READ_ATTRIBUTES READ_DATA READ_NAMED_ATTRS SYNCHRONIZE WRITE_ACL WRITE_ATTRIBUTES WRITE_DATA
    WRITE_NAMED_ATTRS WRITE_OWNER'
  
  javaNioFileAttributeAclEntryType='ALARM ALLOW AUDIT DENY'
  
  javaNioFileAttributePosixFilePermission='GROUP_EXECUTE GROUP_READ GROUP_WRITE OTHERS_EXECUTE
    OTHERS_READ OTHERS_WRITE OWNER_EXECUTE OWNER_READ OWNER_WRITE'
  
  javaNioFileFileVisitOption='FOLLOW_LINKS'
  
  javaNioFileFileVisitResult='CONTINUE SKIP_SIBLINGS SKIP_SUBTREE TERMINATE'
  
  javaNioFileLinkOption='NOFOLLOW_LINKS'
  
  javaNioFileStandardCopyOption='ATOMIC_MOVE COPY_ATTRIBUTES REPLACE_EXISTING'
  
  javaNioFileStandardOpenOption='APPEND CREATE CREATE_NEW DELETE_ON_CLOSE DSYNC READ SPARSE SYNC
    TRUNCATE_EXISTING WRITE'
  
  javaSecurityCertCertPathValidatorExceptionBasicReason='ALGORITHM_CONSTRAINED EXPIRED
    INVALID_SIGNATURE NOT_YET_VALID REVOKED UNDETERMINED_REVOCATION_STATUS UNSPECIFIED'
  
  javaSecurityCertCRLReason='AA_COMPROMISE AFFILIATION_CHANGED CA_COMPROMISE CERTIFICATE_HOLD
    CESSATION_OF_OPERATION KEY_COMPROMISE PRIVILEGE_WITHDRAWN REMOVE_FROM_CRL SUPERSEDED UNSPECIFIED
    UNUSED'
  
  javaSecurityCertPKIXReason='INVALID_KEY_USAGE INVALID_NAME INVALID_POLICY NAME_CHAINING
    NO_TRUST_ANCHOR NOT_CA_CERT PATH_TOO_LONG UNRECOGNIZED_CRIT_EXT'
  
  javaSecurityCertPKIXRevocationCheckerOption='NO_FALLBACK ONLY_END_ENTITY PREFER_CRLS SOFT_FAIL'
  
  javaSecurityCryptoPrimitive='BLOCK_CIPHER KEY_AGREEMENT KEY_ENCAPSULATION KEY_WRAP MAC
    MESSAGE_DIGEST PUBLIC_KEY_ENCRYPTION SECURE_RANDOM SIGNATURE STREAM_CIPHER'
  
  javaSecurityDrbgParametersCapability='NONE PR_AND_RESEED RESEED_ONLY'
  
  javaSecurityKeyRepType='PRIVATE PUBLIC SECRET'
  
  javaSqlClientInfoStatus='REASON_UNKNOWN REASON_UNKNOWN_PROPERTY REASON_VALUE_INVALID
    REASON_VALUE_TRUNCATED'
  
  javaSqlJDBCType='ARRAY BIGINT BINARY BIT BLOB BOOLEAN CHAR CLOB DATALINK DATE DECIMAL DISTINCT
    DOUBLE FLOAT INTEGER JAVA_OBJECT LONGNVARCHAR LONGVARBINARY LONGVARCHAR NCHAR NCLOB NULL NUMERIC
    NVARCHAR OTHER REAL REF REF_CURSOR ROWID SMALLINT SQLXML STRUCT TIME TIME_WITH_TIMEZONE TIMESTAMP
    TIMESTAMP_WITH_TIMEZONE TINYINT VARBINARY VARCHAR'
  
  javaSqlPseudoColumnUsage='NO_USAGE_RESTRICTIONS SELECT_LIST_ONLY USAGE_UNKNOWN WHERE_CLAUSE_ONLY'
  
  javaSqlRowIdLifetime='ROWID_UNSUPPORTED ROWID_VALID_FOREVER ROWID_VALID_OTHER ROWID_VALID_SESSION
    ROWID_VALID_TRANSACTION'
  
  javaTextNormalizerForm='NFC NFD NFKC NFKD'
  
  javaTextNumberFormatStyle='LONG SHORT'
  
  javaTimeChronoHijrahEra='AH'
  
  javaTimeChronoIsoEra='BC BCE'
  
  javaTimeChronoMinguoEra='BEFORE_ROC ROC'
  
  javaTimeChronoThaiBuddhistEra='BE BEFORE_BE'
  
  javaTimeDayOfWeek='FRIDAY MONDAY SATURDAY SUNDAY THURSDAY TUESDAY WEDNESDAY'
  
  javaTimeFormatFormatStyle='FULL LONG MEDIUM SHORT'
  
  javaTimeFormatResolverStyle='LENIENT SMART STRICT'
  
  javaTimeFormatSignStyle='ALWAYS EXCEEDS_PAD NEVER NORMAL NOT_NEGATIVE'
  
  javaTimeFormatTextStyle='FULL FULL_STANDALONE NARROW NARROW_STANDALONE SHORT SHORT_STANDALONE'
  
  javaTimeMonth='APRIL AUGUST DECEMBER FEBRUARY JANUARY JULY JUNE MARCH MAY NOVEMBER OCTOBER SEPTEMBER'
  
  javaTimeTemporalChronoField='ALIGNED_DAY_OF_WEEK_IN_MONTH ALIGNED_DAY_OF_WEEK_IN_YEAR
    ALIGNED_WEEK_OF_MONTH ALIGNED_WEEK_OF_YEAR AMPM_OF_DAY CLOCK_HOUR_OF_AMPM CLOCK_HOUR_OF_DAY
    DAY_OF_MONTH DAY_OF_WEEK DAY_OF_YEAR EPOCH_DAY ERA HOUR_OF_AMPM HOUR_OF_DAY INSTANT_SECONDS
    MICRO_OF_DAY MICRO_OF_SECOND MILLI_OF_DAY MILLI_OF_SECOND MINUTE_OF_DAY MINUTE_OF_HOUR
    MONTH_OF_YEAR NANO_OF_DAY NANO_OF_SECOND OFFSET_SECONDS PROLEPTIC_MONTH SECOND_OF_DAY
    SECOND_OF_MINUTE YEAR YEAR_OF_ERA'
  
  javaTimeTemporalChronoUnit='CENTURIES DAYS DECADES ERAS FOREVER HALF_DAYS HOURS MICROS MILLENNIA
    MILLIS MINUTES MONTHS NANOS SECONDS WEEKS YEARS'
  
  javaTimeZoneZoneOffsetTransitionRuleTimeDefinition='STANDARD UTC WALL'
  
  javaUtilConcurrentTimeUnit='DAYS HOURS MICROSECONDS MILLISECONDS MINUTES NANOSECONDS SECONDS'
  
  javaUtilFormatterBigDecimalLayoutForm='DECIMAL_FLOAT SCIENTIFIC'
  
  javaUtilLocaleCategory='DISPLAY FORMAT'
  
  javaUtilLocaleFilteringMode='AUTOSELECT_FILTERING EXTENDED_FILTERING IGNORE_EXTENDED_RANGES
    MAP_EXTENDED_RANGES REJECT_EXTENDED_RANGES'
  
  javaUtilLocaleIsoCountryCode='PART1_ALPHA2 PART1_ALPHA3 PART3'
  
  javaUtilStreamCollectorCharacteristics='CONCURRENT IDENTITY_FINISH UNORDERED'
  
  javaxLangModelElementElementKind='ANNOTATION_TYPE BINDING_VARIABLE CLASS CONSTRUCTOR ENUM
    ENUM_CONSTANT EXCEPTION_PARAMETER FIELD INSTANCE_INIT INTERFACE LOCAL_VARIABLE METHOD MODULE
    OTHER PACKAGE PARAMETER RECORD RECORD_COMPONENT RESOURCE_VARIABLE STATIC_INIT TYPE_PARAMETER'
  
  javaxLangModelElementModifier='ABSTRACT DEFAULT FINAL NATIVE NON_SEALED PRIVATE PROTECTED PUBLIC
    SEALED STATIC STRICTFP SYNCHRONIZED TRANSIENT VOLATILE'
  
  javaxLangModelElementModuleElementDirectiveKind='EXPORTS OPENS PROVIDES REQUIRES USES'
  
  javaxLangModelElementNestingKind='ANONYMOUS LOCAL MEMBER TOP_LEVEL'
  
  javaxLangModelSourceVersion='RELEASE_0 RELEASE_1 RELEASE_10 RELEASE_11 RELEASE_12 RELEASE_13
    RELEASE_14 RELEASE_15 RELEASE_16 RELEASE_2 RELEASE_3 RELEASE_4 RELEASE_5 RELEASE_6 RELEASE_7
    RELEASE_8 RELEASE_9'
  
  javaxLangModelTypeTypeKind='ARRAY BOOLEAN BYTE CHAR DECLARED DOUBLE ERROR EXECUTABLE FLOAT INT
    INTERSECTION LONG MODULE NONE NULL OTHER PACKAGE SHORT TYPEVAR UNION VOID WILDCARD ARRAY BOOLEAN
    BYTE CHAR DECLARED DOUBLE ERROR EXECUTABLE FLOAT INT INTERSECTION LONG MODULE NONE NULL OTHER
    PACKAGE SHORT TYPEVAR UNION VOID WILDCARD'
  
  javaxLangModelUtilElementsOrigin='EXPLICIT MANDATED SYNTHETIC'
  
  javaxNetSslSSLEngineResultHandshakeStatus='FINISHED NEED_TASK NEED_UNWRAP NEED_UNWRAP_AGAIN
    NEED_WRAP NOT_HANDSHAKING'
  
  javaxNetSslSSLEngineResultStatus='BUFFER_OVERFLOW BUFFER_UNDERFLOW CLOSED OK'
  
  javaxSmartcardioCardTerminalsState='ALL CARD_ABSENT CARD_INSERTION CARD_PRESENT CARD_REMOVAL'
  
  javaxSwingDropMode='INSERT INSERT_COLS INSERT_ROWS ON ON_OR_INSERT ON_OR_INSERT_COLS
    ON_OR_INSERT_ROWS USE_SELECTION'
  
  javaxSwingEventRowSorterEventType='SORT_ORDER_CHANGED SORTED'
  
  javaxSwingGroupLayoutAlignment='BASELINE CENTER LEADING TRAILING'
  
  javaxSwingJTablePrintMode='FIT_WIDTH NORMAL'
  
  javaxSwingLayoutStyleComponentPlacement='INDENT RELATED UNRELATED'
  
  javaxSwingPlafNimbusAbstractRegionPainterPaintContextCacheMode='FIXED_SIZES NINE_SQUARE_SCALE NO_CACHING'
  
  javaxSwingRowFilterComparisonType='AFTER BEFORE EQUAL NOT_EQUAL'
  
  javaxSwingSortOrder='ASCENDING DESCENDING UNSORTED'
  
  javaxSwingSwingWorkerStateValue='DONE PENDING STARTED'
  
  javaxSwingTextHtmlFormSubmitEventMethodType='GET POST'
  
  javaxToolsDiagnosticKind='ERROR MANDATORY_WARNING NOTE OTHER WARNING'
  
  javaxToolsDocumentationToolLocation='DOCLET_PATH DOCUMENTATION_OUTPUT TAGLET_PATH'
  
  javaxToolsJavaFileObjectKind='CLASS HTML OTHER SOURCE'
  
  javaxToolsStandardLocation='ANNOTATION_PROCESSOR_MODULE_PATH ANNOTATION_PROCESSOR_PATH CLASS_OUTPUT
    CLASS_PATH MODULE_PATH MODULE_SOURCE_PATH NATIVE_HEADER_OUTPUT PATCH_MODULE_PATH
    PLATFORM_CLASS_PATH SOURCE_OUTPUT SOURCE_PATH SYSTEM_MODULES UPGRADE_MODULE_PATH'
  
  javaxXmlCatalogCatalogFeaturesFeature='DEFER FILES PREFER RESOLVE'
  
  javaxXmlXpathXPathEvaluationResultXPathResultType='ANY BOOLEAN NODE NODESET NUMBER STRING'
  
  jdkDynalinkLinkerConversionComparatorComparison='INDETERMINATE TYPE_1_BETTER TYPE_2_BETTER'
  
  jdkDynalinkStandardNamespace='ELEMENT METHOD PROPERTY'
  
  jdkDynalinkStandardOperation='CALL GET NEW REMOVE SET'
  
  jdkIncubatorForeignCLinkerTypeKind='CHAR DOUBLE FLOAT INT LONG LONG_LONG POINTER SHORT'
  
  jdkIncubatorVectorVectorShape='S_128_BIT S_256_BIT S_512_BIT S_64_BIT S_Max_BIT'
  
  jdkJavadocDocletDocletEnvironmentModuleMode='ALL API'
  
  jdkJavadocDocletDocletOptionKind='EXTENDED OTHER STANDARD'
  
  jdkJavadocDocletTagletLocation='CONSTRUCTOR FIELD METHOD MODULE OVERVIEW PACKAGE TYPE'
  
  jdkJfrRecordingState='CLOSED DELAYED NEW RUNNING STOPPED'
  
  jdkJshellSnippetKind='ERRONEOUS EXPRESSION IMPORT METHOD STATEMENT TYPE_DECL VAR'
  
  jdkJshellSnippetStatus='DROPPED NONEXISTENT OVERWRITTEN RECOVERABLE_DEFINED RECOVERABLE_NOT_DEFINED REJECTED VALID'
  
  jdkJshellSnippetSubKind='ANNOTATION_TYPE_SUBKIND ASSIGNMENT_SUBKIND CLASS_SUBKIND ENUM_SUBKIND
    INTERFACE_SUBKIND METHOD_SUBKIND OTHER_EXPRESSION_SUBKIND RECORD_SUBKIND
    SINGLE_STATIC_IMPORT_SUBKIND SINGLE_TYPE_IMPORT_SUBKIND STATEMENT_SUBKIND
    STATIC_IMPORT_ON_DEMAND_SUBKIND TEMP_VAR_EXPRESSION_SUBKIND TYPE_IMPORT_ON_DEMAND_SUBKIND
    UNKNOWN_SUBKIND VAR_DECLARATION_SUBKIND VAR_DECLARATION_WITH_INITIALIZER_SUBKIND VAR_VALUE_SUBKIND'
  
  jdkJshellSourceCodeAnalysisCompleteness='COMPLETE COMPLETE_WITH_SEMI CONSIDERED_INCOMPLETE
    DEFINITELY_INCOMPLETE EMPTY UNKNOWN'
  
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  print_static_words() {
    for i in $@
    do
      printf " %s" $i 
    done
  }
  # -------------------------------------- PAGE BREAK ---------------------------------------------- #
  # ---------------------------------------- ENUMS ------------------------------------------------- #
  printf "declare-option str-list com_sun_management_vmoption_origin "
  print_static_words $comSunManagementVMOptionOrigin
  printf "\n"

  printf "declare-option str-list com_sun_nio_sctp_associationchangenotification_assocchangeevent "
  print_static_words $comSunNioSctpAssociationChangeNotificationAssocChangeEvent
  printf "\n"

  printf "declare-option str-list com_sun_nio_sctp_handlerresult "
  print_static_words $comSunNioSctpHandlerResult
  printf "\n"

  printf "declare-option str-list com_sun_nio_sctp_peeraddresschangenotification_addresschangeevent "
  print_static_words $comSunNioSctpPeerAddressChangeNotificationAddressChangeEvent
  printf "\n"

  printf "declare-option str-list com_sun_security_jgss_inquiretype "
  print_static_words $comSunSecurityJgssInquireType
  printf "\n"

  printf "declare-option str-list com_sun_source_doctree_attributetree_valuekind "
  print_static_words $comSunSourceDoctreeAttributeTreeValueKind
  printf "\n"

  printf "declare-option str-list com_sun_source_doctree_doctree_kind "
  print_static_words $comSunSourceDoctreeDocTreeKind
  printf "\n"

  printf "declare-option str-list com_sun_source_tree_casetree_casekind "
  print_static_words $comSunSourceTreeCaseTreeCaseKind
  printf "\n"

  printf "declare-option str-list com_sun_source_tree_lambdaexpressiontree_bodykind "
  print_static_words $comSunSourceTreeLambdaExpressionTreeBodyKind
  printf "\n"

  printf "declare-option str-list com_sun_source_tree_memberreferencetree_referencemode "
  print_static_words $comSunSourceTreeMemberReferenceTreeReferenceMode
  printf "\n"

  printf "declare-option str-list com_sun_source_tree_moduletree_modulekind "
  print_static_words $comSunSourceTreeModuleTreeModuleKind
  printf "\n"

  printf "declare-option str-list com_sun_source_tree_tree_kind "
  print_static_words $comSunSourceTreeTreeKind
  printf "\n"

  printf "declare-option str-list com_sun_source_util_taskevent_kind "
  print_static_words $comSunSourceUtilTaskEventKind
  printf "\n"

  printf "declare-option str-list com_sun_tools_jconsole_jconsolecontext_connectionstate "
  print_static_words $comSunToolsJconsoleJConsoleContextConnectionState
  printf "\n"

  printf "declare-option str-list java_awt_component_baselineresizebehavior "
  print_static_words $javaAwtComponentBaselineResizeBehavior
  printf "\n"

  printf "declare-option str-list java_awt_desktop_action "
  print_static_words $javaAwtDesktopAction
  printf "\n"

  printf "declare-option str-list java_awt_desktop_quitstrategy "
  print_static_words $javaAwtDesktopQuitStrategy
  printf "\n"

  printf "declare-option str-list java_awt_desktop_usersessionevent_reason "
  print_static_words $javaAwtDesktopUserSessionEventReason
  printf "\n"

  printf "declare-option str-list java_awt_dialog_modalexclusiontype "
  print_static_words $javaAwtDialogModalExclusionType
  printf "\n"

  printf "declare-option str-list java_awt_dialog_modalitytype "
  print_static_words $javaAwtDialogModalityType
  printf "\n"

  printf "declare-option str-list java_awt_event_focusevent_cause "
  print_static_words $javaAwtEventFocusEventCause
  printf "\n"

  printf "declare-option str-list java_awt_font_numericshaper_range "
  print_static_words $javaAwtFontNumericShaperRange
  printf "\n"

  printf "declare-option str-list java_awt_graphicsdevice_windowtranslucency "
  print_static_words $javaAwtGraphicsDeviceWindowTranslucency
  printf "\n"

  printf "declare-option str-list java_awt_multiplegradientpaint_colorspacetype "
  print_static_words $javaAwtMultipleGradientPaintColorSpaceType
  printf "\n"

  printf "declare-option str-list java_awt_multiplegradientpaint_cyclemethod "
  print_static_words $javaAwtMultipleGradientPaintCycleMethod
  printf "\n"

  printf "declare-option str-list java_awt_taskbar_feature "
  print_static_words $javaAwtTaskbarFeature
  printf "\n"

  printf "declare-option str-list java_awt_taskbar_state "
  print_static_words $javaAwtTaskbarState
  printf "\n"

  printf "declare-option str-list java_awt_trayicon_messagetype "
  print_static_words $javaAwtTrayIconMessageType
  printf "\n"

  printf "declare-option str-list java_awt_window_type "
  print_static_words $javaAwtWindowType
  printf "\n"

  printf "declare-option str-list java_io_objectinputfilter_status "
  print_static_words $javaIoObjectInputFilterStatus
  printf "\n"

  printf "declare-option str-list java_lang_annotation_elementtype "
  print_static_words $javaLangAnnotationElementType
  printf "\n"

  printf "declare-option str-list java_lang_annotation_retentionpolicy "
  print_static_words $javaLangAnnotationRetentionPolicy
  printf "\n"

  printf "declare-option str-list java_lang_character_unicodescript "
  print_static_words $javaLangCharacterUnicodeScript
  printf "\n"

  printf "declare-option str-list java_lang_constant_directmethodhandledesc_kind "
  print_static_words $javaLangConstantDirectMethodHandleDescKind
  printf "\n"

  printf "declare-option str-list java_lang_invoke_methodhandles_lookup_classoption "
  print_static_words $javaLangInvokeMethodHandlesLookupClassOption
  printf "\n"

  printf "declare-option str-list java_lang_invoke_varhandle_accessmode "
  print_static_words $javaLangInvokeVarHandleAccessMode
  printf "\n"

  printf "declare-option str-list java_lang_management_memorytype "
  print_static_words $javaLangManagementMemoryType
  printf "\n"

  printf "declare-option str-list java_lang_module_moduledescriptor_exports_modifier "
  print_static_words $javaLangModuleModuleDescriptorExportsModifier
  printf "\n"

  printf "declare-option str-list java_lang_module_moduledescriptor_modifier "
  print_static_words $javaLangModuleModuleDescriptorModifier
  printf "\n"

  printf "declare-option str-list java_lang_module_moduledescriptor_opens_modifier "
  print_static_words $javaLangModuleModuleDescriptorOpensModifier
  printf "\n"

  printf "declare-option str-list java_lang_module_moduledescriptor_requires_modifier "
  print_static_words $javaLangModuleModuleDescriptorRequiresModifier
  printf "\n"

  printf "declare-option str-list java_lang_processbuilder_redirect_type "
  print_static_words $javaLangProcessBuilderRedirectType
  printf "\n"

  printf "declare-option str-list java_lang_stackwalker_option "
  print_static_words $javaLangStackWalkerOption
  printf "\n"

  printf "declare-option str-list java_lang_system_logger_level "
  print_static_words $javaLangSystemLoggerLevel
  printf "\n"

  printf "declare-option str-list java_lang_thread_state "
  print_static_words $javaLangThreadState
  printf "\n"

  printf "declare-option str-list java_math_roundingmode "
  print_static_words $javaMathRoundingMode
  printf "\n"

  printf "declare-option str-list java_net_authenticator_requestortype "
  print_static_words $javaNetAuthenticatorRequestorType
  printf "\n"

  printf "declare-option str-list java_net_http_httpclient_redirect "
  print_static_words $javaNetHttpHttpClientRedirect
  printf "\n"

  printf "declare-option str-list java_net_http_httpclient_version "
  print_static_words $javaNetHttpHttpClientVersion
  printf "\n"

  printf "declare-option str-list java_net_proxy_type "
  print_static_words $javaNetProxyType
  printf "\n"

  printf "declare-option str-list java_net_standardprotocolfamily "
  print_static_words $javaNetStandardProtocolFamily
  printf "\n"

  printf "declare-option str-list java_nio_file_accessmode "
  print_static_words $javaNioFileAccessMode
  printf "\n"

  printf "declare-option str-list java_nio_file_attribute_aclentryflag "
  print_static_words $javaNioFileAttributeAclEntryFlag
  printf "\n"

  printf "declare-option str-list java_nio_file_attribute_aclentrypermission "
  print_static_words $javaNioFileAttributeAclEntryPermission
  printf "\n"

  printf "declare-option str-list java_nio_file_attribute_aclentrytype "
  print_static_words $javaNioFileAttributeAclEntryType
  printf "\n"

  printf "declare-option str-list java_nio_file_attribute_posixfilepermission "
  print_static_words $javaNioFileAttributePosixFilePermission
  printf "\n"

  printf "declare-option str-list java_nio_file_filevisitoption "
  print_static_words $javaNioFileFileVisitOption
  printf "\n"

  printf "declare-option str-list java_nio_file_filevisitresult "
  print_static_words $javaNioFileFileVisitResult
  printf "\n"

  printf "declare-option str-list java_nio_file_linkoption "
  print_static_words $javaNioFileLinkOption
  printf "\n"

  printf "declare-option str-list java_nio_file_standardcopyoption "
  print_static_words $javaNioFileStandardCopyOption
  printf "\n"

  printf "declare-option str-list java_nio_file_standardopenoption "
  print_static_words $javaNioFileStandardOpenOption
  printf "\n"

  printf "declare-option str-list java_security_cert_certpathvalidatorexception_basicreason "
  print_static_words $javaSecurityCertCertPathValidatorExceptionBasicReason
  printf "\n"

  printf "declare-option str-list java_security_cert_crlreason "
  print_static_words $javaSecurityCertCRLReason
  printf "\n"

  printf "declare-option str-list java_security_cert_pkixreason "
  print_static_words $javaSecurityCertPKIXReason
  printf "\n"

  printf "declare-option str-list java_security_cert_pkixrevocationchecker_option "
  print_static_words $javaSecurityCertPKIXRevocationCheckerOption
  printf "\n"

  printf "declare-option str-list java_security_cryptoprimitive "
  print_static_words $javaSecurityCryptoPrimitive
  printf "\n"

  printf "declare-option str-list java_security_drbgparameters_capability "
  print_static_words $javaSecurityDrbgParametersCapability
  printf "\n"

  printf "declare-option str-list java_security_keyrep_type "
  print_static_words $javaSecurityKeyRepType
  printf "\n"

  printf "declare-option str-list java_sql_clientinfostatus "
  print_static_words $javaSqlClientInfoStatus
  printf "\n"

  printf "declare-option str-list java_sql_jdbctype "
  print_static_words $javaSqlJDBCType
  printf "\n"

  printf "declare-option str-list java_sql_pseudocolumnusage "
  print_static_words $javaSqlPseudoColumnUsage
  printf "\n"

  printf "declare-option str-list java_sql_rowidlifetime "
  print_static_words $javaSqlRowIdLifetime
  printf "\n"

  printf "declare-option str-list java_text_normalizer_form "
  print_static_words $javaTextNormalizerForm
  printf "\n"

  printf "declare-option str-list java_text_numberformat_style "
  print_static_words $javaTextNumberFormatStyle
  printf "\n"

  printf "declare-option str-list java_time_chrono_hijrahera "
  print_static_words $javaTimeChronoHijrahEra
  printf "\n"

  printf "declare-option str-list java_time_chrono_isoera "
  print_static_words $javaTimeChronoIsoEra
  printf "\n"

  printf "declare-option str-list java_time_chrono_minguoera "
  print_static_words $javaTimeChronoMinguoEra
  printf "\n"

  printf "declare-option str-list java_time_chrono_thaibuddhistera "
  print_static_words $javaTimeChronoThaiBuddhistEra
  printf "\n"

  printf "declare-option str-list java_time_dayofweek "
  print_static_words $javaTimeDayOfWeek
  printf "\n"

  printf "declare-option str-list java_time_format_formatstyle "
  print_static_words $javaTimeFormatFormatStyle
  printf "\n"

  printf "declare-option str-list java_time_format_resolverstyle "
  print_static_words $javaTimeFormatResolverStyle
  printf "\n"

  printf "declare-option str-list java_time_format_signstyle "
  print_static_words $javaTimeFormatSignStyle
  printf "\n"

  printf "declare-option str-list java_time_format_textstyle "
  print_static_words $javaTimeFormatTextStyle
  printf "\n"

  printf "declare-option str-list java_time_month "
  print_static_words $javaTimeMonth
  printf "\n"

  printf "declare-option str-list java_time_temporal_chronofield "
  print_static_words $javaTimeTemporalChronoField
  printf "\n"

  printf "declare-option str-list java_time_temporal_chronounit "
  print_static_words $javaTimeTemporalChronoUnit
  printf "\n"

  printf "declare-option str-list java_time_zone_zoneoffsettransitionrule_timedefinition "
  print_static_words $javaTimeZoneZoneOffsetTransitionRuleTimeDefinition
  printf "\n"

  printf "declare-option str-list java_util_concurrent_timeunit "
  print_static_words $javaUtilConcurrentTimeUnit
  printf "\n"

  printf "declare-option str-list java_util_formatter_bigdecimallayoutform "
  print_static_words $javaUtilFormatterBigDecimalLayoutForm
  printf "\n"

  printf "declare-option str-list java_util_locale_category "
  print_static_words $javaUtilLocaleCategory
  printf "\n"

  printf "declare-option str-list java_util_locale_filteringmode "
  print_static_words $javaUtilLocaleFilteringMode
  printf "\n"

  printf "declare-option str-list java_util_locale_isocountrycode "
  print_static_words $javaUtilLocaleIsoCountryCode
  printf "\n"

  printf "declare-option str-list java_util_stream_collector_characteristics "
  print_static_words $javaUtilStreamCollectorCharacteristics
  printf "\n"

  printf "declare-option str-list javax_lang_model_element_elementkind "
  print_static_words $javaxLangModelElementElementKind
  printf "\n"

  printf "declare-option str-list javax_lang_model_element_modifier "
  print_static_words $javaxLangModelElementModifier
  printf "\n"

  printf "declare-option str-list javax_lang_model_element_moduleelement_directivekind "
  print_static_words $javaxLangModelElementModuleElementDirectiveKind
  printf "\n"

  printf "declare-option str-list javax_lang_model_element_nestingkind "
  print_static_words $javaxLangModelElementNestingKind
  printf "\n"

  printf "declare-option str-list javax_lang_model_sourceversion "
  print_static_words $javaxLangModelSourceVersion
  printf "\n"

  printf "declare-option str-list javax_lang_model_type_typekind "
  print_static_words $javaxLangModelTypeTypeKind
  printf "\n"

  printf "declare-option str-list javax_lang_model_util_elements_origin "
  print_static_words $javaxLangModelUtilElementsOrigin
  printf "\n"

  printf "declare-option str-list javax_net_ssl_sslengineresult_handshakestatus "
  print_static_words $javaxNetSslSSLEngineResultHandshakeStatus
  printf "\n"

  printf "declare-option str-list javax_net_ssl_sslengineresult_status "
  print_static_words $javaxNetSslSSLEngineResultStatus
  printf "\n"

  printf "declare-option str-list javax_smartcardio_cardterminals_state "
  print_static_words $javaxSmartcardioCardTerminalsState
  printf "\n"

  printf "declare-option str-list javax_swing_dropmode "
  print_static_words $javaxSwingDropMode
  printf "\n"

  printf "declare-option str-list javax_swing_event_rowsorterevent_type "
  print_static_words $javaxSwingEventRowSorterEventType
  printf "\n"

  printf "declare-option str-list javax_swing_grouplayout_alignment "
  print_static_words $javaxSwingGroupLayoutAlignment
  printf "\n"

  printf "declare-option str-list javax_swing_jtable_printmode "
  print_static_words $javaxSwingJTablePrintMode
  printf "\n"

  printf "declare-option str-list javax_swing_layoutstyle_componentplacement "
  print_static_words $javaxSwingLayoutStyleComponentPlacement
  printf "\n"

  printf "declare-option str-list javax_swing_plaf_nimbus_abstractregionpainter_paintcontext_cachemode "
  print_static_words $javaxSwingPlafNimbusAbstractRegionPainterPaintContextCacheMode
  printf "\n"

  printf "declare-option str-list javax_swing_rowfilter_comparisontype "
  print_static_words $javaxSwingRowFilterComparisonType
  printf "\n"

  printf "declare-option str-list javax_swing_sortorder "
  print_static_words $javaxSwingSortOrder
  printf "\n"

  printf "declare-option str-list javax_swing_swingworker_statevalue "
  print_static_words $javaxSwingSwingWorkerStateValue
  printf "\n"

  printf "declare-option str-list javax_swing_text_html_formsubmitevent_methodtype "
  print_static_words $javaxSwingTextHtmlFormSubmitEventMethodType
  printf "\n"

  printf "declare-option str-list javax_tools_diagnostic_kind "
  print_static_words $javaxToolsDiagnosticKind
  printf "\n"

  printf "declare-option str-list javax_tools_documentationtool_location "
  print_static_words $javaxToolsDocumentationToolLocation
  printf "\n"

  printf "declare-option str-list javax_tools_javafileobject_kind "
  print_static_words $javaxToolsJavaFileObjectKind
  printf "\n"

  printf "declare-option str-list javax_tools_standardlocation "
  print_static_words $javaxToolsStandardLocation
  printf "\n"

  printf "declare-option str-list javax_xml_catalog_catalogfeatures_feature "
  print_static_words $javaxXmlCatalogCatalogFeaturesFeature
  printf "\n"

  printf "declare-option str-list javax_xml_xpath_xpathevaluationresult_xpathresulttype "
  print_static_words $javaxXmlXpathXPathEvaluationResultXPathResultType
  printf "\n"

  printf "declare-option str-list jdk_dynalink_linker_conversioncomparator_comparison "
  print_static_words $jdkDynalinkLinkerConversionComparatorComparison
  printf "\n"

  printf "declare-option str-list jdk_dynalink_standardnamespace "
  print_static_words $jdkDynalinkStandardNamespace
  printf "\n"

  printf "declare-option str-list jdk_dynalink_standardoperation "
  print_static_words $jdkDynalinkStandardOperation
  printf "\n"

  printf "declare-option str-list jdk_incubator_foreign_clinker_typekind "
  print_static_words $jdkIncubatorForeignCLinkerTypeKind
  printf "\n"

  printf "declare-option str-list jdk_incubator_vector_vectorshape "
  print_static_words $jdkIncubatorVectorVectorShape
  printf "\n"

  printf "declare-option str-list jdk_javadoc_doclet_docletenvironment_modulemode "
  print_static_words $jdkJavadocDocletDocletEnvironmentModuleMode
  printf "\n"

  printf "declare-option str-list jdk_javadoc_doclet_doclet_option_kind "
  print_static_words $jdkJavadocDocletDocletOptionKind
  printf "\n"

  printf "declare-option str-list jdk_javadoc_doclet_taglet_location "
  print_static_words $jdkJavadocDocletTagletLocation
  printf "\n"

  printf "declare-option str-list jdk_jfr_recordingstate "
  print_static_words $jdkJfrRecordingState
  printf "\n"

  printf "declare-option str-list jdk_jshell_snippet_kind "
  print_static_words $jdkJshellSnippetKind
  printf "\n"

  printf "declare-option str-list jdk_jshell_snippet_status "
  print_static_words $jdkJshellSnippetStatus
  printf "\n"

  printf "declare-option str-list jdk_jshell_snippet_subkind "
  print_static_words $jdkJshellSnippetSubKind
  printf "\n"

  printf "declare-option str-list jdk_jshell_sourcecodeanalysis_completeness "
  print_static_words $jdkJshellSourceCodeAnalysisCompleteness
  printf "\n"
}
§
# -------------------------------------------------------------------------------------------------- #
# -------------------------------------------------------------------------------------------------- #
