# Java SE includes a default Policy implementation that reads its authorisation
# data from one or more ASCII (UTF-8) files configured in the security properties
# file. These policy files contain the exact sets of permissions granted to code:
# specifically, the exact sets of permissions granted to code loaded from
# particular locations, signed by particular entities, and executing as particular
# users (Security Policy).
#
# A permission represents access to a system resource. In order for a resource
# access to be allowed for an applet (or an application running with a security
# manager), the corresponding permission must be explicitly granted to the code
# attempting the access (Permissions in the JDK).
#
# Default policy file locations,
#   system policy file is <java-home>/conf/security/java.policy
#   user policy file is <user-home>/.java.policy
#   locations are specified in the security properties file <java-home>/conf/security/java.security.
#
# Each grant entry includes one or more "permission entries" preceded by optional
# codeBase, signedBy, and principal name/value pairs that specify which code you
# want to grant the permissions (Grant Entries).
#
# The signedBy, codeBase, and principal, order of these fields does not matter
# and there case insensitive (Grant Entries).
#
# The exact meaning of a codeBase value depends on the characters at the end. A
# codeBase with a trailing "/" matches all class files (not JAR files) in the
# specified directory. A codeBase with a trailing "/*" matches all files (both
# class and JAR files) contained in that directory. A codeBase with a trailing
# "/-" matches all files (both class and JAR files) in the directory and
# recursively all files in subdirectories contained in that directory
# (The SignedBy, Principal, and CodeBase Fields).
#
#
# @author Oracle 2020, Security Policy, Security Developer’s Guide,
#         Java SE 15, viewed 30 November 2020,
#         https://docs.oracle.com/en/java/javase/15/security/java-security-overview1.html
#
# @author Oracle 2020, Permissions in the JDK, Security Developer’s Guide,
#         Java SE 15, viewed 30 November 2020,
#         https://docs.oracle.com/en/java/javase/15/security/permissions-jdk1.html
#
# @author Oracle 2020, Default Policy File Locations, Permissions in the JDK,
#         Security Developer’s Guide, Java SE 15, viewed 30 November 2020,
#         https://docs.oracle.com/en/java/javase/15/security/permissions-jdk1.html
#
# @author Oracle 2020, Grant Entries, Permissions in the JDK,
#         Security Developer’s Guide, Java SE 15, viewed 30 November 2020,
#         https://docs.oracle.com/en/java/javase/15/security/permissions-jdk1.html
#
# @author Oracle 2020, The SignedBy, Principal, and CodeBase Fields, Permissions in the JDK,
#         Security Developer’s Guide, Java SE 15, viewed 30 November 2020,
#         https://docs.oracle.com/en/java/javase/15/security/permissions-jdk1.html
#
# -------------------------------------------------------------------------------------------------- #
# Detection
# ‾‾‾‾‾‾‾‾‾
hook global ModuleLoaded javaPolicy %{
  hook global BufCreate .*\.policy %{ set-option buffer filetype javaPolicy }
}
# -------------------------------------------------------------------------------------------------- #
# Initialization
# ‾‾‾‾‾‾‾‾‾‾‾‾‾‾
hook global WinSetOption filetype=javaPolicy %{
  try %{
    require-module javaPolicy

    set-option window comment_line '//'
    set-option window comment_block_begin '/*'
    set-option window comment_block_end '*/'

    set-option window static_words %opt{javaPolicy_static_words}

    # cleanup trailing whitespaces when exiting insert mode
    hook window ModeChange pop:insert:.* -group javaPolicy-trim-indent %{ try %{ execute-keys -draft <a-x>s^\h+$<ret>d } }

    hook -once -always window WinSetOption filetype=.* %{ remove-hooks window javaPolicy-.+ }

  } catch %{
    echo -markup '{Error}JavaPolicy hightlighers and completers failed to load.'
  }
}
# -------------------------------------------------------------------------------------------------- #
hook -group javaPolicy-highlighter global WinSetOption filetype=javaPolicy %{
  add-highlighter window/javaPolicy ref javaPolicy
  hook -once -always window WinSetOption filetype=.* %{ remove-highlighter window/javaPolicy }
}
# -------------------------------------------------------------------------------------------------- #
# Module
# ‾‾‾‾‾‾
provide-module javaPolicy %{
  # ------------------------------------------------------------------------------------------------ #
  add-highlighter shared/javaPolicy regions
  add-highlighter shared/javaPolicy/code default-region group
  add-highlighter shared/javaPolicy/string region %{(?<!')"} %{(?<!\\)(\\\\)*"} fill string
  add-highlighter shared/javaPolicy/comment region /\* \*/ fill comment
  add-highlighter shared/javaPolicy/inline_documentation region /// $ fill documentation
  add-highlighter shared/javaPolicy/line_comment region // $ fill comment
  # ------------------------------------------------------------------------------------------------ #
  # Shell
  # ‾‾‾‾‾
  # style macro: 102l<a-t><space>i<ret><esc><esc>
  evaluate-commands %sh{
    # these are case insensitive
    keywords='grant codeBase signedBy principal permission keystore keystorePasswordURL'

        # <<ALL FILES>>
    metasA='ALL_FILES file jrt user.home user.dir user.language'

    metasB='12 128 64 DES DESede RC2 RC4 RC5 RSA'

    metasC='jrt:/java.compiler jrt:/java.net.http jrt:/java.scripting jrt:/java.security.jgss
      jrt:/java.smartcardio jrt:/java.sql jrt:/java.sql.rowset jrt:/java.xml.crypto jrt:/jdk.accessibility
      jrt:/jdk.charsets jrt:/jdk.crypto.cryptoki jrt:/jdk.crypto.ec jrt:/jdk.dynalink jrt:/jdk.httpserver
      jrt:/jdk.internal.le jrt:/jdk.internal.vm.compiler jrt:/jdk.internal.vm.compiler.management
      jrt:/jdk.jsobject jrt:/jdk.localedata jrt:/jdk.naming.dns jrt:/jdk.scripting.nashorn
      jrt:/jdk.scripting.nashorn.shell jrt:/jdk.security.auth jrt:/jdk.security.jgss jrt:/jdk.zipfs'

    actionsA='KeyEscrow KeyRecovery KeyWeakening accept accessClassInPackage accessClipboard
      accessDeclaredMembers accessEventQueue accessSystemModules addIdentityCertificate allow authProvider
      charsetProvider clearProviderProperties closeClassLoader connect createAccessControlContext
      createClassLoader createLoginConfiguration createLoginContext createPolicy createRobot
      createSecurityManager default defineClass defineClassInPackage delete disallow doAs doAsPrivileged
      enableContextClassLoaderOverride enableSubclassImplementation enableSubstitution execute exitVM
      fileSystemProvider getClassLoader getDomainCombiner getFileSystemAttributes getLoginConfiguration
      getPolicy getProperty getProtectionDomain getProxySelector getSignerPrivateKey getStackTrace
      getStackWalkerWithClassReference getSubject getSubjectFromDomainCombiner getenv insertProvider listen
      listenToAllAWTEvents loadLibrary localeServiceProvider loggerFinder manageProcess modifyThread
      modifyThreadGroup newProxyInPackage preferences printIdentity putProviderProperty queuePrintJob read
      readDisplayPixels readFileDescriptor refreshLoginConfiguration removeIdentityCertificate
      removeProvider removeProviderProperty requestPasswordAuthentication resolve setContextClassLoader
      setDefaultAuthenticator setDefaultUncaughtExceptionHandler setFactory setIO setIdentityInfo
      setIdentityPublicKey setLog setLoginConfiguration setPolicy setProperty setReadOnly
      setSecurityManager setSignerKeypair setSystemScope showWindowWithoutWarningBanner shutdownHooks
      specifyStreamHandler stopThread suppressAccessChecks write writeFileDescriptor'

    actionsB='accessClassInPackage.com.apple accessClassInPackage.com.sun.beans
      accessClassInPackage.com.sun.java.swing.plaf accessClassInPackage.com.sun.org.apache.xml.internal
      accessClassInPackage.com.sun.org.apache.xpath.internal accessClassInPackage.jdk.internal.misc
      accessClassInPackage.jdk.internal.vm.compiler.collections accessClassInPackage.jdk.vm.ci.runtime
      accessClassInPackage.jdk.vm.ci.services accessClassInPackage.org.graalvm.compiler.core.common
      accessClassInPackage.org.graalvm.compiler.debug accessClassInPackage.org.graalvm.compiler.hotspot
      accessClassInPackage.org.graalvm.compiler.options
      accessClassInPackage.org.graalvm.compiler.phases.common.jmx
      accessClassInPackage.org.graalvm.compiler.serviceprovider accessClassInPackage.sun.awt
      accessClassInPackage.sun.net accessClassInPackage.sun.net.util accessClassInPackage.sun.net.www
      accessClassInPackage.sun.nio.ch accessClassInPackage.sun.nio.cs accessClassInPackage.sun.security
      accessClassInPackage.sun.security accessClassInPackage.sun.security.jca
      accessClassInPackage.sun.security.util accessClassInPackage.sun.text accessClassInPackage.sun.util
      clearProviderProperties.SunEC clearProviderProperties.SunPCSC clearProviderProperties.XMLDSig
      com.sun.org.apache.xml.internal.security.register file.separator
      getProperty.auth.login.defaultCallbackHandler getProperty.jdk.xml.dsig.secureValidationPolicy
      java.class.version java.security.KeyStore java.specification.name java.specification.vendor
      java.specification.version java.vendor java.vendor.url java.version java.vm.name
      java.vm.specification.name java.vm.specification.vendor java.vm.specification.version java.vm.vendor
      java.vm.version javax.crypto.spec.RC2ParameterSpec javax.crypto.spec.RC5ParameterSpec
      javax.security.auth.x500.X500Principal javax.smartcardio.TerminalFactory.DefaultType
      jdk.crypto.KeyAgreement.legacyKDF line.separator loadLibrary.j2pcsc loadLibrary.j2pkcs11
      loadLibrary.sunec os.arch os.name os.version path.separator putProviderProperty.SunEC
      putProviderProperty.SunPCSC putProviderProperty.XMLDSig removeProviderProperty.SunEC
      removeProviderProperty.SunPCSC removeProviderProperty.XMLDSig sun.arch.data.model sun.nio.cs.map
      sun.security.pkcs11.allowSingleThreadedModules sun.security.pkcs11.disableKeyExtraction
      sun.security.smartcardio.library sun.security.smartcardio.t0GetResponse
      sun.security.smartcardio.t1GetResponse sun.security.smartcardio.t1StripLe'

    actionsC='http https socket ws wss'

    standardClass='java.awt.AWTPermission java.io.FilePermission java.io.SerializablePermission
      java.lang.RuntimePermission java.lang.management.ManagementPermission
      java.lang.reflect.ReflectPermission java.net.NetPermission java.net.SocketPermission
      java.net.URLPermission java.nio.file.LinkPermission java.security.AllPermission
      java.security.SecurityPermission java.security.UnresolvedPermission java.sql.SQLPermission
      java.util.PropertyPermission java.util.logging.LoggingPermission javax.crypto.CryptoAllPermission
      javax.crypto.CryptoPermission javax.management.MBeanPermission javax.management.MBeanServerPermission
      javax.management.MBeanTrustPermission javax.management.remote.SubjectDelegationPermission
      javax.net.ssl.SSLPermission javax.security.auth.AuthPermission
      javax.security.auth.PrivateCredentialPermission javax.security.auth.kerberos.DelegationPermission
      javax.security.auth.kerberos.ServicePermission javax.smartcardio.CardPermission
      javax.sound.sampled.AudioPermission'

    nonStandardClass='com.sun.jdi.JDIPermission com.sun.security.jgss.InquireSecContextPermission
      com.sun.tools.attach.AttachPermission jdk.jfr.FlightRecorderPermission jdk.net.NetworkPermission'

    nonStandardActions='accessFlightRecorder attachVirtualMachine registerEvent setOption.SO_FLOW_SLA virtualMachineManager'

    # ---------------------------------------------------------------------------------------------- #
    # c-family.kak: <https://github.com/mawww/kakoune/blob/master/rc/filetype/c-family.kak#L271>
    join() { sep=$2; eval set -- $1; IFS="$sep"; echo "$*"; }
    # ---------------------------------------------------------------------------------------------- #
    add_highlighter() { printf "add-highlighter shared/javaPolicy/code/ regex %s %s\n" "$1" "$2"; }
    # ---------------------------------------------------------------------------------------------- #
    # alexherbo2: <https://github.com/alexherbo2/plug.kak/blob/master/rc/plug.kak#L108>
    add_word_highlighter() {

      while [ $# -gt 0 ]; do
        words=$1 face=$2; shift 2
        regex="\\b($(join "${words}" '|'))\\b"
        add_highlighter "$regex" "1:$face"
      done

    }
    # ---------------------------------------------------------------------------------------------- #
    # lesson learnt: style macro in for loop use: $@ not "$@"
    print_static_words() {

      for name in $@
      do
        printf " %s" "$name"
      done

    }
    # ---------------------------------------------------------------------------------------------- #
    printf "declare-option str-list javaPolicy_static_words "

    print_static_words "$keywords"
    print_static_words "$metasA" "$metasB" "$metasC"
    print_static_words "$actionsA" "$actionsB" "$actionsC"
    print_static_words "$nonStandardActions" "$standardClass" "$nonStandardClass"

    printf "\n"
    # ---------------------------------------------------------------------------------------------- #
    add_word_highlighter "$keywords" "keyword"
    add_word_highlighter "$metasA" "meta" "$metasB" "meta" "$metasC" "meta"
    add_word_highlighter "$actionsA" "value" "$actionsB" "value" "$actionsC" "value"
    add_word_highlighter "$nonStandardActions" "value" "$standardClass" "type" "$nonStandardClass" "type"
    # ---------------------------------------------------------------------------------------------- #
  }
}
