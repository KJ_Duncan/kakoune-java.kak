# References
# ‾‾‾‾‾‾‾‾‾‾
# Oracle 2022, Hierarchy For All Packages, Java SE 18, viewed 26 August 2022, https://docs.oracle.com/en/java/javase/18/docs/api/overview-tree.html
#
hook global WinSetOption filetype=java %{
  require-module java

  declare-option -hidden completions java_hierarchy_completions

  set-option window completers option=java_hierarchy_completions %opt{completers}

  hook -group java-import-statements window InsertCompletionShow .* %{
    try %{
      execute-keys -draft 2b s \A^import<space>\z<ret>

      evaluate-commands -draft %{
        execute-keys h <a-i>w <a-semicolon>

        set-option window java_hierarchy_completions \
          "%val{cursor_line}.%val{cursor_column}+%val{selection_length}@%val{timestamp}"
      }
      # problem: maintain via module, package or overview-tree
      # problem: jdk.jdi module exports package com.sun.jdi, which has both Class and Interface Hierarchy
      #
      # first attempt worked well at the overview-tree level with all hierarchies
      #
      # lesson learnt: \(unwanted meta content\), \<Generics\>, show-whitespaces
      #                 kakoune buffer sluggish needed to divide and conquer
      #                 kakoune buffer multiple cursors, need to automate with macro
      #
      # observation: java language updates every six months
      #
      # style macro: 102l<a-t><space>i<ret><esc><esc>
      #
      # lesson learnt: style macro -- do it on one single page split groups with a unique token [bookends]
      #                then N * macro or shift and hold the macro key
      #                then <a-f>token<i><ret><esc> replace tokens with ' and repeat...
      #
      #                Note: unique bookend tokens allow for quick find and jump on the unique group
      #                alternatively using '' for bookends works well for an optimist.
      #
      # lesson learnt: style macro undo -- <a-f>'s$<ret>ds\h+<ret>c<space><esc> and repeat...
      #
      #                Now a word from our sponsor: Kakoune, the modal editor you know and love.
      #                                             So, stop breaking it. No!
      #
      #                                             shell stderr: <<<
      #                                             execve failed: 7
      #                                             >>>
      #
      #                A rebuttal from our sponsor: Kakoune, you break it, you fix it!
      #                                             Not a problem :).
      #
      # lesson learnt: divide and conquer the evaluate-commands blocks that overflow the environment.
      #
      # knowledge gained: $ echo | xargs --show-limits
      #
      # Screwtapello 2021, Kakhist: persist command history, history UI, discuss kakoune, viewed 02 July 2021,
      #                    <https://discuss.kakoune.com/t/kakhist-persist-command-history-history-ui/1795/6>
      #
      # lesson learnt: style macro in for loop for completers does not matter due to format var: $@ or "$@"
      #
      # -------------------------------------------------------------------------------------------- #
      # Stage 1 to 3 - Java Class Hierarchy
      # Stage 4 to 6 - Java Interface Hierarchy
      # Stage 7 to 7 - Java Annotation Interface Hierarchy
      # Stage 7 to 7 - Java Enum Class Hierarchy
      #
      # --------------- STAGE 1 OF 7 --------------------------------------------------------------- #
      evaluate-commands %sh{
        # --------------- JAVA CLASS HIERARCHY ----------------------------------------------------- #

        javaClassHierarchy_A='java.applet.Applet.AccessibleApplet java.awt.AlphaComposite
          java.awt.AWTError java.awt.AWTEvent java.awt.AWTEventMulticaster java.awt.AWTException
          java.awt.AWTKeyStroke java.awt.AWTPermission java.awt.BasicStroke java.awt.BorderLayout
          java.awt.BufferCapabilities java.awt.BufferCapabilities.FlipContents java.awt.Button
          java.awt.Button.AccessibleAWTButton java.awt.Canvas java.awt.Canvas.AccessibleAWTCanvas
          java.awt.CardLayout java.awt.Checkbox java.awt.Checkbox.AccessibleAWTCheckbox java.awt.CheckboxGroup
          java.awt.CheckboxMenuItem java.awt.CheckboxMenuItem.AccessibleAWTCheckboxMenuItem java.awt.Choice
          java.awt.Choice.AccessibleAWTChoice java.awt.Color java.awt.color.CMMException
          java.awt.color.ColorSpace java.awt.color.ICC_ColorSpace java.awt.color.ICC_Profile
          java.awt.color.ICC_ProfileGray java.awt.color.ICC_ProfileRGB java.awt.color.ProfileDataException
          java.awt.Component java.awt.Component.AccessibleAWTComponent
          java.awt.Component.AccessibleAWTComponent.AccessibleAWTComponentHandler
          java.awt.Component.AccessibleAWTComponent.AccessibleAWTFocusHandler
          java.awt.Component.BltBufferStrategy java.awt.Component.FlipBufferStrategy
          java.awt.ComponentOrientation java.awt.Container java.awt.Container.AccessibleAWTContainer
          java.awt.Container.AccessibleAWTContainer.AccessibleContainerHandler
          java.awt.ContainerOrderFocusTraversalPolicy java.awt.Cursor java.awt.datatransfer.Clipboard
          java.awt.datatransfer.DataFlavor java.awt.datatransfer.FlavorEvent
          java.awt.datatransfer.MimeTypeParseException java.awt.datatransfer.StringSelection
          java.awt.datatransfer.SystemFlavorMap java.awt.datatransfer.UnsupportedFlavorException
          java.awt.DefaultFocusTraversalPolicy java.awt.DefaultKeyboardFocusManager java.awt.Desktop
          java.awt.desktop.AboutEvent java.awt.desktop.AppEvent java.awt.desktop.AppForegroundEvent
          java.awt.desktop.AppHiddenEvent java.awt.desktop.AppReopenedEvent java.awt.desktop.FilesEvent
          java.awt.desktop.OpenFilesEvent java.awt.desktop.OpenURIEvent java.awt.desktop.PreferencesEvent
          java.awt.desktop.PrintFilesEvent java.awt.desktop.QuitEvent java.awt.desktop.ScreenSleepEvent
          java.awt.desktop.SystemSleepEvent java.awt.desktop.UserSessionEvent java.awt.Dialog
          java.awt.Dialog.AccessibleAWTDialog java.awt.Dimension java.awt.DisplayMode java.awt.dnd.DnDConstants
          java.awt.dnd.DragGestureEvent java.awt.dnd.DragGestureRecognizer java.awt.dnd.DragSource
          java.awt.dnd.DragSourceAdapter java.awt.dnd.DragSourceContext java.awt.dnd.DragSourceDragEvent
          java.awt.dnd.DragSourceDropEvent java.awt.dnd.DragSourceEvent java.awt.dnd.DropTarget
          java.awt.dnd.DropTargetAdapter java.awt.dnd.DropTargetContext
          java.awt.dnd.DropTargetContext.TransferableProxy java.awt.dnd.DropTargetDragEvent
          java.awt.dnd.DropTargetDropEvent java.awt.dnd.DropTarget.DropTargetAutoScroller
          java.awt.dnd.DropTargetEvent java.awt.dnd.InvalidDnDOperationException
          java.awt.dnd.MouseDragGestureRecognizer java.awt.event.ActionEvent
          java.awt.event.AdjustmentEvent java.awt.event.AWTEventListenerProxy java.awt.event.ComponentAdapter
          java.awt.event.ComponentEvent java.awt.event.ContainerAdapter java.awt.event.ContainerEvent
          java.awt.event.FocusAdapter java.awt.event.FocusEvent java.awt.event.HierarchyBoundsAdapter
          java.awt.event.HierarchyEvent java.awt.event.InputEvent java.awt.event.InputMethodEvent
          java.awt.event.InvocationEvent java.awt.event.ItemEvent java.awt.event.KeyAdapter
          java.awt.event.KeyEvent java.awt.event.MouseAdapter java.awt.event.MouseEvent
          java.awt.event.MouseMotionAdapter java.awt.event.MouseWheelEvent java.awt.event.PaintEvent
          java.awt.EventQueue java.awt.event.TextEvent java.awt.event.WindowAdapter java.awt.event.WindowEvent
          java.awt.FileDialog java.awt.FlowLayout java.awt.FocusTraversalPolicy java.awt.Font
          java.awt.font.FontRenderContext java.awt.FontFormatException java.awt.font.GlyphJustificationInfo
          java.awt.font.GlyphMetrics java.awt.font.GlyphVector java.awt.font.GraphicAttribute
          java.awt.font.ImageGraphicAttribute java.awt.font.LayoutPath java.awt.font.LineBreakMeasurer
          java.awt.font.LineMetrics java.awt.FontMetrics java.awt.font.NumericShaper
          java.awt.font.ShapeGraphicAttribute java.awt.font.TextAttribute java.awt.font.TextHitInfo
          java.awt.font.TextLayout java.awt.font.TextLayout.CaretPolicy java.awt.font.TextMeasurer
          java.awt.font.TransformAttribute java.awt.Frame java.awt.Frame.AccessibleAWTFrame
          java.awt.geom.AffineTransform java.awt.geom.Arc2D java.awt.geom.Arc2D.Double
          java.awt.geom.Arc2D.Float java.awt.geom.Area java.awt.geom.CubicCurve2D
          java.awt.geom.CubicCurve2D.Double java.awt.geom.CubicCurve2D.Float java.awt.geom.Dimension2D
          java.awt.geom.Ellipse2D java.awt.geom.Ellipse2D.Double java.awt.geom.Ellipse2D.Float
          java.awt.geom.FlatteningPathIterator java.awt.geom.GeneralPath
          java.awt.geom.IllegalPathStateException java.awt.geom.Line2D java.awt.geom.Line2D.Double
          java.awt.geom.Line2D.Float java.awt.geom.NoninvertibleTransformException java.awt.geom.Path2D
          java.awt.geom.Path2D.Double java.awt.geom.Path2D.Float java.awt.geom.Point2D
          java.awt.geom.Point2D.Double java.awt.geom.Point2D.Float java.awt.geom.QuadCurve2D
          java.awt.geom.QuadCurve2D.Double java.awt.geom.QuadCurve2D.Float java.awt.geom.Rectangle2D
          java.awt.geom.Rectangle2D.Double java.awt.geom.Rectangle2D.Float java.awt.geom.RectangularShape
          java.awt.geom.RoundRectangle2D java.awt.geom.RoundRectangle2D.Double
          java.awt.geom.RoundRectangle2D.Float java.awt.GradientPaint java.awt.Graphics java.awt.Graphics2D
          java.awt.GraphicsConfigTemplate java.awt.GraphicsConfiguration java.awt.GraphicsDevice
          java.awt.GraphicsEnvironment java.awt.GridBagConstraints java.awt.GridBagLayout
          java.awt.GridBagLayoutInfo java.awt.GridLayout java.awt.HeadlessException
          java.awt.IllegalComponentStateException java.awt.Image java.awt.image.AbstractMultiResolutionImage
          java.awt.image.AffineTransformOp java.awt.image.AreaAveragingScaleFilter java.awt.image.BandCombineOp
          java.awt.image.BandedSampleModel java.awt.image.BaseMultiResolutionImage java.awt.image.BufferedImage
          java.awt.image.BufferedImageFilter java.awt.image.BufferStrategy java.awt.image.ByteLookupTable
          java.awt.ImageCapabilities java.awt.image.ColorConvertOp java.awt.image.ColorModel
          java.awt.image.ComponentColorModel java.awt.image.ComponentSampleModel java.awt.image.ConvolveOp
          java.awt.image.CropImageFilter java.awt.image.DataBuffer java.awt.image.DataBufferByte
          java.awt.image.DataBufferDouble java.awt.image.DataBufferFloat java.awt.image.DataBufferInt
          java.awt.image.DataBufferShort java.awt.image.DataBufferUShort java.awt.image.DirectColorModel
          java.awt.image.FilteredImageSource java.awt.image.ImageFilter java.awt.image.ImagingOpException
          java.awt.image.IndexColorModel java.awt.image.Kernel java.awt.image.LookupOp
          java.awt.image.LookupTable java.awt.image.MemoryImageSource
          java.awt.image.MultiPixelPackedSampleModel java.awt.image.PackedColorModel
          java.awt.image.PixelGrabber java.awt.image.PixelInterleavedSampleModel java.awt.image.Raster
          java.awt.image.RasterFormatException java.awt.image.renderable.ParameterBlock
          java.awt.image.renderable.RenderableImageOp java.awt.image.renderable.RenderableImageProducer
          java.awt.image.renderable.RenderContext java.awt.image.ReplicateScaleFilter java.awt.image.RescaleOp
          java.awt.image.RGBImageFilter java.awt.image.SampleModel java.awt.image.ShortLookupTable
          java.awt.image.SinglePixelPackedSampleModel java.awt.image.VolatileImage
          java.awt.image.WritableRaster java.awt.im.InputContext java.awt.im.InputMethodHighlight
          java.awt.im.InputSubset java.awt.Insets java.awt.JobAttributes
          java.awt.JobAttributes.DefaultSelectionType java.awt.JobAttributes.DestinationType
          java.awt.JobAttributes.DialogType java.awt.JobAttributes.MultipleDocumentHandlingType
          java.awt.JobAttributes.SidesType java.awt.KeyboardFocusManager java.awt.Label
          java.awt.Label.AccessibleAWTLabel java.awt.LinearGradientPaint java.awt.List
          java.awt.List.AccessibleAWTList java.awt.List.AccessibleAWTList.AccessibleAWTListChild
          java.awt.MediaTracker java.awt.Menu java.awt.Menu.AccessibleAWTMenu java.awt.MenuBar
          java.awt.MenuBar.AccessibleAWTMenuBar java.awt.MenuComponent
          java.awt.MenuComponent.AccessibleAWTMenuComponent java.awt.MenuItem
          java.awt.MenuItem.AccessibleAWTMenuItem java.awt.MenuShortcut java.awt.MouseInfo
          java.awt.MultipleGradientPaint java.awt.PageAttributes java.awt.PageAttributes.ColorType
          java.awt.PageAttributes.MediaType java.awt.PageAttributes.OrientationRequestedType
          java.awt.PageAttributes.OriginType java.awt.PageAttributes.PrintQualityType java.awt.Panel
          java.awt.Panel.AccessibleAWTPanel java.awt.Point java.awt.PointerInfo java.awt.Polygon
          java.awt.PopupMenu java.awt.PopupMenu.AccessibleAWTPopupMenu java.awt.print.Book java.awt.PrintJob
          java.awt.print.PageFormat java.awt.print.Paper java.awt.print.PrinterAbortException
          java.awt.print.PrinterException java.awt.print.PrinterIOException java.awt.print.PrinterJob
          java.awt.RadialGradientPaint java.awt.Rectangle java.awt.RenderingHints java.awt.RenderingHints.Key
          java.awt.Robot java.awt.Scrollbar java.awt.Scrollbar.AccessibleAWTScrollBar java.awt.ScrollPane
          java.awt.ScrollPane.AccessibleAWTScrollPane java.awt.ScrollPaneAdjustable java.awt.SplashScreen
          java.awt.SystemColor java.awt.SystemTray java.awt.Taskbar java.awt.TextArea
          java.awt.TextArea.AccessibleAWTTextArea java.awt.TextComponent
          java.awt.TextComponent.AccessibleAWTTextComponent java.awt.TextField
          java.awt.TextField.AccessibleAWTTextField java.awt.TexturePaint java.awt.Toolkit java.awt.TrayIcon
          java.awt.Window java.awt.Window.AccessibleAWTWindow'

        javaClassHierarchy_B='java.beans.beancontext.BeanContextChildSupport
          java.beans.beancontext.BeanContextEvent java.beans.beancontext.BeanContextMembershipEvent
          java.beans.beancontext.BeanContextServiceAvailableEvent
          java.beans.beancontext.BeanContextServiceRevokedEvent
          java.beans.beancontext.BeanContextServicesSupport
          java.beans.beancontext.BeanContextServicesSupport.BCSSChild
          java.beans.beancontext.BeanContextServicesSupport.BCSSProxyServiceProvider
          java.beans.beancontext.BeanContextServicesSupport.BCSSServiceProvider
          java.beans.beancontext.BeanContextSupport java.beans.beancontext.BeanContextSupport.BCSChild
          java.beans.beancontext.BeanContextSupport.BCSIterator java.beans.BeanDescriptor java.beans.Beans
          java.beans.DefaultPersistenceDelegate java.beans.Encoder java.beans.EventHandler
          java.beans.EventSetDescriptor java.beans.Expression java.beans.FeatureDescriptor
          java.beans.IndexedPropertyChangeEvent java.beans.IndexedPropertyDescriptor
          java.beans.IntrospectionException java.beans.Introspector java.beans.MethodDescriptor
          java.beans.ParameterDescriptor java.beans.PersistenceDelegate java.beans.PropertyChangeEvent
          java.beans.PropertyChangeListenerProxy java.beans.PropertyChangeSupport java.beans.PropertyDescriptor
          java.beans.PropertyEditorManager java.beans.PropertyEditorSupport java.beans.PropertyVetoException
          java.beans.SimpleBeanInfo java.beans.Statement java.beans.VetoableChangeListenerProxy
          java.beans.VetoableChangeSupport java.beans.XMLDecoder java.beans.XMLEncoder'

        javaClassHierarchy_C='java.io.BufferedInputStream java.io.BufferedOutputStream java.io.BufferedReader
          java.io.BufferedWriter java.io.ByteArrayInputStream java.io.ByteArrayOutputStream
          java.io.CharArrayReader java.io.CharArrayWriter java.io.CharConversionException java.io.Console
          java.io.DataInputStream java.io.DataOutputStream java.io.EOFException java.io.File
          java.io.FileDescriptor java.io.FileInputStream java.io.FileNotFoundException java.io.FileOutputStream
          java.io.FilePermission java.io.FileReader java.io.FileWriter java.io.FilterInputStream
          java.io.FilterOutputStream java.io.FilterReader java.io.FilterWriter java.io.InputStream
          java.io.InputStreamReader java.io.InterruptedIOException java.io.InvalidClassException
          java.io.InvalidObjectException java.io.IOError java.io.IOException
          java.io.LineNumberReader java.io.NotActiveException java.io.NotSerializableException
          java.io.ObjectInputFilter.Config java.io.ObjectInputStream java.io.ObjectInputStream.GetField
          java.io.ObjectOutputStream java.io.ObjectOutputStream.PutField java.io.ObjectStreamClass
          java.io.ObjectStreamException java.io.ObjectStreamField java.io.OptionalDataException
          java.io.OutputStream java.io.OutputStreamWriter java.io.PipedInputStream java.io.PipedOutputStream
          java.io.PipedReader java.io.PipedWriter java.io.PrintStream java.io.PrintWriter
          java.io.PushbackInputStream java.io.PushbackReader java.io.RandomAccessFile java.io.Reader
          java.io.SequenceInputStream java.io.SerializablePermission java.io.StreamCorruptedException
          java.io.StreamTokenizer java.io.StringReader java.io.StringWriter
          java.io.SyncFailedException java.io.UncheckedIOException java.io.UnsupportedEncodingException
          java.io.UTFDataFormatException java.io.WriteAbortedException java.io.Writer'

        javaClassHierarchy_D='java.lang.AbstractMethodError java.lang.annotation.AnnotationFormatError
          java.lang.annotation.AnnotationTypeMismatchException
          java.lang.annotation.IncompleteAnnotationException java.lang.ArithmeticException
          java.lang.ArrayIndexOutOfBoundsException java.lang.ArrayStoreException java.lang.AssertionError
          java.lang.Boolean java.lang.BootstrapMethodError java.lang.Byte java.lang.Character
          java.lang.Character.Subset java.lang.Character.UnicodeBlock java.lang.Class
          java.lang.ClassCastException java.lang.ClassCircularityError java.lang.ClassFormatError
          java.lang.ClassLoader java.lang.ClassNotFoundException java.lang.ClassValue
          java.lang.CloneNotSupportedException java.lang.constant.ConstantDescs
          java.lang.constant.DynamicCallSiteDesc java.lang.constant.DynamicConstantDesc java.lang.Double
          java.lang.Enum java.lang.EnumConstantNotPresentException java.lang.Enum.EnumDesc java.lang.Error
          java.lang.Exception java.lang.ExceptionInInitializerError java.lang.Float
          java.lang.IllegalAccessError java.lang.IllegalAccessException java.lang.IllegalArgumentException
          java.lang.IllegalCallerException java.lang.IllegalMonitorStateException
          java.lang.IllegalStateException java.lang.IllegalThreadStateException
          java.lang.IncompatibleClassChangeError java.lang.IndexOutOfBoundsException
          java.lang.InheritableThreadLocal java.lang.InstantiationError java.lang.InstantiationException
          java.lang.instrument.ClassDefinition java.lang.instrument.IllegalClassFormatException
          java.lang.instrument.UnmodifiableClassException java.lang.instrument.UnmodifiableModuleException
          java.lang.Integer java.lang.InternalError java.lang.InterruptedException java.lang.invoke.CallSite
          java.lang.invoke.ConstantBootstraps java.lang.invoke.ConstantCallSite
          java.lang.invoke.LambdaConversionException java.lang.invoke.LambdaMetafactory
          java.lang.invoke.MethodHandle java.lang.invoke.MethodHandleProxies java.lang.invoke.MethodHandles
          java.lang.invoke.MethodHandles.Lookup java.lang.invoke.MethodType java.lang.invoke.MutableCallSite
          java.lang.invoke.SerializedLambda java.lang.invoke.StringConcatException
          java.lang.invoke.StringConcatFactory java.lang.invoke.SwitchPoint java.lang.invoke.VarHandle
          java.lang.invoke.VarHandle.VarHandleDesc java.lang.invoke.VolatileCallSite
          java.lang.invoke.WrongMethodTypeException java.lang.LayerInstantiationException
          java.lang.LinkageError java.lang.Long java.lang.management.LockInfo
          java.lang.management.ManagementFactory java.lang.management.ManagementPermission
          java.lang.management.MemoryNotificationInfo java.lang.management.MemoryUsage
          java.lang.management.MonitorInfo java.lang.management.ThreadInfo java.lang.Math java.lang.Module
          java.lang.module.Configuration java.lang.module.FindException
          java.lang.module.InvalidModuleDescriptorException java.lang.ModuleLayer
          java.lang.ModuleLayer.Controller java.lang.module.ModuleDescriptor
          java.lang.module.ModuleDescriptor.Builder java.lang.module.ModuleDescriptor.Exports
          java.lang.module.ModuleDescriptor.Opens java.lang.module.ModuleDescriptor.Provides
          java.lang.module.ModuleDescriptor.Requires java.lang.module.ModuleDescriptor.Version
          java.lang.module.ModuleReference java.lang.module.ResolutionException java.lang.module.ResolvedModule
          java.lang.NegativeArraySizeException java.lang.NoClassDefFoundError java.lang.NoSuchFieldError
          java.lang.NoSuchFieldException java.lang.NoSuchMethodError java.lang.NoSuchMethodException
          java.lang.NullPointerException java.lang.Number java.lang.NumberFormatException java.lang.Object
          java.lang.OutOfMemoryError java.lang.Package java.lang.Process java.lang.ProcessBuilder
          java.lang.ProcessBuilder.Redirect java.lang.Record java.lang.ref.Cleaner
          java.lang.reflect.AccessibleObject java.lang.reflect.Array java.lang.reflect.Constructor
          java.lang.reflect.Executable java.lang.reflect.Field java.lang.reflect.GenericSignatureFormatError
          java.lang.reflect.InaccessibleObjectException java.lang.reflect.InvocationTargetException
          java.lang.ReflectiveOperationException java.lang.reflect.MalformedParameterizedTypeException
          java.lang.reflect.MalformedParametersException java.lang.reflect.Method java.lang.reflect.Modifier
          java.lang.reflect.Parameter java.lang.reflect.Proxy java.lang.reflect.RecordComponent
          java.lang.reflect.ReflectPermission java.lang.reflect.UndeclaredThrowableException
          java.lang.ref.PhantomReference java.lang.ref.Reference java.lang.ref.ReferenceQueue
          java.lang.ref.SoftReference java.lang.ref.WeakReference java.lang.Runtime java.lang.RuntimeException
          java.lang.runtime.ObjectMethods java.lang.RuntimePermission java.lang.Runtime.Version
          java.lang.SecurityException java.lang.Short java.lang.StackOverflowError
          java.lang.StackTraceElement java.lang.StackWalker java.lang.StrictMath java.lang.String
          java.lang.StringBuffer java.lang.StringBuilder java.lang.StringIndexOutOfBoundsException
          java.lang.System java.lang.System.LoggerFinder java.lang.Thread java.lang.ThreadDeath
          java.lang.ThreadGroup java.lang.ThreadLocal java.lang.Throwable java.lang.TypeNotPresentException
          java.lang.UnknownError java.lang.UnsatisfiedLinkError java.lang.UnsupportedClassVersionError
          java.lang.UnsupportedOperationException java.lang.VerifyError java.lang.VirtualMachineError
          java.lang.Void'

        javaClassHierarchy_E='java.math.BigDecimal java.math.BigInteger java.math.MathContext'

        javaClassHierarchy_F='java.net.Authenticator java.net.BindException
          java.net.CacheRequest java.net.CacheResponse java.net.ConnectException java.net.ContentHandler
          java.net.CookieHandler java.net.CookieManager java.net.DatagramPacket java.net.DatagramSocket
          java.net.DatagramSocketImpl java.net.HttpCookie java.net.http.HttpClient
          java.net.http.HttpConnectTimeoutException java.net.http.HttpHeaders java.net.http.HttpRequest
          java.net.http.HttpRequest.BodyPublishers java.net.http.HttpResponse.BodyHandlers
          java.net.http.HttpResponse.BodySubscribers java.net.http.HttpTimeoutException
          java.net.HttpRetryException java.net.HttpURLConnection java.net.http.WebSocketHandshakeException
          java.net.IDN java.net.Inet4Address java.net.Inet6Address java.net.InetAddress
          java.net.InetSocketAddress java.net.UnixDomainSocketAddress java.net.InterfaceAddress
          java.net.JarURLConnection java.net.MalformedURLException java.net.MulticastSocket
          java.net.NetPermission java.net.NetworkInterface java.net.NoRouteToHostException
          java.net.PasswordAuthentication java.net.PortUnreachableException java.net.ProtocolException
          java.net.Proxy java.net.ProxySelector java.net.ResponseCache java.net.SecureCacheResponse
          java.net.ServerSocket java.net.Socket java.net.SocketAddress java.net.SocketException
          java.net.SocketImpl java.net.SocketPermission java.net.SocketTimeoutException
          java.net.spi.URLStreamHandlerProvider java.net.StandardSocketOptions java.net.UnknownHostException
          java.net.UnknownServiceException java.net.URI java.net.URISyntaxException java.net.URL
          java.net.URLClassLoader java.net.URLConnection java.net.URLDecoder java.net.URLEncoder
          java.net.URLPermission java.net.URLStreamHandler'

        javaClassHierarchy_G='java.nio.Buffer java.nio.BufferOverflowException
          java.nio.BufferUnderflowException java.nio.ByteBuffer java.nio.ByteOrder
          java.nio.channels.AcceptPendingException java.nio.channels.AlreadyBoundException
          java.nio.channels.AlreadyConnectedException java.nio.channels.AsynchronousChannelGroup
          java.nio.channels.AsynchronousCloseException java.nio.channels.AsynchronousFileChannel
          java.nio.channels.AsynchronousServerSocketChannel java.nio.channels.AsynchronousSocketChannel
          java.nio.channels.CancelledKeyException java.nio.channels.Channels
          java.nio.channels.ClosedByInterruptException java.nio.channels.ClosedChannelException
          java.nio.channels.ClosedSelectorException java.nio.channels.ConnectionPendingException
          java.nio.channels.DatagramChannel java.nio.channels.FileChannel java.nio.channels.FileChannel.MapMode
          java.nio.channels.FileLock java.nio.channels.FileLockInterruptionException
          java.nio.channels.IllegalBlockingModeException java.nio.channels.IllegalChannelGroupException
          java.nio.channels.IllegalSelectorException java.nio.channels.InterruptedByTimeoutException
          java.nio.channels.MembershipKey java.nio.channels.NoConnectionPendingException
          java.nio.channels.NonReadableChannelException java.nio.channels.NonWritableChannelException
          java.nio.channels.NotYetBoundException java.nio.channels.NotYetConnectedException
          java.nio.channels.OverlappingFileLockException java.nio.channels.Pipe
          java.nio.channels.Pipe.SinkChannel java.nio.channels.Pipe.SourceChannel
          java.nio.channels.ReadPendingException java.nio.channels.SelectableChannel
          java.nio.channels.SelectionKey java.nio.channels.Selector java.nio.channels.ServerSocketChannel
          java.nio.channels.ShutdownChannelGroupException java.nio.channels.SocketChannel
          java.nio.channels.spi.AbstractInterruptibleChannel java.nio.channels.spi.AbstractSelectableChannel
          java.nio.channels.spi.AbstractSelectionKey java.nio.channels.spi.AbstractSelector
          java.nio.channels.spi.AsynchronousChannelProvider java.nio.channels.spi.SelectorProvider
          java.nio.channels.UnresolvedAddressException java.nio.channels.UnsupportedAddressTypeException
          java.nio.channels.WritePendingException java.nio.CharBuffer java.nio.charset.CharacterCodingException
          java.nio.charset.Charset java.nio.charset.CharsetDecoder java.nio.charset.CharsetEncoder
          java.nio.charset.CoderMalfunctionError java.nio.charset.CoderResult
          java.nio.charset.CodingErrorAction java.nio.charset.IllegalCharsetNameException
          java.nio.charset.MalformedInputException java.nio.charset.spi.CharsetProvider
          java.nio.charset.StandardCharsets java.nio.charset.UnmappableCharacterException
          java.nio.charset.UnsupportedCharsetException java.nio.DoubleBuffer
          java.nio.file.AccessDeniedException java.nio.file.AtomicMoveNotSupportedException
          java.nio.file.attribute.AclEntry java.nio.file.attribute.AclEntry.Builder
          java.nio.file.attribute.FileTime java.nio.file.attribute.PosixFilePermissions
          java.nio.file.attribute.UserPrincipalLookupService
          java.nio.file.attribute.UserPrincipalNotFoundException java.nio.file.ClosedDirectoryStreamException
          java.nio.file.ClosedFileSystemException java.nio.file.ClosedWatchServiceException
          java.nio.file.DirectoryIteratorException java.nio.file.DirectoryNotEmptyException
          java.nio.file.FileAlreadyExistsException java.nio.file.Files java.nio.file.FileStore
          java.nio.file.FileSystem java.nio.file.FileSystemAlreadyExistsException
          java.nio.file.FileSystemException java.nio.file.FileSystemLoopException
          java.nio.file.FileSystemNotFoundException java.nio.file.FileSystems
          java.nio.file.InvalidPathException java.nio.file.LinkPermission java.nio.file.NoSuchFileException
          java.nio.file.NotDirectoryException java.nio.file.NotLinkException java.nio.file.Paths
          java.nio.file.ProviderMismatchException java.nio.file.ProviderNotFoundException
          java.nio.file.ReadOnlyFileSystemException java.nio.file.SimpleFileVisitor
          java.nio.file.spi.FileSystemProvider java.nio.file.spi.FileTypeDetector
          java.nio.file.StandardWatchEventKinds java.nio.FloatBuffer java.nio.IntBuffer
          java.nio.InvalidMarkException java.nio.LongBuffer java.nio.MappedByteBuffer
          java.nio.ReadOnlyBufferException java.nio.ShortBuffer'

        javaClassHierarchy_H='java.rmi.AccessException java.rmi.activation.Activatable
          java.rmi.activation.ActivateFailedException java.rmi.activation.ActivationDesc
          java.rmi.activation.ActivationException java.rmi.activation.ActivationGroup
          java.rmi.activation.ActivationGroupDesc java.rmi.activation.ActivationGroupDesc.CommandEnvironment
          java.rmi.activation.ActivationGroupID java.rmi.activation.ActivationGroup_Stub
          java.rmi.activation.ActivationID java.rmi.activation.UnknownGroupException
          java.rmi.activation.UnknownObjectException java.rmi.AlreadyBoundException java.rmi.ConnectException
          java.rmi.ConnectIOException java.rmi.dgc.Lease java.rmi.dgc.VMID java.rmi.MarshalException
          java.rmi.MarshalledObject java.rmi.Naming java.rmi.NoSuchObjectException java.rmi.NotBoundException
          java.rmi.registry.LocateRegistry java.rmi.RemoteException
          java.rmi.ServerError java.rmi.ServerException
          java.rmi.server.ExportException java.rmi.server.ObjID
          java.rmi.server.RemoteObject java.rmi.server.RemoteObjectInvocationHandler
          java.rmi.server.RemoteServer java.rmi.server.RMIClassLoader
          java.rmi.server.RMIClassLoaderSpi java.rmi.server.RMISocketFactory
          java.rmi.server.ServerCloneException java.rmi.server.ServerNotActiveException
          java.rmi.server.UID java.rmi.server.UnicastRemoteObject
          java.rmi.StubNotFoundException java.rmi.UnexpectedException java.rmi.UnknownHostException
          java.rmi.UnmarshalException'

        javaClassHierarchy_I='java.security.AlgorithmParameterGenerator
          java.security.AlgorithmParameterGeneratorSpi java.security.AlgorithmParameters
          java.security.AlgorithmParametersSpi java.security.AllPermission java.security.AuthProvider
          java.security.BasicPermission java.security.cert.Certificate
          java.security.cert.Certificate.CertificateRep java.security.cert.CertificateEncodingException
          java.security.cert.CertificateException java.security.cert.CertificateExpiredException
          java.security.cert.CertificateFactory java.security.cert.CertificateFactorySpi
          java.security.cert.CertificateNotYetValidException java.security.cert.CertificateParsingException
          java.security.cert.CertificateRevokedException java.security.cert.CertPath
          java.security.cert.CertPathBuilder java.security.cert.CertPathBuilderException
          java.security.cert.CertPathBuilderSpi java.security.cert.CertPath.CertPathRep
          java.security.cert.CertPathValidator java.security.cert.CertPathValidatorException
          java.security.cert.CertPathValidatorSpi java.security.cert.CertStore
          java.security.cert.CertStoreException java.security.cert.CertStoreSpi
          java.security.cert.CollectionCertStoreParameters java.security.cert.CRL
          java.security.cert.CRLException java.security.cert.LDAPCertStoreParameters
          java.security.cert.PKIXBuilderParameters java.security.cert.PKIXCertPathBuilderResult
          java.security.cert.PKIXCertPathChecker java.security.cert.PKIXCertPathValidatorResult
          java.security.cert.PKIXParameters java.security.cert.PKIXRevocationChecker
          java.security.cert.PolicyQualifierInfo java.security.cert.TrustAnchor
          java.security.cert.URICertStoreParameters java.security.cert.X509Certificate
          java.security.cert.X509CertSelector java.security.cert.X509CRL java.security.cert.X509CRLEntry
          java.security.cert.X509CRLSelector java.security.CodeSigner java.security.CodeSource
          java.security.DigestException java.security.DigestInputStream java.security.DigestOutputStream
          java.security.DomainLoadStoreParameter java.security.DrbgParameters
          java.security.DrbgParameters.Instantiation java.security.DrbgParameters.NextBytes
          java.security.DrbgParameters.Reseed java.security.GeneralSecurityException
          java.security.GuardedObject
          java.security.InvalidAlgorithmParameterException java.security.InvalidKeyException
          java.security.InvalidParameterException java.security.KeyException java.security.KeyFactory
          java.security.KeyFactorySpi java.security.KeyManagementException java.security.KeyPair
          java.security.KeyPairGenerator java.security.KeyPairGeneratorSpi java.security.KeyRep
          java.security.KeyStore java.security.KeyStore.Builder
          java.security.KeyStore.CallbackHandlerProtection java.security.KeyStoreException
          java.security.KeyStore.PasswordProtection java.security.KeyStore.PrivateKeyEntry
          java.security.KeyStore.SecretKeyEntry java.security.KeyStoreSpi
          java.security.KeyStore.TrustedCertificateEntry java.security.MessageDigest
          java.security.MessageDigestSpi java.security.NoSuchAlgorithmException
          java.security.NoSuchProviderException java.security.Permission java.security.PermissionCollection
          java.security.Permissions java.security.PKCS12Attribute
          java.security.PrivilegedActionException java.security.ProtectionDomain java.security.Provider
          java.security.ProviderException java.security.Provider.Service java.security.SecureClassLoader
          java.security.SecureRandom java.security.SecureRandomSpi java.security.Security
          java.security.SecurityPermission java.security.Signature java.security.SignatureException
          java.security.SignatureSpi java.security.SignedObject
          java.security.spec.DSAGenParameterSpec java.security.spec.DSAParameterSpec
          java.security.spec.DSAPrivateKeySpec java.security.spec.DSAPublicKeySpec
          java.security.spec.ECFieldF2m java.security.spec.ECFieldFp java.security.spec.ECGenParameterSpec
          java.security.spec.ECParameterSpec java.security.spec.ECPoint java.security.spec.ECPrivateKeySpec
          java.security.spec.ECPublicKeySpec java.security.spec.EdDSAParameterSpec java.security.spec.EdECPoint
          java.security.spec.EdECPrivateKeySpec java.security.spec.EdECPublicKeySpec
          java.security.spec.EllipticCurve java.security.spec.EncodedKeySpec
          java.security.spec.InvalidKeySpecException java.security.spec.InvalidParameterSpecException
          java.security.spec.MGF1ParameterSpec java.security.spec.NamedParameterSpec
          java.security.spec.PKCS8EncodedKeySpec java.security.spec.PSSParameterSpec
          java.security.spec.RSAKeyGenParameterSpec java.security.spec.RSAMultiPrimePrivateCrtKeySpec
          java.security.spec.RSAOtherPrimeInfo java.security.spec.RSAPrivateCrtKeySpec
          java.security.spec.RSAPrivateKeySpec java.security.spec.RSAPublicKeySpec
          java.security.spec.X509EncodedKeySpec java.security.spec.XECPrivateKeySpec
          java.security.spec.XECPublicKeySpec java.security.Timestamp java.security.UnrecoverableEntryException
          java.security.UnrecoverableKeyException java.security.UnresolvedPermission
          java.security.URIParameter'

        javaClassHierarchy_J='java.sql.BatchUpdateException java.sql.DataTruncation
          java.sql.Date java.sql.DriverManager java.sql.DriverPropertyInfo
          java.sql.SQLClientInfoException java.sql.SQLDataException java.sql.SQLException
          java.sql.SQLFeatureNotSupportedException java.sql.SQLIntegrityConstraintViolationException
          java.sql.SQLInvalidAuthorizationSpecException java.sql.SQLNonTransientConnectionException
          java.sql.SQLNonTransientException java.sql.SQLPermission java.sql.SQLRecoverableException
          java.sql.SQLSyntaxErrorException java.sql.SQLTimeoutException
          java.sql.SQLTransactionRollbackException java.sql.SQLTransientConnectionException
          java.sql.SQLTransientException java.sql.SQLWarning java.sql.Time java.sql.Timestamp java.sql.Types'

        javaClassHierarchy_K='java.text.Annotation java.text.AttributedCharacterIterator.Attribute
          java.text.AttributedString java.text.Bidi java.text.BreakIterator java.text.ChoiceFormat
          java.text.CollationElementIterator java.text.CollationKey java.text.Collator
          java.text.CompactNumberFormat java.text.DateFormat java.text.DateFormat.Field
          java.text.DateFormatSymbols java.text.DecimalFormat java.text.DecimalFormatSymbols
          java.text.FieldPosition java.text.Format java.text.Format.Field java.text.MessageFormat
          java.text.MessageFormat.Field java.text.Normalizer java.text.NumberFormat
          java.text.NumberFormat.Field java.text.ParseException java.text.ParsePosition
          java.text.RuleBasedCollator java.text.SimpleDateFormat java.text.spi.BreakIteratorProvider
          java.text.spi.CollatorProvider java.text.spi.DateFormatProvider
          java.text.spi.DateFormatSymbolsProvider java.text.spi.DecimalFormatSymbolsProvider
          java.text.spi.NumberFormatProvider java.text.StringCharacterIterator'

        javaClassHierarchy_L='java.time.chrono.AbstractChronology java.time.chrono.HijrahChronology
          java.time.chrono.HijrahDate java.time.chrono.IsoChronology java.time.chrono.JapaneseChronology
          java.time.chrono.JapaneseDate java.time.chrono.JapaneseEra java.time.chrono.MinguoChronology
          java.time.chrono.MinguoDate java.time.chrono.ThaiBuddhistChronology java.time.chrono.ThaiBuddhistDate
          java.time.Clock java.time.DateTimeException java.time.Duration java.time.format.DateTimeFormatter
          java.time.format.DateTimeFormatterBuilder java.time.format.DateTimeParseException
          java.time.format.DecimalStyle java.time.Instant java.time.LocalDate java.time.LocalDateTime
          java.time.LocalTime java.time.MonthDay java.time.OffsetDateTime java.time.OffsetTime java.time.Period
          java.time.temporal.IsoFields java.time.temporal.JulianFields java.time.temporal.TemporalAdjusters
          java.time.temporal.TemporalQueries java.time.temporal.UnsupportedTemporalTypeException
          java.time.temporal.ValueRange java.time.temporal.WeekFields java.time.Year java.time.YearMonth
          java.time.ZonedDateTime java.time.ZoneId java.time.ZoneOffset java.time.zone.ZoneOffsetTransition
          java.time.zone.ZoneOffsetTransitionRule java.time.zone.ZoneRules java.time.zone.ZoneRulesException
          java.time.zone.ZoneRulesProvider'

        javaClassHierarchy_M='java.util.AbstractCollection java.util.AbstractList java.util.AbstractMap
          java.util.AbstractMap.SimpleEntry java.util.AbstractMap.SimpleImmutableEntry
          java.util.AbstractQueue java.util.AbstractSequentialList java.util.AbstractSet
          java.util.ArrayDeque java.util.ArrayList java.util.Arrays java.util.Base64
          java.util.Base64.Decoder java.util.Base64.Encoder java.util.BitSet java.util.Calendar
          java.util.Calendar.Builder java.util.Collections java.util.concurrent.AbstractExecutorService
          java.util.concurrent.ArrayBlockingQueue java.util.concurrent.atomic.AtomicBoolean
          java.util.concurrent.atomic.AtomicInteger java.util.concurrent.atomic.AtomicIntegerArray
          java.util.concurrent.atomic.AtomicIntegerFieldUpdater java.util.concurrent.atomic.AtomicLong
          java.util.concurrent.atomic.AtomicLongArray java.util.concurrent.atomic.AtomicLongFieldUpdater
          java.util.concurrent.atomic.AtomicMarkableReference java.util.concurrent.atomic.AtomicReference
          java.util.concurrent.atomic.AtomicReferenceArray
          java.util.concurrent.atomic.AtomicReferenceFieldUpdater
          java.util.concurrent.atomic.AtomicStampedReference java.util.concurrent.atomic.DoubleAccumulator
          java.util.concurrent.atomic.DoubleAdder java.util.concurrent.atomic.LongAccumulator
          java.util.concurrent.atomic.LongAdder java.util.concurrent.BrokenBarrierException
          java.util.concurrent.CancellationException java.util.concurrent.CompletableFuture
          java.util.concurrent.CompletionException java.util.concurrent.ConcurrentHashMap
          java.util.concurrent.ConcurrentHashMap.KeySetView java.util.concurrent.ConcurrentLinkedDeque
          java.util.concurrent.ConcurrentLinkedQueue java.util.concurrent.ConcurrentSkipListMap
          java.util.concurrent.ConcurrentSkipListSet java.util.concurrent.CopyOnWriteArrayList
          java.util.concurrent.CopyOnWriteArraySet java.util.concurrent.CountDownLatch
          java.util.concurrent.CountedCompleter java.util.concurrent.CyclicBarrier
          java.util.concurrent.DelayQueue java.util.concurrent.Exchanger
          java.util.concurrent.ExecutionException java.util.concurrent.ExecutorCompletionService
          java.util.concurrent.Executors java.util.concurrent.Flow java.util.concurrent.ForkJoinPool
          java.util.concurrent.ForkJoinTask java.util.concurrent.ForkJoinWorkerThread
          java.util.concurrent.FutureTask java.util.concurrent.LinkedBlockingDeque
          java.util.concurrent.LinkedBlockingQueue java.util.concurrent.LinkedTransferQueue
          java.util.concurrent.locks.AbstractOwnableSynchronizer
          java.util.concurrent.locks.AbstractQueuedLongSynchronizer
          java.util.concurrent.locks.AbstractQueuedLongSynchronizer.ConditionObject
          java.util.concurrent.locks.AbstractQueuedSynchronizer
          java.util.concurrent.locks.AbstractQueuedSynchronizer.ConditionObject
          java.util.concurrent.locks.LockSupport java.util.concurrent.locks.ReentrantLock
          java.util.concurrent.locks.ReentrantReadWriteLock
          java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock
          java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock java.util.concurrent.locks.StampedLock
          java.util.ConcurrentModificationException java.util.concurrent.Phaser
          java.util.concurrent.PriorityBlockingQueue java.util.concurrent.RecursiveAction
          java.util.concurrent.RecursiveTask java.util.concurrent.RejectedExecutionException
          java.util.concurrent.ScheduledThreadPoolExecutor java.util.concurrent.Semaphore
          java.util.concurrent.SubmissionPublisher java.util.concurrent.SynchronousQueue
          java.util.concurrent.ThreadLocalRandom java.util.concurrent.ThreadPoolExecutor
          java.util.concurrent.ThreadPoolExecutor.AbortPolicy
          java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy
          java.util.concurrent.ThreadPoolExecutor.DiscardOldestPolicy
          java.util.concurrent.ThreadPoolExecutor.DiscardPolicy java.util.concurrent.TimeoutException
          java.util.Currency java.util.Date java.util.Dictionary java.util.DoubleSummaryStatistics
          java.util.DuplicateFormatFlagsException java.util.EmptyStackException java.util.EnumMap
          java.util.EnumSet java.util.EventListenerProxy java.util.EventObject
          java.util.FormatFlagsConversionMismatchException java.util.FormattableFlags java.util.Formatter
          java.util.FormatterClosedException java.util.GregorianCalendar java.util.HashMap java.util.HashSet
          java.util.Hashtable java.util.IdentityHashMap java.util.IllegalFormatCodePointException
          java.util.IllegalFormatConversionException java.util.IllegalFormatException
          java.util.IllegalFormatFlagsException java.util.IllegalFormatPrecisionException
          java.util.IllegalFormatWidthException java.util.IllformedLocaleException
          java.util.InputMismatchException java.util.IntSummaryStatistics
          java.util.InvalidPropertiesFormatException java.util.jar.Attributes java.util.jar.Attributes.Name
          java.util.jar.JarEntry java.util.jar.JarException java.util.jar.JarFile java.util.jar.JarInputStream
          java.util.jar.JarOutputStream java.util.jar.Manifest java.util.LinkedHashMap java.util.LinkedHashSet
          java.util.LinkedList java.util.ListResourceBundle java.util.Locale java.util.Locale.Builder
          java.util.Locale.LanguageRange java.util.logging.ConsoleHandler java.util.logging.ErrorManager
          java.util.logging.FileHandler java.util.logging.Formatter java.util.logging.Handler
          java.util.logging.Level java.util.logging.Logger java.util.logging.LoggingPermission
          java.util.logging.LogManager java.util.logging.LogRecord java.util.logging.MemoryHandler
          java.util.logging.SimpleFormatter java.util.logging.SocketHandler java.util.logging.StreamHandler
          java.util.logging.XMLFormatter java.util.LongSummaryStatistics
          java.util.MissingFormatArgumentException java.util.MissingFormatWidthException
          java.util.MissingResourceException java.util.NoSuchElementException java.util.Objects
          java.util.Optional java.util.OptionalDouble java.util.OptionalInt
          java.util.OptionalLong java.util.prefs.AbstractPreferences java.util.prefs.BackingStoreException
          java.util.prefs.InvalidPreferencesFormatException java.util.prefs.NodeChangeEvent
          java.util.prefs.PreferenceChangeEvent java.util.prefs.Preferences java.util.PriorityQueue
          java.util.Properties java.util.PropertyPermission java.util.PropertyResourceBundle java.util.Random
          java.util.regex.Matcher java.util.regex.Pattern java.util.regex.PatternSyntaxException
          java.util.ResourceBundle java.util.ResourceBundle.Control java.util.Scanner
          java.util.ServiceConfigurationError java.util.ServiceLoader java.util.SimpleTimeZone
          java.util.spi.AbstractResourceBundleProvider java.util.spi.CalendarDataProvider
          java.util.spi.CalendarNameProvider java.util.spi.CurrencyNameProvider
          java.util.spi.LocaleNameProvider java.util.spi.LocaleServiceProvider
          java.util.spi.TimeZoneNameProvider java.util.Spliterators
          java.util.Spliterators.AbstractDoubleSpliterator java.util.Spliterators.AbstractIntSpliterator
          java.util.Spliterators.AbstractLongSpliterator java.util.Spliterators.AbstractSpliterator
          java.util.SplittableRandom java.util.Stack java.util.stream.Collectors java.util.stream.StreamSupport
          java.util.StringJoiner java.util.StringTokenizer java.util.Timer java.util.TimerTask
          java.util.TimeZone java.util.TooManyListenersException java.util.TreeMap java.util.TreeSet
          java.util.UnknownFormatConversionException java.util.UnknownFormatFlagsException java.util.UUID
          java.util.Vector java.util.WeakHashMap java.util.zip.Adler32 java.util.zip.CheckedInputStream
          java.util.zip.CheckedOutputStream java.util.zip.CRC32 java.util.zip.CRC32C
          java.util.zip.DataFormatException java.util.zip.Deflater java.util.zip.DeflaterInputStream
          java.util.zip.DeflaterOutputStream java.util.zip.GZIPInputStream java.util.zip.GZIPOutputStream
          java.util.zip.Inflater java.util.zip.InflaterInputStream java.util.zip.InflaterOutputStream
          java.util.zip.ZipEntry java.util.zip.ZipError java.util.zip.ZipException java.util.zip.ZipFile
          java.util.zip.ZipInputStream java.util.zip.ZipOutputStream'

        # ------------------------------------------------------------------------------------------ #
        print_completers() {

          format=' %s\;||%s'

          for import in $@
          do
            printf "$format" "$import" "$import"
          done

        }
        # ------------------------------------------------------------------------------------------ #
        printf "set-option -add window java_hierarchy_completions "

        print_completers $javaClassHierarchy_A $javaClassHierarchy_B $javaClassHierarchy_C $javaClassHierarchy_D
        print_completers $javaClassHierarchy_E $javaClassHierarchy_F $javaClassHierarchy_G $javaClassHierarchy_H
        print_completers $javaClassHierarchy_I $javaClassHierarchy_J $javaClassHierarchy_K $javaClassHierarchy_L
        print_completers $javaClassHierarchy_M

        printf "\n"
        # ------------------------------------------------------------------------------------------ #
      }
      # --------------- STAGE 2 OF 7 --------------------------------------------------------------- #
      evaluate-commands %sh{
      # --------------- JAVAX CLASS HIERARCHY ------------------------------------------------------ #

        javaxClassHierarchy_A='javax.accessibility.AccessibilityProvider
          javax.accessibility.AccessibleAttributeSequence javax.accessibility.AccessibleBundle
          javax.accessibility.AccessibleContext javax.accessibility.AccessibleHyperlink
          javax.accessibility.AccessibleRelation javax.accessibility.AccessibleRelationSet
          javax.accessibility.AccessibleRole
          javax.accessibility.AccessibleState javax.accessibility.AccessibleStateSet
          javax.accessibility.AccessibleTextSequence javax.annotation.processing.AbstractProcessor
          javax.annotation.processing.Completions javax.annotation.processing.FilerException'

        javaxClassHierarchy_B='javax.crypto.AEADBadTagException javax.crypto.BadPaddingException
          javax.crypto.Cipher javax.crypto.CipherInputStream javax.crypto.CipherOutputStream
          javax.crypto.CipherSpi javax.crypto.EncryptedPrivateKeyInfo javax.crypto.ExemptionMechanism
          javax.crypto.ExemptionMechanismException javax.crypto.ExemptionMechanismSpi
          javax.crypto.IllegalBlockSizeException javax.crypto.KeyAgreement javax.crypto.KeyAgreementSpi
          javax.crypto.KeyGenerator javax.crypto.KeyGeneratorSpi javax.crypto.Mac javax.crypto.MacSpi
          javax.crypto.NoSuchPaddingException javax.crypto.NullCipher javax.crypto.SealedObject
          javax.crypto.SecretKeyFactory javax.crypto.SecretKeyFactorySpi javax.crypto.ShortBufferException
          javax.crypto.spec.ChaCha20ParameterSpec javax.crypto.spec.DESedeKeySpec javax.crypto.spec.DESKeySpec
          javax.crypto.spec.DHGenParameterSpec javax.crypto.spec.DHParameterSpec
          javax.crypto.spec.DHPrivateKeySpec javax.crypto.spec.DHPublicKeySpec
          javax.crypto.spec.GCMParameterSpec javax.crypto.spec.IvParameterSpec
          javax.crypto.spec.OAEPParameterSpec javax.crypto.spec.PBEKeySpec javax.crypto.spec.PBEParameterSpec
          javax.crypto.spec.PSource javax.crypto.spec.PSource.PSpecified javax.crypto.spec.RC2ParameterSpec
          javax.crypto.spec.RC5ParameterSpec javax.crypto.spec.SecretKeySpec'

        javaxClassHierarchy_C='javax.imageio.IIOException javax.imageio.IIOImage javax.imageio.IIOParam
          javax.imageio.ImageIO javax.imageio.ImageReader javax.imageio.ImageReadParam
          javax.imageio.ImageTypeSpecifier javax.imageio.ImageWriteParam javax.imageio.ImageWriter
          javax.imageio.metadata.IIOInvalidTreeException javax.imageio.metadata.IIOMetadata
          javax.imageio.metadata.IIOMetadataFormatImpl javax.imageio.metadata.IIOMetadataNode
          javax.imageio.plugins.bmp.BMPImageWriteParam javax.imageio.plugins.jpeg.JPEGHuffmanTable
          javax.imageio.plugins.jpeg.JPEGImageReadParam javax.imageio.plugins.jpeg.JPEGImageWriteParam
          javax.imageio.plugins.jpeg.JPEGQTable javax.imageio.plugins.tiff.BaselineTIFFTagSet
          javax.imageio.plugins.tiff.ExifGPSTagSet javax.imageio.plugins.tiff.ExifInteroperabilityTagSet
          javax.imageio.plugins.tiff.ExifParentTIFFTagSet javax.imageio.plugins.tiff.ExifTIFFTagSet
          javax.imageio.plugins.tiff.FaxTIFFTagSet javax.imageio.plugins.tiff.GeoTIFFTagSet
          javax.imageio.plugins.tiff.TIFFDirectory javax.imageio.plugins.tiff.TIFFField
          javax.imageio.plugins.tiff.TIFFImageReadParam javax.imageio.plugins.tiff.TIFFTag
          javax.imageio.plugins.tiff.TIFFTagSet javax.imageio.spi.IIORegistry
          javax.imageio.spi.IIOServiceProvider javax.imageio.spi.ImageInputStreamSpi
          javax.imageio.spi.ImageOutputStreamSpi javax.imageio.spi.ImageReaderSpi
          javax.imageio.spi.ImageReaderWriterSpi javax.imageio.spi.ImageTranscoderSpi
          javax.imageio.spi.ImageWriterSpi javax.imageio.spi.ServiceRegistry
          javax.imageio.stream.FileCacheImageInputStream javax.imageio.stream.FileCacheImageOutputStream
          javax.imageio.stream.FileImageInputStream javax.imageio.stream.FileImageOutputStream
          javax.imageio.stream.IIOByteBuffer javax.imageio.stream.ImageInputStreamImpl
          javax.imageio.stream.ImageOutputStreamImpl javax.imageio.stream.MemoryCacheImageInputStream
          javax.imageio.stream.MemoryCacheImageOutputStream'

        javaxClassHierarchy_D='javax.lang.model.element.UnknownAnnotationValueException
          javax.lang.model.element.UnknownDirectiveException javax.lang.model.element.UnknownElementException
          javax.lang.model.type.MirroredTypeException javax.lang.model.type.MirroredTypesException
          javax.lang.model.type.UnknownTypeException javax.lang.model.UnknownEntityException
          javax.lang.model.util.AbstractAnnotationValueVisitor14
          javax.lang.model.util.AbstractAnnotationValueVisitor6
          javax.lang.model.util.AbstractAnnotationValueVisitor7
          javax.lang.model.util.AbstractAnnotationValueVisitor8
          javax.lang.model.util.AbstractAnnotationValueVisitor9 javax.lang.model.util.AbstractElementVisitor14
          javax.lang.model.util.AbstractElementVisitor6 javax.lang.model.util.AbstractElementVisitor7
          javax.lang.model.util.AbstractElementVisitor8 javax.lang.model.util.AbstractElementVisitor9
          javax.lang.model.util.AbstractTypeVisitor14 javax.lang.model.util.AbstractTypeVisitor6
          javax.lang.model.util.AbstractTypeVisitor7 javax.lang.model.util.AbstractTypeVisitor8
          javax.lang.model.util.AbstractTypeVisitor9 javax.lang.model.util.ElementFilter
          javax.lang.model.util.ElementKindVisitor14 javax.lang.model.util.ElementKindVisitor6
          javax.lang.model.util.ElementKindVisitor7 javax.lang.model.util.ElementKindVisitor8
          javax.lang.model.util.ElementKindVisitor9 javax.lang.model.util.ElementScanner14
          javax.lang.model.util.ElementScanner6 javax.lang.model.util.ElementScanner7
          javax.lang.model.util.ElementScanner8 javax.lang.model.util.ElementScanner9
          javax.lang.model.util.SimpleAnnotationValueVisitor14
          javax.lang.model.util.SimpleAnnotationValueVisitor6
          javax.lang.model.util.SimpleAnnotationValueVisitor7
          javax.lang.model.util.SimpleAnnotationValueVisitor8
          javax.lang.model.util.SimpleAnnotationValueVisitor9 javax.lang.model.util.SimpleElementVisitor14
          javax.lang.model.util.SimpleElementVisitor6 javax.lang.model.util.SimpleElementVisitor7
          javax.lang.model.util.SimpleElementVisitor8 javax.lang.model.util.SimpleElementVisitor9
          javax.lang.model.util.SimpleTypeVisitor14 javax.lang.model.util.SimpleTypeVisitor6
          javax.lang.model.util.SimpleTypeVisitor7 javax.lang.model.util.SimpleTypeVisitor8
          javax.lang.model.util.SimpleTypeVisitor9 javax.lang.model.util.TypeKindVisitor14
          javax.lang.model.util.TypeKindVisitor6 javax.lang.model.util.TypeKindVisitor7
          javax.lang.model.util.TypeKindVisitor8 javax.lang.model.util.TypeKindVisitor9'

        javaxClassHierarchy_E='javax.management.Attribute javax.management.AttributeChangeNotification
          javax.management.AttributeChangeNotificationFilter javax.management.AttributeList
          javax.management.AttributeNotFoundException javax.management.AttributeValueExp
          javax.management.BadAttributeValueExpException javax.management.BadBinaryOpValueExpException
          javax.management.BadStringOperationException
          javax.management.ImmutableDescriptor javax.management.InstanceAlreadyExistsException
          javax.management.InstanceNotFoundException javax.management.IntrospectionException
          javax.management.InvalidApplicationException javax.management.InvalidAttributeValueException
          javax.management.JMException javax.management.JMRuntimeException javax.management.JMX
          javax.management.ListenerNotFoundException
          javax.management.loading.MLet javax.management.loading.MLetContent
          javax.management.loading.PrivateMLet javax.management.MalformedObjectNameException
          javax.management.MBeanAttributeInfo javax.management.MBeanConstructorInfo
          javax.management.MBeanException javax.management.MBeanFeatureInfo javax.management.MBeanInfo
          javax.management.MBeanNotificationInfo javax.management.MBeanOperationInfo
          javax.management.MBeanParameterInfo javax.management.MBeanPermission
          javax.management.MBeanRegistrationException javax.management.MBeanServerBuilder
          javax.management.MBeanServerDelegate javax.management.MBeanServerFactory
          javax.management.MBeanServerInvocationHandler javax.management.MBeanServerNotification
          javax.management.MBeanServerPermission javax.management.MBeanTrustPermission
          javax.management.modelmbean.DescriptorSupport
          javax.management.modelmbean.InvalidTargetObjectTypeException
          javax.management.modelmbean.ModelMBeanAttributeInfo
          javax.management.modelmbean.ModelMBeanConstructorInfo
          javax.management.modelmbean.ModelMBeanInfoSupport
          javax.management.modelmbean.ModelMBeanNotificationInfo
          javax.management.modelmbean.ModelMBeanOperationInfo javax.management.modelmbean.RequiredModelMBean
          javax.management.modelmbean.XMLParseException javax.management.monitor.CounterMonitor
          javax.management.monitor.GaugeMonitor javax.management.monitor.Monitor
          javax.management.monitor.MonitorNotification javax.management.monitor.MonitorSettingException
          javax.management.monitor.StringMonitor javax.management.NotCompliantMBeanException
          javax.management.Notification javax.management.NotificationBroadcasterSupport
          javax.management.NotificationFilterSupport javax.management.ObjectInstance
          javax.management.ObjectName javax.management.openmbean.ArrayType
          javax.management.openmbean.CompositeDataInvocationHandler
          javax.management.openmbean.CompositeDataSupport javax.management.openmbean.CompositeType
          javax.management.openmbean.InvalidKeyException javax.management.openmbean.InvalidOpenTypeException
          javax.management.openmbean.KeyAlreadyExistsException javax.management.openmbean.OpenDataException
          javax.management.openmbean.OpenMBeanAttributeInfoSupport
          javax.management.openmbean.OpenMBeanConstructorInfoSupport
          javax.management.openmbean.OpenMBeanInfoSupport
          javax.management.openmbean.OpenMBeanOperationInfoSupport
          javax.management.openmbean.OpenMBeanParameterInfoSupport javax.management.openmbean.OpenType
          javax.management.openmbean.SimpleType javax.management.openmbean.TabularDataSupport
          javax.management.openmbean.TabularType javax.management.OperationsException javax.management.Query
          javax.management.QueryEval javax.management.ReflectionException
          javax.management.relation.InvalidRelationIdException
          javax.management.relation.InvalidRelationServiceException
          javax.management.relation.InvalidRelationTypeException
          javax.management.relation.InvalidRoleInfoException
          javax.management.relation.InvalidRoleValueException
          javax.management.relation.MBeanServerNotificationFilter javax.management.relation.RelationException
          javax.management.relation.RelationNotFoundException javax.management.relation.RelationNotification
          javax.management.relation.RelationService
          javax.management.relation.RelationServiceNotRegisteredException
          javax.management.relation.RelationSupport javax.management.relation.RelationTypeNotFoundException
          javax.management.relation.RelationTypeSupport javax.management.relation.Role
          javax.management.relation.RoleInfo javax.management.relation.RoleInfoNotFoundException
          javax.management.relation.RoleList javax.management.relation.RoleNotFoundException
          javax.management.relation.RoleResult javax.management.relation.RoleStatus
          javax.management.relation.RoleUnresolved javax.management.relation.RoleUnresolvedList
          javax.management.remote.JMXConnectionNotification javax.management.remote.JMXConnectorFactory
          javax.management.remote.JMXConnectorServer javax.management.remote.JMXConnectorServerFactory
          javax.management.remote.JMXPrincipal javax.management.remote.JMXProviderException
          javax.management.remote.JMXServerErrorException javax.management.remote.JMXServiceURL
          javax.management.remote.NotificationResult javax.management.remote.rmi.RMIConnectionImpl
          javax.management.remote.rmi.RMIConnectionImpl_Stub javax.management.remote.rmi.RMIConnector
          javax.management.remote.rmi.RMIConnectorServer
          javax.management.remote.rmi.RMIJRMPServerImpl javax.management.remote.rmi.RMIServerImpl
          javax.management.remote.rmi.RMIServerImpl_Stub javax.management.remote.SubjectDelegationPermission
          javax.management.remote.TargetedNotification javax.management.RuntimeErrorException
          javax.management.RuntimeMBeanException javax.management.RuntimeOperationsException
          javax.management.ServiceNotFoundException javax.management.StandardEmitterMBean
          javax.management.StandardMBean javax.management.StringValueExp javax.management.timer.Timer
          javax.management.timer.TimerNotification'

        javaxClassHierarchy_F='javax.naming.AuthenticationException
          javax.naming.AuthenticationNotSupportedException javax.naming.BinaryRefAddr javax.naming.Binding
          javax.naming.CannotProceedException javax.naming.CommunicationException javax.naming.CompositeName
          javax.naming.CompoundName javax.naming.ConfigurationException javax.naming.ContextNotEmptyException
          javax.naming.directory.AttributeInUseException javax.naming.directory.AttributeModificationException
          javax.naming.directory.BasicAttribute javax.naming.directory.BasicAttributes
          javax.naming.directory.InitialDirContext javax.naming.directory.InvalidAttributeIdentifierException
          javax.naming.directory.InvalidAttributesException
          javax.naming.directory.InvalidAttributeValueException
          javax.naming.directory.InvalidSearchControlsException
          javax.naming.directory.InvalidSearchFilterException javax.naming.directory.ModificationItem
          javax.naming.directory.NoSuchAttributeException javax.naming.directory.SchemaViolationException
          javax.naming.directory.SearchControls javax.naming.directory.SearchResult
          javax.naming.event.NamingEvent javax.naming.event.NamingExceptionEvent javax.naming.InitialContext
          javax.naming.InsufficientResourcesException javax.naming.InterruptedNamingException
          javax.naming.InvalidNameException javax.naming.ldap.BasicControl javax.naming.ldap.ControlFactory
          javax.naming.ldap.InitialLdapContext javax.naming.ldap.LdapName
          javax.naming.ldap.LdapReferralException javax.naming.ldap.ManageReferralControl
          javax.naming.ldap.PagedResultsControl javax.naming.ldap.PagedResultsResponseControl
          javax.naming.ldap.Rdn javax.naming.ldap.SortControl javax.naming.ldap.SortKey
          javax.naming.ldap.SortResponseControl javax.naming.ldap.spi.LdapDnsProvider
          javax.naming.ldap.spi.LdapDnsProviderResult javax.naming.ldap.StartTlsRequest
          javax.naming.ldap.StartTlsResponse javax.naming.ldap.UnsolicitedNotificationEvent
          javax.naming.LimitExceededException javax.naming.LinkException javax.naming.LinkLoopException
          javax.naming.LinkRef javax.naming.MalformedLinkException javax.naming.NameAlreadyBoundException
          javax.naming.NameClassPair javax.naming.NameNotFoundException javax.naming.NamingException
          javax.naming.NamingSecurityException javax.naming.NoInitialContextException
          javax.naming.NoPermissionException javax.naming.NotContextException
          javax.naming.OperationNotSupportedException javax.naming.PartialResultException javax.naming.RefAddr
          javax.naming.Reference javax.naming.ReferralException javax.naming.ServiceUnavailableException
          javax.naming.SizeLimitExceededException javax.naming.spi.DirectoryManager
          javax.naming.spi.DirStateFactory.Result javax.naming.spi.NamingManager javax.naming.spi.ResolveResult
          javax.naming.StringRefAddr javax.naming.TimeLimitExceededException'

        javaxClassHierarchy_G='javax.net.ServerSocketFactory javax.net.SocketFactory
          javax.net.ssl.CertPathTrustManagerParameters javax.net.ssl.ExtendedSSLSession
          javax.net.ssl.HandshakeCompletedEvent javax.net.ssl.HttpsURLConnection
          javax.net.ssl.KeyManagerFactory javax.net.ssl.KeyManagerFactorySpi
          javax.net.ssl.KeyStoreBuilderParameters javax.net.ssl.SNIHostName javax.net.ssl.SNIMatcher
          javax.net.ssl.SNIServerName javax.net.ssl.SSLContext javax.net.ssl.SSLContextSpi
          javax.net.ssl.SSLEngine javax.net.ssl.SSLEngineResult javax.net.ssl.SSLException
          javax.net.ssl.SSLHandshakeException javax.net.ssl.SSLKeyException javax.net.ssl.SSLParameters
          javax.net.ssl.SSLPeerUnverifiedException javax.net.ssl.SSLPermission
          javax.net.ssl.SSLProtocolException javax.net.ssl.SSLServerSocket javax.net.ssl.SSLServerSocketFactory
          javax.net.ssl.SSLSessionBindingEvent javax.net.ssl.SSLSocket javax.net.ssl.SSLSocketFactory
          javax.net.ssl.StandardConstants javax.net.ssl.TrustManagerFactory
          javax.net.ssl.TrustManagerFactorySpi javax.net.ssl.X509ExtendedKeyManager
          javax.net.ssl.X509ExtendedTrustManager'

        javaxClassHierarchy_H='javax.print.attribute.AttributeSetUtilities
          javax.print.attribute.DateTimeSyntax javax.print.attribute.EnumSyntax
          javax.print.attribute.HashAttributeSet javax.print.attribute.HashDocAttributeSet
          javax.print.attribute.HashPrintJobAttributeSet javax.print.attribute.HashPrintRequestAttributeSet
          javax.print.attribute.HashPrintServiceAttributeSet javax.print.attribute.IntegerSyntax
          javax.print.attribute.ResolutionSyntax javax.print.attribute.SetOfIntegerSyntax
          javax.print.attribute.Size2DSyntax javax.print.attribute.standard.Chromaticity
          javax.print.attribute.standard.ColorSupported javax.print.attribute.standard.Compression
          javax.print.attribute.standard.Copies javax.print.attribute.standard.CopiesSupported
          javax.print.attribute.standard.DateTimeAtCompleted javax.print.attribute.standard.DateTimeAtCreation
          javax.print.attribute.standard.DateTimeAtProcessing javax.print.attribute.standard.Destination
          javax.print.attribute.standard.DialogOwner javax.print.attribute.standard.DialogTypeSelection
          javax.print.attribute.standard.DocumentName javax.print.attribute.standard.Fidelity
          javax.print.attribute.standard.Finishings javax.print.attribute.standard.JobHoldUntil
          javax.print.attribute.standard.JobImpressions javax.print.attribute.standard.JobImpressionsCompleted
          javax.print.attribute.standard.JobImpressionsSupported javax.print.attribute.standard.JobKOctets
          javax.print.attribute.standard.JobKOctetsProcessed javax.print.attribute.standard.JobKOctetsSupported
          javax.print.attribute.standard.JobMediaSheets javax.print.attribute.standard.JobMediaSheetsCompleted
          javax.print.attribute.standard.JobMediaSheetsSupported
          javax.print.attribute.standard.JobMessageFromOperator javax.print.attribute.standard.JobName
          javax.print.attribute.standard.JobOriginatingUserName javax.print.attribute.standard.JobPriority
          javax.print.attribute.standard.JobPrioritySupported javax.print.attribute.standard.JobSheets
          javax.print.attribute.standard.JobState javax.print.attribute.standard.JobStateReason
          javax.print.attribute.standard.JobStateReasons javax.print.attribute.standard.Media
          javax.print.attribute.standard.MediaName javax.print.attribute.standard.MediaPrintableArea
          javax.print.attribute.standard.MediaSize javax.print.attribute.standard.MediaSize.Engineering
          javax.print.attribute.standard.MediaSize.ISO javax.print.attribute.standard.MediaSize.JIS
          javax.print.attribute.standard.MediaSize.NA javax.print.attribute.standard.MediaSizeName
          javax.print.attribute.standard.MediaSize.Other javax.print.attribute.standard.MediaTray
          javax.print.attribute.standard.MultipleDocumentHandling
          javax.print.attribute.standard.NumberOfDocuments
          javax.print.attribute.standard.NumberOfInterveningJobs javax.print.attribute.standard.NumberUp
          javax.print.attribute.standard.NumberUpSupported javax.print.attribute.standard.OrientationRequested
          javax.print.attribute.standard.OutputDeviceAssigned javax.print.attribute.standard.PageRanges
          javax.print.attribute.standard.PagesPerMinute javax.print.attribute.standard.PagesPerMinuteColor
          javax.print.attribute.standard.PDLOverrideSupported
          javax.print.attribute.standard.PresentationDirection javax.print.attribute.standard.PrinterInfo
          javax.print.attribute.standard.PrinterIsAcceptingJobs javax.print.attribute.standard.PrinterLocation
          javax.print.attribute.standard.PrinterMakeAndModel
          javax.print.attribute.standard.PrinterMessageFromOperator
          javax.print.attribute.standard.PrinterMoreInfo
          javax.print.attribute.standard.PrinterMoreInfoManufacturer javax.print.attribute.standard.PrinterName
          javax.print.attribute.standard.PrinterResolution javax.print.attribute.standard.PrinterState
          javax.print.attribute.standard.PrinterStateReason javax.print.attribute.standard.PrinterStateReasons
          javax.print.attribute.standard.PrinterURI javax.print.attribute.standard.PrintQuality
          javax.print.attribute.standard.QueuedJobCount
          javax.print.attribute.standard.ReferenceUriSchemesSupported
          javax.print.attribute.standard.RequestingUserName javax.print.attribute.standard.Severity
          javax.print.attribute.standard.SheetCollate javax.print.attribute.standard.Sides
          javax.print.attribute.TextSyntax javax.print.attribute.UnmodifiableSetException
          javax.print.attribute.URISyntax javax.print.DocFlavor javax.print.DocFlavor.BYTE_ARRAY
          javax.print.DocFlavor.CHAR_ARRAY javax.print.DocFlavor.INPUT_STREAM javax.print.DocFlavor.READER
          javax.print.DocFlavor.SERVICE_FORMATTED javax.print.DocFlavor.STRING javax.print.DocFlavor.URL
          javax.print.event.PrintEvent javax.print.event.PrintJobAdapter
          javax.print.event.PrintJobAttributeEvent javax.print.event.PrintJobEvent
          javax.print.event.PrintServiceAttributeEvent javax.print.PrintException
          javax.print.PrintServiceLookup javax.print.ServiceUI javax.print.ServiceUIFactory
          javax.print.SimpleDoc javax.print.StreamPrintService javax.print.StreamPrintServiceFactory'

        javaxClassHierarchy_I='javax.rmi.ssl.SslRMIClientSocketFactory
          javax.rmi.ssl.SslRMIServerSocketFactory'

        javaxClassHierarchy_J='javax.script.AbstractScriptEngine javax.script.CompiledScript
          javax.script.ScriptEngineManager javax.script.ScriptException javax.script.SimpleBindings
          javax.script.SimpleScriptContext'

        javaxClassHierarchy_K='javax.security.auth.AuthPermission
          javax.security.auth.callback.ChoiceCallback javax.security.auth.callback.ConfirmationCallback
          javax.security.auth.callback.LanguageCallback javax.security.auth.callback.NameCallback
          javax.security.auth.callback.PasswordCallback javax.security.auth.callback.TextInputCallback
          javax.security.auth.callback.TextOutputCallback
          javax.security.auth.callback.UnsupportedCallbackException javax.security.auth.DestroyFailedException
          javax.security.auth.kerberos.DelegationPermission javax.security.auth.kerberos.EncryptionKey
          javax.security.auth.kerberos.KerberosCredMessage javax.security.auth.kerberos.KerberosKey
          javax.security.auth.kerberos.KerberosPrincipal javax.security.auth.kerberos.KerberosTicket
          javax.security.auth.kerberos.KeyTab javax.security.auth.kerberos.ServicePermission
          javax.security.auth.login.AccountException javax.security.auth.login.AccountExpiredException
          javax.security.auth.login.AccountLockedException javax.security.auth.login.AccountNotFoundException
          javax.security.auth.login.AppConfigurationEntry
          javax.security.auth.login.AppConfigurationEntry.LoginModuleControlFlag
          javax.security.auth.login.Configuration javax.security.auth.login.ConfigurationSpi
          javax.security.auth.login.CredentialException javax.security.auth.login.CredentialExpiredException
          javax.security.auth.login.CredentialNotFoundException javax.security.auth.login.FailedLoginException
          javax.security.auth.login.LoginContext javax.security.auth.login.LoginException
          javax.security.auth.PrivateCredentialPermission javax.security.auth.RefreshFailedException
          javax.security.auth.Subject
          javax.security.auth.x500.X500Principal javax.security.auth.x500.X500PrivateCredential
          javax.security.sasl.AuthenticationException
          javax.security.sasl.AuthorizeCallback javax.security.sasl.RealmCallback
          javax.security.sasl.RealmChoiceCallback javax.security.sasl.Sasl javax.security.sasl.SaslException'

        javaxClassHierarchy_L='javax.smartcardio.ATR javax.smartcardio.Card javax.smartcardio.CardChannel
          javax.smartcardio.CardException javax.smartcardio.CardNotPresentException
          javax.smartcardio.CardPermission javax.smartcardio.CardTerminal javax.smartcardio.CardTerminals
          javax.smartcardio.CommandAPDU javax.smartcardio.ResponseAPDU javax.smartcardio.TerminalFactory
          javax.smartcardio.TerminalFactorySpi'

        javaxClassHierarchy_M='javax.sound.midi.Instrument
          javax.sound.midi.InvalidMidiDataException javax.sound.midi.MetaMessage
          javax.sound.midi.MidiDevice.Info javax.sound.midi.MidiEvent javax.sound.midi.MidiFileFormat
          javax.sound.midi.MidiMessage javax.sound.midi.MidiSystem javax.sound.midi.MidiUnavailableException
          javax.sound.midi.Patch javax.sound.midi.Sequence javax.sound.midi.Sequencer.SyncMode
          javax.sound.midi.ShortMessage javax.sound.midi.SoundbankResource
          javax.sound.midi.spi.MidiDeviceProvider javax.sound.midi.spi.MidiFileReader
          javax.sound.midi.spi.MidiFileWriter javax.sound.midi.spi.SoundbankReader
          javax.sound.midi.SysexMessage javax.sound.midi.Track javax.sound.midi.VoiceStatus
          javax.sound.sampled.AudioFileFormat javax.sound.sampled.AudioFileFormat.Type
          javax.sound.sampled.AudioFormat javax.sound.sampled.AudioFormat.Encoding
          javax.sound.sampled.AudioInputStream javax.sound.sampled.AudioPermission
          javax.sound.sampled.AudioSystem javax.sound.sampled.BooleanControl
          javax.sound.sampled.BooleanControl.Type javax.sound.sampled.CompoundControl
          javax.sound.sampled.CompoundControl.Type javax.sound.sampled.Control javax.sound.sampled.Control.Type
          javax.sound.sampled.DataLine.Info javax.sound.sampled.EnumControl
          javax.sound.sampled.EnumControl.Type javax.sound.sampled.FloatControl
          javax.sound.sampled.FloatControl.Type javax.sound.sampled.LineEvent
          javax.sound.sampled.LineEvent.Type javax.sound.sampled.Line.Info
          javax.sound.sampled.LineUnavailableException javax.sound.sampled.Mixer.Info
          javax.sound.sampled.Port.Info javax.sound.sampled.ReverbType javax.sound.sampled.spi.AudioFileReader
          javax.sound.sampled.spi.AudioFileWriter javax.sound.sampled.spi.FormatConversionProvider
          javax.sound.sampled.spi.MixerProvider javax.sound.sampled.UnsupportedAudioFileException'

        javaxClassHierarchy_N='javax.sql.ConnectionEvent javax.sql.rowset.BaseRowSet javax.sql.RowSetEvent
          javax.sql.rowset.RowSetMetaDataImpl javax.sql.rowset.RowSetProvider javax.sql.rowset.RowSetWarning
          javax.sql.rowset.serial.SerialArray javax.sql.rowset.serial.SerialBlob
          javax.sql.rowset.serial.SerialClob javax.sql.rowset.serial.SerialDatalink
          javax.sql.rowset.serial.SerialException javax.sql.rowset.serial.SerialJavaObject
          javax.sql.rowset.serial.SerialRef javax.sql.rowset.serial.SerialStruct
          javax.sql.rowset.serial.SQLInputImpl javax.sql.rowset.serial.SQLOutputImpl
          javax.sql.rowset.spi.SyncFactory javax.sql.rowset.spi.SyncFactoryException
          javax.sql.rowset.spi.SyncProvider javax.sql.rowset.spi.SyncProviderException
          javax.sql.StatementEvent'

        javaxClassHierarchy_O='javax.swing.AbstractAction
          javax.swing.AbstractButton javax.swing.AbstractButton.AccessibleAbstractButton
          javax.swing.AbstractButton.ButtonChangeListener javax.swing.AbstractCellEditor
          javax.swing.AbstractListModel javax.swing.AbstractSpinnerModel javax.swing.ActionMap
          javax.swing.border.AbstractBorder javax.swing.border.BevelBorder javax.swing.border.CompoundBorder
          javax.swing.border.EmptyBorder javax.swing.border.EtchedBorder javax.swing.BorderFactory
          javax.swing.border.LineBorder javax.swing.border.MatteBorder javax.swing.border.SoftBevelBorder
          javax.swing.border.StrokeBorder javax.swing.border.TitledBorder javax.swing.Box
          javax.swing.Box.AccessibleBox javax.swing.Box.Filler javax.swing.Box.Filler.AccessibleBoxFiller
          javax.swing.BoxLayout javax.swing.ButtonGroup javax.swing.CellRendererPane
          javax.swing.CellRendererPane.AccessibleCellRendererPane
          javax.swing.colorchooser.AbstractColorChooserPanel
          javax.swing.colorchooser.ColorChooserComponentFactory
          javax.swing.colorchooser.DefaultColorSelectionModel javax.swing.ComponentInputMap
          javax.swing.DebugGraphics javax.swing.DefaultBoundedRangeModel javax.swing.DefaultButtonModel
          javax.swing.DefaultCellEditor javax.swing.DefaultCellEditor.EditorDelegate
          javax.swing.DefaultComboBoxModel javax.swing.DefaultDesktopManager javax.swing.DefaultFocusManager
          javax.swing.DefaultListCellRenderer javax.swing.DefaultListCellRenderer.UIResource
          javax.swing.DefaultListModel javax.swing.DefaultListSelectionModel javax.swing.DefaultRowSorter
          javax.swing.DefaultRowSorter.ModelWrapper javax.swing.DefaultSingleSelectionModel
          javax.swing.event.AncestorEvent javax.swing.event.CaretEvent javax.swing.event.ChangeEvent
          javax.swing.event.DocumentEvent.EventType javax.swing.event.EventListenerList
          javax.swing.event.HyperlinkEvent javax.swing.event.HyperlinkEvent.EventType
          javax.swing.event.InternalFrameAdapter javax.swing.event.InternalFrameEvent
          javax.swing.event.ListDataEvent javax.swing.event.ListSelectionEvent
          javax.swing.event.MenuDragMouseEvent javax.swing.event.MenuEvent javax.swing.event.MenuKeyEvent
          javax.swing.event.MouseInputAdapter javax.swing.event.PopupMenuEvent javax.swing.event.RowSorterEvent
          javax.swing.event.SwingPropertyChangeSupport javax.swing.event.TableColumnModelEvent
          javax.swing.event.TableModelEvent javax.swing.event.TreeExpansionEvent
          javax.swing.event.TreeModelEvent javax.swing.event.TreeSelectionEvent
          javax.swing.event.UndoableEditEvent javax.swing.filechooser.FileFilter
          javax.swing.filechooser.FileNameExtensionFilter javax.swing.filechooser.FileSystemView
          javax.swing.filechooser.FileView javax.swing.FocusManager javax.swing.GrayFilter
          javax.swing.GroupLayout javax.swing.GroupLayout.Group javax.swing.GroupLayout.ParallelGroup
          javax.swing.GroupLayout.SequentialGroup javax.swing.ImageIcon
          javax.swing.ImageIcon.AccessibleImageIcon javax.swing.InputMap javax.swing.InputVerifier
          javax.swing.InternalFrameFocusTraversalPolicy
          javax.swing.JApplet.AccessibleJApplet javax.swing.JButton javax.swing.JButton.AccessibleJButton
          javax.swing.JCheckBox javax.swing.JCheckBox.AccessibleJCheckBox javax.swing.JCheckBoxMenuItem
          javax.swing.JCheckBoxMenuItem.AccessibleJCheckBoxMenuItem javax.swing.JColorChooser
          javax.swing.JColorChooser.AccessibleJColorChooser javax.swing.JComboBox
          javax.swing.JComboBox.AccessibleJComboBox javax.swing.JComponent
          javax.swing.JComponent.AccessibleJComponent
          javax.swing.JComponent.AccessibleJComponent.AccessibleContainerHandler
          javax.swing.JDesktopPane
          javax.swing.JDesktopPane.AccessibleJDesktopPane javax.swing.JDialog
          javax.swing.JDialog.AccessibleJDialog javax.swing.JEditorPane
          javax.swing.JEditorPane.AccessibleJEditorPane javax.swing.JEditorPane.AccessibleJEditorPaneHTML
          javax.swing.JEditorPane.JEditorPaneAccessibleHypertextSupport
          javax.swing.JEditorPane.JEditorPaneAccessibleHypertextSupport.HTMLLink javax.swing.JFileChooser
          javax.swing.JFileChooser.AccessibleJFileChooser javax.swing.JFormattedTextField
          javax.swing.JFormattedTextField.AbstractFormatter
          javax.swing.JFormattedTextField.AbstractFormatterFactory javax.swing.JFrame
          javax.swing.JFrame.AccessibleJFrame javax.swing.JInternalFrame
          javax.swing.JInternalFrame.AccessibleJInternalFrame javax.swing.JInternalFrame.JDesktopIcon
          javax.swing.JInternalFrame.JDesktopIcon.AccessibleJDesktopIcon javax.swing.JLabel
          javax.swing.JLabel.AccessibleJLabel javax.swing.JLayer javax.swing.JLayeredPane
          javax.swing.JLayeredPane.AccessibleJLayeredPane javax.swing.JList javax.swing.JList.AccessibleJList
          javax.swing.JList.AccessibleJList.AccessibleJListChild javax.swing.JList.DropLocation
          javax.swing.JMenu javax.swing.JMenu.AccessibleJMenu javax.swing.JMenuBar
          javax.swing.JMenuBar.AccessibleJMenuBar javax.swing.JMenuItem
          javax.swing.JMenuItem.AccessibleJMenuItem javax.swing.JMenu.WinListener javax.swing.JOptionPane
          javax.swing.JOptionPane.AccessibleJOptionPane javax.swing.JPanel javax.swing.JPanel.AccessibleJPanel
          javax.swing.JPasswordField javax.swing.JPasswordField.AccessibleJPasswordField javax.swing.JPopupMenu
          javax.swing.JPopupMenu.AccessibleJPopupMenu javax.swing.JPopupMenu.Separator javax.swing.JProgressBar
          javax.swing.JProgressBar.AccessibleJProgressBar javax.swing.JRadioButton
          javax.swing.JRadioButton.AccessibleJRadioButton javax.swing.JRadioButtonMenuItem
          javax.swing.JRadioButtonMenuItem.AccessibleJRadioButtonMenuItem javax.swing.JRootPane
          javax.swing.JRootPane.AccessibleJRootPane javax.swing.JRootPane.RootLayout javax.swing.JScrollBar
          javax.swing.JScrollBar.AccessibleJScrollBar javax.swing.JScrollPane
          javax.swing.JScrollPane.AccessibleJScrollPane javax.swing.JScrollPane.ScrollBar
          javax.swing.JSeparator javax.swing.JSeparator.AccessibleJSeparator javax.swing.JSlider
          javax.swing.JSlider.AccessibleJSlider javax.swing.JSpinner javax.swing.JSpinner.AccessibleJSpinner
          javax.swing.JSpinner.DateEditor javax.swing.JSpinner.DefaultEditor javax.swing.JSpinner.ListEditor
          javax.swing.JSpinner.NumberEditor javax.swing.JSplitPane javax.swing.JSplitPane.AccessibleJSplitPane
          javax.swing.JTabbedPane javax.swing.JTabbedPane.AccessibleJTabbedPane
          javax.swing.JTabbedPane.ModelListener javax.swing.JTable javax.swing.JTable.AccessibleJTable
          javax.swing.JTable.AccessibleJTable.AccessibleJTableCell
          javax.swing.JTable.AccessibleJTable.AccessibleJTableModelChange javax.swing.JTable.DropLocation
          javax.swing.JTextArea javax.swing.JTextArea.AccessibleJTextArea javax.swing.JTextField
          javax.swing.JTextField.AccessibleJTextField javax.swing.JTextPane javax.swing.JToggleButton
          javax.swing.JToggleButton.AccessibleJToggleButton javax.swing.JToggleButton.ToggleButtonModel
          javax.swing.JToolBar javax.swing.JToolBar.AccessibleJToolBar javax.swing.JToolBar.Separator
          javax.swing.JToolTip javax.swing.JToolTip.AccessibleJToolTip javax.swing.JTree
          javax.swing.JTree.AccessibleJTree javax.swing.JTree.AccessibleJTree.AccessibleJTreeNode
          javax.swing.JTree.DropLocation javax.swing.JTree.DynamicUtilTreeNode
          javax.swing.JTree.EmptySelectionModel javax.swing.JTree.TreeModelHandler
          javax.swing.JTree.TreeSelectionRedirector javax.swing.JViewport
          javax.swing.JViewport.AccessibleJViewport javax.swing.JViewport.ViewListener javax.swing.JWindow
          javax.swing.JWindow.AccessibleJWindow javax.swing.KeyStroke javax.swing.LayoutFocusTraversalPolicy
          javax.swing.LayoutStyle javax.swing.LookAndFeel javax.swing.MenuSelectionManager
          javax.swing.OverlayLayout javax.swing.plaf.ActionMapUIResource
          javax.swing.plaf.basic.BasicArrowButton javax.swing.plaf.basic.BasicBorders
          javax.swing.plaf.basic.BasicBorders.ButtonBorder javax.swing.plaf.basic.BasicBorders.FieldBorder
          javax.swing.plaf.basic.BasicBorders.MarginBorder javax.swing.plaf.basic.BasicBorders.MenuBarBorder
          javax.swing.plaf.basic.BasicBorders.RadioButtonBorder
          javax.swing.plaf.basic.BasicBorders.RolloverButtonBorder
          javax.swing.plaf.basic.BasicBorders.SplitPaneBorder
          javax.swing.plaf.basic.BasicBorders.ToggleButtonBorder javax.swing.plaf.basic.BasicButtonListener
          javax.swing.plaf.basic.BasicButtonUI javax.swing.plaf.basic.BasicCheckBoxMenuItemUI
          javax.swing.plaf.basic.BasicCheckBoxUI javax.swing.plaf.basic.BasicColorChooserUI
          javax.swing.plaf.basic.BasicColorChooserUI.PropertyHandler javax.swing.plaf.basic.BasicComboBoxEditor
          javax.swing.plaf.basic.BasicComboBoxEditor.UIResource javax.swing.plaf.basic.BasicComboBoxRenderer
          javax.swing.plaf.basic.BasicComboBoxRenderer.UIResource javax.swing.plaf.basic.BasicComboBoxUI
          javax.swing.plaf.basic.BasicComboBoxUI.ComboBoxLayoutManager
          javax.swing.plaf.basic.BasicComboBoxUI.FocusHandler
          javax.swing.plaf.basic.BasicComboBoxUI.ItemHandler javax.swing.plaf.basic.BasicComboBoxUI.KeyHandler
          javax.swing.plaf.basic.BasicComboBoxUI.ListDataHandler
          javax.swing.plaf.basic.BasicComboBoxUI.PropertyChangeHandler javax.swing.plaf.basic.BasicComboPopup
          javax.swing.plaf.basic.BasicComboPopup.InvocationKeyHandler
          javax.swing.plaf.basic.BasicComboPopup.InvocationMouseHandler
          javax.swing.plaf.basic.BasicComboPopup.InvocationMouseMotionHandler
          javax.swing.plaf.basic.BasicComboPopup.ItemHandler
          javax.swing.plaf.basic.BasicComboPopup.ListDataHandler
          javax.swing.plaf.basic.BasicComboPopup.ListMouseHandler
          javax.swing.plaf.basic.BasicComboPopup.ListMouseMotionHandler
          javax.swing.plaf.basic.BasicComboPopup.ListSelectionHandler
          javax.swing.plaf.basic.BasicComboPopup.PropertyChangeHandler
          javax.swing.plaf.basic.BasicDesktopIconUI javax.swing.plaf.basic.BasicDesktopIconUI.MouseInputHandler
          javax.swing.plaf.basic.BasicDesktopPaneUI javax.swing.plaf.basic.BasicDesktopPaneUI.CloseAction
          javax.swing.plaf.basic.BasicDesktopPaneUI.MaximizeAction
          javax.swing.plaf.basic.BasicDesktopPaneUI.MinimizeAction
          javax.swing.plaf.basic.BasicDesktopPaneUI.NavigateAction
          javax.swing.plaf.basic.BasicDesktopPaneUI.OpenAction javax.swing.plaf.basic.BasicDirectoryModel
          javax.swing.plaf.basic.BasicEditorPaneUI javax.swing.plaf.basic.BasicFileChooserUI
          javax.swing.plaf.basic.BasicFileChooserUI.AcceptAllFileFilter
          javax.swing.plaf.basic.BasicFileChooserUI.ApproveSelectionAction
          javax.swing.plaf.basic.BasicFileChooserUI.BasicFileView
          javax.swing.plaf.basic.BasicFileChooserUI.CancelSelectionAction
          javax.swing.plaf.basic.BasicFileChooserUI.ChangeToParentDirectoryAction
          javax.swing.plaf.basic.BasicFileChooserUI.DoubleClickListener
          javax.swing.plaf.basic.BasicFileChooserUI.GoHomeAction
          javax.swing.plaf.basic.BasicFileChooserUI.NewFolderAction
          javax.swing.plaf.basic.BasicFileChooserUI.SelectionListener
          javax.swing.plaf.basic.BasicFileChooserUI.UpdateAction
          javax.swing.plaf.basic.BasicFormattedTextFieldUI javax.swing.plaf.basic.BasicGraphicsUtils
          javax.swing.plaf.basic.BasicHTML javax.swing.plaf.basic.BasicIconFactory
          javax.swing.plaf.basic.BasicInternalFrameTitlePane
          javax.swing.plaf.basic.BasicInternalFrameTitlePane.CloseAction
          javax.swing.plaf.basic.BasicInternalFrameTitlePane.IconifyAction
          javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction
          javax.swing.plaf.basic.BasicInternalFrameTitlePane.MoveAction
          javax.swing.plaf.basic.BasicInternalFrameTitlePane.PropertyChangeHandler
          javax.swing.plaf.basic.BasicInternalFrameTitlePane.RestoreAction
          javax.swing.plaf.basic.BasicInternalFrameTitlePane.SizeAction
          javax.swing.plaf.basic.BasicInternalFrameTitlePane.SystemMenuBar
          javax.swing.plaf.basic.BasicInternalFrameTitlePane.TitlePaneLayout
          javax.swing.plaf.basic.BasicInternalFrameUI
          javax.swing.plaf.basic.BasicInternalFrameUI.BasicInternalFrameListener
          javax.swing.plaf.basic.BasicInternalFrameUI.BorderListener
          javax.swing.plaf.basic.BasicInternalFrameUI.ComponentHandler
          javax.swing.plaf.basic.BasicInternalFrameUI.GlassPaneDispatcher
          javax.swing.plaf.basic.BasicInternalFrameUI.InternalFrameLayout
          javax.swing.plaf.basic.BasicInternalFrameUI.InternalFramePropertyChangeListener
          javax.swing.plaf.basic.BasicLabelUI javax.swing.plaf.basic.BasicListUI
          javax.swing.plaf.basic.BasicListUI.FocusHandler javax.swing.plaf.basic.BasicListUI.ListDataHandler
          javax.swing.plaf.basic.BasicListUI.ListSelectionHandler
          javax.swing.plaf.basic.BasicListUI.MouseInputHandler
          javax.swing.plaf.basic.BasicListUI.PropertyChangeHandler javax.swing.plaf.basic.BasicLookAndFeel
          javax.swing.plaf.basic.BasicMenuBarUI javax.swing.plaf.basic.BasicMenuItemUI
          javax.swing.plaf.basic.BasicMenuUI
          javax.swing.plaf.basic.BasicMenuUI.ChangeHandler
          javax.swing.plaf.basic.BasicOptionPaneUI
          javax.swing.plaf.basic.BasicOptionPaneUI.ButtonActionListener
          javax.swing.plaf.basic.BasicOptionPaneUI.ButtonAreaLayout
          javax.swing.plaf.basic.BasicOptionPaneUI.PropertyChangeHandler javax.swing.plaf.basic.BasicPanelUI
          javax.swing.plaf.basic.BasicPasswordFieldUI javax.swing.plaf.basic.BasicPopupMenuSeparatorUI
          javax.swing.plaf.basic.BasicPopupMenuUI javax.swing.plaf.basic.BasicProgressBarUI
          javax.swing.plaf.basic.BasicProgressBarUI.ChangeHandler
          javax.swing.plaf.basic.BasicRadioButtonMenuItemUI javax.swing.plaf.basic.BasicRadioButtonUI
          javax.swing.plaf.basic.BasicRootPaneUI javax.swing.plaf.basic.BasicScrollBarUI
          javax.swing.plaf.basic.BasicScrollBarUI.ArrowButtonListener
          javax.swing.plaf.basic.BasicScrollBarUI.ModelListener
          javax.swing.plaf.basic.BasicScrollBarUI.PropertyChangeHandler
          javax.swing.plaf.basic.BasicScrollBarUI.ScrollListener
          javax.swing.plaf.basic.BasicScrollBarUI.TrackListener javax.swing.plaf.basic.BasicScrollPaneUI
          javax.swing.plaf.basic.BasicScrollPaneUI.MouseWheelHandler
          javax.swing.plaf.basic.BasicSeparatorUI
          javax.swing.plaf.basic.BasicSliderUI javax.swing.plaf.basic.BasicSliderUI.ActionScroller
          javax.swing.plaf.basic.BasicSliderUI.ChangeHandler
          javax.swing.plaf.basic.BasicSliderUI.ComponentHandler
          javax.swing.plaf.basic.BasicSliderUI.FocusHandler
          javax.swing.plaf.basic.BasicSliderUI.PropertyChangeHandler
          javax.swing.plaf.basic.BasicSliderUI.ScrollListener
          javax.swing.plaf.basic.BasicSliderUI.TrackListener javax.swing.plaf.basic.BasicSpinnerUI
          javax.swing.plaf.basic.BasicSplitPaneDivider
          javax.swing.plaf.basic.BasicSplitPaneDivider.DividerLayout
          javax.swing.plaf.basic.BasicSplitPaneDivider.DragController
          javax.swing.plaf.basic.BasicSplitPaneDivider.MouseHandler
          javax.swing.plaf.basic.BasicSplitPaneDivider.VerticalDragController
          javax.swing.plaf.basic.BasicSplitPaneUI
          javax.swing.plaf.basic.BasicSplitPaneUI.BasicHorizontalLayoutManager
          javax.swing.plaf.basic.BasicSplitPaneUI.BasicVerticalLayoutManager
          javax.swing.plaf.basic.BasicSplitPaneUI.FocusHandler
          javax.swing.plaf.basic.BasicSplitPaneUI.KeyboardDownRightHandler
          javax.swing.plaf.basic.BasicSplitPaneUI.KeyboardEndHandler
          javax.swing.plaf.basic.BasicSplitPaneUI.KeyboardHomeHandler
          javax.swing.plaf.basic.BasicSplitPaneUI.KeyboardResizeToggleHandler
          javax.swing.plaf.basic.BasicSplitPaneUI.KeyboardUpLeftHandler
          javax.swing.plaf.basic.BasicSplitPaneUI.PropertyHandler javax.swing.plaf.basic.BasicTabbedPaneUI
          javax.swing.plaf.basic.BasicTabbedPaneUI.FocusHandler
          javax.swing.plaf.basic.BasicTabbedPaneUI.MouseHandler
          javax.swing.plaf.basic.BasicTabbedPaneUI.PropertyChangeHandler
          javax.swing.plaf.basic.BasicTabbedPaneUI.TabbedPaneLayout
          javax.swing.plaf.basic.BasicTabbedPaneUI.TabSelectionHandler
          javax.swing.plaf.basic.BasicTableHeaderUI javax.swing.plaf.basic.BasicTableHeaderUI.MouseInputHandler
          javax.swing.plaf.basic.BasicTableUI javax.swing.plaf.basic.BasicTableUI.FocusHandler
          javax.swing.plaf.basic.BasicTableUI.KeyHandler javax.swing.plaf.basic.BasicTableUI.MouseInputHandler
          javax.swing.plaf.basic.BasicTextAreaUI javax.swing.plaf.basic.BasicTextFieldUI
          javax.swing.plaf.basic.BasicTextPaneUI javax.swing.plaf.basic.BasicTextUI
          javax.swing.plaf.basic.BasicTextUI.BasicCaret javax.swing.plaf.basic.BasicTextUI.BasicHighlighter
          javax.swing.plaf.basic.BasicToggleButtonUI javax.swing.plaf.basic.BasicToolBarSeparatorUI
          javax.swing.plaf.basic.BasicToolBarUI javax.swing.plaf.basic.BasicToolBarUI.DockingListener
          javax.swing.plaf.basic.BasicToolBarUI.DragWindow javax.swing.plaf.basic.BasicToolBarUI.FrameListener
          javax.swing.plaf.basic.BasicToolBarUI.PropertyListener
          javax.swing.plaf.basic.BasicToolBarUI.ToolBarContListener
          javax.swing.plaf.basic.BasicToolBarUI.ToolBarFocusListener javax.swing.plaf.basic.BasicToolTipUI
          javax.swing.plaf.basic.BasicTreeUI javax.swing.plaf.basic.BasicTreeUI.CellEditorHandler
          javax.swing.plaf.basic.BasicTreeUI.ComponentHandler javax.swing.plaf.basic.BasicTreeUI.FocusHandler
          javax.swing.plaf.basic.BasicTreeUI.KeyHandler javax.swing.plaf.basic.BasicTreeUI.MouseHandler
          javax.swing.plaf.basic.BasicTreeUI.MouseInputHandler
          javax.swing.plaf.basic.BasicTreeUI.NodeDimensionsHandler
          javax.swing.plaf.basic.BasicTreeUI.PropertyChangeHandler
          javax.swing.plaf.basic.BasicTreeUI.SelectionModelPropertyChangeHandler
          javax.swing.plaf.basic.BasicTreeUI.TreeCancelEditingAction
          javax.swing.plaf.basic.BasicTreeUI.TreeExpansionHandler
          javax.swing.plaf.basic.BasicTreeUI.TreeHomeAction
          javax.swing.plaf.basic.BasicTreeUI.TreeIncrementAction
          javax.swing.plaf.basic.BasicTreeUI.TreeModelHandler javax.swing.plaf.basic.BasicTreeUI.TreePageAction
          javax.swing.plaf.basic.BasicTreeUI.TreeSelectionHandler
          javax.swing.plaf.basic.BasicTreeUI.TreeToggleAction
          javax.swing.plaf.basic.BasicTreeUI.TreeTraverseAction javax.swing.plaf.basic.BasicViewportUI
          javax.swing.plaf.basic.DefaultMenuLayout javax.swing.plaf.BorderUIResource
          javax.swing.plaf.BorderUIResource.BevelBorderUIResource
          javax.swing.plaf.BorderUIResource.CompoundBorderUIResource
          javax.swing.plaf.BorderUIResource.EmptyBorderUIResource
          javax.swing.plaf.BorderUIResource.EtchedBorderUIResource
          javax.swing.plaf.BorderUIResource.LineBorderUIResource
          javax.swing.plaf.BorderUIResource.MatteBorderUIResource
          javax.swing.plaf.BorderUIResource.TitledBorderUIResource javax.swing.plaf.ButtonUI
          javax.swing.plaf.ColorChooserUI javax.swing.plaf.ColorUIResource javax.swing.plaf.ComboBoxUI
          javax.swing.plaf.ComponentInputMapUIResource javax.swing.plaf.ComponentUI
          javax.swing.plaf.DesktopIconUI javax.swing.plaf.DesktopPaneUI javax.swing.plaf.DimensionUIResource
          javax.swing.plaf.FileChooserUI javax.swing.plaf.FontUIResource javax.swing.plaf.IconUIResource
          javax.swing.plaf.InputMapUIResource javax.swing.plaf.InsetsUIResource
          javax.swing.plaf.InternalFrameUI javax.swing.plaf.LabelUI javax.swing.plaf.LayerUI
          javax.swing.plaf.ListUI javax.swing.plaf.MenuBarUI javax.swing.plaf.MenuItemUI
          javax.swing.plaf.metal.DefaultMetalTheme javax.swing.plaf.metal.MetalBorders
          javax.swing.plaf.metal.MetalBorders.ButtonBorder javax.swing.plaf.metal.MetalBorders.Flush3DBorder
          javax.swing.plaf.metal.MetalBorders.InternalFrameBorder
          javax.swing.plaf.metal.MetalBorders.MenuBarBorder javax.swing.plaf.metal.MetalBorders.MenuItemBorder
          javax.swing.plaf.metal.MetalBorders.OptionDialogBorder
          javax.swing.plaf.metal.MetalBorders.PaletteBorder javax.swing.plaf.metal.MetalBorders.PopupMenuBorder
          javax.swing.plaf.metal.MetalBorders.RolloverButtonBorder
          javax.swing.plaf.metal.MetalBorders.ScrollPaneBorder
          javax.swing.plaf.metal.MetalBorders.TableHeaderBorder
          javax.swing.plaf.metal.MetalBorders.TextFieldBorder
          javax.swing.plaf.metal.MetalBorders.ToggleButtonBorder
          javax.swing.plaf.metal.MetalBorders.ToolBarBorder javax.swing.plaf.metal.MetalButtonUI
          javax.swing.plaf.metal.MetalCheckBoxIcon javax.swing.plaf.metal.MetalCheckBoxUI
          javax.swing.plaf.metal.MetalComboBoxButton javax.swing.plaf.metal.MetalComboBoxEditor
          javax.swing.plaf.metal.MetalComboBoxEditor.UIResource javax.swing.plaf.metal.MetalComboBoxIcon
          javax.swing.plaf.metal.MetalComboBoxUI
          javax.swing.plaf.metal.MetalComboBoxUI.MetalComboBoxLayoutManager
          javax.swing.plaf.metal.MetalComboBoxUI.MetalPropertyChangeListener
          javax.swing.plaf.metal.MetalDesktopIconUI javax.swing.plaf.metal.MetalFileChooserUI
          javax.swing.plaf.metal.MetalFileChooserUI.DirectoryComboBoxAction
          javax.swing.plaf.metal.MetalFileChooserUI.DirectoryComboBoxModel
          javax.swing.plaf.metal.MetalFileChooserUI.FilterComboBoxModel
          javax.swing.plaf.metal.MetalFileChooserUI.FilterComboBoxRenderer
          javax.swing.plaf.metal.MetalIconFactory
          javax.swing.plaf.metal.MetalIconFactory.FileIcon16
          javax.swing.plaf.metal.MetalIconFactory.FolderIcon16
          javax.swing.plaf.metal.MetalIconFactory.PaletteCloseIcon
          javax.swing.plaf.metal.MetalIconFactory.TreeControlIcon
          javax.swing.plaf.metal.MetalIconFactory.TreeFolderIcon
          javax.swing.plaf.metal.MetalIconFactory.TreeLeafIcon
          javax.swing.plaf.metal.MetalInternalFrameTitlePane javax.swing.plaf.metal.MetalInternalFrameUI
          javax.swing.plaf.metal.MetalLabelUI javax.swing.plaf.metal.MetalLookAndFeel
          javax.swing.plaf.metal.MetalMenuBarUI javax.swing.plaf.metal.MetalPopupMenuSeparatorUI
          javax.swing.plaf.metal.MetalProgressBarUI javax.swing.plaf.metal.MetalRadioButtonUI
          javax.swing.plaf.metal.MetalRootPaneUI javax.swing.plaf.metal.MetalScrollBarUI
          javax.swing.plaf.metal.MetalScrollButton javax.swing.plaf.metal.MetalScrollPaneUI
          javax.swing.plaf.metal.MetalSeparatorUI javax.swing.plaf.metal.MetalSliderUI
          javax.swing.plaf.metal.MetalSliderUI.MetalPropertyListener javax.swing.plaf.metal.MetalSplitPaneUI
          javax.swing.plaf.metal.MetalTabbedPaneUI javax.swing.plaf.metal.MetalTabbedPaneUI.TabbedPaneLayout
          javax.swing.plaf.metal.MetalTextFieldUI javax.swing.plaf.metal.MetalTheme
          javax.swing.plaf.metal.MetalToggleButtonUI javax.swing.plaf.metal.MetalToolBarUI
          javax.swing.plaf.metal.MetalToolBarUI.MetalContainerListener
          javax.swing.plaf.metal.MetalToolBarUI.MetalDockingListener
          javax.swing.plaf.metal.MetalToolBarUI.MetalRolloverListener javax.swing.plaf.metal.MetalToolTipUI
          javax.swing.plaf.metal.MetalTreeUI javax.swing.plaf.metal.OceanTheme
          javax.swing.plaf.multi.MultiButtonUI javax.swing.plaf.multi.MultiColorChooserUI
          javax.swing.plaf.multi.MultiComboBoxUI javax.swing.plaf.multi.MultiDesktopIconUI
          javax.swing.plaf.multi.MultiDesktopPaneUI javax.swing.plaf.multi.MultiFileChooserUI
          javax.swing.plaf.multi.MultiInternalFrameUI javax.swing.plaf.multi.MultiLabelUI
          javax.swing.plaf.multi.MultiListUI javax.swing.plaf.multi.MultiLookAndFeel
          javax.swing.plaf.multi.MultiMenuBarUI javax.swing.plaf.multi.MultiMenuItemUI
          javax.swing.plaf.multi.MultiOptionPaneUI javax.swing.plaf.multi.MultiPanelUI
          javax.swing.plaf.multi.MultiPopupMenuUI javax.swing.plaf.multi.MultiProgressBarUI
          javax.swing.plaf.multi.MultiRootPaneUI javax.swing.plaf.multi.MultiScrollBarUI
          javax.swing.plaf.multi.MultiScrollPaneUI javax.swing.plaf.multi.MultiSeparatorUI
          javax.swing.plaf.multi.MultiSliderUI javax.swing.plaf.multi.MultiSpinnerUI
          javax.swing.plaf.multi.MultiSplitPaneUI javax.swing.plaf.multi.MultiTabbedPaneUI
          javax.swing.plaf.multi.MultiTableHeaderUI javax.swing.plaf.multi.MultiTableUI
          javax.swing.plaf.multi.MultiTextUI javax.swing.plaf.multi.MultiToolBarUI
          javax.swing.plaf.multi.MultiToolTipUI javax.swing.plaf.multi.MultiTreeUI
          javax.swing.plaf.multi.MultiViewportUI javax.swing.plaf.nimbus.AbstractRegionPainter
          javax.swing.plaf.nimbus.AbstractRegionPainter.PaintContext javax.swing.plaf.nimbus.NimbusLookAndFeel
          javax.swing.plaf.nimbus.NimbusStyle javax.swing.plaf.nimbus.State javax.swing.plaf.OptionPaneUI
          javax.swing.plaf.PanelUI javax.swing.plaf.PopupMenuUI javax.swing.plaf.ProgressBarUI
          javax.swing.plaf.RootPaneUI javax.swing.plaf.ScrollBarUI javax.swing.plaf.ScrollPaneUI
          javax.swing.plaf.SeparatorUI javax.swing.plaf.SliderUI javax.swing.plaf.SpinnerUI
          javax.swing.plaf.SplitPaneUI javax.swing.plaf.synth.ColorType javax.swing.plaf.synth.Region
          javax.swing.plaf.synth.SynthButtonUI javax.swing.plaf.synth.SynthCheckBoxMenuItemUI
          javax.swing.plaf.synth.SynthCheckBoxUI javax.swing.plaf.synth.SynthColorChooserUI
          javax.swing.plaf.synth.SynthComboBoxUI javax.swing.plaf.synth.SynthContext
          javax.swing.plaf.synth.SynthDesktopIconUI javax.swing.plaf.synth.SynthDesktopPaneUI
          javax.swing.plaf.synth.SynthEditorPaneUI javax.swing.plaf.synth.SynthFormattedTextFieldUI
          javax.swing.plaf.synth.SynthGraphicsUtils javax.swing.plaf.synth.SynthInternalFrameUI
          javax.swing.plaf.synth.SynthLabelUI javax.swing.plaf.synth.SynthListUI
          javax.swing.plaf.synth.SynthLookAndFeel javax.swing.plaf.synth.SynthMenuBarUI
          javax.swing.plaf.synth.SynthMenuItemUI javax.swing.plaf.synth.SynthMenuUI
          javax.swing.plaf.synth.SynthOptionPaneUI javax.swing.plaf.synth.SynthPainter
          javax.swing.plaf.synth.SynthPanelUI javax.swing.plaf.synth.SynthPasswordFieldUI
          javax.swing.plaf.synth.SynthPopupMenuUI javax.swing.plaf.synth.SynthProgressBarUI
          javax.swing.plaf.synth.SynthRadioButtonMenuItemUI javax.swing.plaf.synth.SynthRadioButtonUI
          javax.swing.plaf.synth.SynthRootPaneUI javax.swing.plaf.synth.SynthScrollBarUI
          javax.swing.plaf.synth.SynthScrollPaneUI javax.swing.plaf.synth.SynthSeparatorUI
          javax.swing.plaf.synth.SynthSliderUI javax.swing.plaf.synth.SynthSpinnerUI
          javax.swing.plaf.synth.SynthSplitPaneUI javax.swing.plaf.synth.SynthStyle
          javax.swing.plaf.synth.SynthStyleFactory javax.swing.plaf.synth.SynthTabbedPaneUI
          javax.swing.plaf.synth.SynthTableHeaderUI javax.swing.plaf.synth.SynthTableUI
          javax.swing.plaf.synth.SynthTextAreaUI javax.swing.plaf.synth.SynthTextFieldUI
          javax.swing.plaf.synth.SynthTextPaneUI javax.swing.plaf.synth.SynthToggleButtonUI
          javax.swing.plaf.synth.SynthToolBarUI javax.swing.plaf.synth.SynthToolTipUI
          javax.swing.plaf.synth.SynthTreeUI javax.swing.plaf.synth.SynthViewportUI
          javax.swing.plaf.TabbedPaneUI javax.swing.plaf.TableHeaderUI javax.swing.plaf.TableUI
          javax.swing.plaf.TextUI javax.swing.plaf.ToolBarUI javax.swing.plaf.ToolTipUI javax.swing.plaf.TreeUI
          javax.swing.plaf.ViewportUI javax.swing.Popup javax.swing.PopupFactory javax.swing.ProgressMonitor
          javax.swing.ProgressMonitor.AccessibleProgressMonitor javax.swing.ProgressMonitorInputStream
          javax.swing.RepaintManager javax.swing.RowFilter javax.swing.RowFilter.Entry javax.swing.RowSorter
          javax.swing.RowSorter.SortKey javax.swing.ScrollPaneLayout javax.swing.ScrollPaneLayout.UIResource
          javax.swing.SizeRequirements javax.swing.SizeSequence javax.swing.SortingFocusTraversalPolicy
          javax.swing.SpinnerDateModel javax.swing.SpinnerListModel javax.swing.SpinnerNumberModel
          javax.swing.Spring javax.swing.SpringLayout javax.swing.SpringLayout.Constraints
          javax.swing.SwingUtilities javax.swing.SwingWorker javax.swing.table.AbstractTableModel
          javax.swing.table.DefaultTableCellRenderer javax.swing.table.DefaultTableCellRenderer.UIResource
          javax.swing.table.DefaultTableColumnModel javax.swing.table.DefaultTableModel
          javax.swing.table.JTableHeader javax.swing.table.JTableHeader.AccessibleJTableHeader
          javax.swing.table.JTableHeader.AccessibleJTableHeader.AccessibleJTableHeaderEntry
          javax.swing.table.TableColumn javax.swing.table.TableRowSorter javax.swing.table.TableStringConverter
          javax.swing.text.AbstractDocument javax.swing.text.AbstractDocument.AbstractElement
          javax.swing.text.AbstractDocument.BranchElement
          javax.swing.text.AbstractDocument.DefaultDocumentEvent javax.swing.text.AbstractDocument.ElementEdit
          javax.swing.text.AbstractDocument.LeafElement javax.swing.text.AbstractWriter
          javax.swing.text.AsyncBoxView javax.swing.text.AsyncBoxView.ChildLocator
          javax.swing.text.AsyncBoxView.ChildState javax.swing.text.BadLocationException
          javax.swing.text.BoxView javax.swing.text.ChangedCharSetException javax.swing.text.ComponentView
          javax.swing.text.CompositeView javax.swing.text.DateFormatter javax.swing.text.DefaultCaret
          javax.swing.text.DefaultEditorKit javax.swing.text.DefaultEditorKit.BeepAction
          javax.swing.text.DefaultEditorKit.CopyAction javax.swing.text.DefaultEditorKit.CutAction
          javax.swing.text.DefaultEditorKit.DefaultKeyTypedAction
          javax.swing.text.DefaultEditorKit.InsertBreakAction
          javax.swing.text.DefaultEditorKit.InsertContentAction
          javax.swing.text.DefaultEditorKit.InsertTabAction javax.swing.text.DefaultEditorKit.PasteAction
          javax.swing.text.DefaultFormatter javax.swing.text.DefaultFormatterFactory
          javax.swing.text.DefaultHighlighter javax.swing.text.DefaultHighlighter.DefaultHighlightPainter
          javax.swing.text.DefaultStyledDocument javax.swing.text.DefaultStyledDocument.AttributeUndoableEdit
          javax.swing.text.DefaultStyledDocument.ElementBuffer
          javax.swing.text.DefaultStyledDocument.ElementSpec
          javax.swing.text.DefaultStyledDocument.SectionElement
          javax.swing.text.DocumentFilter javax.swing.text.DocumentFilter.FilterBypass
          javax.swing.text.EditorKit javax.swing.text.ElementIterator javax.swing.text.FieldView
          javax.swing.text.FlowView javax.swing.text.FlowView.FlowStrategy javax.swing.text.GapContent
          javax.swing.text.GlyphView javax.swing.text.GlyphView.GlyphPainter javax.swing.text.html.BlockView
          javax.swing.text.html.CSS javax.swing.text.html.CSS.Attribute javax.swing.text.html.FormSubmitEvent
          javax.swing.text.html.FormView javax.swing.text.html.FormView.MouseEventListener
          javax.swing.text.html.HTML javax.swing.text.html.HTML.Attribute javax.swing.text.html.HTMLDocument
          javax.swing.text.html.HTMLDocument.BlockElement javax.swing.text.html.HTMLDocument.HTMLReader
          javax.swing.text.html.HTMLDocument.HTMLReader.BlockAction
          javax.swing.text.html.HTMLDocument.HTMLReader.CharacterAction
          javax.swing.text.html.HTMLDocument.HTMLReader.FormAction
          javax.swing.text.html.HTMLDocument.HTMLReader.HiddenAction
          javax.swing.text.html.HTMLDocument.HTMLReader.IsindexAction
          javax.swing.text.html.HTMLDocument.HTMLReader.ParagraphAction
          javax.swing.text.html.HTMLDocument.HTMLReader.PreAction
          javax.swing.text.html.HTMLDocument.HTMLReader.SpecialAction
          javax.swing.text.html.HTMLDocument.HTMLReader.TagAction javax.swing.text.html.HTMLDocument.Iterator
          javax.swing.text.html.HTMLDocument.RunElement javax.swing.text.html.HTMLEditorKit
          javax.swing.text.html.HTMLEditorKit.HTMLFactory javax.swing.text.html.HTMLEditorKit.HTMLTextAction
          javax.swing.text.html.HTMLEditorKit.InsertHTMLTextAction
          javax.swing.text.html.HTMLEditorKit.LinkController javax.swing.text.html.HTMLEditorKit.Parser
          javax.swing.text.html.HTMLEditorKit.ParserCallback javax.swing.text.html.HTMLFrameHyperlinkEvent
          javax.swing.text.html.HTML.Tag javax.swing.text.html.HTML.UnknownTag javax.swing.text.html.HTMLWriter
          javax.swing.text.html.ImageView javax.swing.text.html.InlineView javax.swing.text.html.ListView
          javax.swing.text.html.MinimalHTMLWriter javax.swing.text.html.ObjectView javax.swing.text.html.Option
          javax.swing.text.html.ParagraphView javax.swing.text.html.parser.AttributeList
          javax.swing.text.html.parser.ContentModel javax.swing.text.html.parser.DocumentParser
          javax.swing.text.html.parser.DTD javax.swing.text.html.parser.Element
          javax.swing.text.html.parser.Entity javax.swing.text.html.parser.Parser
          javax.swing.text.html.parser.ParserDelegator javax.swing.text.html.parser.TagElement
          javax.swing.text.html.StyleSheet javax.swing.text.html.StyleSheet.BoxPainter
          javax.swing.text.html.StyleSheet.ListPainter javax.swing.text.IconView
          javax.swing.text.InternationalFormatter javax.swing.text.JTextComponent
          javax.swing.text.JTextComponent.AccessibleJTextComponent javax.swing.text.JTextComponent.DropLocation
          javax.swing.text.JTextComponent.KeyBinding javax.swing.text.LabelView
          javax.swing.text.LayeredHighlighter javax.swing.text.LayeredHighlighter.LayerPainter
          javax.swing.text.LayoutQueue javax.swing.text.MaskFormatter javax.swing.text.NavigationFilter
          javax.swing.text.NavigationFilter.FilterBypass javax.swing.text.NumberFormatter
          javax.swing.text.ParagraphView javax.swing.text.PasswordView javax.swing.text.PlainDocument
          javax.swing.text.PlainView javax.swing.text.Position.Bias javax.swing.text.rtf.RTFEditorKit
          javax.swing.text.Segment javax.swing.text.SimpleAttributeSet javax.swing.text.StringContent
          javax.swing.text.StyleConstants javax.swing.text.StyleConstants.CharacterConstants
          javax.swing.text.StyleConstants.ColorConstants javax.swing.text.StyleConstants.FontConstants
          javax.swing.text.StyleConstants.ParagraphConstants javax.swing.text.StyleContext
          javax.swing.text.StyleContext.NamedStyle javax.swing.text.StyleContext.SmallAttributeSet
          javax.swing.text.StyledEditorKit javax.swing.text.StyledEditorKit.AlignmentAction
          javax.swing.text.StyledEditorKit.BoldAction javax.swing.text.StyledEditorKit.FontFamilyAction
          javax.swing.text.StyledEditorKit.FontSizeAction javax.swing.text.StyledEditorKit.ForegroundAction
          javax.swing.text.StyledEditorKit.ItalicAction javax.swing.text.StyledEditorKit.StyledTextAction
          javax.swing.text.StyledEditorKit.UnderlineAction javax.swing.text.TableView
          javax.swing.text.TableView.TableRow javax.swing.text.TabSet
          javax.swing.text.TabStop javax.swing.text.TextAction javax.swing.text.Utilities javax.swing.text.View
          javax.swing.text.WrappedPlainView javax.swing.text.ZoneView javax.swing.Timer
          javax.swing.ToolTipManager javax.swing.ToolTipManager.insideTimerAction
          javax.swing.ToolTipManager.outsideTimerAction javax.swing.ToolTipManager.stillInsideTimerAction
          javax.swing.TransferHandler javax.swing.TransferHandler.DropLocation
          javax.swing.TransferHandler.TransferSupport javax.swing.tree.AbstractLayoutCache
          javax.swing.tree.AbstractLayoutCache.NodeDimensions javax.swing.tree.DefaultMutableTreeNode
          javax.swing.tree.DefaultTreeCellEditor javax.swing.tree.DefaultTreeCellEditor.DefaultTextField
          javax.swing.tree.DefaultTreeCellEditor.EditorContainer javax.swing.tree.DefaultTreeCellRenderer
          javax.swing.tree.DefaultTreeModel javax.swing.tree.DefaultTreeSelectionModel
          javax.swing.tree.ExpandVetoException javax.swing.tree.FixedHeightLayoutCache
          javax.swing.tree.TreePath javax.swing.tree.VariableHeightLayoutCache javax.swing.UIDefaults
          javax.swing.UIDefaults.LazyInputMap javax.swing.UIDefaults.ProxyLazyValue javax.swing.UIManager
          javax.swing.UIManager.LookAndFeelInfo javax.swing.undo.AbstractUndoableEdit
          javax.swing.undo.CannotRedoException javax.swing.undo.CannotUndoException
          javax.swing.undo.CompoundEdit javax.swing.undo.StateEdit javax.swing.undo.UndoableEditSupport
          javax.swing.undo.UndoManager javax.swing.UnsupportedLookAndFeelException
          javax.swing.ViewportLayout'

        javaxClassHierarchy_P='javax.tools.DiagnosticCollector
          javax.tools.ForwardingFileObject javax.tools.ForwardingJavaFileManager
          javax.tools.ForwardingJavaFileObject javax.tools.SimpleJavaFileObject javax.tools.ToolProvider'

        javaxClassHierarchy_Q='javax.transaction.xa.XAException'

        javaxClassHierarchy_R='javax.xml.catalog.CatalogException javax.xml.catalog.CatalogFeatures
          javax.xml.catalog.CatalogFeatures.Builder javax.xml.catalog.CatalogManager
          javax.xml.crypto.dom.DOMCryptoContext javax.xml.crypto.dom.DOMStructure
          javax.xml.crypto.dsig.dom.DOMSignContext javax.xml.crypto.dsig.dom.DOMValidateContext
          javax.xml.crypto.dsig.keyinfo.KeyInfoFactory javax.xml.crypto.dsig.spec.ExcC14NParameterSpec
          javax.xml.crypto.dsig.spec.HMACParameterSpec javax.xml.crypto.dsig.spec.XPathFilter2ParameterSpec
          javax.xml.crypto.dsig.spec.XPathFilterParameterSpec javax.xml.crypto.dsig.spec.XPathType
          javax.xml.crypto.dsig.spec.XPathType.Filter javax.xml.crypto.dsig.spec.XSLTTransformParameterSpec
          javax.xml.crypto.dsig.TransformException javax.xml.crypto.dsig.TransformService
          javax.xml.crypto.dsig.XMLSignatureException javax.xml.crypto.dsig.XMLSignatureFactory
          javax.xml.crypto.KeySelector javax.xml.crypto.KeySelectorException
          javax.xml.crypto.KeySelector.Purpose javax.xml.crypto.MarshalException
          javax.xml.crypto.NoSuchMechanismException javax.xml.crypto.OctetStreamData
          javax.xml.crypto.URIReferenceException javax.xml.datatype.DatatypeConfigurationException
          javax.xml.datatype.DatatypeConstants javax.xml.datatype.DatatypeConstants.Field
          javax.xml.datatype.DatatypeFactory javax.xml.datatype.Duration
          javax.xml.datatype.XMLGregorianCalendar javax.xml.namespace.QName javax.xml.parsers.DocumentBuilder
          javax.xml.parsers.DocumentBuilderFactory javax.xml.parsers.FactoryConfigurationError
          javax.xml.parsers.ParserConfigurationException javax.xml.parsers.SAXParser
          javax.xml.parsers.SAXParserFactory javax.xml.stream.FactoryConfigurationError
          javax.xml.stream.util.EventReaderDelegate javax.xml.stream.util.StreamReaderDelegate
          javax.xml.stream.XMLEventFactory javax.xml.stream.XMLInputFactory javax.xml.stream.XMLOutputFactory
          javax.xml.stream.XMLStreamException javax.xml.transform.dom.DOMResult
          javax.xml.transform.dom.DOMSource javax.xml.transform.OutputKeys javax.xml.transform.sax.SAXResult
          javax.xml.transform.sax.SAXSource javax.xml.transform.sax.SAXTransformerFactory
          javax.xml.transform.stax.StAXResult javax.xml.transform.stax.StAXSource
          javax.xml.transform.stream.StreamResult javax.xml.transform.stream.StreamSource
          javax.xml.transform.Transformer javax.xml.transform.TransformerConfigurationException
          javax.xml.transform.TransformerException javax.xml.transform.TransformerFactory
          javax.xml.transform.TransformerFactoryConfigurationError javax.xml.validation.Schema
          javax.xml.validation.SchemaFactory javax.xml.validation.SchemaFactoryConfigurationError
          javax.xml.validation.SchemaFactoryLoader javax.xml.validation.TypeInfoProvider
          javax.xml.validation.Validator javax.xml.validation.ValidatorHandler javax.xml.XMLConstants
          javax.xml.xpath.XPathConstants javax.xml.xpath.XPathException
          javax.xml.xpath.XPathExpressionException javax.xml.xpath.XPathFactory
          javax.xml.xpath.XPathFactoryConfigurationException javax.xml.xpath.XPathFunctionException'

        # ------------------------------------------------------------------------------------------ #
        print_completers() {

          format=' %s\;||%s'

          for import in $@
          do
            printf "$format" "$import" "$import"
          done

        }
        # ---------------------------------------------------------------------------------------------- #
        printf "set-option -add window java_hierarchy_completions "

        print_completers $javaxClassHierarchy_A $javaxClassHierarchy_B $javaxClassHierarchy_C $javaxClassHierarchy_D $javaxClassHierarchy_E $javaxClassHierarchy_F
        print_completers $javaxClassHierarchy_G $javaxClassHierarchy_H $javaxClassHierarchy_I $javaxClassHierarchy_J $javaxClassHierarchy_K $javaxClassHierarchy_L
        print_completers $javaxClassHierarchy_M $javaxClassHierarchy_N $javaxClassHierarchy_O $javaxClassHierarchy_P $javaxClassHierarchy_Q $javaxClassHierarchy_R

        printf "\n"
        # ------------------------------------------------------------------------------------------ #
      }
      # --------------- STAGE 3 OF 7 --------------------------------------------------------------- #
      evaluate-commands %sh{
        # ------------------ MISC CLASS HIERARCHY -------------------------------------------------- #

        sunClassHierarchy='com.sun.java.accessibility.util.AccessibilityEventMonitor
          com.sun.java.accessibility.util.AccessibilityListenerList
          com.sun.java.accessibility.util.AWTEventMonitor com.sun.java.accessibility.util.EventID
          com.sun.java.accessibility.util.EventQueueMonitor com.sun.java.accessibility.util.SwingEventMonitor
          com.sun.java.accessibility.util.Translator com.sun.jdi.AbsentInformationException
          com.sun.jdi.Bootstrap com.sun.jdi.ClassNotLoadedException com.sun.jdi.ClassNotPreparedException
          com.sun.jdi.connect.IllegalConnectorArgumentsException
          com.sun.jdi.connect.spi.ClosedConnectionException com.sun.jdi.connect.spi.Connection
          com.sun.jdi.connect.spi.TransportService com.sun.jdi.connect.spi.TransportService.Capabilities
          com.sun.jdi.connect.spi.TransportService.ListenKey com.sun.jdi.connect.TransportTimeoutException
          com.sun.jdi.connect.VMStartException com.sun.jdi.IncompatibleThreadStateException
          com.sun.jdi.InconsistentDebugInfoException com.sun.jdi.InternalException
          com.sun.jdi.InvalidModuleException com.sun.jdi.InvalidStackFrameException
          com.sun.jdi.InvalidTypeException com.sun.jdi.InvocationException com.sun.jdi.JDIPermission
          com.sun.jdi.NativeMethodException com.sun.jdi.ObjectCollectedException
          com.sun.jdi.request.DuplicateRequestException com.sun.jdi.request.InvalidRequestStateException
          com.sun.jdi.VMCannotBeModifiedException com.sun.jdi.VMDisconnectedException
          com.sun.jdi.VMMismatchException com.sun.jdi.VMOutOfMemoryException
          com.sun.management.GarbageCollectionNotificationInfo com.sun.management.GcInfo
          com.sun.management.VMOption com.sun.net.httpserver.Authenticator
          com.sun.net.httpserver.Authenticator.Failure com.sun.net.httpserver.Authenticator.Result
          com.sun.net.httpserver.Authenticator.Retry com.sun.net.httpserver.Authenticator.Success
          com.sun.net.httpserver.BasicAuthenticator com.sun.net.httpserver.Filter
          com.sun.net.httpserver.Filter.Chain com.sun.net.httpserver.Headers com.sun.net.httpserver.HttpContext
          com.sun.net.httpserver.HttpExchange com.sun.net.httpserver.HttpPrincipal
          com.sun.net.httpserver.HttpsConfigurator com.sun.net.httpserver.HttpServer
          com.sun.net.httpserver.HttpsExchange com.sun.net.httpserver.HttpsParameters
          com.sun.net.httpserver.HttpsServer com.sun.net.httpserver.spi.HttpServerProvider
          com.sun.nio.sctp.AbstractNotificationHandler com.sun.nio.sctp.Association
          com.sun.nio.sctp.AssociationChangeNotification com.sun.nio.sctp.IllegalReceiveException
          com.sun.nio.sctp.IllegalUnbindException com.sun.nio.sctp.InvalidStreamException
          com.sun.nio.sctp.MessageInfo com.sun.nio.sctp.PeerAddressChangeNotification
          com.sun.nio.sctp.SctpChannel com.sun.nio.sctp.SctpMultiChannel com.sun.nio.sctp.SctpServerChannel
          com.sun.nio.sctp.SctpStandardSocketOptions com.sun.nio.sctp.SctpStandardSocketOptions.InitMaxStreams
          com.sun.nio.sctp.SendFailedNotification com.sun.nio.sctp.ShutdownNotification
          com.sun.security.auth.callback.TextCallbackHandler com.sun.security.auth.LdapPrincipal
          com.sun.security.auth.login.ConfigFile com.sun.security.auth.module.JndiLoginModule
          com.sun.security.auth.module.KeyStoreLoginModule com.sun.security.auth.module.Krb5LoginModule
          com.sun.security.auth.module.LdapLoginModule com.sun.security.auth.module.NTLoginModule
          com.sun.security.auth.module.NTSystem com.sun.security.auth.module.UnixLoginModule
          com.sun.security.auth.module.UnixSystem com.sun.security.auth.NTDomainPrincipal
          com.sun.security.auth.NTNumericCredential com.sun.security.auth.NTSid
          com.sun.security.auth.NTSidDomainPrincipal com.sun.security.auth.NTSidGroupPrincipal
          com.sun.security.auth.NTSidPrimaryGroupPrincipal com.sun.security.auth.NTSidUserPrincipal
          com.sun.security.auth.NTUserPrincipal com.sun.security.auth.UnixNumericGroupPrincipal
          com.sun.security.auth.UnixNumericUserPrincipal com.sun.security.auth.UnixPrincipal
          com.sun.security.auth.UserPrincipal com.sun.security.jgss.AuthorizationDataEntry
          com.sun.security.jgss.GSSUtil com.sun.security.jgss.InquireSecContextPermission
          com.sun.source.util.DocTreePath com.sun.source.util.DocTreePathScanner com.sun.source.util.DocTrees
          com.sun.source.util.DocTreeScanner com.sun.source.util.JavacTask
          com.sun.source.util.SimpleDocTreeVisitor com.sun.source.util.SimpleTreeVisitor
          com.sun.source.util.TaskEvent com.sun.source.util.TreePath com.sun.source.util.TreePathScanner
          com.sun.source.util.Trees com.sun.source.util.TreeScanner
          com.sun.tools.attach.AgentInitializationException com.sun.tools.attach.AgentLoadException
          com.sun.tools.attach.AttachNotSupportedException com.sun.tools.attach.AttachOperationFailedException
          com.sun.tools.attach.AttachPermission com.sun.tools.attach.spi.AttachProvider
          com.sun.tools.attach.VirtualMachine com.sun.tools.attach.VirtualMachineDescriptor
          com.sun.tools.javac.Main com.sun.tools.jconsole.JConsolePlugin'

        # ------------------------------------------------------------------------------------------ #

        jdkClassHierarchy='jdk.dynalink.beans.BeansLinker jdk.dynalink.beans.StaticClass
          jdk.dynalink.CallSiteDescriptor jdk.dynalink.DynamicLinker jdk.dynalink.DynamicLinkerFactory
          jdk.dynalink.linker.GuardedInvocation jdk.dynalink.linker.GuardingDynamicLinkerExporter
          jdk.dynalink.linker.support.CompositeGuardingDynamicLinker
          jdk.dynalink.linker.support.CompositeTypeBasedGuardingDynamicLinker
          jdk.dynalink.linker.support.DefaultInternalObjectFilter jdk.dynalink.linker.support.Guards
          jdk.dynalink.linker.support.Lookup jdk.dynalink.linker.support.SimpleLinkRequest
          jdk.dynalink.linker.support.TypeUtilities jdk.dynalink.NamedOperation jdk.dynalink.NamespaceOperation
          jdk.dynalink.NoSuchDynamicMethodException jdk.dynalink.SecureLookupSupplier
          jdk.dynalink.support.AbstractRelinkableCallSite jdk.dynalink.support.ChainedCallSite
          jdk.dynalink.support.SimpleRelinkableCallSite jdk.incubator.foreign.GroupLayout
          jdk.incubator.foreign.MemoryHandles jdk.incubator.foreign.MemoryLayouts
          jdk.incubator.foreign.SequenceLayout jdk.incubator.foreign.ValueLayout jdk.internal.event.Event
          jdk.javadoc.doclet.StandardDoclet jdk.jfr.AnnotationElement jdk.jfr.Configuration
          jdk.jfr.consumer.RecordedClass jdk.jfr.consumer.RecordedClassLoader jdk.jfr.consumer.RecordedEvent
          jdk.jfr.consumer.RecordedFrame jdk.jfr.consumer.RecordedMethod jdk.jfr.consumer.RecordedObject
          jdk.jfr.consumer.RecordedStackTrace jdk.jfr.consumer.RecordedThread
          jdk.jfr.consumer.RecordedThreadGroup jdk.jfr.consumer.RecordingFile jdk.jfr.consumer.RecordingStream
          jdk.jfr.Event jdk.jfr.EventFactory jdk.jfr.EventSettings jdk.jfr.EventType jdk.jfr.FlightRecorder
          jdk.jfr.FlightRecorderPermission jdk.jfr.internal.Control jdk.jfr.Recording jdk.jfr.SettingControl
          jdk.jfr.SettingDescriptor jdk.jfr.ValueDescriptor jdk.jshell.DeclarationSnippet jdk.jshell.Diag
          jdk.jshell.ErroneousSnippet jdk.jshell.EvalException jdk.jshell.execution.DirectExecutionControl
          jdk.jshell.execution.FailOverExecutionControlProvider jdk.jshell.execution.JdiDefaultExecutionControl
          jdk.jshell.execution.JdiExecutionControl jdk.jshell.execution.JdiExecutionControlProvider
          jdk.jshell.execution.JdiInitiator jdk.jshell.execution.LocalExecutionControl
          jdk.jshell.execution.LocalExecutionControlProvider jdk.jshell.execution.RemoteExecutionControl
          jdk.jshell.execution.StreamingExecutionControl jdk.jshell.execution.Util jdk.jshell.ExpressionSnippet
          jdk.jshell.ImportSnippet jdk.jshell.JShell jdk.jshell.JShell.Builder jdk.jshell.JShellException
          jdk.jshell.JShell.Subscription jdk.jshell.MethodSnippet jdk.jshell.PersistentSnippet
          jdk.jshell.Snippet jdk.jshell.SnippetEvent jdk.jshell.SourceCodeAnalysis
          jdk.jshell.SourceCodeAnalysis.QualifiedNames jdk.jshell.spi.ExecutionControl.ClassBytecodes
          jdk.jshell.spi.ExecutionControl.ClassInstallException
          jdk.jshell.spi.ExecutionControl.EngineTerminationException
          jdk.jshell.spi.ExecutionControl.ExecutionControlException
          jdk.jshell.spi.ExecutionControl.InternalException
          jdk.jshell.spi.ExecutionControl.NotImplementedException
          jdk.jshell.spi.ExecutionControl.ResolutionException jdk.jshell.spi.ExecutionControl.RunException
          jdk.jshell.spi.ExecutionControl.StoppedException jdk.jshell.spi.ExecutionControl.UserException
          jdk.jshell.spi.SPIResolutionException jdk.jshell.StatementSnippet jdk.jshell.TypeDeclSnippet
          jdk.jshell.UnresolvedReferenceException jdk.jshell.VarSnippet jdk.management.jfr.ConfigurationInfo
          jdk.management.jfr.EventTypeInfo jdk.management.jfr.RecordingInfo
          jdk.management.jfr.SettingDescriptorInfo jdk.net.ExtendedSocketOptions jdk.net.NetworkPermission
          jdk.nio.Channels jdk.nio.mapmode.ExtendedMapMode jdk.security.jarsigner.JarSigner
          jdk.security.jarsigner.JarSigner.Builder jdk.security.jarsigner.JarSignerException'

        # -------------------------------------------------------------------------------------------------- #

        netscapeClassHierarchy='netscape.javascript.JSException netscape.javascript.JSObject'

        # -------------------------------------------------------------------------------------------------- #

        jgssClassHierarchy='org.ietf.jgss.ChannelBinding org.ietf.jgss.GSSException org.ietf.jgss.GSSManager
          org.ietf.jgss.MessageProp org.ietf.jgss.Oid'

        # -------------------------------------------------------------------------------------------------- #

        w3cClassHierarchy='org.w3c.dom.bootstrap.DOMImplementationRegistry org.w3c.dom.DOMException
          org.w3c.dom.events.EventException org.w3c.dom.ls.LSException org.w3c.dom.ranges.RangeException
          org.w3c.dom.xpath.XPathException'

        # -------------------------------------------------------------------------------------------------- #

        saxClassHierarchy='org.xml.sax.ext.Attributes2Impl org.xml.sax.ext.DefaultHandler2
          org.xml.sax.ext.Locator2Impl
          org.xml.sax.helpers.AttributesImpl org.xml.sax.helpers.DefaultHandler org.xml.sax.helpers.LocatorImpl
          org.xml.sax.helpers.NamespaceSupport org.xml.sax.helpers.ParserAdapter
          org.xml.sax.helpers.XMLFilterImpl
          org.xml.sax.helpers.XMLReaderAdapter org.xml.sax.InputSource
          org.xml.sax.SAXException org.xml.sax.SAXNotRecognizedException org.xml.sax.SAXNotSupportedException
          org.xml.sax.SAXParseException'

        # -------------------------------------------------------------------------------------------------- #
        print_completers() {

          format=' %s\;||%s'

          for import in $@
          do
            printf "$format" "$import" "$import"
          done

        }
        # ---------------------------------------------------------------------------------------------- #
        printf "set-option -add window java_hierarchy_completions "

        print_completers $sunClassHierarchy $jdkClassHierarchy $netscapeClassHierarchy
        print_completers $jgssClassHierarchy $w3cClassHierarchy $saxClassHierarchy

        printf "\n"
        # ------------------------------------------------------------------------------------------ #
      }
      # --------------- STAGE 4 OF 7 --------------------------------------------------------------- #
      evaluate-commands %sh{
        # --------------- JAVA INTERFACE HIERARCHY ------------------------------------------------- #

        sunInterfaceHierarchy='com.sun.java.accessibility.util.GUIInitializedListener
          com.sun.java.accessibility.util.TopLevelWindowListener com.sun.jdi.Accessible
          com.sun.jdi.ArrayReference com.sun.jdi.ArrayType com.sun.jdi.ArrayType com.sun.jdi.ArrayType
          com.sun.jdi.BooleanType com.sun.jdi.BooleanValue com.sun.jdi.ByteType com.sun.jdi.ByteValue
          com.sun.jdi.ByteValue com.sun.jdi.CharType com.sun.jdi.CharValue com.sun.jdi.CharValue
          com.sun.jdi.ClassLoaderReference com.sun.jdi.ClassObjectReference com.sun.jdi.ClassType
          com.sun.jdi.ClassType com.sun.jdi.ClassType com.sun.jdi.connect.AttachingConnector
          com.sun.jdi.connect.Connector com.sun.jdi.connect.Connector.Argument
          com.sun.jdi.connect.Connector.BooleanArgument com.sun.jdi.connect.Connector.IntegerArgument
          com.sun.jdi.connect.Connector.SelectedArgument com.sun.jdi.connect.Connector.StringArgument
          com.sun.jdi.connect.LaunchingConnector com.sun.jdi.connect.ListeningConnector
          com.sun.jdi.connect.Transport com.sun.jdi.DoubleType com.sun.jdi.DoubleValue com.sun.jdi.DoubleValue
          com.sun.jdi.event.AccessWatchpointEvent com.sun.jdi.event.AccessWatchpointEvent
          com.sun.jdi.event.BreakpointEvent com.sun.jdi.event.BreakpointEvent
          com.sun.jdi.event.ClassPrepareEvent com.sun.jdi.event.ClassUnloadEvent com.sun.jdi.event.Event
          com.sun.jdi.event.EventIterator com.sun.jdi.event.EventQueue com.sun.jdi.event.EventSet
          com.sun.jdi.event.EventSet com.sun.jdi.event.ExceptionEvent com.sun.jdi.event.ExceptionEvent
          com.sun.jdi.event.LocatableEvent com.sun.jdi.event.LocatableEvent com.sun.jdi.event.MethodEntryEvent
          com.sun.jdi.event.MethodEntryEvent com.sun.jdi.event.MethodExitEvent
          com.sun.jdi.event.MethodExitEvent com.sun.jdi.event.ModificationWatchpointEvent
          com.sun.jdi.event.ModificationWatchpointEvent com.sun.jdi.event.MonitorContendedEnteredEvent
          com.sun.jdi.event.MonitorContendedEnteredEvent com.sun.jdi.event.MonitorContendedEnterEvent
          com.sun.jdi.event.MonitorContendedEnterEvent com.sun.jdi.event.MonitorWaitedEvent
          com.sun.jdi.event.MonitorWaitedEvent com.sun.jdi.event.MonitorWaitEvent
          com.sun.jdi.event.MonitorWaitEvent com.sun.jdi.event.StepEvent com.sun.jdi.event.StepEvent
          com.sun.jdi.event.ThreadDeathEvent com.sun.jdi.event.ThreadStartEvent com.sun.jdi.event.VMDeathEvent
          com.sun.jdi.event.VMDisconnectEvent com.sun.jdi.event.VMStartEvent com.sun.jdi.event.WatchpointEvent
          com.sun.jdi.event.WatchpointEvent com.sun.jdi.Field com.sun.jdi.Field com.sun.jdi.Field
          com.sun.jdi.FloatType com.sun.jdi.FloatValue com.sun.jdi.FloatValue com.sun.jdi.IntegerType
          com.sun.jdi.IntegerValue com.sun.jdi.IntegerValue com.sun.jdi.InterfaceType com.sun.jdi.InterfaceType
          com.sun.jdi.InterfaceType com.sun.jdi.LocalVariable com.sun.jdi.LocalVariable com.sun.jdi.Locatable
          com.sun.jdi.Location com.sun.jdi.Location com.sun.jdi.LongType com.sun.jdi.LongValue
          com.sun.jdi.LongValue com.sun.jdi.Method com.sun.jdi.Method com.sun.jdi.Method com.sun.jdi.Method
          com.sun.jdi.Mirror com.sun.jdi.ModuleReference com.sun.jdi.MonitorInfo com.sun.jdi.ObjectReference
          com.sun.jdi.PathSearchingVirtualMachine com.sun.jdi.PrimitiveType com.sun.jdi.PrimitiveValue
          com.sun.jdi.ReferenceType com.sun.jdi.ReferenceType com.sun.jdi.ReferenceType
          com.sun.jdi.request.AccessWatchpointRequest com.sun.jdi.request.BreakpointRequest
          com.sun.jdi.request.BreakpointRequest com.sun.jdi.request.ClassPrepareRequest
          com.sun.jdi.request.ClassUnloadRequest com.sun.jdi.request.EventRequest
          com.sun.jdi.request.EventRequestManager com.sun.jdi.request.ExceptionRequest
          com.sun.jdi.request.MethodEntryRequest com.sun.jdi.request.MethodExitRequest
          com.sun.jdi.request.ModificationWatchpointRequest com.sun.jdi.request.MonitorContendedEnteredRequest
          com.sun.jdi.request.MonitorContendedEnterRequest com.sun.jdi.request.MonitorWaitedRequest
          com.sun.jdi.request.MonitorWaitRequest com.sun.jdi.request.StepRequest
          com.sun.jdi.request.ThreadDeathRequest com.sun.jdi.request.ThreadStartRequest
          com.sun.jdi.request.VMDeathRequest com.sun.jdi.request.WatchpointRequest com.sun.jdi.ShortType
          com.sun.jdi.ShortValue com.sun.jdi.ShortValue com.sun.jdi.StackFrame com.sun.jdi.StackFrame
          com.sun.jdi.StringReference com.sun.jdi.ThreadGroupReference com.sun.jdi.ThreadReference
          com.sun.jdi.Type com.sun.jdi.TypeComponent com.sun.jdi.TypeComponent com.sun.jdi.Value
          com.sun.jdi.VirtualMachine com.sun.jdi.VirtualMachineManager com.sun.jdi.VoidType
          com.sun.jdi.VoidValue com.sun.management.DiagnosticCommandMBean
          com.sun.management.GarbageCollectorMXBean com.sun.management.HotSpotDiagnosticMXBean
          com.sun.management.OperatingSystemMXBean com.sun.management.ThreadMXBean
          com.sun.management.UnixOperatingSystemMXBean com.sun.net.httpserver.HttpHandler
          com.sun.nio.sctp.Notification com.sun.nio.sctp.NotificationHandler com.sun.nio.sctp.SctpSocketOption
          com.sun.security.auth.PrincipalComparator com.sun.security.jgss.ExtendedGSSContext
          com.sun.security.jgss.ExtendedGSSCredential com.sun.source.doctree.AttributeTree
          com.sun.source.doctree.AuthorTree com.sun.source.doctree.BlockTagTree
          com.sun.source.doctree.CommentTree com.sun.source.doctree.DeprecatedTree
          com.sun.source.doctree.DocCommentTree com.sun.source.doctree.DocRootTree
          com.sun.source.doctree.DocTree com.sun.source.doctree.DocTreeVisitor
          com.sun.source.doctree.DocTypeTree com.sun.source.doctree.EndElementTree
          com.sun.source.doctree.EntityTree com.sun.source.doctree.ErroneousTree
          com.sun.source.doctree.HiddenTree com.sun.source.doctree.IdentifierTree
          com.sun.source.doctree.IndexTree com.sun.source.doctree.InheritDocTree
          com.sun.source.doctree.InlineTagTree com.sun.source.doctree.LinkTree
          com.sun.source.doctree.LiteralTree com.sun.source.doctree.ParamTree
          com.sun.source.doctree.ProvidesTree com.sun.source.doctree.ReferenceTree
          com.sun.source.doctree.ReturnTree com.sun.source.doctree.SeeTree
          com.sun.source.doctree.SerialDataTree com.sun.source.doctree.SerialFieldTree
          com.sun.source.doctree.SerialTree com.sun.source.doctree.SinceTree
          com.sun.source.doctree.StartElementTree com.sun.source.doctree.SummaryTree
          com.sun.source.doctree.SystemPropertyTree com.sun.source.doctree.TextTree
          com.sun.source.doctree.ThrowsTree com.sun.source.doctree.UnknownBlockTagTree
          com.sun.source.doctree.UnknownInlineTagTree com.sun.source.doctree.UsesTree
          com.sun.source.doctree.ValueTree com.sun.source.doctree.VersionTree
          com.sun.source.tree.AnnotatedTypeTree com.sun.source.tree.AnnotationTree
          com.sun.source.tree.ArrayAccessTree com.sun.source.tree.ArrayTypeTree com.sun.source.tree.AssertTree
          com.sun.source.tree.AssignmentTree com.sun.source.tree.BinaryTree
          com.sun.source.tree.BindingPatternTree com.sun.source.tree.BlockTree com.sun.source.tree.BreakTree
          com.sun.source.tree.CaseTree com.sun.source.tree.CatchTree com.sun.source.tree.ClassTree
          com.sun.source.tree.CompilationUnitTree com.sun.source.tree.CompoundAssignmentTree
          com.sun.source.tree.ConditionalExpressionTree com.sun.source.tree.ContinueTree
          com.sun.source.tree.DirectiveTree com.sun.source.tree.DoWhileLoopTree
          com.sun.source.tree.EmptyStatementTree com.sun.source.tree.EnhancedForLoopTree
          com.sun.source.tree.ErroneousTree com.sun.source.tree.ExportsTree
          com.sun.source.tree.ExpressionStatementTree com.sun.source.tree.ExpressionTree
          com.sun.source.tree.ForLoopTree com.sun.source.tree.IdentifierTree com.sun.source.tree.IfTree
          com.sun.source.tree.ImportTree com.sun.source.tree.InstanceOfTree
          com.sun.source.tree.IntersectionTypeTree com.sun.source.tree.LabeledStatementTree
          com.sun.source.tree.LambdaExpressionTree com.sun.source.tree.LineMap com.sun.source.tree.LiteralTree
          com.sun.source.tree.MemberReferenceTree com.sun.source.tree.MemberSelectTree
          com.sun.source.tree.MethodInvocationTree com.sun.source.tree.MethodTree
          com.sun.source.tree.ModifiersTree com.sun.source.tree.ModuleTree com.sun.source.tree.NewArrayTree
          com.sun.source.tree.NewClassTree com.sun.source.tree.OpensTree com.sun.source.tree.PackageTree
          com.sun.source.tree.ParameterizedTypeTree com.sun.source.tree.ParenthesizedTree
          com.sun.source.tree.PatternTree com.sun.source.tree.PrimitiveTypeTree
          com.sun.source.tree.ProvidesTree com.sun.source.tree.RequiresTree com.sun.source.tree.ReturnTree
          com.sun.source.tree.Scope com.sun.source.tree.StatementTree com.sun.source.tree.SwitchExpressionTree
          com.sun.source.tree.SwitchTree com.sun.source.tree.SynchronizedTree com.sun.source.tree.ThrowTree
          com.sun.source.tree.Tree com.sun.source.tree.TreeVisitor com.sun.source.tree.TryTree
          com.sun.source.tree.TypeCastTree com.sun.source.tree.TypeParameterTree com.sun.source.tree.UnaryTree
          com.sun.source.tree.UnionTypeTree com.sun.source.tree.UsesTree com.sun.source.tree.VariableTree
          com.sun.source.tree.WhileLoopTree com.sun.source.tree.WildcardTree com.sun.source.tree.YieldTree
          com.sun.source.util.DocSourcePositions com.sun.source.util.DocTreeFactory
          com.sun.source.util.ParameterNameProvider com.sun.source.util.Plugin
          com.sun.source.util.SourcePositions com.sun.source.util.TaskListener
          com.sun.tools.jconsole.JConsoleContext'

        awtInterfaceHierarchy='java.awt.ActiveEvent
          java.awt.Adjustable java.awt.Composite java.awt.CompositeContext java.awt.datatransfer.ClipboardOwner
          java.awt.datatransfer.FlavorListener java.awt.datatransfer.FlavorMap
          java.awt.datatransfer.FlavorTable java.awt.datatransfer.Transferable java.awt.desktop.AboutHandler
          java.awt.desktop.AppForegroundListener java.awt.desktop.AppHiddenListener
          java.awt.desktop.AppReopenedListener java.awt.desktop.OpenFilesHandler
          java.awt.desktop.OpenURIHandler java.awt.desktop.PreferencesHandler
          java.awt.desktop.PrintFilesHandler java.awt.desktop.QuitHandler java.awt.desktop.QuitResponse
          java.awt.desktop.ScreenSleepListener java.awt.desktop.SystemEventListener
          java.awt.desktop.SystemSleepListener java.awt.desktop.UserSessionListener java.awt.dnd.Autoscroll
          java.awt.dnd.DragGestureListener java.awt.dnd.DragSourceListener
          java.awt.dnd.DragSourceMotionListener java.awt.dnd.DropTargetListener java.awt.event.ActionListener
          java.awt.event.AdjustmentListener java.awt.event.AWTEventListener java.awt.event.ComponentListener
          java.awt.event.ContainerListener java.awt.event.FocusListener java.awt.event.HierarchyBoundsListener
          java.awt.event.HierarchyListener java.awt.event.InputMethodListener java.awt.event.ItemListener
          java.awt.event.KeyListener java.awt.event.MouseListener java.awt.event.MouseMotionListener
          java.awt.event.MouseWheelListener java.awt.event.TextListener java.awt.event.WindowFocusListener
          java.awt.event.WindowListener java.awt.event.WindowStateListener java.awt.font.MultipleMaster
          java.awt.font.OpenType java.awt.geom.PathIterator java.awt.image.BufferedImageOp
          java.awt.image.ImageConsumer java.awt.image.ImageObserver java.awt.image.ImageProducer
          java.awt.image.MultiResolutionImage java.awt.image.RasterOp
          java.awt.image.renderable.ContextualRenderedImageFactory java.awt.image.renderable.RenderableImage
          java.awt.image.renderable.RenderedImageFactory java.awt.image.RenderedImage
          java.awt.image.TileObserver java.awt.image.WritableRenderedImage java.awt.im.InputMethodRequests
          java.awt.im.spi.InputMethod java.awt.im.spi.InputMethodContext java.awt.im.spi.InputMethodDescriptor
          java.awt.ItemSelectable java.awt.KeyEventDispatcher java.awt.KeyEventPostProcessor
          java.awt.LayoutManager java.awt.LayoutManager2 java.awt.MenuContainer java.awt.Paint
          java.awt.PaintContext java.awt.PrintGraphics java.awt.print.Pageable java.awt.print.Printable
          java.awt.print.PrinterGraphics java.awt.SecondaryLoop java.awt.Shape java.awt.Stroke
          java.awt.Transparency'

        beansInterfaceHierarchy='java.beans.beancontext.BeanContext java.beans.beancontext.BeanContext
          java.beans.beancontext.BeanContext java.beans.beancontext.BeanContext
          java.beans.beancontext.BeanContextChild java.beans.beancontext.BeanContextChildComponentProxy
          java.beans.beancontext.BeanContextContainerProxy java.beans.beancontext.BeanContextMembershipListener
          java.beans.beancontext.BeanContextProxy java.beans.beancontext.BeanContextServiceProvider
          java.beans.beancontext.BeanContextServiceProviderBeanInfo
          java.beans.beancontext.BeanContextServiceRevokedListener java.beans.beancontext.BeanContextServices
          java.beans.beancontext.BeanContextServices java.beans.beancontext.BeanContextServices
          java.beans.beancontext.BeanContextServices java.beans.beancontext.BeanContextServices
          java.beans.beancontext.BeanContextServicesListener java.beans.BeanInfo java.beans.Customizer
          java.beans.DesignMode java.beans.ExceptionListener java.beans.PropertyChangeListener
          java.beans.PropertyEditor java.beans.VetoableChangeListener java.beans.Visibility'

        ioInterfaceHierarchy='java.io.Closeable java.io.DataInput java.io.DataOutput java.io.Externalizable
          java.io.FileFilter java.io.FilenameFilter java.io.Flushable java.io.ObjectInput java.io.ObjectInput
          java.io.ObjectInputFilter java.io.ObjectInputFilter.FilterInfo java.io.ObjectInputValidation
          java.io.ObjectOutput java.io.ObjectOutput java.io.ObjectStreamConstants java.io.Serializable'

        langInterfaceHierarchy='java.lang.annotation.Annotation java.lang.Appendable java.lang.AutoCloseable
          java.lang.CharSequence java.lang.Cloneable java.lang.Comparable java.lang.constant.ClassDesc
          java.lang.constant.ClassDesc java.lang.constant.Constable java.lang.constant.ConstantDesc
          java.lang.constant.DirectMethodHandleDesc java.lang.constant.MethodHandleDesc
          java.lang.constant.MethodTypeDesc java.lang.constant.MethodTypeDesc
          java.lang.instrument.ClassFileTransformer java.lang.instrument.Instrumentation
          java.lang.invoke.MethodHandleInfo java.lang.invoke.TypeDescriptor
          java.lang.invoke.TypeDescriptor.OfField java.lang.invoke.TypeDescriptor.OfMethod java.lang.Iterable
          java.lang.management.BufferPoolMXBean java.lang.management.ClassLoadingMXBean
          java.lang.management.CompilationMXBean java.lang.management.GarbageCollectorMXBean
          java.lang.management.MemoryManagerMXBean java.lang.management.MemoryMXBean
          java.lang.management.MemoryPoolMXBean java.lang.management.OperatingSystemMXBean
          java.lang.management.PlatformLoggingMXBean java.lang.management.PlatformManagedObject
          java.lang.management.RuntimeMXBean java.lang.management.ThreadMXBean java.lang.module.ModuleFinder
          java.lang.module.ModuleReader java.lang.ProcessHandle java.lang.ProcessHandle.Info java.lang.Readable
          java.lang.ref.Cleaner.Cleanable java.lang.reflect.AnnotatedArrayType
          java.lang.reflect.AnnotatedElement java.lang.reflect.AnnotatedParameterizedType
          java.lang.reflect.AnnotatedType java.lang.reflect.AnnotatedTypeVariable
          java.lang.reflect.AnnotatedWildcardType java.lang.reflect.GenericArrayType
          java.lang.reflect.GenericDeclaration java.lang.reflect.InvocationHandler java.lang.reflect.Member
          java.lang.reflect.ParameterizedType java.lang.reflect.Type java.lang.reflect.TypeVariable
          java.lang.reflect.TypeVariable java.lang.reflect.WildcardType java.lang.Runnable
          java.lang.StackWalker.StackFrame java.lang.System.Logger java.lang.Thread.UncaughtExceptionHandler'

        netInterfaceHierarchy='java.net.ContentHandlerFactory java.net.CookiePolicy java.net.CookieStore
          java.net.DatagramSocketImplFactory java.net.FileNameMap java.net.http.HttpClient.Builder
          java.net.http.HttpRequest.BodyPublisher java.net.http.HttpRequest.Builder java.net.http.HttpResponse
          java.net.http.HttpResponse.BodyHandler java.net.http.HttpResponse.BodySubscriber
          java.net.http.HttpResponse.PushPromiseHandler java.net.http.HttpResponse.ResponseInfo
          java.net.http.WebSocket java.net.http.WebSocket.Builder java.net.http.WebSocket.Listener
          java.net.ProtocolFamily java.net.SocketImplFactory java.net.SocketOption java.net.SocketOptions
          java.net.URLStreamHandlerFactory'

        nioInterfaceHierarchy='java.nio.channels.AsynchronousByteChannel
          java.nio.channels.AsynchronousChannel java.nio.channels.ByteChannel java.nio.channels.ByteChannel
          java.nio.channels.Channel java.nio.channels.CompletionHandler java.nio.channels.GatheringByteChannel
          java.nio.channels.InterruptibleChannel java.nio.channels.MulticastChannel
          java.nio.channels.NetworkChannel java.nio.channels.ReadableByteChannel
          java.nio.channels.ScatteringByteChannel java.nio.channels.SeekableByteChannel
          java.nio.channels.SeekableByteChannel java.nio.channels.WritableByteChannel
          java.nio.file.attribute.AclFileAttributeView java.nio.file.attribute.AttributeView
          java.nio.file.attribute.BasicFileAttributes java.nio.file.attribute.BasicFileAttributeView
          java.nio.file.attribute.DosFileAttributes java.nio.file.attribute.DosFileAttributeView
          java.nio.file.attribute.FileAttribute java.nio.file.attribute.FileAttributeView
          java.nio.file.attribute.FileOwnerAttributeView java.nio.file.attribute.FileStoreAttributeView
          java.nio.file.attribute.GroupPrincipal java.nio.file.attribute.PosixFileAttributes
          java.nio.file.attribute.PosixFileAttributeView java.nio.file.attribute.PosixFileAttributeView
          java.nio.file.attribute.UserDefinedFileAttributeView java.nio.file.attribute.UserPrincipal
          java.nio.file.CopyOption java.nio.file.DirectoryStream java.nio.file.DirectoryStream
          java.nio.file.DirectoryStream.Filter java.nio.file.FileVisitor java.nio.file.OpenOption
          java.nio.file.Path java.nio.file.Path java.nio.file.Path java.nio.file.PathMatcher
          java.nio.file.SecureDirectoryStream java.nio.file.SecureDirectoryStream java.nio.file.Watchable
          java.nio.file.WatchEvent java.nio.file.WatchEvent.Kind java.nio.file.WatchEvent.Modifier
          java.nio.file.WatchKey java.nio.file.WatchService'

        # ------------------------------------------------------------------------------------------ #
        print_completers() {

          format=' %s\;||%s'

          for import in $@
          do
            printf "$format" "$import" "$import"
          done

        }
        # ------------------------------------------------------------------------------------------ #
        printf "set-option -add window java_hierarchy_completions "

        print_completers $sunInterfaceHierarchy $awtInterfaceHierarchy $beansInterfaceHierarchy
        print_completers $ioInterfaceHierarchy $langInterfaceHierarchy $netInterfaceHierarchy $nioInterfaceHierarchy

        printf "\n"
        # ------------------------------------------------------------------------------------------ #
      }
      # --------------- STAGE 5 OF 7 --------------------------------------------------------------- #
      evaluate-commands %sh{

        rmiInterfaceHierarchy='java.rmi.activation.ActivationInstantiator
          java.rmi.activation.ActivationMonitor java.rmi.activation.ActivationSystem
          java.rmi.activation.Activator java.rmi.dgc.DGC java.rmi.registry.Registry
          java.rmi.Remote
          java.rmi.server.RemoteRef java.rmi.server.RMIClientSocketFactory
          java.rmi.server.RMIFailureHandler java.rmi.server.RMIServerSocketFactory
          java.rmi.server.Unreferenced'

        securityInterfaceHierarchy='java.security.AlgorithmConstraints
          java.security.cert.CertPathBuilderResult java.security.cert.CertPathChecker
          java.security.cert.CertPathParameters java.security.cert.CertPathValidatorException.Reason
          java.security.cert.CertPathValidatorResult java.security.cert.CertSelector
          java.security.cert.CertStoreParameters java.security.cert.CRLSelector java.security.cert.Extension
          java.security.cert.PolicyNode java.security.cert.X509Extension
          java.security.Guard java.security.interfaces.DSAKey
          java.security.interfaces.DSAKeyPairGenerator java.security.interfaces.DSAParams
          java.security.interfaces.DSAPrivateKey java.security.interfaces.DSAPrivateKey
          java.security.interfaces.DSAPrivateKey java.security.interfaces.DSAPublicKey
          java.security.interfaces.DSAPublicKey java.security.interfaces.ECKey
          java.security.interfaces.ECPrivateKey java.security.interfaces.ECPrivateKey
          java.security.interfaces.ECPrivateKey java.security.interfaces.ECPublicKey
          java.security.interfaces.ECPublicKey java.security.interfaces.EdECKey
          java.security.interfaces.EdECPrivateKey java.security.interfaces.EdECPrivateKey
          java.security.interfaces.EdECPrivateKey java.security.interfaces.EdECPublicKey
          java.security.interfaces.EdECPublicKey java.security.interfaces.RSAKey
          java.security.interfaces.RSAMultiPrimePrivateCrtKey
          java.security.interfaces.RSAMultiPrimePrivateCrtKey
          java.security.interfaces.RSAMultiPrimePrivateCrtKey java.security.interfaces.RSAPrivateCrtKey
          java.security.interfaces.RSAPrivateCrtKey java.security.interfaces.RSAPrivateCrtKey
          java.security.interfaces.RSAPrivateKey java.security.interfaces.RSAPrivateKey
          java.security.interfaces.RSAPrivateKey java.security.interfaces.RSAPublicKey
          java.security.interfaces.RSAPublicKey java.security.interfaces.XECKey
          java.security.interfaces.XECPrivateKey java.security.interfaces.XECPrivateKey
          java.security.interfaces.XECPrivateKey java.security.interfaces.XECPublicKey
          java.security.interfaces.XECPublicKey java.security.Key java.security.KeyStore.Entry
          java.security.KeyStore.Entry.Attribute java.security.KeyStore.LoadStoreParameter
          java.security.KeyStore.ProtectionParameter java.security.Principal
          java.security.PrivateKey java.security.PrivateKey java.security.PrivilegedAction
          java.security.PrivilegedExceptionAction java.security.PublicKey java.security.SecureRandomParameters
          java.security.spec.AlgorithmParameterSpec java.security.spec.ECField java.security.spec.KeySpec'

         sqlInterfaceHierarchy='java.sql.Array java.sql.Blob java.sql.CallableStatement
           java.sql.CallableStatement java.sql.Clob java.sql.Connection java.sql.Connection
           java.sql.ConnectionBuilder java.sql.DatabaseMetaData java.sql.Driver java.sql.DriverAction
           java.sql.NClob java.sql.ParameterMetaData java.sql.PreparedStatement java.sql.PreparedStatement
           java.sql.Ref java.sql.ResultSet java.sql.ResultSet java.sql.ResultSetMetaData java.sql.RowId
           java.sql.Savepoint java.sql.ShardingKey java.sql.ShardingKeyBuilder java.sql.SQLData
           java.sql.SQLInput java.sql.SQLOutput java.sql.SQLType java.sql.SQLXML java.sql.Statement
           java.sql.Statement java.sql.Struct java.sql.Wrapper'

        textInterfaceHierarchy='java.text.AttributedCharacterIterator java.text.CharacterIterator'

        timeInterfaceHierarchy='java.time.chrono.ChronoLocalDate java.time.chrono.ChronoLocalDate
          java.time.chrono.ChronoLocalDate java.time.chrono.ChronoLocalDateTime
          java.time.chrono.ChronoLocalDateTime java.time.chrono.ChronoLocalDateTime java.time.chrono.Chronology
          java.time.chrono.ChronoPeriod java.time.chrono.ChronoZonedDateTime
          java.time.chrono.ChronoZonedDateTime java.time.chrono.Era java.time.chrono.Era
          java.time.temporal.Temporal java.time.temporal.TemporalAccessor java.time.temporal.TemporalAdjuster
          java.time.temporal.TemporalAmount java.time.temporal.TemporalField java.time.temporal.TemporalQuery
          java.time.temporal.TemporalUnit'

        utilInterfaceHierarchy='java.util.Collection
          java.util.Comparator java.util.concurrent.BlockingDeque java.util.concurrent.BlockingDeque
          java.util.concurrent.BlockingQueue java.util.concurrent.Callable
          java.util.concurrent.CompletableFuture.AsynchronousCompletionTask
          java.util.concurrent.CompletionService java.util.concurrent.CompletionStage
          java.util.concurrent.ConcurrentMap java.util.concurrent.ConcurrentNavigableMap
          java.util.concurrent.ConcurrentNavigableMap java.util.concurrent.Delayed
          java.util.concurrent.Executor java.util.concurrent.ExecutorService
          java.util.concurrent.Flow.Processor java.util.concurrent.Flow.Processor
          java.util.concurrent.Flow.Publisher java.util.concurrent.Flow.Subscriber
          java.util.concurrent.Flow.Subscription java.util.concurrent.ForkJoinPool.ForkJoinWorkerThreadFactory
          java.util.concurrent.ForkJoinPool.ManagedBlocker java.util.concurrent.Future
          java.util.concurrent.locks.Condition java.util.concurrent.locks.Lock
          java.util.concurrent.locks.ReadWriteLock java.util.concurrent.RejectedExecutionHandler
          java.util.concurrent.RunnableFuture java.util.concurrent.RunnableFuture
          java.util.concurrent.RunnableScheduledFuture java.util.concurrent.RunnableScheduledFuture
          java.util.concurrent.RunnableScheduledFuture java.util.concurrent.RunnableScheduledFuture
          java.util.concurrent.ScheduledExecutorService java.util.concurrent.ScheduledFuture
          java.util.concurrent.ScheduledFuture java.util.concurrent.ThreadFactory
          java.util.concurrent.TransferQueue java.util.Deque java.util.Enumeration java.util.EventListener
          java.util.Formattable java.util.function.BiConsumer java.util.function.BiFunction
          java.util.function.BinaryOperator java.util.function.BiPredicate java.util.function.BooleanSupplier
          java.util.function.Consumer java.util.function.DoubleBinaryOperator java.util.function.DoubleConsumer
          java.util.function.DoubleFunction java.util.function.DoublePredicate
          java.util.function.DoubleSupplier java.util.function.DoubleToIntFunction
          java.util.function.DoubleToLongFunction java.util.function.DoubleUnaryOperator
          java.util.function.Function java.util.function.IntBinaryOperator java.util.function.IntConsumer
          java.util.function.IntFunction java.util.function.IntPredicate java.util.function.IntSupplier
          java.util.function.IntToDoubleFunction java.util.function.IntToLongFunction
          java.util.function.IntUnaryOperator java.util.function.LongBinaryOperator
          java.util.function.LongConsumer java.util.function.LongFunction java.util.function.LongPredicate
          java.util.function.LongSupplier java.util.function.LongToDoubleFunction
          java.util.function.LongToIntFunction java.util.function.LongUnaryOperator
          java.util.function.ObjDoubleConsumer java.util.function.ObjIntConsumer
          java.util.function.ObjLongConsumer java.util.function.Predicate java.util.function.Supplier
          java.util.function.ToDoubleBiFunction java.util.function.ToDoubleFunction
          java.util.function.ToIntBiFunction java.util.function.ToIntFunction
          java.util.function.ToLongBiFunction java.util.function.ToLongFunction
          java.util.function.UnaryOperator java.util.Iterator java.util.List java.util.ListIterator
          java.util.logging.Filter java.util.Map java.util.Map.Entry
          java.util.NavigableMap java.util.NavigableSet java.util.prefs.NodeChangeListener
          java.util.prefs.PreferenceChangeListener java.util.prefs.PreferencesFactory
          java.util.PrimitiveIterator java.util.PrimitiveIterator.OfDouble java.util.PrimitiveIterator.OfInt
          java.util.PrimitiveIterator.OfLong java.util.Queue java.util.RandomAccess java.util.regex.MatchResult
          java.util.ServiceLoader.Provider java.util.Set java.util.SortedMap java.util.SortedSet
          java.util.spi.ResourceBundleControlProvider java.util.spi.ResourceBundleProvider
          java.util.spi.ToolProvider java.util.Spliterator java.util.Spliterator.OfDouble
          java.util.Spliterator.OfInt java.util.Spliterator.OfLong java.util.Spliterator.OfPrimitive
          java.util.stream.BaseStream java.util.stream.Collector java.util.stream.DoubleStream
          java.util.stream.DoubleStream.Builder java.util.stream.DoubleStream.DoubleMapMultiConsumer
          java.util.stream.IntStream java.util.stream.IntStream.Builder
          java.util.stream.IntStream.IntMapMultiConsumer java.util.stream.LongStream
          java.util.stream.LongStream.Builder java.util.stream.LongStream.LongMapMultiConsumer
          java.util.stream.Stream java.util.stream.Stream.Builder java.util.zip.Checksum'

        # ------------------------------------------------------------------------------------------ #
        print_completers() {

          format=' %s\;||%s'

          for import in $@
          do
            printf "$format" "$import" "$import"
          done

        }
        # ------------------------------------------------------------------------------------------ #
        printf "set-option -add window java_hierarchy_completions "

        print_completers $rmiInterfaceHierarchy $securityInterfaceHierarchy $sqlInterfaceHierarchy
        print_completers $textInterfaceHierarchy $timeInterfaceHierarchy $utilInterfaceHierarchy

        printf "\n"
        # ------------------------------------------------------------------------------------------ #
      }
      # --------------- STAGE 6 OF 7 --------------------------------------------------------------- #
      evaluate-commands %sh{

        javaxInterfaceHierarchy='javax.accessibility.Accessible javax.accessibility.AccessibleAction
          javax.accessibility.AccessibleComponent javax.accessibility.AccessibleEditableText
          javax.accessibility.AccessibleExtendedComponent javax.accessibility.AccessibleExtendedTable
          javax.accessibility.AccessibleExtendedText javax.accessibility.AccessibleHypertext
          javax.accessibility.AccessibleIcon javax.accessibility.AccessibleKeyBinding
          javax.accessibility.AccessibleSelection javax.accessibility.AccessibleStreamable
          javax.accessibility.AccessibleTable javax.accessibility.AccessibleTableModelChange
          javax.accessibility.AccessibleText javax.accessibility.AccessibleValue
          javax.annotation.processing.Completion javax.annotation.processing.Filer
          javax.annotation.processing.Messager javax.annotation.processing.ProcessingEnvironment
          javax.annotation.processing.Processor javax.annotation.processing.RoundEnvironment
          javax.crypto.interfaces.DHKey javax.crypto.interfaces.DHPrivateKey
          javax.crypto.interfaces.DHPrivateKey javax.crypto.interfaces.DHPrivateKey
          javax.crypto.interfaces.DHPublicKey javax.crypto.interfaces.DHPublicKey
          javax.crypto.interfaces.PBEKey javax.crypto.interfaces.PBEKey javax.crypto.SecretKey
          javax.crypto.SecretKey javax.imageio.event.IIOReadProgressListener
          javax.imageio.event.IIOReadUpdateListener javax.imageio.event.IIOReadWarningListener
          javax.imageio.event.IIOWriteProgressListener javax.imageio.event.IIOWriteWarningListener
          javax.imageio.IIOParamController javax.imageio.ImageTranscoder
          javax.imageio.metadata.IIOMetadataController javax.imageio.metadata.IIOMetadataFormat
          javax.imageio.spi.RegisterableService javax.imageio.spi.ServiceRegistry.Filter
          javax.imageio.stream.ImageInputStream javax.imageio.stream.ImageInputStream
          javax.imageio.stream.ImageOutputStream javax.imageio.stream.ImageOutputStream
          javax.imageio.stream.ImageOutputStream javax.lang.model.AnnotatedConstruct
          javax.lang.model.element.AnnotationMirror javax.lang.model.element.AnnotationValue
          javax.lang.model.element.AnnotationValueVisitor javax.lang.model.element.Element
          javax.lang.model.element.ElementVisitor javax.lang.model.element.ExecutableElement
          javax.lang.model.element.ExecutableElement javax.lang.model.element.ModuleElement
          javax.lang.model.element.ModuleElement javax.lang.model.element.ModuleElement.Directive
          javax.lang.model.element.ModuleElement.DirectiveVisitor
          javax.lang.model.element.ModuleElement.ExportsDirective
          javax.lang.model.element.ModuleElement.OpensDirective
          javax.lang.model.element.ModuleElement.ProvidesDirective
          javax.lang.model.element.ModuleElement.RequiresDirective
          javax.lang.model.element.ModuleElement.UsesDirective javax.lang.model.element.Name
          javax.lang.model.element.PackageElement javax.lang.model.element.PackageElement
          javax.lang.model.element.Parameterizable javax.lang.model.element.QualifiedNameable
          javax.lang.model.element.RecordComponentElement javax.lang.model.element.TypeElement
          javax.lang.model.element.TypeElement javax.lang.model.element.TypeElement
          javax.lang.model.element.TypeParameterElement javax.lang.model.element.VariableElement
          javax.lang.model.type.ArrayType javax.lang.model.type.DeclaredType javax.lang.model.type.ErrorType
          javax.lang.model.type.ExecutableType javax.lang.model.type.IntersectionType
          javax.lang.model.type.NoType javax.lang.model.type.NullType javax.lang.model.type.PrimitiveType
          javax.lang.model.type.ReferenceType javax.lang.model.type.TypeMirror
          javax.lang.model.type.TypeVariable javax.lang.model.type.TypeVisitor javax.lang.model.type.UnionType
          javax.lang.model.type.WildcardType javax.lang.model.util.Elements javax.lang.model.util.Types
          javax.management.Descriptor javax.management.Descriptor javax.management.DescriptorAccess
          javax.management.DescriptorRead javax.management.DynamicMBean
          javax.management.loading.ClassLoaderRepository javax.management.loading.MLetMBean
          javax.management.loading.PrivateClassLoader javax.management.MBeanRegistration
          javax.management.MBeanServer javax.management.MBeanServerConnection
          javax.management.MBeanServerDelegateMBean javax.management.modelmbean.ModelMBean
          javax.management.modelmbean.ModelMBean javax.management.modelmbean.ModelMBean
          javax.management.modelmbean.ModelMBeanInfo
          javax.management.modelmbean.ModelMBeanNotificationBroadcaster
          javax.management.monitor.CounterMonitorMBean javax.management.monitor.GaugeMonitorMBean
          javax.management.monitor.MonitorMBean javax.management.monitor.StringMonitorMBean
          javax.management.NotificationBroadcaster javax.management.NotificationEmitter
          javax.management.NotificationFilter javax.management.NotificationListener
          javax.management.openmbean.CompositeData javax.management.openmbean.CompositeDataView
          javax.management.openmbean.OpenMBeanAttributeInfo javax.management.openmbean.OpenMBeanConstructorInfo
          javax.management.openmbean.OpenMBeanInfo javax.management.openmbean.OpenMBeanOperationInfo
          javax.management.openmbean.OpenMBeanParameterInfo javax.management.openmbean.TabularData
          javax.management.PersistentMBean javax.management.QueryExp javax.management.relation.Relation
          javax.management.relation.RelationServiceMBean javax.management.relation.RelationSupportMBean
          javax.management.relation.RelationType javax.management.remote.JMXAddressable
          javax.management.remote.JMXAuthenticator javax.management.remote.JMXConnector
          javax.management.remote.JMXConnectorProvider javax.management.remote.JMXConnectorServerMBean
          javax.management.remote.JMXConnectorServerProvider javax.management.remote.MBeanServerForwarder
          javax.management.remote.rmi.RMIConnection javax.management.remote.rmi.RMIConnection
          javax.management.remote.rmi.RMIServer javax.management.timer.TimerMBean javax.management.ValueExp
          javax.naming.Context javax.naming.directory.Attribute javax.naming.directory.Attribute
          javax.naming.directory.Attributes javax.naming.directory.Attributes javax.naming.directory.DirContext
          javax.naming.event.EventContext javax.naming.event.EventDirContext javax.naming.event.EventDirContext
          javax.naming.event.NamespaceChangeListener javax.naming.event.NamingListener
          javax.naming.event.ObjectChangeListener javax.naming.ldap.Control javax.naming.ldap.ExtendedRequest
          javax.naming.ldap.ExtendedResponse javax.naming.ldap.HasControls javax.naming.ldap.LdapContext
          javax.naming.ldap.UnsolicitedNotification javax.naming.ldap.UnsolicitedNotification
          javax.naming.ldap.UnsolicitedNotificationListener javax.naming.Name javax.naming.Name
          javax.naming.Name javax.naming.NameParser javax.naming.NamingEnumeration javax.naming.Referenceable
          javax.naming.spi.DirObjectFactory javax.naming.spi.DirStateFactory
          javax.naming.spi.InitialContextFactory javax.naming.spi.InitialContextFactoryBuilder
          javax.naming.spi.ObjectFactory javax.naming.spi.ObjectFactoryBuilder javax.naming.spi.Resolver
          javax.naming.spi.StateFactory javax.net.ssl.HandshakeCompletedListener javax.net.ssl.HostnameVerifier
          javax.net.ssl.KeyManager javax.net.ssl.ManagerFactoryParameters javax.net.ssl.SSLSession
          javax.net.ssl.SSLSessionBindingListener javax.net.ssl.SSLSessionContext javax.net.ssl.TrustManager
          javax.net.ssl.X509KeyManager javax.net.ssl.X509TrustManager javax.print.attribute.Attribute
          javax.print.attribute.AttributeSet javax.print.attribute.DocAttribute
          javax.print.attribute.DocAttributeSet javax.print.AttributeException
          javax.print.attribute.PrintJobAttribute javax.print.attribute.PrintJobAttributeSet
          javax.print.attribute.PrintRequestAttribute javax.print.attribute.PrintRequestAttributeSet
          javax.print.attribute.PrintServiceAttribute javax.print.attribute.PrintServiceAttributeSet
          javax.print.attribute.SupportedValuesAttribute javax.print.CancelablePrintJob javax.print.Doc
          javax.print.DocPrintJob javax.print.event.PrintJobAttributeListener
          javax.print.event.PrintJobListener javax.print.event.PrintServiceAttributeListener
          javax.print.FlavorException javax.print.MultiDoc javax.print.MultiDocPrintJob
          javax.print.MultiDocPrintService javax.print.PrintService javax.print.URIException
          javax.script.Bindings javax.script.Compilable javax.script.Invocable javax.script.ScriptContext
          javax.script.ScriptEngine javax.script.ScriptEngineFactory javax.security.auth.callback.Callback
          javax.security.auth.callback.CallbackHandler javax.security.auth.Destroyable
          javax.security.auth.login.Configuration.Parameters javax.security.auth.Refreshable
          javax.security.auth.spi.LoginModule javax.security.sasl.SaslClient
          javax.security.sasl.SaslClientFactory javax.security.sasl.SaslServer
          javax.security.sasl.SaslServerFactory javax.sound.midi.ControllerEventListener
          javax.sound.midi.MetaEventListener javax.sound.midi.MidiChannel javax.sound.midi.MidiDevice
          javax.sound.midi.MidiDeviceReceiver javax.sound.midi.MidiDeviceTransmitter javax.sound.midi.Receiver
          javax.sound.midi.Sequencer javax.sound.midi.Soundbank javax.sound.midi.Synthesizer
          javax.sound.midi.Transmitter javax.sound.sampled.Clip javax.sound.sampled.DataLine
          javax.sound.sampled.Line javax.sound.sampled.LineListener javax.sound.sampled.Mixer
          javax.sound.sampled.Port javax.sound.sampled.SourceDataLine javax.sound.sampled.TargetDataLine
          javax.sql.CommonDataSource javax.sql.ConnectionEventListener javax.sql.ConnectionPoolDataSource
          javax.sql.DataSource javax.sql.DataSource javax.sql.PooledConnection
          javax.sql.PooledConnectionBuilder javax.sql.RowSet javax.sql.RowSet javax.sql.rowset.CachedRowSet
          javax.sql.rowset.CachedRowSet javax.sql.rowset.CachedRowSet javax.sql.rowset.FilteredRowSet
          javax.sql.rowset.FilteredRowSet javax.sql.rowset.FilteredRowSet javax.sql.RowSetInternal
          javax.sql.rowset.JdbcRowSet javax.sql.rowset.JdbcRowSet javax.sql.rowset.JdbcRowSet
          javax.sql.rowset.Joinable javax.sql.rowset.JoinRowSet javax.sql.rowset.JoinRowSet
          javax.sql.rowset.JoinRowSet javax.sql.RowSetListener javax.sql.RowSetMetaData
          javax.sql.rowset.Predicate javax.sql.RowSetReader javax.sql.rowset.RowSetFactory
          javax.sql.rowset.spi.SyncResolver javax.sql.rowset.spi.SyncResolver
          javax.sql.rowset.spi.TransactionalWriter javax.sql.rowset.spi.XmlReader
          javax.sql.rowset.spi.XmlWriter javax.sql.rowset.WebRowSet javax.sql.rowset.WebRowSet
          javax.sql.rowset.WebRowSet javax.sql.RowSetWriter javax.sql.StatementEventListener
          javax.sql.XAConnection javax.sql.XAConnectionBuilder javax.sql.XADataSource javax.swing.Action
          javax.swing.border.Border javax.swing.BoundedRangeModel javax.swing.ButtonModel
          javax.swing.CellEditor javax.swing.colorchooser.ColorSelectionModel javax.swing.ComboBoxEditor
          javax.swing.ComboBoxModel javax.swing.DesktopManager javax.swing.event.AncestorListener
          javax.swing.event.CaretListener javax.swing.event.CellEditorListener javax.swing.event.ChangeListener
          javax.swing.event.DocumentEvent javax.swing.event.DocumentEvent.ElementChange
          javax.swing.event.DocumentListener javax.swing.event.HyperlinkListener
          javax.swing.event.InternalFrameListener javax.swing.event.ListDataListener
          javax.swing.event.ListSelectionListener javax.swing.event.MenuDragMouseListener
          javax.swing.event.MenuKeyListener javax.swing.event.MenuListener javax.swing.event.MouseInputListener
          javax.swing.event.MouseInputListener javax.swing.event.PopupMenuListener
          javax.swing.event.RowSorterListener javax.swing.event.TableColumnModelListener
          javax.swing.event.TableModelListener javax.swing.event.TreeExpansionListener
          javax.swing.event.TreeModelListener javax.swing.event.TreeSelectionListener
          javax.swing.event.TreeWillExpandListener javax.swing.event.UndoableEditListener javax.swing.Icon
          javax.swing.JComboBox.KeySelectionManager javax.swing.ListCellRenderer javax.swing.ListModel
          javax.swing.ListSelectionModel javax.swing.MenuElement javax.swing.MutableComboBoxModel
          javax.swing.Painter javax.swing.plaf.basic.ComboPopup javax.swing.plaf.synth.SynthConstants
          javax.swing.plaf.synth.SynthIcon javax.swing.plaf.synth.SynthUI javax.swing.plaf.UIResource
          javax.swing.Renderer javax.swing.RootPaneContainer javax.swing.Scrollable
          javax.swing.ScrollPaneConstants javax.swing.SingleSelectionModel javax.swing.SpinnerModel
          javax.swing.SwingConstants javax.swing.table.TableCellEditor javax.swing.table.TableCellRenderer
          javax.swing.table.TableColumnModel javax.swing.table.TableModel
          javax.swing.text.AbstractDocument.AttributeContext javax.swing.text.AbstractDocument.Content
          javax.swing.text.AttributeSet javax.swing.text.AttributeSet.CharacterAttribute
          javax.swing.text.AttributeSet.ColorAttribute javax.swing.text.AttributeSet.FontAttribute
          javax.swing.text.AttributeSet.ParagraphAttribute javax.swing.text.Caret javax.swing.text.Document
          javax.swing.text.Element javax.swing.text.Highlighter javax.swing.text.Highlighter.Highlight
          javax.swing.text.Highlighter.HighlightPainter javax.swing.text.html.parser.DTDConstants
          javax.swing.text.Keymap javax.swing.text.MutableAttributeSet javax.swing.text.Position
          javax.swing.text.Style javax.swing.text.StyledDocument javax.swing.text.TabableView
          javax.swing.text.TabExpander javax.swing.text.ViewFactory javax.swing.tree.MutableTreeNode
          javax.swing.tree.RowMapper javax.swing.tree.TreeCellEditor javax.swing.tree.TreeCellRenderer
          javax.swing.tree.TreeModel javax.swing.tree.TreeNode javax.swing.tree.TreeSelectionModel
          javax.swing.UIClientPropertyKey javax.swing.UIDefaults.ActiveValue javax.swing.UIDefaults.LazyValue
          javax.swing.undo.StateEditable javax.swing.undo.UndoableEdit javax.swing.WindowConstants
          javax.tools.Diagnostic javax.tools.DiagnosticListener javax.tools.DocumentationTool
          javax.tools.DocumentationTool javax.tools.DocumentationTool.DocumentationTask javax.tools.FileObject
          javax.tools.JavaCompiler javax.tools.JavaCompiler javax.tools.JavaCompiler.CompilationTask
          javax.tools.JavaFileManager javax.tools.JavaFileManager javax.tools.JavaFileManager
          javax.tools.JavaFileManager.Location javax.tools.JavaFileObject javax.tools.OptionChecker
          javax.tools.StandardJavaFileManager javax.tools.StandardJavaFileManager
          javax.tools.StandardJavaFileManager javax.tools.StandardJavaFileManager.PathFactory javax.tools.Tool
          javax.transaction.xa.XAResource javax.transaction.xa.Xid javax.xml.catalog.Catalog
          javax.xml.catalog.CatalogResolver javax.xml.catalog.CatalogResolver javax.xml.catalog.CatalogResolver
          javax.xml.catalog.CatalogResolver javax.xml.crypto.AlgorithmMethod javax.xml.crypto.Data
          javax.xml.crypto.dom.DOMURIReference javax.xml.crypto.dsig.CanonicalizationMethod
          javax.xml.crypto.dsig.CanonicalizationMethod javax.xml.crypto.dsig.DigestMethod
          javax.xml.crypto.dsig.DigestMethod javax.xml.crypto.dsig.keyinfo.KeyInfo
          javax.xml.crypto.dsig.keyinfo.KeyName javax.xml.crypto.dsig.keyinfo.KeyValue
          javax.xml.crypto.dsig.keyinfo.PGPData javax.xml.crypto.dsig.keyinfo.RetrievalMethod
          javax.xml.crypto.dsig.keyinfo.RetrievalMethod javax.xml.crypto.dsig.keyinfo.X509Data
          javax.xml.crypto.dsig.keyinfo.X509IssuerSerial javax.xml.crypto.dsig.Manifest
          javax.xml.crypto.dsig.Reference javax.xml.crypto.dsig.Reference javax.xml.crypto.dsig.SignatureMethod
          javax.xml.crypto.dsig.SignatureMethod javax.xml.crypto.dsig.SignatureProperties
          javax.xml.crypto.dsig.SignatureProperty javax.xml.crypto.dsig.SignedInfo
          javax.xml.crypto.dsig.spec.C14NMethodParameterSpec
          javax.xml.crypto.dsig.spec.DigestMethodParameterSpec
          javax.xml.crypto.dsig.spec.SignatureMethodParameterSpec
          javax.xml.crypto.dsig.spec.TransformParameterSpec javax.xml.crypto.dsig.Transform
          javax.xml.crypto.dsig.Transform javax.xml.crypto.dsig.XMLObject javax.xml.crypto.dsig.XMLSignature
          javax.xml.crypto.dsig.XMLSignature.SignatureValue javax.xml.crypto.dsig.XMLSignContext
          javax.xml.crypto.dsig.XMLValidateContext javax.xml.crypto.KeySelectorResult
          javax.xml.crypto.NodeSetData javax.xml.crypto.NodeSetData javax.xml.crypto.URIDereferencer
          javax.xml.crypto.URIReference javax.xml.crypto.XMLCryptoContext javax.xml.crypto.XMLStructure
          javax.xml.namespace.NamespaceContext javax.xml.stream.EventFilter javax.xml.stream.events.Attribute
          javax.xml.stream.events.Characters javax.xml.stream.events.Comment javax.xml.stream.events.DTD
          javax.xml.stream.events.EndDocument javax.xml.stream.events.EndElement
          javax.xml.stream.events.EntityDeclaration javax.xml.stream.events.EntityReference
          javax.xml.stream.events.Namespace javax.xml.stream.events.NotationDeclaration
          javax.xml.stream.events.ProcessingInstruction javax.xml.stream.events.StartDocument
          javax.xml.stream.events.StartElement javax.xml.stream.events.XMLEvent javax.xml.stream.Location
          javax.xml.stream.StreamFilter javax.xml.stream.util.XMLEventAllocator
          javax.xml.stream.util.XMLEventConsumer javax.xml.stream.XMLEventReader
          javax.xml.stream.XMLEventWriter javax.xml.stream.XMLReporter javax.xml.stream.XMLResolver
          javax.xml.stream.XMLStreamConstants javax.xml.stream.XMLStreamReader javax.xml.stream.XMLStreamWriter
          javax.xml.transform.dom.DOMLocator javax.xml.transform.ErrorListener javax.xml.transform.Result
          javax.xml.transform.sax.TemplatesHandler javax.xml.transform.sax.TransformerHandler
          javax.xml.transform.sax.TransformerHandler javax.xml.transform.sax.TransformerHandler
          javax.xml.transform.Source javax.xml.transform.SourceLocator javax.xml.transform.Templates
          javax.xml.transform.URIResolver javax.xml.xpath.XPath javax.xml.xpath.XPathEvaluationResult
          javax.xml.xpath.XPathExpression javax.xml.xpath.XPathFunction javax.xml.xpath.XPathFunctionResolver
          javax.xml.xpath.XPathNodes javax.xml.xpath.XPathVariableResolver'

        jdkInterfaceHierarchy='jdk.dynalink.beans.MissingMemberHandlerFactory
          jdk.dynalink.linker.ConversionComparator jdk.dynalink.linker.GuardedInvocationTransformer
          jdk.dynalink.linker.GuardingDynamicLinker jdk.dynalink.linker.GuardingTypeConverterFactory
          jdk.dynalink.linker.LinkerServices jdk.dynalink.linker.LinkRequest
          jdk.dynalink.linker.MethodHandleTransformer jdk.dynalink.linker.MethodTypeConversionStrategy
          jdk.dynalink.linker.TypeBasedGuardingDynamicLinker jdk.dynalink.Namespace jdk.dynalink.Operation
          jdk.dynalink.RelinkableCallSite jdk.incubator.foreign.MappedMemorySegment
          jdk.incubator.foreign.MemoryAddress jdk.incubator.foreign.MemoryLayout
          jdk.incubator.foreign.MemoryLayout.PathElement jdk.incubator.foreign.MemorySegment
          jdk.javadoc.doclet.Doclet jdk.javadoc.doclet.DocletEnvironment jdk.javadoc.doclet.Doclet.Option
          jdk.javadoc.doclet.Reporter jdk.javadoc.doclet.Taglet jdk.jfr.consumer.EventStream
          jdk.jfr.FlightRecorderListener jdk.jshell.execution.LoaderDelegate
          jdk.jshell.SourceCodeAnalysis.CompletionInfo jdk.jshell.SourceCodeAnalysis.Documentation
          jdk.jshell.SourceCodeAnalysis.SnippetWrapper jdk.jshell.SourceCodeAnalysis.Suggestion
          jdk.jshell.spi.ExecutionControl jdk.jshell.spi.ExecutionControlProvider jdk.jshell.spi.ExecutionEnv
          jdk.jshell.tool.JavaShellToolBuilder jdk.management.jfr.FlightRecorderMXBean
          jdk.nio.Channels.SelectableChannelCloser'

        jgssInterfaceHierarchy='org.ietf.jgss.GSSContext org.ietf.jgss.GSSCredential org.ietf.jgss.GSSName'

        w3cInterfaceHierarchy='org.w3c.dom.Attr
          org.w3c.dom.CDATASection org.w3c.dom.CharacterData org.w3c.dom.Comment org.w3c.dom.css.Counter
          org.w3c.dom.css.CSS2Properties org.w3c.dom.css.CSSCharsetRule org.w3c.dom.css.CSSFontFaceRule
          org.w3c.dom.css.CSSImportRule org.w3c.dom.css.CSSMediaRule org.w3c.dom.css.CSSPageRule
          org.w3c.dom.css.CSSPrimitiveValue org.w3c.dom.css.CSSRule org.w3c.dom.css.CSSRuleList
          org.w3c.dom.css.CSSStyleDeclaration org.w3c.dom.css.CSSStyleRule org.w3c.dom.css.CSSStyleSheet
          org.w3c.dom.css.CSSUnknownRule org.w3c.dom.css.CSSValue org.w3c.dom.css.CSSValueList
          org.w3c.dom.css.DocumentCSS org.w3c.dom.css.DOMImplementationCSS
          org.w3c.dom.css.ElementCSSInlineStyle org.w3c.dom.css.Rect org.w3c.dom.css.RGBColor
          org.w3c.dom.css.ViewCSS org.w3c.dom.Document org.w3c.dom.DocumentFragment org.w3c.dom.DocumentType
          org.w3c.dom.DOMConfiguration org.w3c.dom.DOMError org.w3c.dom.DOMErrorHandler
          org.w3c.dom.DOMImplementation org.w3c.dom.DOMImplementationList org.w3c.dom.DOMImplementationSource
          org.w3c.dom.DOMLocator org.w3c.dom.DOMStringList org.w3c.dom.Element org.w3c.dom.ElementTraversal
          org.w3c.dom.Entity org.w3c.dom.EntityReference org.w3c.dom.events.DocumentEvent
          org.w3c.dom.events.Event org.w3c.dom.events.EventListener org.w3c.dom.events.EventTarget
          org.w3c.dom.events.MouseEvent org.w3c.dom.events.MutationEvent org.w3c.dom.events.UIEvent
          org.w3c.dom.html.HTMLAnchorElement org.w3c.dom.html.HTMLAppletElement
          org.w3c.dom.html.HTMLAreaElement org.w3c.dom.html.HTMLBaseElement
          org.w3c.dom.html.HTMLBaseFontElement org.w3c.dom.html.HTMLBodyElement org.w3c.dom.html.HTMLBRElement
          org.w3c.dom.html.HTMLButtonElement org.w3c.dom.html.HTMLCollection
          org.w3c.dom.html.HTMLDirectoryElement org.w3c.dom.html.HTMLDivElement
          org.w3c.dom.html.HTMLDListElement org.w3c.dom.html.HTMLDocument
          org.w3c.dom.html.HTMLDOMImplementation org.w3c.dom.html.HTMLElement
          org.w3c.dom.html.HTMLFieldSetElement org.w3c.dom.html.HTMLFontElement
          org.w3c.dom.html.HTMLFormElement org.w3c.dom.html.HTMLFrameElement
          org.w3c.dom.html.HTMLFrameSetElement org.w3c.dom.html.HTMLHeadElement
          org.w3c.dom.html.HTMLHeadingElement org.w3c.dom.html.HTMLHRElement org.w3c.dom.html.HTMLHtmlElement
          org.w3c.dom.html.HTMLIFrameElement org.w3c.dom.html.HTMLImageElement
          org.w3c.dom.html.HTMLInputElement org.w3c.dom.html.HTMLIsIndexElement
          org.w3c.dom.html.HTMLLabelElement org.w3c.dom.html.HTMLLegendElement org.w3c.dom.html.HTMLLIElement
          org.w3c.dom.html.HTMLLinkElement org.w3c.dom.html.HTMLMapElement org.w3c.dom.html.HTMLMenuElement
          org.w3c.dom.html.HTMLMetaElement org.w3c.dom.html.HTMLModElement org.w3c.dom.html.HTMLObjectElement
          org.w3c.dom.html.HTMLOListElement org.w3c.dom.html.HTMLOptGroupElement
          org.w3c.dom.html.HTMLOptionElement org.w3c.dom.html.HTMLParagraphElement
          org.w3c.dom.html.HTMLParamElement org.w3c.dom.html.HTMLPreElement org.w3c.dom.html.HTMLQuoteElement
          org.w3c.dom.html.HTMLScriptElement org.w3c.dom.html.HTMLSelectElement
          org.w3c.dom.html.HTMLStyleElement org.w3c.dom.html.HTMLTableCaptionElement
          org.w3c.dom.html.HTMLTableCellElement org.w3c.dom.html.HTMLTableColElement
          org.w3c.dom.html.HTMLTableElement org.w3c.dom.html.HTMLTableRowElement
          org.w3c.dom.html.HTMLTableSectionElement org.w3c.dom.html.HTMLTextAreaElement
          org.w3c.dom.html.HTMLTitleElement org.w3c.dom.html.HTMLUListElement
          org.w3c.dom.ls.DOMImplementationLS org.w3c.dom.ls.LSInput org.w3c.dom.ls.LSLoadEvent
          org.w3c.dom.ls.LSOutput org.w3c.dom.ls.LSParser org.w3c.dom.ls.LSParserFilter
          org.w3c.dom.ls.LSProgressEvent org.w3c.dom.ls.LSResourceResolver org.w3c.dom.ls.LSSerializer
          org.w3c.dom.ls.LSSerializerFilter org.w3c.dom.NamedNodeMap org.w3c.dom.NameList org.w3c.dom.Node
          org.w3c.dom.NodeList org.w3c.dom.Notation org.w3c.dom.ProcessingInstruction
          org.w3c.dom.ranges.DocumentRange org.w3c.dom.ranges.Range org.w3c.dom.stylesheets.DocumentStyle
          org.w3c.dom.stylesheets.LinkStyle org.w3c.dom.stylesheets.MediaList
          org.w3c.dom.stylesheets.StyleSheet org.w3c.dom.stylesheets.StyleSheetList org.w3c.dom.Text
          org.w3c.dom.traversal.DocumentTraversal org.w3c.dom.traversal.NodeFilter
          org.w3c.dom.traversal.NodeIterator org.w3c.dom.traversal.TreeWalker org.w3c.dom.TypeInfo
          org.w3c.dom.UserDataHandler org.w3c.dom.views.AbstractView org.w3c.dom.views.DocumentView
          org.w3c.dom.xpath.XPathEvaluator org.w3c.dom.xpath.XPathExpression org.w3c.dom.xpath.XPathNamespace
          org.w3c.dom.xpath.XPathNSResolver org.w3c.dom.xpath.XPathResult'

        xmlInterfaceHierarchy='org.xml.sax.Attributes org.xml.sax.ContentHandler
          org.xml.sax.DTDHandler org.xml.sax.EntityResolver
          org.xml.sax.ErrorHandler org.xml.sax.ext.Attributes2 org.xml.sax.ext.DeclHandler
          org.xml.sax.ext.EntityResolver2 org.xml.sax.ext.LexicalHandler org.xml.sax.ext.Locator2
          org.xml.sax.Locator org.xml.sax.XMLFilter org.xml.sax.XMLReader'

        # ------------------------------------------------------------------------------------------ #
        print_completers() {

          format=' %s\;||%s'

          for import in $@
          do
            printf "$format" "$import" "$import"
          done

        }
        # ------------------------------------------------------------------------------------------ #
        printf "set-option -add window java_hierarchy_completions "

        print_completers $javaxInterfaceHierarchy $jdkInterfaceHierarchy $jgssInterfaceHierarchy
        print_completers $w3cInterfaceHierarchy $xmlInterfaceHierarchy

        printf "\n"
        # ------------------------------------------------------------------------------------------ #
      }
      # --------------- STAGE 7 OF 7 --------------------------------------------------------------- #
      evaluate-commands %sh{
        # --------------- ANNOTATION INTERFACE HIERARCHY ------------------------------------------- #

        annotationTypeHierarchy='java.beans.BeanProperty java.beans.ConstructorProperties java.beans.JavaBean
          java.beans.Transient java.io.Serial java.lang.annotation.Documented java.lang.annotation.Inherited
          java.lang.annotation.Native java.lang.annotation.Repeatable java.lang.annotation.Retention
          java.lang.annotation.Target java.lang.Deprecated java.lang.FunctionalInterface java.lang.Override
          java.lang.SafeVarargs java.lang.SuppressWarnings javax.annotation.processing.Generated
          javax.annotation.processing.SupportedAnnotationTypes javax.annotation.processing.SupportedOptions
          javax.annotation.processing.SupportedSourceVersion javax.management.ConstructorParameters
          javax.management.DescriptorKey javax.management.MXBean javax.swing.SwingContainer jdk.jfr.BooleanFlag
          jdk.jfr.Category jdk.jfr.ContentType jdk.jfr.DataAmount jdk.jfr.Description jdk.jfr.Enabled
          jdk.jfr.Experimental jdk.jfr.Frequency jdk.jfr.Label jdk.jfr.MemoryAddress jdk.jfr.MetadataDefinition
          jdk.jfr.Name jdk.jfr.Percentage jdk.jfr.Period jdk.jfr.Registered jdk.jfr.Relational
          jdk.jfr.SettingDefinition jdk.jfr.StackTrace jdk.jfr.Threshold jdk.jfr.Timespan jdk.jfr.Timestamp
          jdk.jfr.TransitionFrom jdk.jfr.TransitionTo jdk.jfr.Unsigned'

        # --------------- ENUM CLASS HIERARCHY ----------------------------------------------------- #

        enumHierarchy='com.sun.management.VMOption.Origin
          com.sun.nio.sctp.AssociationChangeNotification.AssocChangeEvent com.sun.nio.sctp.HandlerResult
          com.sun.nio.sctp.PeerAddressChangeNotification.AddressChangeEvent com.sun.security.jgss.InquireType
          com.sun.source.doctree.AttributeTree.ValueKind com.sun.source.doctree.DocTree.Kind
          com.sun.source.tree.CaseTree.CaseKind com.sun.source.tree.LambdaExpressionTree.BodyKind
          com.sun.source.tree.MemberReferenceTree.ReferenceMode com.sun.source.tree.ModuleTree.ModuleKind
          com.sun.source.tree.Tree.Kind com.sun.source.util.TaskEvent.Kind
          com.sun.tools.jconsole.JConsoleContext.ConnectionState java.awt.Component.BaselineResizeBehavior
          java.awt.Desktop.Action java.awt.desktop.QuitStrategy java.awt.desktop.UserSessionEvent.Reason
          java.awt.Dialog.ModalExclusionType java.awt.Dialog.ModalityType java.awt.event.FocusEvent.Cause
          java.awt.font.NumericShaper.Range java.awt.GraphicsDevice.WindowTranslucency
          java.awt.MultipleGradientPaint.ColorSpaceType java.awt.MultipleGradientPaint.CycleMethod
          java.awt.Taskbar.Feature java.awt.Taskbar.State java.awt.TrayIcon.MessageType java.awt.Window.Type
          java.io.ObjectInputFilter.Status java.lang.annotation.ElementType
          java.lang.annotation.RetentionPolicy java.lang.Character.UnicodeScript
          java.lang.constant.DirectMethodHandleDesc.Kind java.lang.Enum
          java.lang.invoke.MethodHandles.Lookup.ClassOption java.lang.invoke.VarHandle.AccessMode
          java.lang.management.MemoryType java.lang.module.ModuleDescriptor.Exports.Modifier
          java.lang.module.ModuleDescriptor.Modifier java.lang.module.ModuleDescriptor.Opens.Modifier
          java.lang.module.ModuleDescriptor.Requires.Modifier java.lang.Object
          java.lang.ProcessBuilder.Redirect.Type java.lang.StackWalker.Option java.lang.System.Logger.Level
          java.lang.Thread.State java.math.RoundingMode java.net.Authenticator.RequestorType
          java.net.http.HttpClient.Redirect java.net.http.HttpClient.Version java.net.Proxy.Type
          java.net.StandardProtocolFamily java.nio.file.AccessMode java.nio.file.attribute.AclEntryFlag
          java.nio.file.attribute.AclEntryPermission java.nio.file.attribute.AclEntryType
          java.nio.file.attribute.PosixFilePermission java.nio.file.FileVisitOption
          java.nio.file.FileVisitResult java.nio.file.LinkOption java.nio.file.StandardCopyOption
          java.nio.file.StandardOpenOption java.security.cert.CertPathValidatorException.BasicReason
          java.security.cert.CRLReason java.security.cert.PKIXReason
          java.security.cert.PKIXRevocationChecker.Option java.security.CryptoPrimitive
          java.security.DrbgParameters.Capability java.security.KeyRep.Type java.sql.ClientInfoStatus
          java.sql.JDBCType java.sql.PseudoColumnUsage java.sql.RowIdLifetime java.text.Normalizer.Form
          java.text.NumberFormat.Style java.time.chrono.HijrahEra java.time.chrono.IsoEra
          java.time.chrono.MinguoEra java.time.chrono.ThaiBuddhistEra java.time.DayOfWeek
          java.time.format.FormatStyle java.time.format.ResolverStyle java.time.format.SignStyle
          java.time.format.TextStyle java.time.Month java.time.temporal.ChronoField
          java.time.temporal.ChronoUnit java.time.zone.ZoneOffsetTransitionRule.TimeDefinition
          java.util.concurrent.TimeUnit java.util.Formatter.BigDecimalLayoutForm java.util.Locale.Category
          java.util.Locale.FilteringMode java.util.Locale.IsoCountryCode
          java.util.stream.Collector.Characteristics javax.lang.model.element.ElementKind
          javax.lang.model.element.Modifier javax.lang.model.element.ModuleElement.DirectiveKind
          javax.lang.model.element.NestingKind javax.lang.model.SourceVersion javax.lang.model.type.TypeKind
          javax.lang.model.util.Elements.Origin javax.net.ssl.SSLEngineResult.HandshakeStatus
          javax.net.ssl.SSLEngineResult.Status javax.smartcardio.CardTerminals.State javax.swing.DropMode
          javax.swing.event.RowSorterEvent.Type javax.swing.GroupLayout.Alignment javax.swing.JTable.PrintMode
          javax.swing.LayoutStyle.ComponentPlacement
          javax.swing.plaf.nimbus.AbstractRegionPainter.PaintContext.CacheMode
          javax.swing.RowFilter.ComparisonType javax.swing.SortOrder javax.swing.SwingWorker.StateValue
          javax.swing.text.html.FormSubmitEvent.MethodType javax.tools.Diagnostic.Kind
          javax.tools.DocumentationTool.Location javax.tools.JavaFileObject.Kind javax.tools.StandardLocation
          javax.xml.catalog.CatalogFeatures.Feature javax.xml.xpath.XPathEvaluationResult.XPathResultType
          jdk.dynalink.linker.ConversionComparator.Comparison jdk.dynalink.StandardNamespace
          jdk.dynalink.StandardOperation jdk.javadoc.doclet.DocletEnvironment.ModuleMode
          jdk.javadoc.doclet.Doclet.Option.Kind jdk.javadoc.doclet.Taglet.Location jdk.jfr.RecordingState
          jdk.jshell.Snippet.Kind jdk.jshell.Snippet.Status jdk.jshell.Snippet.SubKind
          jdk.jshell.SourceCodeAnalysis.Completenes'

        # ------------------------------------------------------------------------------------------ #
        print_completers() {

          format=' %s\;||%s'

          for import in $@
          do
            printf "$format" "$import" "$import"
          done

        }
        # ------------------------------------------------------------------------------------------ #
        printf "set-option -add window java_hierarchy_completions "

        print_completers $annotationTypeHierarchy $enumHierarchy

        printf "\n"
        # ------------------------------------------------------------------------------------------ #
      }
    } catch %{
      set-option window java_hierarchy_completions
    }
  }
  hook -once -always window WinSetOption filetype=.* %{ remove-hooks window java-import-statements }
}
