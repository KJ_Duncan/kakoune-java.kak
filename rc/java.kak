# References
# ‾‾‾‾‾‾‾‾‾‾
# The textblock negative look-behind (?<!\\) is required for the contrived use-case outlined in: https://openjdk.java.net/jeps/378
#
#   System.out.println("""
#      1 "
#      2 ""
#      3 ""\"
#      4 ""\""
#      5 ""\"""
#      6 ""\"""\"
#      7 ""\"""\""
#      8 ""\"""\"""
#      9 ""\"""\"""\"
#     10 ""\"""\"""\""
#     11 ""\"""\"""\"""
#     12 ""\"""\"""\"""\"
#   """);
#
# Oracle 2020, Programmer's Guide to Text Blocks, Java SE 16, viewed 22 June 2021, https://docs.oracle.com/en/java/javase/16/text-blocks/index.html
# Oracle 2022, 3.9 Keywords, Chapter 3. Lexical Structure, Java Language Specification, Java SE 18, viewed 26 August 2022, https://docs.oracle.com/javase/specs/jls/se18/html/jls-3.html#jls-3.9
# Oracle 2020, FunctionalInterface, java.util.function, viewed 08 December 2020, https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/util/function/package-summary.html
# Oracle 2020, Java Language Updates for Java SE 15, Java Language Updates, viewed 06 December 2020, https://docs.oracle.com/en/java/javase/15/language/java-language-changes.html
# Oracle 2020, Java Platform, Standard Edition & Java Development Kit, Version 15 API Specification, viewed 11 December 2020, https://docs.oracle.com/en/java/javase/15/docs/api/index.html
# Oracle 2020, The Java Language Specification, Java SE 15 Edition, viewed 11 December 2020, https://docs.oracle.com/javase/specs/jls/se15/html/index.html
# Oracle 2021, Constant Field Values, Java SE 15, viewed 28 January 2021, https://docs.oracle.com/en/java/javase/15/docs/api/constant-values.html
# Oracle 2021, Hierarchy For All Packages, Java SE 15, viewed 29 January 2021, https://docs.oracle.com/en/java/javase/15/docs/api/overview-tree.html
# Oracle 2021, JSR 391: Java SE 16, JSR 391: Java SE 16 Specification, viewed 14 April 2021, https://cr.openjdk.java.net/~iris/se/16/latestSpec/apidiffs/overview-summary.html
# Oracle 2021, JSR 391: Java SE 16: Annex 3,Final Release Specification JLS & JVMS, viewed 14 April 2021, https://cr.openjdk.java.net/~iris/se/16/latestSpec/java-se-16-annex-3.html
#
# -------------------------------------------------------------------------------------------------- #
# Detection
# ‾‾‾‾‾‾‾‾‾
hook global BufCreate .*\.java %{
  set-option buffer filetype java
}
# -------------------------------------------------------------------------------------------------- #
# Initialisation
# ‾‾‾‾‾‾‾‾‾‾‾‾‾‾
hook global WinSetOption filetype=java %{
  require-module java

  set-option -add window static_words %opt{main_static_words}
  set-option -add window static_words %opt{user_static_words}
  set-option -add window static_words %opt{error_static_words}
  set-option -add window static_words %opt{exception_static_words}


  # cleanup trailing white-spaces when exiting insert mode
  hook window ModeChange pop:insert:.* -group java-indents java-trim-indents
  hook window InsertChar \n -group java-inserts java-insert-on-newlines
  hook window InsertChar \n -group java-indents java-indent-on-newlines
  hook window InsertChar \{ -group java-indents java-indent-on-opening-curly-braces
  hook window InsertChar \} -group java-indents java-indent-on-closing-curly-braces

  hook -once -always window WinSetOption filetype=.* %{ remove-hooks window java-.+ }
}

hook -group java-highlighter global WinSetOption filetype=java %{
  add-highlighter -override window/java ref java
  hook -once -always window WinSetOption filetype=.* %{ remove-highlighter window/java }
}
# -------------------------------------------------------------------------------------------------- #
# Module
# ‾‾‾‾‾‾
provide-module -override java %§

add-highlighter shared/java regions
add-highlighter shared/java/code default-region group
add-highlighter shared/java/textblock    region %{"""} %{(?<!\\)"""} fill string
add-highlighter shared/java/string       region %{"} %{(?<!\\)"} fill string
add-highlighter shared/java/character    region %{'} %{(?<!\\)'} fill value
add-highlighter shared/java/comment      region /\* \*/ fill comment
add-highlighter shared/java/inline_docs  region /// $ fill documentation
add-highlighter shared/java/line_comment region // $ fill comment

add-highlighter shared/java/code/ regex "(?<!\w)@\w+\b" 0:meta
add-highlighter shared/java/code/ regex "^(package|import|import static)(?S)(.+)" 2:default+fa
add-highlighter shared/java/code/ regex %{(=|>|<|!|~|\?|:|->|==|>=|<=|!=|&&|\|\||\+\+|--|\+|-|\*|/|&|\||^|%|<<|>>|>>>|\+=|-=|\*=|/=|&=|\|=|^=|%=|<<=|>>=|>>>=)} 1:operator
# -------------------------------------------------------------------------------------------------- #
# Commands source edited c-family.kak: <https://raw.githubusercontent.com/mawww/kakoune/master/rc/filetype/c-family.kak>
# ‾‾‾‾‾‾‾‾
# NOTE: c-family.kak - indent and insert commands: <https://delapouite.github.io/kakoune-explain/>
# NOTE: error running hook InsertChar( )/java-insert: 1:1: 'java-insert-on-new-line': no such command
define-command -override -hidden java-insert-on-new-line %[ ]
define-command -override -hidden java-indent-on-new-line %[ ]
define-command -override -hidden java-indent-on-opening-curly-brace %[ ]
define-command -override -hidden java-indent-on-closing-curly-brace %[ ]

define-command -hidden java-trim-indents %{
  # remove the line if it's empty when leaving the insert mode
  try %{ execute-keys -draft x 1s^(\h+)$<ret> d }
}

define-command -hidden java-indent-on-newlines %< evaluate-commands -draft -itersel %<
  try %<
      # if previous line is part of a comment, do nothing
      execute-keys -draft <a-?>/\*<ret> <a-K>^\h*[^/*\h]<ret>
  > catch %<
      # preserve previous line indent
      execute-keys -draft <semicolon>K<a-&>
  >
  # indent after lines ending with { or (
  try %[ execute-keys -draft kx <a-k> [{(]\h*$ <ret> j<a-gt> ]
  # cleanup trailing white spaces on the previous line
  try %{ execute-keys -draft kx s \h+$ <ret>d }
  # align to opening paren of previous line
  try %{ execute-keys -draft [( <a-k> \A\([^\n]+\n[^\n]*\n?\z <ret> s \A\(\h*.|.\z <ret> '<a-;>' & }
  # indent after a switch's case/default statements
  try %[ execute-keys -draft kx <a-k> ^\h*(case|default).*:$ <ret> j<a-gt> ]
  # indent after keywords
  try %[ execute-keys -draft <semicolon><a-F>)MB <a-k> \A(catch|else|for|if|try|while)\h*\(.*\)\h*\n\h*\n?\z <ret> s \A|.\z <ret> 1<a-&>1<a-,><a-gt> ]
  # deindent closing brace(s) when after cursor
  try %[ execute-keys -draft x <a-k> ^\h*[})] <ret> gh / [})] <ret> m <a-S> 1<a-&> ]
> >

define-command -hidden java-insert-on-newlines %[ evaluate-commands -itersel -draft %[
  execute-keys <semicolon>
  try %[
    evaluate-commands -draft -save-regs '/"' %[
      # copy the commenting prefix
      execute-keys -save-regs '' k x1s^\h*(//+\h*)<ret> y
      try %[
        # if the previous comment isn't empty, create a new one
        execute-keys x<a-K>^\h*//+\h*$<ret> jxs^\h*<ret>P
      ] catch %[
        # if there is no text in the previous comment, remove it completely
        execute-keys d
      ]
    ]

    # trim trailing whitespace on the previous line
    try %[ execute-keys -draft k x s\h+$<ret> d ]
  ]
  try %[
    # if the previous line isn't within a comment scope, break
    execute-keys -draft kx <a-k>^(\h*/\*|\h+\*(?!/))<ret>

    # find comment opening, validate it was not closed, and check its using star prefixes
    execute-keys -draft <a-?>/\*<ret><a-H> <a-K>\*/<ret> <a-k>\A\h*/\*([^\n]*\n\h*\*)*[^\n]*\n\h*.\z<ret>

    try %[
      # if the previous line is opening the comment, insert star preceeded by space
      execute-keys -draft kx<a-k>^\h*/\*<ret>
      execute-keys -draft i*<space><esc>
    ] catch %[
     try %[
        # if the next line is a comment line insert a star
        execute-keys -draft jx<a-k>^\h+\*<ret>
        execute-keys -draft i*<space><esc>
      ] catch %[
        try %[
          # if the previous line is an empty comment line, close the comment scope
          execute-keys -draft kx<a-k>^\h+\*\h+$<ret> x1s\*(\h*)<ret>c/<esc>
        ] catch %[
          # if the previous line is a non-empty comment line, add a star
          execute-keys -draft i*<space><esc>
        ]
      ]
    ]

    # trim trailing whitespace on the previous line
    try %[ execute-keys -draft k x s\h+$<ret> d ]
    # align the new star with the previous one
    execute-keys Kx1s^[^*]*(\*)<ret>&
  ]
] ]

define-command -hidden java-indent-on-opening-curly-braces %[
  # align indent with opening paren when { is entered on a new line after the closing paren
  try %[ execute-keys -draft -itersel h<a-F>)M <a-k> \A\(.*\)\h*\n\h*\{\z <ret> <a-S> 1<a-&> ]
]

define-command -hidden java-indent-on-closing-curly-braces %[
  # align to opening curly brace when alone on a line
  try %[ execute-keys -itersel -draft <a-h><a-:><a-k>^\h+\}$<ret>hm<a-S>1<a-&> ]
]

# -------------------------------------------------------------------------------------------------- #
# Shell
# ‾‾‾‾‾
# macro: 93le<a-;>i<ret><esc><esc>
evaluate-commands %sh{
  values='false null this true'

  types='boolean byte char double float int long short unsigned void'

  keywords='assert break case catch class continue default do else enum extends finally for if implements import instanceof interface new package return static strictfp super switch throw throws try while'

  attributes='abstract final native non-sealed permits private protected public record sealed synchronized transient volatile'

  modules='exports module open opens provides requires to transitive uses with'

  restricted='var yield'

  reserved='const goto'

  # ------------------------------------------------------------------------------------------------- #
  # c-family.kak: <https://github.com/mawww/kakoune/blob/master/rc/filetype/c-family.kak#L271>
  join() { sep=$2; eval set -- $1; IFS="$sep"; echo "$*"; }
  # ---------------------------------------------------------------------------------------------- #
  add_highlighter() { printf "add-highlighter shared/java/code/ regex %s %s\n" "$1" "$2"; }
  # ---------------------------------------------------------------------------------------------- #
  # alexherbo2: <https://github.com/alexherbo2/plug.kak/blob/master/rc/plug.kak#L108>
  add_word_highlighter() {

    while [ $# -gt 0 ]; do
      words=$1 face=$2; shift 2
      regex="\\b($(join "${words}" '|'))\\b"
      add_highlighter "$regex" "1:$face"
    done

  }

  # highlight: open<space> not open()
  add_module_highlighter() {

    while [ $# -gt 0 ]; do
      words=$1 face=$2; shift 2
      regex="\\b($(join "${words}" '|'))\\b(?=\\s)"
      add_highlighter "$regex" "1:$face"
    done

  }
  # ---------------------------------------------------------------------------------------------- #
  printf %s\\n "declare-option str-list main_static_words $(join "${values} ${types} ${keywords} ${attributes} ${modules} ${restricted} ${reserved}" ' ')"
  # ---------------------------------------------------------------------------------------------- #
  add_word_highlighter "$values" "value" "$types" "type" "$keywords" "keyword" "$attributes" "attribute" "$restricted" "value" "$reserved" "value"
  # ------------------------------------------------------------------------------------------------- #
  add_module_highlighter "$modules" "module"
  # ------------------------------------------------------------------------------------------------- #
}

evaluate-commands %sh{
  object='Boolean Byte Character Class Double Enum Float Integer Long Number Object Short String'

  functional='BiConsumer BiFunction BinaryOperator BiPredicate BooleanSupplier Consumer
    DoubleBinaryOperator DoubleConsumer DoubleFunction DoublePredicate DoubleSupplier
    DoubleToIntFunction DoubleToLongFunction DoubleUnaryOperator Function IntBinaryOperator
    IntConsumer IntFunction IntPredicate IntSupplier IntToDoubleFunction IntToLongFunction
    IntUnaryOperator LongBinaryOperator LongConsumer LongFunction LongPredicate LongSupplier
    LongToDoubleFunction LongToIntFunction LongUnaryOperator ObjDoubleConsumer ObjIntConsumer
    ObjLongConsumer Predicate Supplier ToDoubleBiFunction ToDoubleFunction ToIntBiFunction
    ToIntFunction ToLongBiFunction ToLongFunction UnaryOperator'

  collection='AbstractCollection AbstractList AbstractQueue AbstractSequentialList
    AbstractSet ArrayDeque ArrayList AttributeList BeanContext BeanContextServices
    BeanContextServicesSupport BeanContextSupport Deque EnumSet EventSet HashSet Iterable
    JobStateReasons LinkedHashSet LinkedList List NavigableSet PriorityQueue Queue RoleList
    RoleUnresolvedList Set SortedSet Stack TreeSet Vector'

  concurrent='AbstractExecutorService ArrayBlockingQueue BlockingDeque BlockingQueue Callable
    CompletableFuture AsynchronousCompletionTask CompletionService
    CompletionStage ConcurrentHashMap KeySetView ConcurrentLinkedDeque
    ConcurrentLinkedQueue ConcurrentMap ConcurrentNavigableMap ConcurrentSkipListMap
    ConcurrentSkipListSet CopyOnWriteArrayList CopyOnWriteArraySet CountDownLatch
    CountedCompleter CyclicBarrier DelayQueue Delayed Exchanger Executor
    ExecutorCompletionService ExecutorService Executors Flow Processor Publisher
    Subscriber Subscription ForkJoinPool ForkJoinWorkerThreadFactory
    ManagedBlocker ForkJoinTask ForkJoinWorkerThread Future FutureTask
    LinkedBlockingDeque LinkedBlockingQueue LinkedTransferQueue Phaser PriorityBlockingQueue
    RecursiveAction RecursiveTask RejectedExecutionHandler RunnableFuture RunnableScheduledFuture
    ScheduledExecutorService ScheduledFuture ScheduledThreadPoolExecutor Semaphore
    SubmissionPublisher SynchronousQueue ThreadFactory ThreadLocalRandom ThreadPoolExecutor
    AbortPolicy CallerRunsPolicy DiscardOldestPolicy DiscardPolicy TransferQueue'

  # ------------------------------------------------------------------------------------------------- #
  # c-family.kak: <https://github.com/mawww/kakoune/blob/master/rc/filetype/c-family.kak#L271>
  join() { sep=$2; eval set -- $1; IFS="$sep"; echo "$*"; }
  # ---------------------------------------------------------------------------------------------- #
  add_highlighter() { printf "add-highlighter shared/java/code/ regex %s %s\n" "$1" "$2"; }
  # ---------------------------------------------------------------------------------------------- #
  # alexherbo2: <https://github.com/alexherbo2/plug.kak/blob/master/rc/plug.kak#L108>
  add_word_highlighter() {

    while [ $# -gt 0 ]; do
      words=$1 face=$2; shift 2
      regex="\\b($(join "${words}" '|'))\\b"
      add_highlighter "$regex" "1:$face"
    done

  }
  # ---------------------------------------------------------------------------------------------- #
  printf %s\\n "declare-option str-list user_static_words $(join "${object} ${functional} ${collection} ${concurrent}" ' ')"
  # ---------------------------------------------------------------------------------------------- #
  add_word_highlighter "$object" "defualt" "$functional" "function" "$collection" "rgb:78a0ba" "$concurrent" "rgb:9bb7d4"
  # ------------------------------------------------------------------------------------------------- #
}

evaluate-commands %sh{
  errors_ALL='AbstractMethodError AnnotationFormatError AssertionError AWTError
    BootstrapMethodError ClassCircularityError ClassFormatError CoderMalfunctionError
    ExceptionInInitializerError FactoryConfigurationError GenericSignatureFormatError
    IllegalAccessError IncompatibleClassChangeError InstantiationError InternalError IOError
    JMXServerErrorException​ LinkageError NoClassDefFoundError NoSuchFieldError NoSuchMethodError
    OutOfMemoryError RuntimeErrorException RuntimeErrorException SchemaFactoryConfigurationError
    ServerError​ ServiceConfigurationError StackOverflowError ThreadDeath
    TransformerFactoryConfigurationError UnknownError UnsatisfiedLinkError
    UnsupportedClassVersionError VerifyError VirtualMachineError ZipError'

  # ------------------------------------------------------------------------------------------------- #

  exceptions_ALL='AbsentInformationException AcceptPendingException AccessControlException
    AccessDeniedException AccessException AccountException AccountExpiredException
    AccountLockedException AccountNotFoundException ActivateFailedException ActivationException
    AEADBadTagException AgentInitializationException AgentLoadException AlreadyBoundException
    AlreadyBoundException AlreadyConnectedException AnnotationTypeMismatchException
    ArithmeticException ArrayIndexOutOfBoundsException ArrayStoreException
    AsynchronousCloseException AtomicMoveNotSupportedException AttachNotSupportedException
    AttachOperationFailedException AttributeInUseException AttributeModificationException
    AttributeNotFoundException AuthenticationException AuthenticationException
    AuthenticationNotSupportedException AWTException BackingStoreException
    BadAttributeValueExpException BadBinaryOpValueExpException BadLocationException
    BadPaddingException BadStringOperationException BatchUpdateException BindException
    BrokenBarrierException BufferOverflowException BufferUnderflowException CancellationException
    CancelledKeyException CannotProceedException CannotRedoException CannotUndoException
    CardException CardNotPresentException CatalogException CertificateEncodingException
    CertificateEncodingException CertificateException CertificateException
    CertificateExpiredException CertificateExpiredException CertificateNotYetValidException
    CertificateNotYetValidException CertificateParsingException CertificateParsingException
    CertificateRevokedException CertPathBuilderException CertPathValidatorException
    CertStoreException ChangedCharSetException CharacterCodingException CharConversionException
    ClassCastException ClassInstallException ClassNotFoundException ClassNotLoadedException
    ClassNotPreparedException CloneNotSupportedException ClosedByInterruptException
    ClosedChannelException ClosedConnectionException ClosedDirectoryStreamException
    ClosedFileSystemException ClosedSelectorException ClosedWatchServiceException CMMException
    CommunicationException CompletionException ConcurrentModificationException
    ConfigurationException ConnectException ConnectException ConnectIOException
    ConnectionPendingException ContextNotEmptyException CredentialException
    CredentialExpiredException CredentialNotFoundException CRLException DataFormatException
    DataTruncation DatatypeConfigurationException DateTimeException DateTimeParseException
    DestroyFailedException DigestException DirectoryIteratorException DirectoryNotEmptyException
    DOMException DuplicateFormatFlagsException DuplicateRequestException EmptyStackException
    EngineTerminationException EnumConstantNotPresentException EOFException EvalException
    EventException Exception ExecutionControlException ExecutionException
    ExemptionMechanismException ExpandVetoException ExportException FailedLoginException
    FileAlreadyExistsException FileLockInterruptionException FileNotFoundException FilerException
    FileSystemAlreadyExistsException FileSystemException FileSystemLoopException
    FileSystemNotFoundException FindException FontFormatException
    FormatFlagsConversionMismatchException FormatterClosedException GeneralSecurityException
    GSSException HeadlessException HttpConnectTimeoutException HttpRetryException
    HttpTimeoutException IIOException IIOInvalidTreeException IllegalAccessException
    IllegalArgumentException IllegalBlockingModeException IllegalBlockSizeException
    IllegalCallerException IllegalChannelGroupException IllegalCharsetNameException
    IllegalClassFormatException IllegalComponentStateException IllegalConnectorArgumentsException
    IllegalFormatCodePointException IllegalFormatConversionException IllegalFormatException
    IllegalFormatFlagsException IllegalFormatPrecisionException IllegalFormatWidthException
    IllegalMonitorStateException IllegalPathStateException IllegalReceiveException
    IllegalSelectorException IllegalStateException IllegalThreadStateException
    IllegalUnbindException IllformedLocaleException ImagingOpException
    InaccessibleObjectException IncompatibleThreadStateException IncompleteAnnotationException
    InconsistentDebugInfoException IndexOutOfBoundsException InputMismatchException
    InstanceAlreadyExistsException InstanceNotFoundException InstantiationException
    InsufficientResourcesException InternalException InternalException
    InterruptedByTimeoutException InterruptedException InterruptedIOException
    InterruptedNamingException IntrospectionException IntrospectionException
    InvalidAlgorithmParameterException InvalidApplicationException
    InvalidAttributeIdentifierException InvalidAttributesException InvalidAttributeValueException
    InvalidAttributeValueException InvalidClassException InvalidCodeIndexException
    InvalidDnDOperationException InvalidKeyException InvalidKeyException InvalidKeySpecException
    InvalidLineNumberException InvalidMarkException InvalidMidiDataException
    InvalidModuleDescriptorException InvalidModuleException InvalidNameException
    InvalidObjectException InvalidOpenTypeException InvalidParameterException
    InvalidParameterSpecException InvalidPathException InvalidPreferencesFormatException
    InvalidPropertiesFormatException InvalidRelationIdException InvalidRelationServiceException
    InvalidRelationTypeException InvalidRequestStateException InvalidRoleInfoException
    InvalidRoleValueException InvalidSearchControlsException InvalidSearchFilterException
    InvalidStackFrameException InvalidStreamException InvalidTargetObjectTypeException
    InvalidTypeException InvocationException InvocationTargetException IOException JarException
    JarSignerException JMException JMRuntimeException JMXProviderException
    JMXServerErrorException JSException JShellException KeyAlreadyExistsException KeyException
    KeyManagementException KeySelectorException KeyStoreException LambdaConversionException
    LayerInstantiationException LdapReferralException LimitExceededException
    LineUnavailableException LinkException LinkLoopException ListenerNotFoundException
    LoginException LSException MalformedInputException MalformedLinkException
    MalformedObjectNameException MalformedParameterizedTypeException MalformedParametersException
    MalformedURLException MarshalException MarshalException MBeanException
    MBeanRegistrationException MidiUnavailableException MimeTypeParseException
    MirroredTypeException MirroredTypesException MissingFormatArgumentException
    MissingFormatWidthException MissingResourceException MonitorSettingException
    NameAlreadyBoundException NameNotFoundException NamingException NamingSecurityException
    NativeMethodException NegativeArraySizeException NoConnectionPendingException
    NoInitialContextException NoninvertibleTransformException NonReadableChannelException
    NonWritableChannelException NoPermissionException NoRouteToHostException
    NoSuchAlgorithmException NoSuchAttributeException NoSuchDynamicMethodException
    NoSuchElementException NoSuchFieldException NoSuchFileException NoSuchMechanismException
    NoSuchMethodException NoSuchObjectException NoSuchPaddingException NoSuchProviderException
    NotActiveException NotBoundException NotCompliantMBeanException NotContextException
    NotDirectoryException NotImplementedException NotLinkException NotSerializableException
    NotYetBoundException NotYetConnectedException NullPointerException NumberFormatException
    ObjectCollectedException ObjectStreamException OpenDataException
    OperationNotSupportedException OperationsException OptionalDataException
    OverlappingFileLockException ParseException ParserConfigurationException
    PartialResultException PatternSyntaxException PortUnreachableException PrinterAbortException
    PrinterException PrinterIOException PrintException PrivilegedActionException
    ProfileDataException PropertyVetoException ProtocolException ProviderException
    ProviderMismatchException ProviderNotFoundException RangeException RasterFormatException
    ReadOnlyBufferException ReadOnlyFileSystemException ReadPendingException ReferralException
    ReflectionException ReflectiveOperationException RefreshFailedException
    RejectedExecutionException RelationException RelationNotFoundException
    RelationServiceNotRegisteredException RelationTypeNotFoundException RemoteException
    ResolutionException ResolutionException RMISecurityException RoleInfoNotFoundException
    RoleNotFoundException RowSetWarning RunException RuntimeErrorException RuntimeException
    RuntimeMBeanException RuntimeOperationsException SaslException SAXException
    SAXNotRecognizedException SAXNotSupportedException SAXParseException SchemaViolationException
    ScriptException SecurityException SerialException ServerCloneException ServerError
    ServerException ServerNotActiveException ServerRuntimeException ServiceNotFoundException
    ServiceUnavailableException ShortBufferException ShutdownChannelGroupException
    SignatureException SizeLimitExceededException SkeletonMismatchException
    SkeletonNotFoundException SocketException SocketSecurityException SocketTimeoutException
    SPIResolutionException SQLClientInfoException SQLDataException SQLException
    SQLFeatureNotSupportedException SQLIntegrityConstraintViolationException
    SQLInvalidAuthorizationSpecException SQLNonTransientConnectionException
    SQLNonTransientException SQLRecoverableException SQLSyntaxErrorException SQLTimeoutException
    SQLTransactionRollbackException SQLTransientConnectionException SQLTransientException
    SQLWarning SSLException SSLHandshakeException SSLKeyException SSLPeerUnverifiedException
    SSLProtocolException StoppedException StreamCorruptedException StringConcatException
    StringIndexOutOfBoundsException StubNotFoundException SyncFactoryException
    SyncFailedException SyncProviderException TimeLimitExceededException TimeoutException
    TooManyListenersException TransformerConfigurationException TransformerException
    TransformException TransportTimeoutException TypeNotPresentException UncheckedIOException
    UndeclaredThrowableException UnexpectedException UnknownAnnotationValueException
    UnknownDirectiveException UnknownElementException UnknownEntityException
    UnknownFormatConversionException UnknownFormatFlagsException UnknownGroupException
    UnknownHostException UnknownHostException UnknownObjectException UnknownServiceException
    UnknownTypeException UnmappableCharacterException UnmarshalException
    UnmodifiableClassException UnmodifiableModuleException UnmodifiableSetException
    UnrecoverableEntryException UnrecoverableKeyException UnresolvedAddressException
    UnresolvedReferenceException UnsupportedAddressTypeException UnsupportedAudioFileException
    UnsupportedCallbackException UnsupportedCharsetException UnsupportedEncodingException
    UnsupportedFlavorException UnsupportedLookAndFeelException UnsupportedOperationException
    UnsupportedTemporalTypeException URIReferenceException URISyntaxException UserException
    UserPrincipalNotFoundException UTFDataFormatException VMCannotBeModifiedException
    VMDisconnectedException VMMismatchException VMOutOfMemoryException VMStartException
    WebSocketHandshakeException WriteAbortedException WritePendingException
    WrongMethodTypeException XAException XMLParseException XMLSignatureException
    XMLStreamException XPathException XPathException XPathExpressionException
    XPathFactoryConfigurationException XPathFunctionException ZipException ZoneRulesException'

  # ------------------------------------------------------------------------------------------------- #
  # c-family.kak: <https://github.com/mawww/kakoune/blob/master/rc/filetype/c-family.kak#L271>
  join() { sep=$2; eval set -- $1; IFS="$sep"; echo "$*"; }
  # ---------------------------------------------------------------------------------------------- #
  add_highlighter() { printf "add-highlighter shared/java/code/ regex %s %s\n" "$1" "$2"; }
  # ---------------------------------------------------------------------------------------------- #
  # alexherbo2: <https://github.com/alexherbo2/plug.kak/blob/master/rc/plug.kak#L108>
  add_word_highlighter() {

    while [ $# -gt 0 ]; do
      words=$1 face=$2; shift 2
      regex="\\b($(join "${words}" '|'))\\b"
      add_highlighter "$regex" "1:$face"
    done

  }
  # ---------------------------------------------------------------------------------------------- #
  printf %s\\n "declare-option str-list error_static_words $(join "${errors_ALL}" ' ')"

  printf %s\\n "declare-option str-list exception_static_words $(join "${exceptions_ALL}" ' ')"
  
  # avoids debug buffer showing an error on load from the original rc/filetype/java.kak
  printf %s\\n "declare-option str-list java_static_words "
  # ---------------------------------------------------------------------------------------------- #
  add_word_highlighter "$errors_ALL" "rgb:d26937" "$exceptions_ALL" "rgb:d26937"
  # ------------------------------------------------------------------------------------------------- #
}
§
